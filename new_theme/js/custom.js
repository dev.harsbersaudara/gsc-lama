(function($){
	"use strict";
	
	$(document).ready(function() {
		  new WOW().init();
	});
	
	// $fn.scrollSpeed(step, speed, easing);
jQuery.scrollSpeed(200, 500);
	
	/* testimonial slider */
				$('.testimonial-slider').bxSlider({
				  pagerCustom: '#bx-pager',
				  mode: 'horizontal',
				  startSlide:1,
				  auto:'true',
				  controls: false
				});
	
   		
	
})(jQuery);

