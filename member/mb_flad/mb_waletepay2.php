<?php echo '<link rel="stylesheet" href="../css/layout.css" type="text/css" media="screen" />
<link rel="stylesheet" type="text/css" href="../css/demo.css"/>
<style>
  table.list{border:solid 1px #bbb;
border-collapse:collapse;}
table.list tr{}
table.list tr.odd td{background:#e8e8e8;}
table.list tr.even td{}table.list tr th{background:url(../images/list-header-bg.png) repeat-x;background-size:contain;color:white;border:solid 1px #ccc;padding:2px 4px;font-weight:normal;}
table.list tr td{background:white;border-left:solid 1px #ccc;padding:2px 4px;}
table.list tr:hover td{background:#E2F2FC;}
table.data-dark{border:solid 1px #ccc;}
table.data-dark tr td{padding:2px 16px;border-bottom:solid 1px #ccc;background:#eee;}
table.data-dark tr td:first-child{text-align:right;font-weight:bold;background:#555;color:#CCCCCC;}
#load{
position:absolute;
z-index:1;
font-size: 14px;
background-image:url(../images/loadingz.gif);
width:290px;
height:121px;
top:50%;
left:50%; 
margin-top: -104px;
  margin-left: -145px;
text-align:center;
line-height:300px;
font-family:"Trebuchet MS", verdana, arial,tahoma;
color:#FFF;
}
/*----------------------------------------------------------------------{ alert }--------------------------------------------------------------------------*/
.alert					{ margin:3px; padding:5px 10px 5px 35px; display:block; line-height:20px; font-family:Tahoma; background:#f5f5f5; border:1px solid; }
	.success			{ color:#00632e; border-color:#339933; background:#d1e8d2 url(../img/frontend/tick_circle.png) no-repeat 10px 7px; }
	.error				{ color:#820101; border-color:#dc1c1c; background:#facfcf url(../img/frontend/cross_circle.png) no-repeat 10px 7px; }
	.warning			{ color:#675100; border-color:#d4b64b; background:#fdefbd url(../img/frontend/exclamation.png) no-repeat 10px 7px; }
	.info				{ color:#00357b; border-color:#9dbfea; background:#d8e7fa url(../img/frontend/information.png) no-repeat 10px 7px; }
	.note				{ color:#4d4d4d; border-color:#bdbdbd; background:#f4f4f4 url(../img/frontend/notebook.png) no-repeat 10px 7px; }
.notification			{ cursor:pointer; }
/*-------------------------------------------------------------------{ Form }-----------------------------------------------------------------------*/
.form_style				{ line-height:normal; }
.form_style .row		{ margin:0; padding:10px; background:#fff; }
.form_style .row:nth-of-type(even)	{ background-color:#f8f8f9; }
.form_style .row label	{ display:block; float:left; margin:0; padding:0; width:40%; line-height:28px; font-weight:bold; }
.form_style .row span	{ display:block; float:left; position:relative; margin:0; padding:0; width:60%; line-height:20px; }
.form_style .row span select,
.form_style .row span textarea,
.form_style .row span input	{ margin:0; padding:5px; width:200px; color:#888; background:#fff; border:solid 1px #d4d8db;  }
.form_style .row span select:hover,
.form_style .row span textarea:hover,
.form_style .row span input:hover{ color:#9089a2; border:1px solid #c9c2d8; }
.form_style .row span select:focus,
.form_style .row span textarea:focus,
.form_style .row span input:focus{ background:#f5f3f9; color:#663399; border:1px solid #663399; box-shadow:inset 1px 1px 1px #ceccd1; }
.form_style .row span select	{ width:212px; }
.form_style .row span select option	{ padding:0 5px; }

.form_style .row input.submit	{ position:relative; margin:0 0 5px 0; padding:4px 10px; width:auto; color:#EFF4FA; text-shadow:1px 1px 1px #316395; text-align:center; 
								  font-family:Tahoma; font-size:12px; font-weight:bold; text-decoration:none; border:1px solid #316395; border-radius:3px; cursor:pointer;
								  background:#4684C1; box-shadow:inset 0px 0px 3px 1px #75A3D0, 0px 1px 2px 0px rgba(0,0,0,0.4); }
.form_style .row input.submit:hover	{ text-shadow:1px 1px 1px #3D79B6; border:1px solid #3C78B5; background:#6397CB; 
										  box-shadow:inset 0px 0px 3px 1px #8DB3D8, 0px 1px 2px 0px rgba(0,0,0,0.4); }
.form_style .row input.submit:active	{ color:#316395; text-shadow:1px 1px 1px #5991C8; border-color:#316395; 
										  box-shadow:inset 0px 0px 3px 1px #75A3D0, 0px 1px 2px 0px rgba(0,0,0,0.4); }
										  
										  
										  
										  
.form_style .row span.textError		{ left:215px; }
						  
/*-------------------------------------------------------------------{ Form table }-----------------------------------------------------------------------*/
.form_style						{ margin:20px 0; padding:0; width:100%; font-size:13px; }
.form_style fieldset			{ padding:0px; border:solid 1px #cac7cd; border-radius:3px; -moz-border-radius:3px;
								  -khtml-border-radius:3px; -webkit-border-bottom-radius:3px; }
.form_style fieldset legend 	{ color:#444444; border:solid 1px #cac7cd; margin-left:10px; padding:5px 15px; font-weight:bold; border-radius:3px; }

.form_style table				{ margin:0 auto; padding:0; width:100%; border:none; border-collapse:collapse; }
.form_style table tr.row2		{ background:#f4f4f5; }
.form_style table td			{ margin:0; padding:12px 5px; }
.form_style table td span		{ display:block; position:relative; }
.form_style table td label		{ padding:5px 0 0 0; display:block; font-weight:bold; }
.form_style table td input,
.form_style table td textarea, 
.form_style table td select		{ padding:3px 3px 4px 3px; width:190px; color:#888; background:#fff; border:solid 1px #d4d8db; }
.form_style table td input:hover,
.form_style table td textarea:hover, 
.form_style table td select:hover	{ color:#878a91; border:1px solid #878a91; }
.form_style table td input:focus,
.form_style table td textarea:focus, 
.form_style table td select:focus	{ background:#f5f3f9; color:#663399; border:1px solid #663399; box-shadow:inset 1px 1px 1px #ceccd1; }

.form_style table td input.submit	{ position:relative; margin:0 0 5px 0; padding:4px 10px; width:auto; color:#EFF4FA; text-shadow:1px 1px 1px #316395; text-align:center; 
									  font-family:Tahoma; font-size:12px; font-weight:bold; text-decoration:none; border:1px solid #316395; border-radius:3px; cursor:pointer;
									  background:#4684C1; box-shadow:inset 0px 0px 3px 1px #75A3D0, 0px 1px 2px 0px rgba(0,0,0,0.4); }
.form_style table td input.submit:hover	{ text-shadow:1px 1px 1px #3D79B6; border:1px solid #3C78B5; background:#6397CB; 
										  box-shadow:inset 0px 0px 3px 1px #8DB3D8, 0px 1px 2px 0px rgba(0,0,0,0.4); }
.form_style table td  input.submit:active	{ color:#316395; text-shadow:1px 1px 1px #5991C8; border-color:#316395; 
											  box-shadow:inset 0px 0px 3px 1px #75A3D0, 0px 1px 2px 0px rgba(0,0,0,0.4); }






.form_style table td textarea	{ width:300px; height:100px; }

.form_style span.textError		{ position:absolute; top:2px; left:200px; padding-left:7px; z-index:1; 
								  background:url(../img/frontend/span_error_bg2.png) no-repeat left center; }
.form_style span.textError p	{ margin:0; padding:3px 7px; line-height:normal; font-size:10px; color:#ccc; background:#000; 
								  background:rgba(0,0,0,0.9); border:1px solid #000; border-radius:3px; }
.form_style p					{ margin:0px; padding:0px; }
.notification					{ cursor:pointer; }
				
  </style>
';
ob_start();
//(@include ('../dt_page/lic.php')) or die("<script>alert(\"You not have a license to use this script on this domain, Please contact www.primadesain.com to purchase a license.\");"."window.location = './index.php'</script>");
//$lic=$license;if(!$lic){echo "<script>alert(\"You not have a license to use this script on this domain, Please contact www.primadesain.com to purchase a license.\");"."window.location = './index.php'</script>";}$svr=$_SERVER['SERVER_NAME'];$c=curl_init();curl_setopt($c,CURLOPT_URL,"http://www.primadesain.com/verifylicenses.php");curl_setopt($c,CURLOPT_TIMEOUT,30);curl_setopt($c,CURLOPT_POST,1);curl_setopt($c,CURLOPT_RETURNTRANSFER,1);$postfields='svr='.$svr.'&lic='.$lic;curl_setopt($c,CURLOPT_POSTFIELDS,$postfields);$result=curl_exec($c);if($result=="fail"){echo "<script>alert(\"You not have a license to use this script on this domain, Please contact www.primadesain.com to purchase a license.\");"."window.location = './index.php'</script>";die();}
if (basename($_SERVER['SCRIPT_FILENAME']) == basename(__FILE__)){
echo "<p align=center><br><br><br><br><br><br><font size=\"6\" color=\"#FF0000\">ILLEGAL ACCESS !!";
echo "<meta http-equiv=\"refresh\" content=\"2; url=../index.php\">";
exit();} 
;echo ''; // Your license key 
$server = $_SERVER['SERVER_NAME'];

/*$c = curl_init(); 
// Set the full url path to point to your verifyLicense.php on your server 
curl_setopt($c, CURLOPT_URL, "http://www.primadesain.com/verifydomains.php"); 
curl_setopt($c, CURLOPT_TIMEOUT, 30); 
curl_setopt($c, CURLOPT_POST, 1); 
curl_setopt($c, CURLOPT_RETURNTRANSFER, 1); 

$postfields = 'svr='.$server; 
curl_setopt($c, CURLOPT_POSTFIELDS, $postfields); 
$result = curl_exec($c); 
*/
$result="success";
if ($result=="fail") { 
echo "<p style='font-family:Arial, Helvetica, sans-serif; margin-top:80px; font-size:16px; line-height:180%; letter-spacing:2px;' align='center'><img src='https://primadesain.com/images/block.png' width='90' height='90' /></br></br>You not have a license to use this script on this domain,<br>Please contact us to purchase a license.<br><strong><a href='http://www.primadesain.com'>www.primadesain.com</a></strong></p><br><p style='font-family:Arial, Helvetica, sans-serif; margin-top:30px; font-size:12px; line-height:180%; letter-spacing:2px;' align='center'>&copy; 2009 - ".date ("Y")." www.primadesain.com</p>";
die(); }
;echo '
<div class="wrapper" style="width:600px;">
';
	/* 
	############################[  <about> ] #######################
		S Name   ::       Inv-X Primadesain
		Update   ::       2013 � Primadesain.Com
		Author   ::       Agus Susanto S.kom
		Website  ::		  http://primadesain.com
		Contact  ::		  <primapc57@gmail.com> // +62 85228657360
	
	Primadesain melayani pembuatan website MLM dan Investasi
	( dengan sistem binary, trinary atau matrix dan matahari )
	juga menerima pembuatan website Iklan Baris, Website Profile,
	Reseller, Hyip, dll.
	############################[ </about> ] #######################
	*/
;echo '
'; 
if (isset($_GET['do']) && $_GET['do'] == "payment") {
if(isset($_GET["iv"])){ $iv = $_GET["iv"]; }
$coe=base64_decode($iv);	


$query35 = "SELECT * FROM transaksiproduk WHERE stmpkode='$coe'"; 
$result35 = mysql_query($query35);
$numzz1 = mysql_num_rows($result35);
$row35 = mysql_fetch_array($result35);
$statusx = $row35['status'];
$prodd = $row35['produk'];
$qty = $row35['qty'];
$hargaitem = $row35['rp'];
$totalrp = $row35['totalrp'];
$komtrans = $row35['komtrans'];
 $orderuser = $row35['orderuser'];	
 $komtrans = $row35['komtrans'];	
 $toadmins = $row35['komtoadmin'];	
 $tosellers = $row35['komtoseller'];	
 $tobuyers = $row35['komtobuyer'];	
		
if(!$numzz1){
	header("location: page.php?go=waletepay&iv=$iv&result=no_transaction");
	exit;
} else {		
		
if($statusx == 1){

	header("location: page.php?go=waletepay&iv=$iv&result=already_proccess");
	exit;
} else {
		
$jumlahe = $_POST['jumlah'];
$userwalet = $_POST['userwalet'];
$tujuan = $_POST['tujuan'];
$transkode = $_POST['transkode'];
$prod = $_POST['prod'];
 
 if (!isset($_SESSION)) {
             session_start();
             }
			 session_regenerate_id(true);

		
		
$pincods = md5($_POST['pincode']);	
$sqlc = mysql_query("SELECT * FROM pincode WHERE username='$user_session'");
$numc = mysql_num_rows($sqlc);
while($rowc = mysql_fetch_array($sqlc)){
$tgl = formatgl($rowc['tgl']);
$pin = $rowc['pin'];
$sts = $rowc['status'];
$lock = $rowc['locks'];
	}
	if(!$numc) {
	header("location: page.php?go=waletepay&iv=$iv&result=wrong_pin_none");
	exit;
} else {
if(!$pincods || $pincods <> $pin) {
	header("location: page.php?go=waletepay&iv=$iv&result=wrong_pin");
	exit;
} else {
if($lock == 1) {
	header("location: page.php?go=waletepay&iv=$iv&result=wrong_pin_lock");
exit;
	} else {
if($sts == 0) {
	header("location: page.php?go=waletepay&iv=$iv&result=wrong_pin_invalid");
	exit;
} else {	
		
		
		
		
		
		
		
		
		
		// Insert you code for processing the form here, e.g emailing the submission, entering it into a database. 
		$userwalet = anti_injection($_POST['userwalet']);
		//$passewalet = $_POST['passewalet'];
		//$passewalete = md5($passewalet);
		
		$sqlzzv = mysql_query("SELECT * FROM member WHERE username='".mysql_real_escape_string($userwalet)."'");
        $numzzv = mysql_num_rows($sqlzzv);
		$rwzzv=mysql_fetch_array($sqlzzv);
        $statusv = $rwzzv['status'];
		if (!$numzzv){
		 header("location: page.php?go=waletepay&iv=$iv&result=wrong_user");
	     exit;
		}else{
		//if ($statusv == 0){
		// header("location: ./page.php?go=wallets&result=wrong_user&co=$_SESSION[user]&pay=$_SESSION[pay]");
	    // exit;
		//}else{
		
		
		$sqlzz = mysql_query("SELECT * FROM ewalet WHERE username='".mysql_real_escape_string($userwalet)."'");
        $numzz = mysql_num_rows($sqlzz);
        $rwzz=mysql_fetch_array($sqlzz);
        $users = $rwzz['username'];
        $status = $rwzz['status'];
		if (!$numzz){
		 header("location: page.php?go=waletepay&iv=$iv&result=wrong_walet");
	     exit;
		}else{
	//	if ($status == 0){
	//	header("location: page.php?go=waletepay&iv=$iv&result=blocked");
	//  exit;
	//	}else{
		
		//$db->select("username", "ewalet", "username='".mysql_real_escape_string($userwalet)."' and password='".mysql_real_escape_string($passewalete)."'");
		//if ($db->num_rows() > 0) {
			
		$saldocwallete = $db->myewalet($userwalet);
			 $pendingcwallete = $db->myewaletpending($userwalet);
			 $saldoku = $saldocwallete-$pendingcwallete;
			
if($saldoku < $jumlahe){
header("location: page.php?go=waletepay&iv=$iv&result=insufficient");
	  exit;
		}else{

        $sa_ewalet = $saldoku;
		if($sa_ewalet <= $jumlahe) {
header("location: page.php?go=waletepay&iv=$iv&result=insufficient");
	  exit;
		}else{


		$email = $db->dataku("email", $users);
		$nama = $db->dataku("nama", $users);
		$alamat = $db->dataku("alamat", $users);
		$hp = $db->dataku("hp", $users);
		$jumlahdepone = rupiah($jumlahe);
		$trans_code = $coe;


			$db->update("transaksiproduk", "status='1', tglbayar='$clientdate', payment='E-Wallet'", "stmpkode='$coe'");
			$db->insert("dataewalet", "", "'', '$coe', '$users', '$jumlahe', 'Pembayaran $prod (user: $tujuan)', 'administrator', '$clientdate', 1, '$clientdate'"); 
			
			$user=$users;
			$stmpkode=$coe;
			
			$sponsore = $db->dataku("sponsor", $user);
$sponsore2 = $db->dataku("sponsor", $sponsore);
$sponsore3 = $db->dataku("sponsor", $sponsore2);
$sponsore4 = $db->dataku("sponsor", $sponsore3);
$sponsore5 = $db->dataku("sponsor", $sponsore4);

$kmjuale = explode("|", $db->config("komjual"));	
$komjualnya=$kmjuale[0];
$komjualnya2=$kmjuale[1];
$komjualnya3=$kmjuale[2];
$komjualnya4=$kmjuale[3];
$komjualnya5=$kmjuale[4];


$smcb1 = mysql_query("SELECT * FROM komisi WHERE kode='".$stmpkode."' and username='$sponsore'");
$cekodeb1 = mysql_num_rows($smcb1);
if(!$cekodeb1 && $komjualnya>0) {
$db->insert("komisi", "", "'', '$sponsore', '$komjualnya', '$clientdate', '0', '', 'penjualan1','$user','$stmpkode','','1','$komjualnya'");
//$db->insert("dataewalet", "", "'', '$stmpkode', 'administrator', '$komtrans', 'Pembelian Produk $stmpkode Downline $user', '$sponsore', '$clientdate', 1, '$clientdate'"); 
}
$smcb2 = mysql_query("SELECT * FROM komisi WHERE kode='".$stmpkode."' and username='$sponsore2'");
$cekodeb2 = mysql_num_rows($smcb2);
if(!$cekodeb2 && $komjualnya2>0) {
$db->insert("komisi", "", "'', '$sponsore2', '$komjualnya2', '$clientdate', '0', '', 'penjualan2','$user','$stmpkode','','1','$komjualnya2'");
//$db->insert("dataewalet", "", "'', '$stmpkode', 'administrator', '$komtrans', 'Pembelian Produk $stmpkode Downline $user', '$sponsore', '$clientdate', 1, '$clientdate'"); 
}
$smcb3 = mysql_query("SELECT * FROM komisi WHERE kode='".$stmpkode."' and username='$sponsore3'");
$cekodeb3 = mysql_num_rows($smcb3);
if(!$cekodeb3 && $komjualnya3>0) {
$db->insert("komisi", "", "'', '$sponsore3', '$komjualnya3', '$clientdate', '0', '', 'penjualan3','$user','$stmpkode','','1','$komjualnya3'");
//$db->insert("dataewalet", "", "'', '$stmpkode', 'administrator', '$komtrans', 'Pembelian Produk $stmpkode Downline $user', '$sponsore', '$clientdate', 1, '$clientdate'"); 
}
$smcb4 = mysql_query("SELECT * FROM komisi WHERE kode='".$stmpkode."' and username='$sponsore4'");
$cekodeb4 = mysql_num_rows($smcb4);
if(!$cekodeb4 && $komjualnya4>0) {
$db->insert("komisi", "", "'', '$sponsore4', '$komjualnya4', '$clientdate', '0', '', 'penjualan4','$user','$stmpkode','','1','$komjualnya4'");
//$db->insert("dataewalet", "", "'', '$stmpkode', 'administrator', '$komtrans', 'Pembelian Produk $stmpkode Downline $user', '$sponsore', '$clientdate', 1, '$clientdate'"); 
}
$smcb5 = mysql_query("SELECT * FROM komisi WHERE kode='".$stmpkode."' and username='$sponsore5'");
$cekodeb5 = mysql_num_rows($smcb5);
if(!$cekodeb5 && $komjualnya5>0) {
$db->insert("komisi", "", "'', '$sponsore5', '$komjualnya5', '$clientdate', '0', '', 'penjualan5','$user','$stmpkode','','1','$komjualnya5'");
//$db->insert("dataewalet", "", "'', '$stmpkode', 'administrator', '$komtrans', 'Pembelian Produk $stmpkode Downline $user', '$sponsore', '$clientdate', 1, '$clientdate'"); 
}

		


$paymente = "E-Wallet - ".$users."";

			
$isimail="<a href='http://".$domain."'><img src='".$logoinvoice."' style='display:inline;outline-style:none;text-decoration:none;' /></a><br><br><br>
<p>Halo ".$nama." (".$users."),</p>
<p>Order anda telah di bayar.</p>
<p>No: ".$coe."<br>
Produk: ".$prodd."<br>
Harga: ".rupiah($hargaitem)."<br>
Bayar: ".rupiah($totalrp)."<br>
Tanggal Bayar: ".formatglx($clientdate)."<br>
</p>
<p><br><br><br>
Salam,<br>
<b>".$bisnisname."</b><br>
".$domain."<br>".$emailadmin."<br>".$hpadmin."</p>";
	   
	    $mail3 = new PHPMailer;
        $mail3->setFrom($emailadmin, $bisnisname);
        $mail3->addAddress($email, $nama);
	    $mail3->IsHTML(true);       
        $mail3->Subject = ''.$nama.', Order anda telah terbayar vie E-Walet';
        $mail3->msgHTML($isimail);
        $mail3->send();			








;echo '	 <script type="text/javascript">
        function RefreshParent() {
            if (window.opener != null && !window.opener.closed) {
                window.opener.location.reload();
            }
        }
        window.onbeforeunload = RefreshParent;
    </script>			
	';			
			$string = 'Thank You, Invoice ('.$coe.' - '.$jumlahdepone.')\nHas been PAID with your E-Walet.\n\nYou can review this transaction in transaction page.';
        echo "<script>alert(\"$string\");".
        "window.close()</script>";	
				
				
		
		}
		}
		}
		}
		}
		}
		}
}

}
}
;echo ''; 
}else{

;echo '
';
if(isset($_GET["iv"])){ $iv = $_GET["iv"]; }
$coe=base64_decode($iv);

$query35 = "SELECT * FROM transaksiproduk WHERE stmpkode='$coe'"; 
$result35 = mysql_query($query35);
$ceks1 = mysql_num_rows($result35);
$row35 = mysql_fetch_array($result35);
$id = $row35['id'];
$username = $row35['username'];
$nama = $row35['nama'];
$tglbayar = $row35['tglbayar'];
$tgl = $row35['tgl'];
$qty = $row35['qty'];
$kode = $row35['stmpkode'];
$produk = $row35['produk'];
$status = $row35['status'];
$rp = $row35['rp'];
$rpku = $row35['rpku'];
$totalrp = $row35['pay'];
$komisi = $row35['komisi'];
$alamat = $row35['alamat'];
$phone = $row35['phone'];
$hp = $row35['hp'];
$email = $row35['email'];
$pesan = $row35['pesan'];
$stmpkode = $row35['stmpkode'];
$nota = $row35['nota'];
$invoice = $row35['invoice'];


$tgle = date('d/m/Y', strtotime($tgl));
$jumlahsatune = rupiah($rp);
$jumlah = $totalrp;		
		$jumlahdepone = rupiah($jumlah);

$query113 = "SELECT * FROM invoice WHERE kode='$coe'"; 
$result113 = mysql_query($query113);
$row113 = mysql_fetch_array($result113);
$file = $row113['file'];
$prodd="Tagihan ".$stmpkode;
 
 $saldocwallete = $db->myewalet($username);
			 $pendingcwallete = $db->myewaletpending($username);
			 $saldoku = $saldocwallete-$pendingcwallete;
			
		
 if($saldoku < $jumlah){
	  
	  echo "<script type=text/javascript>
              alert('Your active wallet balance is insufficient!');
              window.close()
              </script>";		
	  
	exit;		
	} else {	
		
		
if(!$ceks1){
	$string = 'Transaction Not Found!';
        echo "<script>alert(\"$string\");".
        "window.close()</script>";	
	exit;
} else {		
		
if($status == 1){

$string = 'This transaction Already Process!';
        echo "<script>alert(\"$string\");".
        "window.close()</script>";	
	exit;
} else {
;echo '<div style="margin-left:150px;">
';
$results = $_GET['result'];
if($results == "no_transaction") { 
echo "<div class='alert-box errors'><span>error : </span>Transaction not found!</div>";
}
;echo '	';
$results = $_GET['result'];
if($results == "already_proccess") { 
echo "<div class='alert-box errors'><span>error : </span>This Transaction already proccessed!</div>";
}
;echo '';
$results = $_GET['result'];
if($results == "wrong_walet") { 
echo "<div class='alert-box errors'><span>error : </span>Wallet Not found!</div>";
}
;echo '';
$results = $_GET['result'];
if($results == "wrong_pass") { 
echo "<div class='alert-box warnings'><span>warnings : </span>3 times wrong password your Wallet will be blocked!</div>";
}
;echo '';
$results = $_GET['result'];
if($results == "wrong_captcha") { 
echo "<div class='alert-box warnings'><span>error : </span>Wrong Security Code!</div>";
}
;echo '';
$results = $_GET['result'];
if($results == "wrong_id") { 
echo "<div class='alert-box errors'><span>error : </span>User Ewallet Not Found!</div>";
}
;echo '';
$results = $_GET['result'];
if($results == "wrong_user") { 
echo "<div class='alert-box errors'><span>error : </span>Invalid User eWallet For Members!</div>";
}
;echo '';
$results = $_GET['result'];
if($results == "blocked") { 
echo "<div class='alert-box errors'><span>error : </span>Ewallet blocked!</div>";
}
;echo '';
$results = $_GET['result'];
if($results == "insufficient") { 
echo "<div class='alert-box errors'><span>error : </span>Your balance is insufficient</div>";
}
;echo '</div>




















<article class="module width_full" style="width:450px; margin-left:150px;">

			<header>
			  <h3>&nbsp;&nbsp;<img src="../images/bullet_black.png" />&nbsp;&nbsp;Pay Invoice '; echo $coe; ;echo ' ('; echo rupiah($jumlah); ;echo ')</h3>
			</header>
			
				<div class="module_content" style="width:400px;overflow:visible;">
						<form action="page.php?go=waletepays&do=payment&iv='; echo $iv; ;echo '" id="logwalet-form" name="logwalet-form" method="post" class="form-horizontal">
						<fieldset>
							<label>Saldo E-Wallet :</label>
							 <input name="" type="text" id="" value="'; echo rupiah($saldoku);;echo '" disabled="disabled" style="width:300px;"/>
		
						</fieldset>    
							
<fieldset>
							<label>User Wallet :</label>
							 <input name="userwalet" type="text" id="userwalet" value="'; echo $username;;echo '" readonly="readonly" style="width:300px;"/>
		    <input name="jumlah" type="hidden" id="jumlah" value="'; echo $jumlah;;echo '"/>
		    <input name="transkode" type="hidden" id="transkode" value="'; echo $coe;;echo '"/>
		    <input name="prod" type="hidden" id="prod" value="'; echo $prodd;;echo '"/>
		    <input name="tujuan" type="hidden" id="tujuan" value="'; echo $username;;echo '"/>
						</fieldset>    
						
                        
                      <fieldset>
							<label>PIN :</label>
			'; 
					  $results = $_GET['result'];
					  if($results == "wrong_pin") { 
					  echo "<div class='control-group'><div class='controls'><input class='text' name='pincode' maxlength='25' id='pincode' type='password' style='border-color:#950000; background-color:#FFEAEA;color:#950000;width:200px; '></div></div>";
					 } else if($results == "wrong_pin_none") { 
					 echo "<div class='control-group'><div class='controls'><input class='text' name='pincode' maxlength='25' id='pincode' type='password' style='border-color:#950000; background-color:#FFEAEA;color:#950000;width:200px;'></div></div>";
					  } else if($results == "wrong_pin_lock") { 
					 echo "<div class='control-group'><div class='controls'><input class='text' name='pincode' maxlength='25' id='pincode' type='password' style='border-color:#950000; background-color:#FFEAEA;color:#950000;width:200px;'></div></div>";
					  } else if($results == "wrong_pin_invalid") { 
					 echo "<div class='control-group'><div class='controls'><input class='text' name='pincode' maxlength='25' id='pincode' type='password' style='border-color:#950000; background-color:#FFEAEA;color:#950000;width:200px;'></div></div>";
					 }else{
					 echo "<div class='control-group'><div class='controls'><input class='text' name='pincode' maxlength='25' id='pincode' type='password' style='width:200px;'></div></div>";
					 }
					 ;echo '                     <div id=\'logwalet-form_pincode_errorloc\' style="font-family:Tahoma,Geneva,Arial,sans-serif;font-size:11px; line-height:150%; color:#D72D2d" ></div>
						</fieldset>  
                        
                        
						<div class="clear"></div>
				</div>
			<footer>
			
				<div class="submit_link">
				<button class=\'primapcm\' name=\'submit\' type=\'submit\' >Payment</button>&nbsp;&nbsp;<a href="JavaScript:window.close()"><button class=\'primapc2m\' type=\'button\'>'; echo $LANG["cancel"];;echo '</button></a>
				</div>
				
			</footer>
				
				</form>
			<!--	<script language="javascript">
    storeSomething = function () {
        var test = document.getElementById(\'pincode\');
        test.value = prompt("Enter Your PIN Code", "");
    }
</script>-->
			<script language="JavaScript" src="../js/gen_validatorv4.js" type="text/javascript" xml:space="preserve"></script>
			
			<script language="JavaScript" type="text/javascript"
    xml:space="preserve">//<![CDATA[
//You should create the validator only after the definition of the HTML form
  var frmvalidator  = new Validator("logwalet-form");
  frmvalidator.EnableOnPageErrorDisplay();
frmvalidator.EnableMsgsTogether();

    frmvalidator.addValidation("passewalet","req","&nbsp;&nbsp;&#9679;&nbsp;Enter Password<br><br>");
   frmvalidator.addValidation("php_captcha","req","&nbsp;&nbsp;&#9679;&nbsp;&nbsp;Enter Captcha<br><br>");
   frmvalidator.addValidation("pincode","req","&nbsp;&nbsp;&#9679;&nbsp;&nbsp;Enter PIN<br><br>");
	//  frmvalidator.addValidation("pincode","req","&#9679;&nbsp;&nbsp;Enter PIN!<br><br>");
//]]></script>  		
	
	  </article><!-- end of post new article -->


'; 
}
}
	}
}
;echo ' 

 </div><br />
 <div align="center">
 <button onclick="goBack()">Go Back</button>

<script>
function goBack() {
    window.history.back();
}
</script>
</div>
 '; ob_flush(); 
?>