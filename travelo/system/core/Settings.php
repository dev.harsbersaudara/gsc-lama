<?php
/*
-----------------------------------------------------------------------------------------------------------------
START OF DEMO SCRIPT CODE. THIS CODE IS ONLY USED BY DEMO SCRIPT. YOU DO NOT NEED TO ADD THIS CODE TO YOUR SCRIPT.
-----------------------------------------------------------------------------------------------------------------
*/

//require_once("/../gtx_config.php");
define("ROOT_DIRECTORY", "/home/gotaxibi/public_html/security.gotaxi.biz/");

//Set demo script name
$DEMO_PRODUCT_NAME="GoTaxi";

//Set original product name
$ORIGINAL_PRODUCT_NAME="GoTaxi";


//Set original product url
$ORIGINAL_PRODUCT_URL="https://codecanyon.net/item/gotaxi-on-demand-all-in-one-app-services-android/22612350";


$message="
<br><br><b>Attention!</b> <br>You cannot access admin panel, please install purchase code. You can see the purchase code in the following link: <a href='https://help.market.envato.com/hc/en-us/articles/202822600-Where-Is-My-Purchase-Code-' target='_blank'>purchase code</a>.";

//Set default date to prevent errors from being displayed if user's server requires default date to be set
date_default_timezone_set(date_default_timezone_get());

/*
-----------------------------------------------------------------------------------------------------------------
END OF DEMO SCRIPT CODE. THIS CODE IS ONLY USED BY DEMO SCRIPT. YOU DO NOT NEED TO ADD THIS CODE TO YOUR SCRIPT.
-----------------------------------------------------------------------------------------------------------------
*/

/*
-----------------------------------------------------------------------------------------------------------------
START OF CODE TO INCLUDE REQUIRED FILES. YOU SHOULD ADD THIS CODE TO YOUR SCRIPT.
-----------------------------------------------------------------------------------------------------------------
*/

//Load file with Auto PHP Licenser settings
require_once("../SCRIPT/apl_core_configuration.php");

//Load file with Auto PHP Licenser functions
require_once("../SCRIPT/apl_core_functions.php");



/*
-----------------------------------------------------------------------------------------------------------------
END OF CODE TO INCLUDE REQUIRED FILES. YOU SHOULD ADD THIS CODE TO YOUR SCRIPT.
-----------------------------------------------------------------------------------------------------------------
*/

/*
-----------------------------------------------------------------------------------------------------------------
START OF OPTIONAL AUTO PHP LICENSER SECURITY CHECKS. THIS IS USEFUL FOR EXTRA PROTECTION.
-----------------------------------------------------------------------------------------------------------------
*/

//Check if configuration file is genuine
if (APL_INCLUDE_KEY_CONFIG!="3e791e90ac4e9a27") //Secret key modified
    {
    echo "Unauthorized modification detected.";
    exit();
    }



//Check if hash value of "SCRIPT/apl_core_functions.php" matches previously calculated value
if (md5_file(ROOT_DIRECTORY."SCRIPT/apl_core_functions.php")!="3c086f42c25863b4e004ebd0814d5264") //Checksum doesn't match
    {
    echo "Unauthorized modification detected.";
    exit();
    }


$license_notifications_array=aplVerifyLicense(null, 0);
if ($license_notifications_array['notification_case']!="notification_license_ok")
    {
    echo $license_notifications_array['notification_text'].$message;
    exit();
    }


/*
-----------------------------------------------------------------------------------------------------------------
END OF OPTIONAL AUTO PHP LICENSER SECURITY CHECKS. THIS IS USEFUL FOR EXTRA PROTECTION.
-----------------------------------------------------------------------------------------------------------------
*/
