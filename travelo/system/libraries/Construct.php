<?php
/*
|-----------------------------------------------------------------------------
| DATABASE CONNECTIVITY SETTINGS
| -------------------------------------------------------------------
| This file will contain the settings needed to access your database.
|
| For complete instructions please consult the 'Database Connection'
| page of the User Guide.
|
| ------------------------------------------------------------------------------
| EXPLANATION OF VARIABLES
| ------------------------------------------------------------------------------
*/


require_once("../gtx_config.php");
$SMTP_HOST = SMTP_HOST_EMAIL;
$SMTP_PORT = SMTP_PORT_EMAIL;
$EMAIL_NAME = EMAIL_NAME_HOST;
$SMTP_PASS = SMTP_PASS_EMAIL;



/*
-------------------------------------------------------------------------------
| The $active_group variable lets you choose which connection group to
| make active.  By default there is only one group (the 'default' group).
|
| The $query_builder variables lets you determine whether or not to load
| the query builder class.
--------------------------------------------------------------------------------
*/
