<?php
/*==============================================================================
	Item Name: GoTaxi - On Demand All in One App Services Android
	Version: 1.0.6
	Author: Androgo Design
	Author URL: https://codecanyon.net/item/gotaxi-on-demand-all-in-one-app-services-android/22612350
	Attention: Don't modify this FILE because it will error the script.
================================================================================
*/
$PRODUCT_SLOGAN="On Demand All in One App Services Android";
$PRODUCT_HOMEPAGE="https://gotaxi.biz";
$PRODUCT_CHANGELOG_PAGE="https://support.gotaxi.biz/changelog-category/cryptic-theme/";
$PRODUCT_ENVATO_PAGE="https://codecanyon.net/item/gotaxi-on-demand-all-in-one-app-services-android/22612350";
$PRODUCT_BACKEND="1.0.0";
$PRODUCT_REVIEW_PAGE="https://codecanyon.net/item/gotaxi-on-demand-all-in-one-app-services-android/reviews/22612350";
$PRODUCT_VERSION="1.0.6.1";
$PRODUCT_YEAR_BUILD="2019";
$PRODUCT_STYLIZED_NAME="GoTaxi <b>GoTaxi - On Demand All in One App</b> Services Android";
$PRODUCT_STYLIZED_NAME_SHORT="G<b>T</b>X";
