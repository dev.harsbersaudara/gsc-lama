<!DOCTYPE html>
<html lang="en">
<head> 
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Pertanyaan yang Sering Diajukan | TRAVELO</title>
   
    <link rel="shortcut icon" href="/asset/css_animasi/favicon.png">
    <link href="<?php echo base_url() ?>asset/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>asset/css/font-awesome.min.css" rel="stylesheet">
	<link href="<?php echo base_url() ?>asset/css/main.css" rel="stylesheet">
	<!-- <link href="<?php echo base_url() ?>asset/css/animate.css" rel="stylesheet">	 -->
	<!-- <link href="css/responsive.css" rel="stylesheet"> -->
	<link href="<?php echo base_url() ?>asset/css/style.css" rel="stylesheet">
	<link href="<?php echo base_url() ?>asset/css/simple-sidebar.css" rel="stylesheet">
	<link href="<?php echo base_url() ?>asset/css/responsive-slider.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>asset/css/responsive_new.css" rel="stylesheet">
	<!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>asset/css/style_drop_down.css" media="all" /> -->      
  	<link rel="shortcut icon" href="<?php echo base_url() ?>asset/images/logo.jpg">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url() ?>asset/images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url() ?>asset/images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url() ?>asset/images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="<?php echo base_url() ?>asset/images/ico/apple-touch-icon-57-precomposed.png">
</head>

<body>
    
<section id="faq">
		<div class="container">
	
					   	
					 
			
						<div class="col-md-12 col-xs-12 faq-judul" >
							
							<h2 style="text-align:left; font-size:40px; letter-spacing:3px;">Pertanyaan yang Sering Diajukan</h2>
							
					

						</div>
						
						
						<div class=" col-md-12 col-xs-12 faq-border">
							<div class="col-md-12 col-xs-12 faq-detail">

							<button class="accordion">Apa itu Travelo?</button>
							<div class="panel">
  								<p style="color:white">
Bagaimana cara menggunakan Travelo?
Dengan menggunakan aplikasi Travelo, Anda dapat memesan Driver Travelo untuk mengakses semua layanan kami. Masukkan alamat Anda untuk mengetahui biaya menggunakan layanan ini. Gunakan layanan 'Gunakan lokasi saya' untuk mengarahkan Pengemudi ke tempat Anda berada.

Setelah Anda mengkonfirmasi pesanan, teknologi berbasis lokasi kami akan menemukan Pengemudi terdekat di dekatnya. Setelah Driver ditugaskan, Anda dapat melihat foto Driver, mengirim sms dan juga meneleponnya.</p>
							</div>

							<button class="accordion">Bagaimana cara menggunakan Travelo?</button>
							<div class="panel">
  								<p style="color:white">Anda dapat mengunduh aplikasi Travelo di Google Play untuk Anda yang menggunakan perangkat Android.</p>
							</div>

							<button class="accordion">Dimana dapat download Travelo?</button>
							<div class="panel">
  								<p style="color:white">Travelo bisa di download di playstore <a href="https://play.google.com/store/apps/details?id=com.travelorider.passenger">Dwonload Disini</a></a></p>
							</div>

							<button class="accordion">Fasilitas apa yang ada di Travelo?</button>
							<div class="panel">
  								<p style="color:white">Travelo melayani sejumlah layanan seperti, layanan antar-jemput taksi dan sepeda motor, layanan kurir, layanan kurir dengan mobil box, layanan antar-jemput makanan, layanan pijat panggilan, layanan panggilan, dan beberapa layanan lain yang dapat diperbarui setiap saat.
</p>
							</div>	

							<button class="accordion">Di mana area kerja di Travelo?</button>
							<div class="panel">
  								<p style="color:white">Travelo beroperasi di area Bantaeng dan sekitarnya.</p>
							</div>

							<button class="accordion">Bagaimana cara menilai drivers?</button>
							<div class="panel">
  								<p style="color:white">Di akhir layanan, Anda dapat secara otomatis memberikan peringkat kepada Pengemudi. Kami menangani layanan pelanggan dengan sangat serius, kami meminta Anda memberikan peringkat yang adil dan jujur. Anda dapat meninggalkan pesan yang lebih rinci di bidang komentar.</p>
							</div>

							<button class="accordion">Bisakah saya memesan di muka layanan Travelo?</button>
							<div class="panel">
  								<p style="color:white">Kami tidak melayani layanan pre-order Travelo</p>
							</div>

							<button class="accordion">Bagaimana cara mengubah nomor telepon dan email dalam aplikasi saya?</button>
							<div class="panel">
  								<p style="color:white">Buka fitur PENGATURAN kemudian klik PROFIL. Di sini, Anda dapat mengubah nama, alamat email, dan nomor telepon Anda. Jika Anda telah mendaftarkan nomor Anda melalui pusat panggilan dan ingin mulai memesan melalui aplikasi, maka masukkan nomor yang sama ke dalam proses pendaftaran setelah mengunduh.</p>
							</div>

							<button class="accordion">Bagaimana cara melaporkan masalah dengan aplikasi atau kesulitan pesanan?</button>
							<div class="panel">
  								<p style="color:white">Untuk pelaporan dan kesulitan menggunakan aplikasi, silakan kirim email. Harap sertakan detail selengkap mungkin tentang kendala Anda termasuk tangkapan layar masalah sehingga tim kami dapat membantu sesegera mungkin.</p>
							</div>

							<button class="accordion">Apakah ada asuransi ??</button>
							<div class="panel">
  								<p style="color:white">Beberapa hal penting yang terkait asuransi tidak di atur kecuali sesuai aturan asuransi jiwasraya dan BPJS.</p>
							</div>
					

						</div>
						</div>
						
					
				</div>

					
				
					
			</div>
			
		</div>
	</section>
   
   
<?php
    include 'application/views/footer.php'
?>


<!-- javascript terbaru bro  -->
 <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <script type="text/javascript" src="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>asset/css_animasi/app.js"></script>
    <script type="text/javascript">
  
  
    <script type="text/javascript" src="<?php echo base_url() ?>asset/js/jquery.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>asset/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url() ?>asset/js/smoothscroll.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>asset/js/jquery.parallax.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>asset/js/coundown-timer.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>asset/js/jquery.scrollTo.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>asset/js/jquery.nav.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>asset/js/main.js"></script>

   <script>
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
    acc[i].onclick = function(){
        this.classList.toggle("active");
        this.nextElementSibling.classList.toggle("show");
  }
}
</script>

<script>

    var trigger = $('.hamburger'),
    overlay = $('.overlay'),
     isClosed = false;

    trigger.click(function () {
      hamburger_cross();      
    });

    function hamburger_cross() {

      if (isClosed == true) {          
     	 overlay.hide();
        trigger.removeClass('is-open');
        trigger.addClass('is-closed');
        isClosed = false;
      } else {   
         overlay.show();
        trigger.removeClass('is-closed');
        trigger.addClass('is-open');
        isClosed = true;
      }
  }
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
    </script>

</body>
</html>