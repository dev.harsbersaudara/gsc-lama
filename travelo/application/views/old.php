<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Travelo</title>
    
    <link rel="shortcut icon" href="<?php echo base_url() ?>asset/css_animasi/favicon.png">
	
    <link href="<?php echo base_url() ?>asset/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>asset/css/font-awesome.min.css" rel="stylesheet">
    
    
	
	<link href="<?php echo base_url() ?>asset/css/simple-sidebar.css" rel="stylesheet">
	<link href="<?php echo base_url() ?>asset/css/animate.css" rel="stylesheet">
	
	<!--Pembuat navbar muncul dari atas-->	
	<link href="<?php echo base_url() ?>asset/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>asset/css/slider.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>asset/css/responsive_new.css" rel="stylesheet">
	<link href="<?php echo base_url() ?>asset/css/responsive-slider.css" rel="stylesheet"> 
    
    
 <!-- new style --> 
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="theme-color" content="#ffffff">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/material-design-iconic-font/2.2.0/css/material-design-iconic-font.min.css">
<link rel="stylesheet" href="<?php echo base_url() ?>asset/css/bootstrap-cameo.css">
 
 
    
</head>


<body class="toggled">
<header>    
   <nav class="navigation">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <a href="<?php echo base_url() ?>" class="logo"></a>
                <button class="burger burger3"><span></span></button>
                <ul class="menu">
                    
                    
                    <li>
                        <a href="https://play.google.com/store/apps/details?id=com.jastipmorider.passenger" 
                        class="call-to-action btn-primary">USER Apk</a>
                    </li>
                    
                          
                    <li>
                        <a href="https://play.google.com/store/apps/details?id=com.jastipmorider.driver" 
                        class="call-to-action btn-primary">DRIVER Apk</a>
                    </li>
                    
                    
                    
                    <li>
                        <a href="<?php echo base_url() ?>index.php/home/join_driver" class="call-to-action btn-primary">
                            Join driver      </a>
                    </li>
                    
                </ul>
            </div>
        </div>
    </div>
</nav>
    </header>
    <!--/#header--> 

     <div class="overlay"></div>

 <section id="home">  
    <div id="main-slider" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#main-slider" data-slide-to="0" class="active"></li>
            <li data-target="#main-slider" data-slide-to="1"></li>
            <li data-target="#main-slider" data-slide-to="2"></li>
            
        </ol>
        <div class="carousel-inner">
    
    
            <div class="item active">
                <img class="img-responsive" src="<?php echo base_url() ?>asset/images/banner/asset.jpg" alt="">                      
                
            </div>
            <div class="item">
                <img class="img-responsive" src="<?php echo base_url() ?>asset/images/banner/asset1.jpg" alt="" >  
                
                </div>
            <div class="item">
                <img class="img-responsive" src="<?php echo base_url() ?>asset/images/banner/asset2.jpg" alt="" >  
                
            </div>
            
             </div>
    
           
           
                
            </div>
        </div>
    </div>      
</section> 
    <!--/#home-->



    <section class="content-section" id="whatis">
        <div class="container">
            <div class="row">
                <div class="col-md-5 col-xs-12 image">
                   <div class="illustration-about">
                        <img src="<?php echo base_url() ?>asset/images/booking_taxi.jpg" alt="What Is JASTIPMO">
                    </div>
                </div>
                <div class="col-md-7 col-xs-12 text">
                    <h2 class="section-title">Apa itu <span class="inline">Travelo</span></h2>
                    <div class="line"></div>
                    
                    <p style="text-align: justify;">
                        <span class="inline">Travelo</span> adalah sebuah layanan booking online melalui aplikasi android gratis yang bisa dinikmati oleh semua kalangan.

Travelo Menyediakan layanan transportasi online, pengiriman barang, pesan antar makanan, belanja kebutuhan sehari-hari dan penyedia jasa professional secara on demand dalam satu platform aplikasi mobile.

Travelohadir di kota Bantaeng untuk membantu Anda baik bagi anda yang membutuhkan pekerjaan maupun yang membutuhkan layanan.
                    </p>

                        
                    <p></p>
                </div>
            </div>
        </div>
    </section>




   

<section class="section-top-align Want_to_see radius_5">
    <div class="container  ">
        <div class="row text-center ">
            <div class="col-xs-12  section-top-align">
                <span><i class="fa fa-user wow bounceIn" data-wow-duration="1s" data-wow-delay="0.2s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.2s;"></i></span>
            </div>
            <div class="col-xs-12  ">
       

                <p style="font-size:28px; ">AYO BERGABUNGLAH BERSAMA Travelo</p>
            </div>
            <div class="col-xs-12  see_full_align wow fadeInRight" data-wow-duration="1s" data-wow-delay="0.2s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.2s;">
                <a href="<?php echo base_url() ?>index.php/home/join_driver" class="btn btn-info btn btn-lg  Seo_section_testbtn">DAFTAR SEKARANG</a>
            </div>
        </div>
    </div>
</section>  

 <section class="section section-services">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h2 class="heading">
                            Travelo menyediakan semua layanan kebutuhan Anda                        </h2>
                    </div>
                    <div class="box-services">
                        <div class="col-md-3 col-sm-6 list-services">
                            <div class="card-services">
                                <div class="cover-services">
                                    <img src="<?php echo base_url() ?>asset/images/join_page/icon/mcar.png" alt="">
                                </div>
                                <div class="content-services">
                                    <div class="box-icon">
                                        <i class="services-icon icon-car"></i>
                                    </div>
                                    <h3 class="sub-heading">
                                        Mobil                                   </h3>
                                    <p>
                                       Layanan antar jemput penumpang via kendaraan roda 4 dengan kapasitas 1 - 4 orang.                              </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6">
                            <div class="card-services">
                                <div class="cover-services">
                                    <img src="<?php echo base_url() ?>asset/images/join_page/icon/mride.png" alt="">
                                </div>
                                <div class="content-services">
                                    <div class="box-icon">
                                        <i class="services-icon icon-motor"></i>
                                    </div>
                                    <h3 class="sub-heading">
                                        Motor                                   </h3>
                                    <p>
                                        Layanan transportasi online antar jemput penumpang via kendaraan roda 2 dengan kapasitas 1 orang.                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6">
                            <div class="card-services">
                                <div class="cover-services">
                                    <img src="<?php echo base_url() ?>asset/images/join_page/icon/mmassage.png" alt="">
                                </div>
                                <div class="content-services">
                                    <div class="box-icon">
                                        <i class="services-icon icon-express"></i>
                                    </div>
                                    <h3 class="sub-heading">
                                        Makeup                                    </h3>
                                    <p>
                                        layanan makeup dengan para therapist yang sudah handal dibidangnya.                                 </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6">
                            <div class="card-services">
                                <div class="cover-services">
                                    <img src="<?php echo base_url() ?>asset/images/join_page/icon/mfood.png" alt="">
                                </div>
                                <div class="content-services">
                                    <div class="box-icon">
                                        <i class="services-icon icon-truck"></i>
                                    </div>
                                    <h3 class="sub-heading">
                                        Makanan                                   </h3>
                                    <p>
                                        Layanan pesan antar makanan dan minuman dalam satu rute langsung ke tempat tujuan                                   </p>
                                </div>
                            </div>
                        </div>
                        
                        <!-- batas tambahan -->
                        <div class="col-md-3 col-sm-6">
                            <div class="card-services">
                                <div class="cover-services">
                                    <img src="<?php echo base_url() ?>asset/images/join_page/icon/service.png" alt="">
                                </div>
                                <div class="content-services">
                                    <div class="box-icon">
                                        <i class="services-icon icon-truck"></i>
                                    </div>
                                    <h3 class="sub-heading">
                                       Teknisi-AC                                   </h3>
                                    <p>
                                        Layanan Servis AC dan Cuci AC langsung di tempat pelanggan.                                  </p>
                                </div>
                            </div>
                        </div>
                        
                        
                        
                        <div class="col-md-3 col-sm-6">
                            <div class="card-services">
                                <div class="cover-services">
                                    <img src="<?php echo base_url() ?>asset/images/join_page/icon/mart.png" alt="">
                                </div>
                                <div class="content-services">
                                    <div class="box-icon">
                                        <i class="services-icon icon-truck"></i>
                                    </div>
                                    <h3 class="sub-heading">
                                        Belanja                                    </h3>
                                    <p>
                                        Layanan beli barang-barang langsung dari toko dan diantar ke tempat tujuan pelanggan.                                  </p>
                                </div>
                            </div>
                        </div>
                        
                        
                        
                        <div class="col-md-3 col-sm-6">
                            <div class="card-services">
                                <div class="cover-services">
                                    <img src="<?php echo base_url() ?>asset/images/join_page/icon/box.png" alt="">
                                </div>
                                <div class="content-services">
                                    <div class="box-icon">
                                        <i class="services-icon icon-truck"></i>
                                    </div>
                                    <h3 class="sub-heading">
                                       Box                                  </h3>
                                    <p>
                                        Layanan Antar Makanan dengan Ratusan Restoran dan Rumah Makanan di Kabupaten Bantaeng                                   </p>
                                </div>
                            </div>
                        </div>
                        
                       
                       <div class="col-md-3 col-sm-6">
                            <div class="card-services">
                                <div class="cover-services">
                                    <img src="<?php echo base_url() ?>asset/images/join_page/icon/msend.png" alt="">
                                </div>
                                <div class="content-services">
                                    <div class="box-icon">
                                        <i class="services-icon icon-truck"></i>
                                    </div>
                                    <h3 class="sub-heading">
                                        Antar                                   </h3>
                                    <p>
                                        Layanan Kirim Antar barang dalam kota.                                   </p>
                                </div>
                            </div>
                        </div>
                        
                        
                    </div>
                </div>
            </div>
        </section>
        
        
        <section class="section section-use">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="box-works">
                            <h2 class="heading">Kenapa Memilih Travelo</h2>
                            <div class="clearbox">
                            </div>
                            <ul class="list-work">
                                <li>
                                    <h3 class="sub-heading">Tentukan pengambilan dan pengiriman</h3>
                                    <p>
                                        Tentukan titik pengambilan dan pengiriman. Travelo akan memperkirakan biaya layanan untuk Anda.                                  </p>
                                </li>
                                <li>
                                    <h3 class="sub-heading">Pencarian Driver</h3>
                                    <p>
                                       Travelo akan menemukan pengemudi di sekitar Anda. Anda Tinggal tunggu konfirmasi dari Driver.                                 </p>
                                </li>
                                <li>
                                    <h3 class="sub-heading">Fitur Chat</h3>
                                    <p>
                                       Tersedia Fitur Chat yang mempermudah komunikasi antara driver dan pelanggan langsung dari Aplikasi Travelo                   </p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-showcase">
                <div class="showcase-for">
                  <img alt="" src="/asset/images/banner/view1.jpg">
                    </img>
                 <img alt="" src="/asset/images/banner/view2.jpg">
                    </img>
                     <img alt="" src="/asset/images/banner/view3.jpg">
                    </img> 
                </div>
            </div>
        </section>

   
 <section class="section scene">
            <div class="box-vehicles">
                <div class="vehicles vehicles1"></div>
                <div class="vehicles vehicles2"></div>
                <div class="vehicles vehicles3"></div>
                 <div class="vehicles vehicles2"></div>
            </div>
        </section>
        
        
        
        
    <!--    <section class="section section-download" id="download">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="box-download">
                            <h2 class="heading">Download<br>the app now!<br>It's free</h2>
                            <a href="https://play.google.com/store/apps/details?id=com.gotaxiride.passenger" class="google-play">
                                <img src="/asset/logo/google_play.png" alt="">
                            </a>
                           
                         </div>
                    </div>
                    <div class="col-md-6">
                        <img src="#" alt="" class="show-case">
                    </div>

                </div>
            </div>
        </section> -->
   

    <section class="modal">
        <div class="overlay"></div>
        <div class="box-modal">
            <div class="close">
                <i class="zmdi zmdi-close-circle-o"></i>
            </div>
            <object>
                <param name="movie" value="#"/>
                <param name="allowFullScreen" value="true"/>
                <param name="allowscriptaccess" value="always"/>
                <embed width="640" height="360" src="https://www.youtube.com/embed/xxxxxx" class="youtube-player" type="text/html" allowscriptaccess="always" allowfullscreen="true"/>
            </object>
        </div>
    </section>


  <!-- <div class="direct-install install-user">
        <div class="content">
            <img src="<?php echo base_url() ?>asset/css_animasi/direct.jpg" alt="">
            <label>JASTIPMO</label>
            <a href="https://play.google.com/store/apps/details?id=com.dkurir.passenger" target="_blank" class="install"> Install</a>
            <i class="zmdi zmdi-close-circle" id="close-install"></i>
        </div>
    </div> -->


      
    <footer>
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <a href="" class="logo-footer">
                    <img src="<?php echo base_url() ?>asset/images/logo_white.png" alt="">
                </a>
                <ul class="social-media">
                    <li>
                        <a href="https://www.facebook.com/jastipmo" target="_blank">
                            <i class="zmdi zmdi-facebook"></i>
                        </a>
                    </li>
                    <li>
                        <a href="https://twitter.com/jastipmo" target="_blank">
                            <i class="zmdi zmdi-twitter"></i>
                        </a>
                    </li>
                    <li>
                        <a href="https://www.linkedin.com/jastipmo" target="_blank">
                            <i class="zmdi zmdi-linkedin"></i>
                        </a>
                    </li>
                </ul>
            </div>

            <div class="col-md-3 offset-md-2">
                <h4>Company</h4>
                <ul>
                    <li>
                        <a href="<?php echo base_url() ?>">Home</a>
                    </li>
                    <li>
                        <a href="<?php echo base_url() ?>">Contact Us</a>
                    </li> 
                    <li>
                        <a href="<?php echo base_url() ?>home/faq">F A Q </a>
                    </li> 
                
                    <li>
                        <a href="<?php echo base_url() ?>home/terms_conditions">Terms &amp; Condition</a>
                    </li> 
                    <li>
                        <a href="<?php echo base_url() ?>home/privacy">Privacy Policy</a>
                    </li>
                </ul>
            </div>

            <div class="col-md-4">
                <div class="box-button">
                    <!-- <a href="" class="google-play">
                        <img src="https://anterin.id/images/google-play.svg" alt="">
                    </a> -->
                                            <a href="<?php echo base_url() ?>index.php/home/join_driver" class="btn-primary-o">DAFTAR DRIVER</a>
                                        
                </div>
            </div>

        </div>
        <div class="clearfix"></div>
        <div class="col-md-12">
            <div class="copy">
                Copyright © 2019 Travelo. All rights reserved.
            </div>
        </div>
    </div>
</footer>




<!-- javascript terbaru bro  -->
 <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <script type="text/javascript" src="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>asset/css_animasi/app.js"></script>
    <script type="text/javascript">

  
  
    <script type="text/javascript" src="<?php echo base_url() ?>asset/js/jquery.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>asset/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url() ?>asset/js/smoothscroll.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>asset/js/jquery.parallax.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>asset/js/coundown-timer.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>asset/js/jquery.scrollTo.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>asset/js/jquery.nav.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>asset/js/main.js"></script>

  <script>

        var trigger = $('.hamburger'),
        overlay = $('.overlay'),
        isClosed = false;

        trigger.click(function () {
          hamburger_cross();      
      });

        function hamburger_cross() {

          if (isClosed == true) {          
           overlay.hide();
           trigger.removeClass('is-open');
           trigger.addClass('is-closed');
           isClosed = false;
       } else {   
         overlay.show();
         trigger.removeClass('is-closed');
         trigger.addClass('is-open');
         isClosed = true;
     }
 }
 $("#menu-toggle").click(function(e) {
    e.preventDefault();
    $("#wrapper").toggleClass("toggled");
});
</script>




    <script type="text/javascript">

        /* copy loaded thumbnails into carousel */
$('.row .thumbnail').on('load', function() {
  
}).each(function(i) {
  if(this.complete) {
    var item = $('<div class="item"></div>');
    var itemDiv = $(this).parents('div');
    var title = $(this).parent('a').attr("title");
    
    item.attr("title",title);
    $(itemDiv.html()).appendTo(item);
    item.appendTo('.carousel-inner'); 
    if (i==0){ // set first item active
     item.addClass('active');
    }
  }
});

/* activate the carousel */
$('#modalCarousel').carousel({interval:false});

/* change modal title when slide changes */
$('#modalCarousel').on('slid.bs.carousel', function () {
  $('.modal-title').html($(this).find('.active').attr("title"));
})

/* when clicking a thumbnail */
$('.row .thumbnail').click(function(){
    var idx = $(this).parents('div').index();
    var id = parseInt(idx);
    $('#myModal').modal('show'); // show the modal
    $('#modalCarousel').carousel(id); // slide carousel to selected
    
});



    </script>


</body>
</html>