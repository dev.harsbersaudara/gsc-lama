<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Syarat dan ketentuan | Travelo</title>
    <link rel="shortcut icon" href="/asset/css_animasi/favicon.png">
    <link href="<?php echo base_url() ?>asset/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>asset/css/font-awesome.min.css" rel="stylesheet">
	<link href="<?php echo base_url() ?>asset/css/main.css" rel="stylesheet">
	<!-- <link href="<?php echo base_url() ?>asset/css/animate.css" rel="stylesheet">	 -->
	<!-- <link href="css/responsive.css" rel="stylesheet"> -->
	<link href="<?php echo base_url() ?>asset/css/style.css" rel="stylesheet">
	<link href="<?php echo base_url() ?>asset/css/simple-sidebar.css" rel="stylesheet">
	<link href="<?php echo base_url() ?>asset/css/responsive-slider.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>asset/css/responsive_new.css" rel="stylesheet">
	<!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>asset/css/style_drop_down.css" media="all" /> -->      
  	<link rel="shortcut icon" href="<?php echo base_url() ?>asset/images/logo.jpg">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url() ?>asset/images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url() ?>asset/images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url() ?>asset/images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="<?php echo base_url() ?>asset/images/ico/apple-touch-icon-57-precomposed.png">
</head>

<body>

	<section id="syaratKetentuan"><!--/Blog-->
		<div class="container">
				<div class="row">
					<div class="respond-row">

						<div id="event1">
							<div id="judul_home" >
							<div class="col-md-3 col-xs-12" >
								<div class="single-about-detail" >

								<div class="kotak" style="background:#fff; ">
									<h2>Syarat dan ketentuan</h2>
									<div class="line"></div>
	
								</div>
								
							</div>
					   	</div>


						<div class="col-md-9 col-xs-12">
				            <div class="syaratKetentuan-detail">
				      
				              <div class="syaratKetentuan-judul">
				              	<pSyarat dan ketentuan</p>
				              </div>
				              <hr>

				              <p>Lengkapi data diri Anda dengan benar untuk bergabung menjadi mitra kami. Masukkan data dengan benar dan jujur, data yang Anda masukkan dijamin keamanannya.</p><br>

                      <p>Dengan mengunduh, menginstal, dan / atau menggunakan Aplikasi Travelo, Anda setuju bahwa Anda telah membaca, memahami dan menerima dan menyetujui Ketentuan Penggunaan ini ("Ketentuan Penggunaan").<br>
                     

                      </p><br>
				              

				             
				  			<button class="accordion">1. Definisi</button>
							<div class="panel">
  								<p style="color:white">1. "Kami" berarti Travelo, sebuah perusahaan transportasi online.</p>
  								
                  <p style="color:white">2. "Aplikasi" berarti setiap aplikasi perangkat lunak yang kami kembangkan yang merupakan sarana untuk menemukan Layanan yang disediakan oleh pihak ketiga</p>
                  
                  
                  <p style="color:white">3. "Penyedia Layanan" berarti pengemudi Mobil atau Motor pihak ketiga (driver) yang menyediakan Layanan melalui Aplikasi. </p>
                

							</div>

							<button class="accordion">2. Informasi yang Kami Kumpulkan</button>
							<div class="panel">
  									<p style="color:white">1. Kami mengumpulkan Informasi Pribadi tertentu dari Anda sehingga Aplikasi dapat menemukan Layanan dari Penyedia Layanan. Anda akan langsung memberikan Informasi Pribadi (misalnya, ketika Anda mendaftar) dan beberapa informasi akan dikumpulkan secara otomatis ketika Anda menggunakan Aplikasi.</p>

                  <p style="color:white">2. Ketika Anda mengunjungi Situs web kami, administrator situs web kami akan memproses data teknis seperti alamat IP Anda, halaman web yang Anda kunjungi, browser internet yang Anda gunakan, halaman sebelumnya / berikutnya yang Anda kunjungi dan durasi setiap kunjungan / sesi memungkinkan kami untuk mengirimkan fungsi situs web . Selain itu, dalam beberapa kasus, browser dapat memberi tahu Anda bahwa lokasi geografis Anda memungkinkan kami memberi Anda pengalaman yang lebih baik. Dengan data teknis ini, administrator Situs Web kami dapat mengelola Situs Web, misalnya dengan menyelesaikan kesulitan teknis atau meningkatkan kemampuan untuk mengakses bagian-bagian tertentu dari Situs Web. Dengan cara ini, kami dapat memastikan bahwa Anda dapat (melanjutkan) untuk menemukan informasi di Situs Web dengan cara yang cepat dan sederhana.</p>
                  
                                    <p style="color:white">3. Informasi yang Anda berikan secara langsung.</p>

                  <p style="color:white">4. Saat mendaftar pada Aplikasi, Anda akan memberi kami alamat email / email, nama, nomor telepon, dan kata sandi akun.</p>

                  <p style="color:white">5. Ketika Anda menggunakan Aplikasi untuk menemukan layanan, Anda memberi kami informasi, yaitu lokasi dan tujuan Anda. Anda juga memberi kami informasi tentang barang yang Anda kirim / kirim dan / atau beli dan biaya pengeluaran Anda saat Anda menggunakan kurir instan atau layanan belanja pribadi. Ketika Anda menggunakan Aplikasi kami, kami juga akan memproses data teknis Anda seperti alamat IP, Identitas Perangkat (ID) atau alamat MAC, dan informasi tentang produsen, model, dan sistem operasi perangkat seluler Anda. Kami menggunakan data ini untuk memungkinkan kami mengirimkan fungsi Aplikasi, menyelesaikan kesulitan teknis, memberikan Anda versi Aplikasi yang benar dan terkini dan untuk meningkatkan fungsionalitas Aplikasi.</p>

                   <p style="color:white">6. Kami akan meminta nomor telepon seseorang yang Penyedia Layanan dapat hubungi untuk menyelesaikan pesanan Anda ketika Anda menggunakan Aplikasi untuk menemukan layanan messenger instan. Anda harus mendapatkan persetujuan sebelumnya dari orang yang nomor teleponnya Anda berikan kepada kami untuk memberi kami nomor telepon kepada kami dan bagi kami untuk memberikan nomor telepon kepada Penyedia Layanan.</p>

                   <p style="color:white">7. Ketika Anda mengisi (TOP UP) Kredit Travelo Anda, kami akan mengumpulkan informasi seperti nama bank tempat akun Anda dibuka, nama pemegang akun, dan jumlah yang Anda transfer untuk pengisian ulang (TOP UP).</p>

                   <p style="color:white">8. Anda dapat memberikan kode rujukan kepada teman-teman Anda melalui Aplikasi, di mana, kami hanya akan menyiapkan pesan untuk Anda kirim atau Anda publikasikan melalui penyedia media sosial atau email Anda. Anda dapat mengubah pesan yang kami siapkan sebelum mengirimkannya. Kami tidak akan mengumpulkan data teman Anda.</p>

                   <p style="color:white">9. Informasi yang kami kumpulkan ketika Anda menggunakan Aplikasi.</p>

                   <p style="color:white">10. Saat Anda menggunakan Aplikasi melalui perangkat seluler Anda, kami akan melacak dan mengumpulkan informasi geo-lokasi secara real-time. Kami menggunakan informasi ini untuk memungkinkan Anda melihat Penyedia Layanan yang berlokasi di daerah Anda di dekat lokasi Anda, mengatur lokasi penjemputan dan mengirim informasi ke Penyedia Layanan yang diminta, dan untuk melihat Penyedia Layanan didekati pada peta di real- waktu. Kami juga dapat menggunakan informasi geo-lokasi ini secara real-time untuk memberikan bantuan, menyelesaikan kesulitan teknis atau bisnis yang mungkin timbul ketika Anda menggunakan Aplikasi. Anda dapat menonaktifkan sementara informasi pelacak lokasi geografis di tingkat perangkat. Perangkat seluler Anda akan memberi tahu Anda saat lokasi geografis dilacak dengan menampilkan simbol panah GPS.</p>

                   <p style="color:white">11. Kami juga melacak dan mengumpulkan informasi geo-lokasi Penyedia Layanan. Ini berarti bahwa kami juga mengumpulkan informasi ketika Anda bepergian dengan Penyedia Layanan. Kami juga akan menggunakan informasi geo-lokasi Penyedia Layanan dalam bentuk anonim dan keseluruhan untuk memperoleh informasi statistik dan informasi manajemen dan untuk menyediakan bagi Anda fungsionalitas Aplikasi yang ditingkatkan.</p>

							</div>

							<button class="accordion">3. Penggunaan Informasi yang Kami Kumpulkan</button>
							<div class="panel">
  							<p style="color:white">1. Kami menggunakan email, nama, nomor telepon, dan kata sandi Anda untuk memverifikasi kepemilikan akun, untuk berkomunikasi dengan Anda sehubungan dengan pesanan Anda dan untuk memberi Anda informasi tentang Aplikasi. Kami juga dapat menggunakan nama, email, dan nomor telepon Anda untuk mengirim pesan, pembaruan umum ke Aplikasi, penawaran atau promosi khusus. Kami juga akan mengirim email kepada Anda meminta Anda untuk berlangganan Mailing List kami. Anda dapat setiap saat memilih untuk tidak menerima informasi tentang pengungkapan ini.</p>

                  <p style="color:white">2. Kami menggunakan lokasi geografis dan tujuan Anda untuk menemukan Penyedia Layanan di sekitar Anda, untuk membantu Penyedia Layanan untuk memperhitungkan biaya dan untuk menganalisis pola penggunaan Aplikasi untuk meningkatkan kinerja Aplikasi.</p>


                  <p style="color:white">3. Kami menggunakan informasi seperti barang yang telah Anda kirim / kirim dan / atau beli dan biaya pengeluaran Anda untuk menentukan apakah Aplikasi dapat menerima pesanan Anda berdasarkan Ketentuan Penggunaan.</p>

                  <p style="color:white">4. Kami menggunakan informasi seperti nama bank tempat akun Anda dibuat, nama tempat akun disimpan, dan jumlah yang ditransfer untuk pengisian ulang (TOP UP) untuk memastikan pembayaran yang Anda lakukan pada Kredit Travelo.</p>

                  <p style="color:white">5. Kami menggunakan Informasi Pribadi dalam bentuk anonim dan keseluruhan untuk memonitor secara dekat fitur-fitur Layanan mana yang paling umum digunakan, untuk menganalisis pola penggunaan dan untuk menentukan apakah kami akan menawarkan atau fokus pada Layanan kami. Anda dengan ini setuju bahwa data Anda akan digunakan oleh pemrosesan data internal kami untuk memberikan Layanan yang lebih baik kepada Anda.</p>
							</div>

							<button class="accordion">4. Memberikan Informasi Yang Kami Kumpulkan</button>
							<div class="panel">
  							<p style="color:white">1. Setelah menerima pesanan Anda, kami akan memberikan informasi seperti nama Anda, nomor telepon, lokasi, tujuan, lokasi geografis, barang yang akan dikirim / dikirim atau dibeli dan / atau pengeluaran Anda kepada Penyedia Layanan yang menerima permintaan Anda untuk pelayanan. Informasi ini diperlukan oleh Penyedia Layanan untuk menghubungi Anda, dan / atau menemukan Anda dan / atau memenuhi pesanan Anda</p>

                  <p style="color:white">2. Kami juga akan memberikan nomor telepon pihak yang dapat dihubungi yang telah Anda berikan kepada Penyedia Layanan ketika Anda menggunakan Aplikasi untuk menemukan layanan messenger instan.</p>

                  <p style="color:white">3. Anda dengan ini menyetujui dan mengizinkan kami untuk memberikan Informasi Pribadi Anda kepada Penyedia Layanan sebagai bagian dari Ketentuan Layanan. Meskipun informasi pribadi Anda secara otomatis dihapus dari perangkat seluler Penyedia Layanan setelah Anda menggunakan Layanan, ada kemungkinan Penyedia Layanan dapat menyimpan data Anda di perangkat mereka dengan cara apa pun. Kami tidak bertanggung jawab atas penyimpanan data dengan cara seperti itu dan Anda setuju untuk membela, mengganti rugi dan membebaskan kami dan kami tidak akan bertanggung jawab atas penyalahgunaan Informasi Pribadi Anda oleh Penyedia Layanan setelah pengakhiran Layanan.</p>

                  <p style="color:white">4. Kami dapat mempekerjakan perusahaan pihak ketiga dan individu untuk memfasilitasi atau menyediakan Aplikasi dan layanan tertentu atas nama kami, untuk memberikan bantuan konsumen, memberikan informasi lokasi geografis kepada Penyedia Layanan kami, untuk melakukan layanan yang terkait dengan Situs (mis. Tanpa batasan, pemeliharaan layanan, manajemen basis data, analisis web, dan peningkatan fitur Situs Web) atau untuk membantu kami dalam menganalisis bagaimana Layanan kami digunakan atau untuk penasihat profesional kami dan auditor eksternal, termasuk penasihat hukum, penasihat keuangan, dan konsultan - konsultan. Pihak ketiga ini hanya memiliki akses ke informasi pribadi Anda untuk melakukan tugas tersebut atas nama kami dan terikat kontrak untuk tidak mengungkapkan atau menggunakan informasi pribadi tersebut untuk tujuan lain.</p>

                   <p style="color:white">5. Kami tidak membagikan Informasi Pribadi Anda dengan pihak lain selain kepada Penyedia Layanan masing-masing dan perusahaan pihak ketiga dan individu yang disebutkan dalam Bagian 4.4 di atas, tanpa persetujuan Anda. Namun, kami akan mengungkapkan Informasi Pribadi Anda sejauh yang diminta oleh hukum, atau diperlukan untuk mematuhi persyaratan hukum, peraturan dan pemerintah, atau dalam hal terjadi perselisihan, atau segala bentuk proses hukum antara Anda dan kami, atau antara Anda dan pengguna lain sehubungan dengan, atau sehubungan dengan Layanan, atau dalam keadaan darurat yang berkaitan dengan kesehatan dan / atau keselamatan Anda.</p>

                   <p style="color:white">6. Informasi Pribadi Anda dapat ditransfer, disimpan, digunakan dan diproses di wilayah hukum selain Indonesia di mana server kami berada. Anda memahami dan menyetujui transfer Informasi Pribadi Anda di luar Negara.</p>

                   <p style="color:white">7. Kami tidak menjual atau menyewakan Informasi Pribadi Anda kepada pihak ketiga.</p>
							</div>

							<button class="accordion">5. Penahanan Informasi yang Kami Kumpulkan</button>
							<div class="panel">
  									<p style="color:white">Kami akan menyimpan informasi Anda jika Anda menghapus akun Anda di Aplikasi Travelo.</p>
							</div>
							
							<button class="accordion">6. Keamanan</button>
							<div class="panel">
  								<p style="color:white">Kami tidak menjamin keamanan database kami dan kami juga tidak menjamin bahwa data yang Anda berikan tidak akan ditahan / diinterupsi saat sedang dikirim kepada kami. Setiap transmisi informasi oleh Anda kepada kami adalah risiko Anda sendiri. Anda tidak boleh mengungkapkan kata sandi Anda kepada siapa pun. Tidak peduli seberapa efektif suatu teknologi, tidak ada sistem keamanan yang tidak bisa ditembus.</p>
							</div>							

<button class="accordion">7. Perubahan pada Kebijakan Privasi ini</button>
              <div class="panel">
                  <p style="color:white">Kami dapat mengubah Kebijakan Privasi ini untuk mencerminkan perubahan dalam aktivitas kami. Jika kami mengubah Kebijakan Privasi ini, kami akan memberi tahu Anda melalui email atau pemberitahuan di Situs web 1 hari sebelum perubahan berlaku. Kami mendorong Anda untuk meninjau halaman ini secara berkala untuk informasi terbaru tentang bagaimana ketentuan Kebijakan Privasi ini berlaku. </p>
              </div>

              
              
              <button class="accordion">9. Pengakuan dan Persetujuan</button>
              <div class="panel">
                  <p style="color:white">1. Dengan menggunakan Aplikasi, Anda mengakui bahwa Anda telah membaca dan memahami Kebijakan Privasi dan Ketentuan Penggunaan ini dan menyetujui serta menyetujui penggunaan, praktik, pemrosesan, dan transfer informasi pribadi Anda sebagaimana tercantum dalam Kebijakan Privasi ini..</p>

                  <p style="color:white">2. Anda juga menyatakan bahwa Anda memiliki hak untuk membagikan semua informasi yang telah Anda berikan kepada kami dan memberikan kami hak untuk menggunakan dan berbagi informasi tersebut dengan Penyedia Layanan.</p>
              </div>              
			

              <button class="accordion">10. Berhenti menerima email</button>
              <div class="panel">
                  <p style="color:white">Kami memiliki kebijakan untuk memilih masuk / keluar dari basis data. Jika Anda ingin berhenti menerima email dari kami, silakan klik tautan berhenti berlangganan yang disertakan dengan setiap email.</p>
              </div>

              <button class="accordion">11. Cara untuk Menghubungi Kami</button>
              <div class="panel">
                  <p style="color:white">Jika Anda memiliki pertanyaan lebih lanjut tentang privasi dan keamanan informasi Anda dan ingin memperbarui atau menghapus data Anda, silakan hubungi kami di: mail.travelolive@gmail.com atau hubungi: 082388020323.</p>
                  
                  
                  </div>

				            </div>
						</div>

						</div>
					   
						
					</div>
				</div>
			</div>
	
	</section><!--/#Blog-->
<!-- footer -->
 <?php
    include 'application/views/footer.php'
?> 
  
     <script type="text/javascript" src="<?php echo base_url() ?>asset/js/jquery.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>asset/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url() ?>asset/js/smoothscroll.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>asset/js/jquery.parallax.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>asset/js/coundown-timer.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>asset/js/jquery.scrollTo.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>asset/js/jquery.nav.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>asset/js/main.js"></script> 

     <script>
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
    acc[i].onclick = function(){
        this.classList.toggle("active");
        this.nextElementSibling.classList.toggle("show");
  }
}
</script> 

 <script>

    var trigger = $('.hamburger'),
    overlay = $('.overlay'),
     isClosed = false;

    trigger.click(function () {
      hamburger_cross();      
    });

    function hamburger_cross() {

      if (isClosed == true) {          
     	 overlay.hide();
        trigger.removeClass('is-open');
        trigger.addClass('is-closed');
        isClosed = false;
      } else {   
         overlay.show();
        trigger.removeClass('is-closed');
        trigger.addClass('is-open');
        isClosed = true;
      }
  }
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
    </script>
</body>
</html>