<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="shortcut icon" type="image/png" href="https://travelo.live/asset/images/favicon.png"/>
    <title>Travelo - Ojek,Taxi, Pesan makan, Kirim barang, Pembayaran & Traveling</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?php echo base_url() ?>asset/fs/css/bootstrap.min.css" >
    <!-- Icon -->
    <link rel="stylesheet" href="<?php echo base_url() ?>asset/fs/fonts/line-icons.css">
    <!-- Owl carousel -->
    <link rel="stylesheet" href="<?php echo base_url() ?>asset/fs/css/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>asset/fs/css/owl.theme.css">
    
    <link rel="stylesheet" href="<?php echo base_url() ?>asset/fs/css/magnific-popup.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>asset/fs/css/nivo-lightbox.css">
    <!-- Animate -->
    <link rel="stylesheet" href="<?php echo base_url() ?>asset/fs/css/animate.css">
    <!-- Main Style -->
    <link rel="stylesheet" href="<?php echo base_url() ?>asset/fs/css/main.css">
    <!-- Responsive Style -->
    <link rel="stylesheet" href="<?php echo base_url() ?>asset/fs/css/responsive.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

  </head>
  <body>

    <!-- Header Area wrapper Starts -->
    <header id="header-wrap">
      <!-- Navbar Start -->
      <nav class="navbar navbar-expand-md bg-inverse fixed-top scrolling-navbar">
        <div class="container">
          <!-- Brand and toggle get grouped for better mobile display -->
          <a href="https://travelo.live" class="navbar-brand"><img src="<?php echo base_url() ?>asset/images/logo.png" alt="" style="width:130px!important;" height="35px"></a>       
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <i class="lni-menu"></i>
          </button>
          <div class="collapse navbar-collapse" id="navbarCollapse">
            <ul class="navbar-nav mr-auto w-100 justify-content-end clearfix">
              <li class="nav-item active">
                <a class="nav-link" href="https://travelo.live">
                  Home
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#services">
                  Services
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#team">
                  Team
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#pricing">
                  Pricing
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#testimonial">
                  Testimonial
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#contact">
                  Contact
                </a>
              </li>
            </ul>
          </div>
        </div>
      </nav>
      <!-- Navbar End -->

      <!-- Hero Area Start -->
     
      <!-- Hero Area End -->

    </header>
    <!-- Header Area wrapper End -->

    <!-- Services Section Start -->
    <section id="services" class="section-padding" style="margin-top:30px;">
      <div class="container">
        <div class="section-header text-center">
          <h2 class="section-title wow fadeInDown" data-wow-delay="0.3s">PILIHLAH DI BAWAN INI JENIS DRIVER YANG ANDA INGINKAN</h2>
          <div class="shape wow fadeInDown" data-wow-delay="0.3s"></div>
        </div>
        <div class="row">
          <!-- Services item -->
          <div class="col-md-6 col-lg-3 col-sm-3 col-xs-3">
            <a href="<?php echo base_url() ?>frontend/index.php/register/join_cab">
			<div class="services-item wow fadeInRight" data-wow-delay="0.3s">
              <div class="icon">
                <i class="fa fa-car"></i>
              </div>
              <div class="services-content">
                <h3>MOBIL</a></h3>
               
              </div>
			
            </div>
          </div>
          <!-- Services item -->
          <div class="col-md-6 col-lg-3 col-sm-3 col-xs-3">
            <div class="services-item wow fadeInRight" data-wow-delay="0.6s">
            <a href="<?php echo base_url() ?>frontend/index.php/register/join_moto"> 
			  <div class="icon">
                <i class="fa fa-motorcycle"></i>
              </div>
              <div class="services-content">
                <h3>Motor</a></h3>
              </div>
			
            </div>
          </div>
		    <!-- Services item -->
          <div class="col-md-6 col-lg-3 col-sm-3 col-xs-3">
            <div class="services-item wow fadeInRight" data-wow-delay="1.2s">
            <a href="<?php echo base_url() ?>frontend/index.php/register/join_moto">
				<div class="icon">
                <i class="fa fa-share"></i>
              </div>
              <div class="services-content">
                <h3>Kirim</a></h3>
              </div>
			
            </div>
          </div>
		   <!-- Services item -->
          <div class="col-md-6 col-lg-3 col-sm-3 col-xs-3">
            <div class="services-item wow fadeInRight" data-wow-delay="1.2s">
				<a href="<?php echo base_url() ?>frontend/index.php/register/join_box">
					<div class="icon">
						<i class="fa fa-truck"></i>
					</div>
					<div class="services-content">
						<h3>Box</a></h3>
					</div>
				
            </div>
          </div>
          <!-- Services item -->
          
         
          <!-- Services item -->
          <div class="col-md-6 col-lg-3 col-sm-3 col-xs-3">
            <div class="services-item wow fadeInRight" data-wow-delay="1.5s">
              <div class="icon">
		<i class="fa fa-shopping-cart"></i>
              </div>
              <div class="services-content">
                <h3><a href="#">Belanja</a></h3>
                
              </div>
            </div>
          </div>
		  
		  <div class="col-md-6 col-lg-3 col-sm-3 col-xs-3">
            <div class="services-item wow fadeInRight" data-wow-delay="1.8s">
            <a href="<?php echo base_url() ?>frontend/index.php/register/join_massage">  
			  <div class="icon">
				<i class="fa fa-paint-brush"></i>
              </div>
              <div class="services-content">
                <h3>Make Up/Pijat</a></h3>
              </div>
			  
            </div>
          </div>
		  
          <!-- Services item -->
          <div class="col-md-6 col-lg-3 col-xs-12">
            <div class="services-item wow fadeInRight" data-wow-delay="1.8s">
			<a href="<?php echo base_url() ?>frontend/index.php/register/join_service">  
              <div class="icon">
				<i class="fa fa-cog"></i>
              </div>
              <div class="services-content">
                <h3>TEKNISI-AC</a></h3>
              </div>
            </div>
          </div>
		  
		  <div class="col-md-6 col-lg-3 col-xs-12">
            <div class="services-item wow fadeInRight" data-wow-delay="0.9s">
			<a href="<?php echo base_url() ?>frontend/index.php/register/join_food">  
              <div class="icon">
                <i class="fa fa-cutlery"></i>
              </div>
              <div class="services-content">
                <h3>Restoran</a></h3>
               
              </div>
            </div>
          </div>
		  
        </div>
      </div>
    </section>
    <!-- Services Section End -->

    

    <!-- Footer Section Start -->
    <footer id="footer" class="footer-area section-padding">
      <div class="container">
        <div class="container">
          <div class="row">
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 col-mb-12">
              <div class="widget">
                <h3 class="footer-logo"><img src="<?php echo base_url() ?>asset/images/logo.png"  style="width:250px!important;" height="60px" alt=""></h3>
                <div class="textwidget">
                  <p></p>
                </div>
                <div class="social-icon">
                  <a class="facebook" href="#"><i class="lni-facebook-filled"></i></a>
                  <a class="twitter" href="#"><i class="lni-twitter-filled"></i></a>
                  <a class="instagram" href="#"><i class="lni-instagram-filled"></i></a>
                  <a class="linkedin" href="#"><i class="lni-linkedin-filled"></i></a>
                </div>
              </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
              <h3 class="footer-titel">Products</h3>
              <ul class="footer-link">
                <li><a href="#">Ojek</a></li>
                <li><a href="#">Taxi</a></li>
                <li><a href="#">Pesan Makanan</a></li>
                <li><a href="#">Antar Barang</a></li>           
                <li><a href="#">Belanja</a></li>
                <li><a href="#">Traveling</a></li>
                <li><a href="#">Pembayaran</a></li>
              </ul>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
              <h3 class="footer-titel">Support</h3>
              <ul class="footer-link">
                <li><a href="#">Contact Us</a></li>
                <li><a href="#">Privacy Policy</a></li>
                <li><a href="#">Terms & Conditions</a></li>
                <li><a href="#">Identity Verification</a></li>
                <li><a href="#">Help Centre</a></li>
              </ul>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
              <h3 class="footer-titel">Contact</h3>
              <ul class="address">
                <li>
                  <a href="#"><i class="lni-map-marker"></i> Komp. Plaza Bintan Center Jl. D.I.Pandjaitan no.18 KM.9 Tanjung Pinang Sebelah bank BNI Bintan Center.</a>
                </li>
                <li>
                  <a href="#"><i class="lni-phone-handset"></i> P: +6282388020323</a>
                </li>
                <li>
                  <a href="#"><i class="lni-envelope"></i> E: Admin@Travelo.live</a>
                </li>
              </ul>
            </div>
          </div>
        </div>  
      </div> 
      <div id="copyright">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <div class="copyright-content">
                <p>Copyright © 2020 <a rel="nofollow" href="https://travelo.live">Travelo</a> All Right Reserved</p>
              </div>
            </div>
          </div>
        </div>
      </div>   
    </footer> 
    <!-- Footer Section End -->

    <!-- Go to Top Link -->
    <a href="#" class="back-to-top">
    	<i class="lni-arrow-up"></i>
    </a>
    
    <!-- Preloader -->
    <div id="preloader">
      <div class="loader" id="loader-1"></div>
    </div>
    <!-- End Preloader -->
    
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="<?php echo base_url() ?>asset/fs/js/jquery-min.js"></script>
    <script src="<?php echo base_url() ?>asset/fs/js/popper.min.js"></script>
    <script src="<?php echo base_url() ?>asset/fs/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url() ?>asset/fs/js/owl.carousel.min.js"></script>
    <script src="<?php echo base_url() ?>asset/fs/js/wow.js"></script>
    <script src="<?php echo base_url() ?>asset/fs/js/jquery.nav.js"></script>
    <script src="<?php echo base_url() ?>asset/fs/js/scrolling-nav.js"></script>
    <script src="<?php echo base_url() ?>asset/fs/js/jquery.easing.min.js"></script>
    <script src="<?php echo base_url() ?>asset/fs/js/jquery.counterup.min.js"></script>      
    <script src="<?php echo base_url() ?>asset/fs/js/waypoints.min.js"></script>   
    <script src="<?php echo base_url() ?>asset/fs/js/main.js"></script>
      
  </body>
</html>
