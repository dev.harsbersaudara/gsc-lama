<!DOCTYPE html>
<html lang="en">
  <head>
      
      <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script>
     (adsbygoogle = window.adsbygoogle || []).push({
          google_ad_client: "ca-pub-8939934716039947",
          enable_page_level_ads: true
     });
</script>
      
      
      
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="shortcut icon" type="image/png" href="https://travelo.live/asset/images/favicon.png"/>
    <title>Travelo. - Ojek,Taxi, Pesan makan, Kirim barang, Pembayaran & Traveling</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?php echo base_url() ?>asset/fs/css/bootstrap.min.css" >
    <!-- Icon -->
    <link rel="stylesheet" href="<?php echo base_url() ?>asset/fs/fonts/line-icons.css">
    <!-- Owl carousel -->
    <link rel="stylesheet" href="<?php echo base_url() ?>asset/fs/css/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>asset/fs/css/owl.theme.css">
    
    <link rel="stylesheet" href="<?php echo base_url() ?>asset/fs/css/magnific-popup.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>asset/fs/css/nivo-lightbox.css">
    <!-- Animate -->
    <link rel="stylesheet" href="<?php echo base_url() ?>asset/fs/css/animate.css">
    <!-- Main Style -->
    <link rel="stylesheet" href="<?php echo base_url() ?>asset/fs/css/main.css">
    <!-- Responsive Style -->
    <link rel="stylesheet" href="<?php echo base_url() ?>asset/fs/css/responsive.css">
	<link rel="stylesheet" href="https://travelo.live/asset/css/bootstrap-cameo.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/material-design-iconic-font/2.2.0/css/material-design-iconic-font.min.css">
	<link href="https://travelo.live/asset/css/font-awesome.min.css" rel="stylesheet">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

  </head>
  <body>

    <!-- Header Area wrapper Starts -->
    <header id="header-wrap">
      <!-- Navbar Start -->
      <nav class="navbar navbar-expand-md bg-inverse fixed-top scrolling-navbar">
        <div class="container">
          <!-- Brand and toggle get grouped for better mobile display -->
          <a href="https://travelo.live" class="navbar-brand"><img src="<?php echo base_url() ?>asset/images/logo.png" alt="" style="width:130px!important;" height="35px"></a>       
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <i class="lni-menu"></i>
          </button>
          <div class="collapse navbar-collapse" id="navbarCollapse">
            <ul class="navbar-nav mr-auto w-100 justify-content-end clearfix">
              <li class="nav-item active">
                <a class="nav-link" href="https://travelo.live">
                  Home
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#services">
                  Services
                </a>
              </li>
             
              
              </li>
              
              <li class="nav-item">
                <a class="nav-link" href="#contact">
                  Lokasi Kami
                </a>
              </li>
            </ul>
          </div>
        </div>
      </nav>
      <!-- Navbar End -->

      <!-- Hero Area Start -->
      <div id="hero-area" class="hero-area-bg">
        <div class="container">      
          <div class="row">
            <div class="col-lg-7 col-md-12 col-sm-12 col-xs-12">
              <div class="contents">
                <h2 class="head-title">Tidak ada perbedaan di antara kita, kita adalah sama, saling membutuhkan! </h2>
                <p>Kita adalah manusia biasa yang saling membutuhkan satu sama lainnya, saya, kamu dan anda, kita adalah sama, Travelo hadir untuk kita semua.</p>
                <div class="header-button">
                  <a rel="nofollow" href="https://play.google.com/store/apps/details?id=com.travelorider.passenger" class="btn btn-common">Play Store</a>
                  <a href="<?php echo base_url() ?>index.php/home/join_driver" class="btn btn-border video-popup">Bergabung!</a>
                </div>
              </div>
            </div>
            <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12">
              <div class="intro-img">
                <img class="img-fluid" src="<?php echo base_url() ?>asset/fs/img/intro-mobile.png" alt="">
              </div>            
            </div>
          </div> 
        </div> 
      </div>
      <!-- Hero Area End -->

    </header>
    <!-- Header Area wrapper End -->

    <!-- Services Section Start -->
    <section id="services" class="section-padding">
      <div class="container">
        <div class="section-header text-center">
          <h2 class="section-title wow fadeInDown" data-wow-delay="0.3s">Travelo menyediakan semua layanan kebutuhan Anda</h2>
          <div class="shape wow fadeInDown" data-wow-delay="0.3s"></div>
        </div>
        <div class="row">
          <!-- Services item -->
		  
          <div class="col-md-3 col-lg-3 col-xs-12">
            <div class="services-item wow fadeInRight" data-wow-delay="0.3s">
              <div class="icon">
                <a href="https://travelo.live/frontend/index.php/register/join_cab"><i class="fa fa-car"></i></a>
              </div>
              <div class="services-content">
                <h3><a href="https://travelo.live/frontend/index.php/register/join_cab">Taksi</a></h3>
                <p>Layanan antar jemput penumpang via kendaraan roda 4 dengan kapasitas 1 - 4 orang. </p>
              </div>
            </div>
          </div>
          <!-- Services item -->
          <div class="col-md-3 col-lg-3 col-xs-12">
            <div class="services-item wow fadeInRight" data-wow-delay="0.6s">
              <div class="icon">
                <a href="https://travelo.live/frontend/index.php/register/join_moto"><i class="fa fa-motorcycle"></i></a>
              </div>
              <div class="services-content">
                <h3><a href="https://travelo.live/frontend/index.php/register/join_moto">Ojek</a></h3>
                <p>Layanan transportasi online antar jemput penumpang via kendaraan roda 2 dengan kapasitas 1 orang. </p>
              </div>
            </div>
          </div>
          <!-- Services item -->
		  <div class="col-md-3 col-lg-3 col-xs-12">
            <div class="services-item wow fadeInRight" data-wow-delay="1.2s">
              <div class="icon">
                <a href="https://travelo.live/frontend/index.php/register/join_massage"><i class="fa fa-paint-brush"></i></a>
              </div>
              <div class="services-content">
                <h3><a href="https://travelo.live/frontend/index.php/register/join_massage">Pijat</a></h3>
                <p>layanan makeup dengan para therapist yang sudah handal dibidangnya.</p>
              </div>
            </div>
          </div>
		  <!-- Services item -->
		  
          <div class="col-md-3 col-lg-3 col-xs-12">
            <div class="services-item wow fadeInRight" data-wow-delay="0.9s">
              <div class="icon">
                <a href="https://travelo.live/frontend/index.php/register/join_moto"><i class="fa fa-cutlery"></i></a>
              </div>
              <div class="services-content">
                <h3><a href="https://travelo.live/frontend/index.php/register/join_moto">Makanan</a></h3>
                <p>Layanan pesan antar makanan dan minuman dalam satu rute langsung ke tempat tujuan. </p>
              </div>
            </div>
          </div>
		  <!-- Services item -->
		  <div class="col-md-3 col-lg-3 col-xs-12">
            <div class="services-item wow fadeInRight" data-wow-delay="1.8s">
              <a href="http://travelo.live/frontend/index.php/register/join_service"><div class="icon">
			  <i class="fa fa-cog" aria-hidden="true"></i></a>
              </div>
              <div class="services-content">
                <h3><a href="http://travelo.live/frontend/index.php/register/join_service">Service</a></h3>
                <p>Layanan Servis AC dan Cuci AC langsung di tempat pelanggan. </p>
              </div>
            </div>
          </div>
          
          <!-- Services item -->
		  <div class="col-md-3 col-lg-3 col-xs-12">
            <div class="services-item wow fadeInRight" data-wow-delay="1.8s">
              <a href="https://travelo.live/frontend/index.php/register/join_moto"><div class="icon">
			  <i class="fa fa-shopping-cart" aria-hidden="true"></i></a>
              </div>
              <div class="services-content">
                <h3><a href="https://travelo.live/frontend/index.php/register/join_moto">Belanja</a></h3>
                <p>Layanan beli barang-barang langsung dari toko dan diantar ke tempat tujuan pelanggan. </p>
              </div>
            </div>
          </div>
		  <div class="col-md-3 col-lg-3 col-xs-12">
            <div class="services-item wow fadeInRight" data-wow-delay="1.8s">
              <a href="https://travelo.live/frontend/index.php/register/join_box"><div class="icon">
			  <i class="fa fa-truck"></i></a>
              </div>
              <div class="services-content">
                <h3><a href="https://travelo.live/frontend/index.php/register/join_box">BOX</a></h3>
                <p>Layanan Antar Makanan dengan Ratusan Restoran </p>
              </div>
            </div>
          </div>
		  <!-- Services item -->
          <div class="col-md-3 col-lg-3 col-xs-12">
            <div class="services-item wow fadeInRight" data-wow-delay="1.2s">
              <a href="https://travelo.live/frontend/index.php/register/join_moto"><div class="icon">
              
                <i class="fa fa-share"></i></a>
              </div>
              <div class="services-content">
                <h3><a href="https://travelo.live/frontend/index.php/register/join_moto">Antar</a></h3>
                <p>Layanan Kirim Antar barang dalam kota. </p>
              </div>
            </div>
          </div>
          <!-- Services item -->
          
        </div>
      </div>
    </section>
    <!-- Services Section End -->

    <!-- About Section start -->
    <div class="about-area section-padding bg-gray">
      <div class="container">
        <div class="row">
          <div class="col-lg-6 col-md-12 col-xs-12 info">
            <div class="about-wrapper wow fadeInLeft" data-wow-delay="0.3s">
              <div>
                <div class="site-heading">
                  <p class="mb-3"></p>
                  <h2 class="section-title">APA ITU TRAVELO</h2>
                </div>
                <div class="content">
                  <p>
                    Travelo adalah sebuah layanan booking online melalui aplikasi android gratis yang bisa dinikmati oleh semua kalangan. Travelo Menyediakan layanan transportasi online, pengiriman barang, pesan antar makanan, belanja kebutuhan sehari-hari dan penyedia jasa professional secara on demand dalam satu platform aplikasi mobile. Travelo hadir di berbagai kota di kepulauan riau, untuk membantu Anda, baik itu yang membutuhkan pekerjaan maupun yang membutuhkan layanan. 
                  </p>
                  <a href="https://travelo.live/index.php/home/join_driver" class="btn btn-common mt-3">Bergabung Sekarang!</a>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-6 col-md-12 col-xs-12 wow fadeInRight" data-wow-delay="0.3s">
            <img class="img-fluid" src="<?php echo base_url() ?>asset/fs/img/about/img-1.png" alt="" >
          </div>
        </div>
      </div>
    </div>
    <!-- About Section End -->

    <!-- Features Section Start -->
    <section id="features" class="section-padding">
      <div class="container">
        <div class="section-header text-center">          
          <h2 class="section-title wow fadeInDown" data-wow-delay="0.3s">Kenapa Memilih Travelo</h2>
          <div class="shape wow fadeInDown" data-wow-delay="0.3s"></div>
        </div>
        <div class="row">
          <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
            <div class="content-left">
              <div class="box-item wow fadeInLeft" data-wow-delay="0.3s">
                <span class="icon">
                  <i class="lni-rocket"></i>
                </span>
                <div class="text">
                  <h4>Tentukan pengambilan dan pengiriman</h4>
                  <p>Tentukan titik pengambilan dan pengiriman. Travelo akan memperkirakan biaya layanan untuk Anda.</p>
                </div>
              </div>
              <div class="box-item wow fadeInLeft" data-wow-delay="0.6s">
                <span class="icon">
                  <i class="lni-laptop-phone"></i>
                </span>
                <div class="text">
                  <h4>Pencarian Driver</h4>
                  <p>Travelo akan menemukan pengemudi di sekitar Anda. Anda Tinggal tunggu konfirmasi dari Driver.</p>
                </div>
              </div>
              <div class="box-item wow fadeInLeft" data-wow-delay="0.9s">
                <span class="icon">
                  <i class="lni-cog"></i>
                </span>
                <div class="text">
                  <h4>Fitur Chat</h4>
                  <p>Tersedia Fitur Chat yang mempermudah komunikasi antara driver dan pelanggan langsung dari Aplikasi Travelo</p>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
            <div class="show-box wow fadeInUp" data-wow-delay="0.3s">
              <img src="<?php echo base_url() ?>asset/fs/img/feature/intro-mobile.png" alt="">
            </div>
          </div>
          <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
            <div class="content-right">
              <div class="box-item wow fadeInRight" data-wow-delay="0.3s">
                <span class="icon">
                  <i class="lni-leaf"></i>
                </span>
                <div class="text">
                  <h4>Pesan yang sangat mudah</h4>
                  <p>Aktifkan lokasi kamu, lalu tentukan tujuan dan titik jemput sesuai lokasi.</p>
                </div>
              </div>
              <div class="box-item wow fadeInRight" data-wow-delay="0.6s">
                <span class="icon">
                  <i class="lni-layers"></i>
                </span>
                <div class="text">
                  <h4>Dapatkan Pengemudi terdekat</h4>
                  <p>Kamu akan mendapatkan driver terdekat dengan lokasi kamu, dan kamu akan di hubungi driver tersebut sesegera mungkin.</p>
                </div>
              </div>
              <div class="box-item wow fadeInRight" data-wow-delay="0.9s">
                <span class="icon">
                  <i class="lni-leaf"></i>
                </span>
                <div class="text">
                  <h4>Konfirmasi Driver</h4>
                  <p>YES !! Driver menjemputmu, Tunggu beberapa menit, driver sedang menuju ke lokasi kamu.</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- Features Section End -->   

    <!-- Call To Action Section Start -->
    
    <!-- Call To Action Section Start -->

    <!-- Pricing section Start --> 
    
    <!-- Pricing Table Section End -->
  
    <!-- Testimonial Section Start -->
   

    <!-- Testimonial Section End -->

    <!-- Call To Action Section Start -->

    <!-- Call To Action Section Start -->

    <!-- Contact Section Start -->
    <section id="contact" class="section-padding bg-gray">    
      <div class="container">
        <div class="section-header text-center">          
          <h2 class="section-title wow fadeInDown" data-wow-delay="0.3s">Lokasi Kami</h2>
          <div class="shape wow fadeInDown" data-wow-delay="0.3s"></div>
        </div>
        <div class="row contact-form-area wow fadeInUp" data-wow-delay="0.3s">   
          <div class="col-lg-7 col-md-12 col-sm-12">
    <!-- Call To Action Section Start -->
    <section id="cta" class="section-padding">
      <div class="container">
        <div class="row">
          <div class="col-lg-6 col-md-6 col-xs-12 wow fadeInLeft" data-wow-delay="0.3s">           
            <div class="cta-text">
              <h4>Kamu Bingung untuk menjadi Mitra ?? </h4>
              <h5>Hubungi kami sekarang juga! atau datang langsung ke kantor kami yang terdekat di KOTAMU!</h5>
            </div>
          </div>
          <div class="col-lg-6 col-md-6 col-xs-12 text-right wow fadeInRight" data-wow-delay="0.3s">
            </br><a rel="nofollow" href="https://travelo.live/index.php/home/join_driver" class="btn btn-common">Bergabung Sekarang!</a>
          </div>
        </div>
      </div>
    </section>
    <!-- Call To Action Section Start -->
          </div>
          <div class="col-lg-5 col-md-12 col-xs-12">
            <div class="map">
              <object style="border:0; height: 280px; width: 100%;" data="

<object style="border:0; height: 200px; width: 100%;" data="<div class="mapouter"><div class="gmap_canvas"><iframe width="600" height="200" id="gmap_canvas" src="https://maps.google.com/maps?q=BNI%20bintan%20center&t=k&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>Google Maps Generator by <a href="https://www.embedgooglemap.net">embedgooglemap.net</a></div><style>.mapouter{position:relative;text-align:right;height:500px;width:600px;}.gmap_canvas {overflow:hidden;background:none!important;height:500px;width:600px;}</style></div></object>
            </div></object>
            </div>
          </div>
        </div>
      </div> 
    </section>
    <!-- Contact Section End -->

    <!-- Footer Section Start -->
    <footer id="footer" class="footer-area section-padding">
      <div class="container">
        <div class="container">
          <div class="row">
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 col-mb-12">
              <div class="widget">
                <h3 class="footer-logo"><img src="<?php echo base_url() ?>asset/images/logo.png"  style="width:250px!important;" height="60px" alt=""></h3>
                <div class="textwidget">
                  <p></p>
                </div>
                <div class="social-icon">
                  <a class="facebook" href="https://www.facebook.com/tra.velo.1297"><i class="lni-facebook-filled"></i></a>
                  <a class="twitter" href="https://twitter.com/Travelo90644280"><i class="lni-twitter-filled"></i></a>
                  <a class="instagram" href="#"><i class="lni-instagram-filled"></i></a>
                  <a class="linkedin" href="https://www.linkedin.com/in/travelo-live-b40752193/"><i class="lni-linkedin-filled"></i></a>
                </div>
              </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
              <h3 class="footer-titel">Products</h3>
              <ul class="footer-link">
                <li><a href="#">Ojek</a></li>
                <li><a href="#">Taxi</a></li>
                <li><a href="#">Pesan Makanan</a></li>
                <li><a href="#">Antar Barang</a></li>           
                <li><a href="#">Belanja</a></li>
                <li><a href="#">Traveling</a></li>
                <li><a href="#">Pembayaran</a></li>
              </ul>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
              <h3 class="footer-titel">Kejelasan kami</h3>
              <ul class="footer-link">
                  
                <li><a href="https://travelo.live/application/views/pdf/IZIN_USAHA_.pdf" download><font color="#F63854">* Surat Izin Usaha</font></a></li>
                <p>Kode KBLI :  49422</p>
                <li><a href="https://travelo.live/application/views/pdf/NIB_9120105932009.pdf"><font color="#F63854">* Nomor induk Berusaha</font></a></li>
                <p>NIB: 9120105932009</p>
                <li><a href="#"><font color="#F63854">* Nomor Pokok wajib Pajak</font></a></li>
                <p>NPWP: 92.907.049.8-214.000 </p>
                
              </ul>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
              <h3 class="footer-titel">Contact</h3>
              <ul class="address">
                <li>
                  <a href="#"><i class="lni-map-marker"></i> Komp. Plaza Bintan Center Jl. D.I.Pandjaitan no.18 KM.9 Tanjung Pinang Sebelah bank BNI Bintan Center, Kode Pos 29125 </a>
                </li>
                <li>
                  <a href="#"><i class="lni-phone-handset"></i> P: +6282388020323</a>
                </li>
                <li>
                  <a href="#"><i class="lni-envelope"></i> E: Admin@Travelo.live</a>
                </li>
              </ul>
            </div>
          </div>
        </div>  
      </div> 
      <div id="copyright">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <div class="copyright-content">
                <p>Copyright © 2020 <a rel="nofollow" href="https://travelo.live">Travelo</a> 
                
                <br>
                Travelo adalah Platform dari CV Travelo. Terdaftar di (Online Single Submission ) OSS REPUBLIK INDONESIA
Pelayanan Perizinan Berusaha Terintegrasi Secara Elektronik</p>
                
              </div>
            </div>
          </div>
        </div>
      </div>   
    </footer> 
    <!-- Footer Section End -->

    <!-- Go to Top Link -->
    <a href="#" class="back-to-top">
    	<i class="lni-arrow-up"></i>
    </a>
    
    <!-- Preloader -->
    <div id="preloader">
      <div class="loader" id="loader-1"></div>
    </div>
    <!-- End Preloader -->
    
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="<?php echo base_url() ?>asset/fs/js/jquery-min.js"></script>
    <script src="<?php echo base_url() ?>asset/fs/js/popper.min.js"></script>
    <script src="<?php echo base_url() ?>asset/fs/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url() ?>asset/fs/js/owl.carousel.min.js"></script>
    <script src="<?php echo base_url() ?>asset/fs/js/wow.js"></script>
    <script src="<?php echo base_url() ?>asset/fs/js/jquery.nav.js"></script>
    <script src="<?php echo base_url() ?>asset/fs/js/scrolling-nav.js"></script>
    <script src="<?php echo base_url() ?>asset/fs/js/jquery.easing.min.js"></script>
    <script src="<?php echo base_url() ?>asset/fs/js/jquery.counterup.min.js"></script>      
    <script src="<?php echo base_url() ?>asset/fs/js/waypoints.min.js"></script>   
    <script src="<?php echo base_url() ?>asset/fs/js/main.js"></script>
      
  </body>
</html>
