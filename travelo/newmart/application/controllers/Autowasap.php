<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Autowasap extends CI_Controller{
    
        
    function __construct(){
        parent::__construct();
        error_reporting(E_ALL);
        ini_set('display_errors', 1);
       
    }

    public function index(){
        echo "api autowasap";
    }

    public function getFreeUser(){
    	$data = array();
    	$imei = $this->input->post('imei');
    	$msg = $this->input->post('msg');

    	$sql = "SELECT * FROM freeuser WHERE imei = ?";
    	$q = $this->db->query($sql, $imei);

    	if($q->num_rows()>0){
    		$row = $q->row();
    		$data['result'] = json_encode(array('contactNo'=>$row->contact_no, 'country_code'=>$row->country_code,'emailId'=>$row->email,
    					'imei'=>$row->imei_no,'msg'=>100,'userCountry'=>$row->user_country,'userName'=>$row->username,'userState'=>'1'));
        	$data['isException'] = false;
    	}else{
    		$data['result'] = 'Invalid license.';
        	$data['isException'] = true;
    	}
      
        echo json_encode($data);
    }


    public function insertFreeUser(){
    	$data = array();
    	

    	$userObj = $this->input->post("userObj");

    	// echo $userObj;die();
    	$data = json_decode($userObj, true);
    	$contactNo = $data['contactNo'];
    	$country_code = $data['country_code'];
    	$emailId = $data['emailId'];
    	$imei = $data['imei'];
    	$userName = $data['userName'];
    	$userCountry = $data['userCountry'];
    	
    	$sql = "SELECT * FROM freeuser WHERE imei_no = ?";
    	$q = $this->db->query($sql, $imei);

    	if($q->num_rows() > 0){
    		$data['result'] = 'Imei has been registered on another account.';
        	$data['isException'] = true;

    	}else{

    		$simpan = array();
    		$simpan['contact_no'] = $contactNo;
    		$simpan['country_code'] = $country_code;
    		$simpan['email'] = $emailId;
    		$simpan['imei_no'] = $imei;
    		$simpan['username'] = $userName;
    		$simpan['user_country'] = $userCountry;

			$result = $this->db->insert('freeuser', $simpan);

			if($result){

				$data['result'] = json_encode(array('contactNo'=>$contactNo, 'country_code'=>$country_code,'emailId'=>$emailId,
    					'imei'=>$imei,'msg'=>100,'userCountry'=>$userCountry,'userName'=>$userName,'userState'=>'1'));
        		$data['isException'] = false;
			}else{
				$data['result'] = 'Failed to registere free user';
        		$data['isException'] = true;
			}
    		
    	}

        echo json_encode($data);
    }

    public function updateFreeUser(){

        $userObj = $this->input->post("userObj");

    	// echo $userObj;die();
    	$data = json_decode($userObj, true);
    	$contactNo = $data['contactNo'];
    	$country_code = $data['country_code'];
    	$emailId = $data['emailId'];
    	$imei = $data['imei'];
    	$userName = $data['userName'];
    	$userCountry = $data['userCountry'];

    	$sql = "SELECT * FROM freeuser WHERE imei_no = ?";
    	$q = $this->db->query($sql, $imei);

    	if($q->num_rows() > 0){
    		$simpan = array();
    		$simpan['contact_no'] = $contactNo;
    		$simpan['country_code'] = $country_code;
    		$simpan['email'] = $emailId;
    		$simpan['username'] = $userName;
    		$simpan['user_country'] = $userCountry;

			$this->db->where('imei_no', $imei);
			$result = $this->db->update('freeuser', $simpan);

			if($result){
				$data['result'] = json_encode(array('contactNo'=>$contactNo, 'country_code'=>$country_code,'emailId'=>$emailId,
    					'imei'=>$imei,'msg'=>100,'userCountry'=>$userCountry,'userName'=>$userName,'userState'=>'1'));
        		$data['isException'] = false;
			}else{
				$data['result'] = 'Failed to update data.';
        		$data['isException'] = true;
			}
		
    	}else{
    		$data['result'] = 'User account not found.';
        	$data['isException'] = true;
    		
    	}

        echo json_encode($data);
    }


    public function GetLicense(){
    	$data = array();
    	$licenseNo = $this->input->post("licenseNo");

    	$row = json_decode($licenseNo, true);
    	$nolisen = $row;


    	$sql = "SELECT * FROM license WHERE license_no = ?";
    	$q = $this->db->query($sql, $nolisen);

    	if($q->num_rows() > 0){
    		$row = $q->row();
    		$data['result'] = json_encode(array('NumberOfLicence'=>$row->id, 'TotalAmt'=>$row->total_amt,'activation_date'=>$row->activation_date,
    					'cname'=>$row->cname,'country_code'=>$row->country_code,'did'=>$row->did,'email'=>$row->email,'imei_no'=>$row->imei_no,'licenseNo'=>$row->license_no, 'licenseType'=>$row->license_type,'mobile'=>$row->mobile,'registeredOn'=>$row->registered_on,'status'=>'Active'));
        	$data['isException'] = false;
    	}else{

    		$data['result'] = 'Invalid license';
    		$data['isException'] = true;
    	}
      
        echo json_encode($data);
    }

    public function Activate(){
    	$data = array();
    	$simpan = array();

    	$activation = json_decode($this->input->post("activation"), true);

    	$array = json_encode($activation);
    	$raw = json_decode($array, true);
    	
    	$cname = $raw['cname'];
    	$country_code = $raw['country_code'];
    	$email = $raw['email'];
    	$imei_no = $raw['imei_no'];
    	$licenseNo = $raw['licenseNo'];
    	$mobile = $raw['mobile'];

    	$simpan['total_amt'] = 0;
    	$simpan['activation_date'] = date('Y-m-d H:i:s');
    	$simpan['cname'] = $cname;
    	$simpan['country_code'] = $country_code;
    	$simpan['email'] = $email;
    	$simpan['imei_no'] = $imei_no;
    	$simpan['mobile'] = $mobile;
    	$simpan['status'] = 1;
    	$simpan['license_type'] = 1;

    	

    	$sql = "SELECT * FROM license WHERE license_no = ?";
    	$q = $this->db->query($sql, $licenseNo);

    	if($q->num_rows() > 0 ){
    		$this->db->where('license_no', $licenseNo);

			$res = $this->db->update('license', $simpan);

			if($res){
				$data['result'] = json_encode(array('NumberOfLicence'=>1, 'TotalAmt'=>'0','activation_date'=>$simpan['activation_date'],'cname'=>$cname,'country_code'=>$country_code,'did'=>1,'email'=>$email,'licenseNo'=>$licenseNo,'imei_no'=>$imei_no, 'licenseType'=> 1,'mobile'=>$mobile,'registeredOn'=>'','status'=>'Active'));
				$data['isException'] = false;
			}else{

				$data['result'] = 'Invalid License';
				$data['isException'] = true;
			}
    	}else{
    		$data['result'] = 'Invalid License';
			$data['isException'] = true;
    	}


    
        echo json_encode($data);
    }


    function randomNumber() {
	    $alphabet = "1234567890";
	    //$alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
	    $pass = array(); //remember to declare $pass as an array
	    $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
	    $date = date('dmY');
	    for ($i = 0; $i < 5; $i++) {
	        $n = rand(0, $alphaLength);
	        $pass[] = $alphabet[$n];
	    }
	    $kode = implode($pass);
	    return $date.$kode;
	     //turn the array into a string
	}

	public function generateLicense($jum){
		$simpan = array();
		for($i = 0;$i < $jum; $i++){
			$simpan['license_no'] = $this->randomNumber();
			$simpan['registered_on'] = date('Y-m-d H:i:s');

			$this->db->insert('license', $simpan); 


		}

		echo 'Generate lincese '.$jum. ' success!';
	}


}

/* End of file Groups.php */
/* Location: ./application/controllers/Groups.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2016-05-14 11:23:29 */
/* http://harviacode.com */