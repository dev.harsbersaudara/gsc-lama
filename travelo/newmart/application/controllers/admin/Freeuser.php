<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Freeuser extends CI_Controller {

	public function __construct(){
        parent::__construct();
        check_login_user();
       $this->load->model('common_model');
       $this->load->model('login_model');
    }


    public function index()
    {
        $data = array();
        $data['page_title'] = 'Freeuser';
        $data['freeuser'] = $this->common_model->get_all_freeuser();
        $data['count'] = $this->common_model->get_freeuser_total();
        $data['main_content'] = $this->load->view('admin/freeuser/free_user', $data, TRUE);
        $this->load->view('admin/index', $data);
    }

    //-- add new user by admin
    public function add()
    {   
        if ($_POST) {

            $data = array(
                'first_name' => $_POST['first_name'],
                'last_name' => $_POST['last_name'],
                'email' => $_POST['email'],
                'password' => md5($_POST['password']),
                'mobile' => $_POST['mobile'],
                'country' => $_POST['country'],
                'status' => $_POST['status'],
                'role' => $_POST['role'],
                'created_at' => current_datetime()
            );

            $data = $this->security->xss_clean($data);
            
            //-- check duplicate email
            $email = $this->common_model->check_email($_POST['email']);

            if (empty($email)) {
                $user_id = $this->common_model->insert($data, 'user');
            
                if ($this->input->post('role') == "user") {
                    $actions = $this->input->post('role_action');
                    foreach ($actions as $value) {
                        $role_data = array(
                            'user_id' => $user_id,
                            'action' => $value
                        ); 
                       $role_data = $this->security->xss_clean($role_data);
                       $this->common_model->insert($role_data, 'user_role');
                    }
                }
                $this->session->set_flashdata('msg', 'User added Successfully');
                redirect(base_url('admin/user/all_user_list'));
            } else {
                $this->session->set_flashdata('error_msg', 'Email already exist, try another email');
                redirect(base_url('admin/user'));
            }
            
            
            

        }
    }

    public function all_user_list()
    {
        $data['freeuser'] = $this->common_model->get_all_freeuser();
        $data['count'] = $this->common_model->get_freeuser_total();
        $data['main_content'] = $this->load->view('admin/freeuser/free_user', $data, TRUE);
        $this->load->view('admin/index', $data);
    }

    //-- update users info
    public function update($id)
    {
        if ($_POST) {

            $data = array(
                'first_name' => $_POST['first_name'],
                'last_name' => $_POST['last_name'],
                'mobile' => $_POST['mobile'],
                'country' => $_POST['country'],
                'role' => $_POST['role']
            );
            $data = $this->security->xss_clean($data);

            $powers = $this->input->post('role_action');
            if (!empty($powers)) {
                $this->common_model->delete_user_role($id, 'user_role');
                foreach ($powers as $value) {
                   $role_data = array(
                        'user_id' => $id,
                        'action' => $value
                    ); 
                   $role_data = $this->security->xss_clean($role_data);
                   $this->common_model->insert($role_data, 'user_role');
                }
            }

            $this->common_model->edit_option($data, $id, 'user');
            $this->session->set_flashdata('msg', 'Information Updated Successfully');
            redirect(base_url('admin/user/all_user_list'));

        }

        $data['user'] = $this->common_model->get_single_user_info($id);
        $data['user_role'] = $this->common_model->get_user_role($id);
        $data['power'] = $this->common_model->select('user_power');
        $data['country'] = $this->common_model->select('country');
        $data['main_content'] = $this->load->view('admin/user/edit_user', $data, TRUE);
        $this->load->view('admin/index', $data);
        
    }

    
    //-- active user
    public function active($id) 
    {
        $data = array(
            'user_state' => 1
        );
        $data = $this->security->xss_clean($data);
        $this->common_model->update($data, $id,'freeuser');
        $this->session->set_flashdata('msg', 'User active Successfully');
        redirect(base_url('admin/freeuser'));
    }

    //-- deactive user
    public function deactive($id) 
    {
        $data = array(
            'user_state' => 0
        );
        $data = $this->security->xss_clean($data);
        $this->common_model->update($data, $id,'freeuser');
        $this->session->set_flashdata('msg', 'User deactive Successfully');
        redirect(base_url('admin/freeuser'));
    }

    //-- delete user
    public function delete($id)
    {
        $this->common_model->delete($id,'freeuser'); 
        $this->session->set_flashdata('msg', 'User deleted Successfully');
        redirect(base_url('admin/freeuser'));
    }


    


}