<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class License extends CI_Controller {

	public function __construct(){
        parent::__construct();
        check_login_user();
       $this->load->model('common_model');
       $this->load->model('login_model');
    }


    public function index()
    {
        $data = array();
        $data['page_title'] = 'Lincense';
        $data['license'] = $this->common_model->get_all_license();
        $data['count'] = $this->common_model->get_license_total();
        $data['main_content'] = $this->load->view('admin/license/user', $data, TRUE);
        $this->load->view('admin/index', $data);
    }

    public function user_active(){
        $data = array();
        $data['page_title'] = 'Lincense';
        $data['license'] = $this->common_model->get_active_license("1");
        $data['count'] = $this->common_model->get_license_total();
        $data['main_content'] = $this->load->view('admin/license/user', $data, TRUE);
        $this->load->view('admin/index', $data);
    }

    //-- add new user by admin
    public function add_generate()
    {   
        $data = array();
        $data['page_title'] = 'Lincense';
        $data['license'] = $this->common_model->get_all_license();
        $data['count'] = $this->common_model->get_license_total();
        $data['main_content'] = $this->load->view('admin/license/generate', $data, TRUE);
        $this->load->view('admin/index', $data);
            
    }

    public function generate(){
        if($_POST){
            
            $num = $this->input->post('number');
            for($i = 0;$i < $num; $i++){
                $data = array(
                'license_no' => $this->randomNumber(),
                'registered_on'=>date('Y-m-d H:i:s')
                 );

                $this->common_model->insert($data, 'license');
            }
            $this->session->set_flashdata('msg', 'Generate license Successfully');
            redirect(base_url('admin/license'));

        }else{
            $data = array();
            $data['page_title'] = 'Lincense';
            $data['license'] = $this->common_model->get_all_license();
            $data['count'] = $this->common_model->get_license_total();
            $data['main_content'] = $this->load->view('admin/license/generate', $data, TRUE);
            $this->load->view('admin/index', $data);
        }
    }

    public function all_user_list()
    {
        $data['freeuser'] = $this->common_model->get_all_freeuser();
        $data['count'] = $this->common_model->get_freeuser_total();
        $data['main_content'] = $this->load->view('admin/freeuser/free_user', $data, TRUE);
        $this->load->view('admin/index', $data);
    }


    public function active($id) 
    {
        $data = array(
            'status' => 1
        );
        $data = $this->security->xss_clean($data);
        $this->common_model->update($data, $id,'license');
        $this->session->set_flashdata('msg', 'User active Successfully');
        redirect(base_url('admin/license'));
    }

    //-- deactive user
    public function deactive($id) 
    {
        $data = array(
            'status' => 0
        );
        $data = $this->security->xss_clean($data);
        $this->common_model->update($data, $id,'license');
        $this->session->set_flashdata('msg', 'User deactive Successfully');
        redirect(base_url('admin/license'));
    }

    //-- delete user
    public function delete($id)
    {
        $this->common_model->delete($id,'license'); 
        $this->session->set_flashdata('msg', 'User deleted Successfully');
        redirect(base_url('admin/license'));
    }

    // delete all license
    public function deleteAlls(){
        $ids = $this->input->post('ids');
        $this->db->where_in('id', explode(",", $ids));
        $this->db->delete('license');
 
        echo json_encode(['success'=>"Item Deleted successfully."]);
    }


   function randomNumber() {
        $alphabet = "1234567890";
        //$alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        $date = date('dmY');
        for ($i = 0; $i < 5; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        $kode = implode($pass);
        return $date.$kode;
         //turn the array into a string
    }


}