

<!-- Container fluid  -->

<div class="container-fluid">
    
    <!-- Bread crumb and right sidebar toggle -->
    
    <div class="row page-titles">
        <div class="col-md-5 col-8 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">License</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active">All License Users</li>
            </ol>
        </div>
        <div class="col-md-7 col-4 align-self-center">
            
            
            <div class="d-flex m-t-10 justify-content-end">
                <div class="d-flex m-r-20 m-l-10 hidden-md-down">
                    <div class="chart-text m-r-10">
                        <h6 class="m-b-0"><small>Active User</small></h6>
                        <h4 class="m-t-0 text-info"><?php echo $count->active_user; ?></h4>
                    </div>
                </div>
                <div class="d-flex m-r-20 m-l-10 hidden-md-down">
                    <div class="chart-text m-r-10">
                        <h6 class="m-b-0"><small>Inctive User</small></h6>
                        <h4 class="m-t-0 text-primary"><?php echo $count->inactive_user; ?></h4>
                    </div>
                </div>
                <div class="">
                    <button class="right-side-toggle waves-effect waves-light btn-success btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
                </div>
            </div>
        </div>
    </div>
    
    <!-- End Bread crumb and right sidebar toggle -->
    

    
    <!-- Start Page Content -->

    <div class="row">
        <div class="col-12">

            <?php $msg = $this->session->flashdata('msg'); ?>
            <?php if (isset($msg)): ?>
                <div class="alert alert-success delete_msg pull" style="width: 100%"> <i class="fa fa-check-circle"></i> <?php echo $msg; ?> &nbsp;
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                </div>
            <?php endif ?>

            <?php $error_msg = $this->session->flashdata('error_msg'); ?>
            <?php if (isset($error_msg)): ?>
                <div class="alert alert-danger delete_msg pull" style="width: 100%"> <i class="fa fa-times"></i> <?php echo $error_msg; ?> &nbsp;
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                </div>
            <?php endif ?>

            <div class="card">

                <div class="card-body">

               

                    <div class="table table-striped">
                        <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                                <tr>

                                    <th>
                                        <label class="custom-control custom-checkbox">
                                            <input type="checkbox" id="select-all" class="custom-control-input" name="select-all" />
                                            <span class="custom-control-indicator"></span>
                                            <span class="custom-control-description">Check All</span>
                                        </label>
                                    </th>
                                    <th>Username</th>
                                    <th>Email</th>
                                    <th>Mobile</th>
                                    <th>Activation Date</th>
                                    <th>Imei</th>
                                    <th>Lincense No</th>
                                 
                                    <th>Action</th>
                                </tr>
                            </thead>
                           
                            
                            <tbody>
                            <?php foreach ($license as $user): ?>
                                
                                <tr>
                                    <td class="active">                                        
                                        <label class="custom-control custom-checkbox">
                                            <input id= "select-item" type="checkbox" class="custom-control-input" name="select-item[]" value="<?php echo $user['id']; ?>" />
                                            <span class="custom-control-indicator"></span>
                                            <span class="custom-control-description"><?php echo $user['id']; ?></span>
                                        </label>
                                    </td>
                                    <td><?php echo $user['cname']; ?></td>
                                    <td><?php echo $user['email']; ?></td>
                                    <td><?php echo $user['mobile']; ?></td>
                                    <td><?php echo $user['activation_date']; ?></td>
                                    <td><?php echo $user['imei_no']; ?></td>
                                    <td><?php echo $user['license_no']; ?></td>
                                   
                                    <td class="text-nowrap">

                                        <?php if ($this->session->userdata('role') == 'admin'): ?>
                                           

                                            <a id="delete" data-toggle="modal" data-target="#confirm_delete_<?php echo $user['id'];?>" href="#"  data-toggle="tooltip" data-original-title="Delete"> <i class="fa fa-trash text-danger m-r-10"></i> </a>


                                        <?php else: ?>

                                            <!-- check logged user role permissions -->

                                           
                                            <?php if(check_power(3)):?>
                                                <a href="<?php echo base_url('admin/license/delete/'.$user['id']) ?>" onClick="return doconfirm();" data-toggle="tooltip" data-original-title="Delete"> <i class="fa fa-trash text-danger m-r-10"></i> </a>
                                            <?php endif; ?>

                                        <?php endif ?>

                                        
                                        
                                        <?php if ($user['status'] == 1): ?>
                                            <a href="<?php echo base_url('admin/license/deactive/'.$user['id']) ?>" data-toggle="tooltip" data-original-title="Deactive"> <i class="fa fa-close text-danger m-r-10"></i> </a>
                                        <?php else: ?>
                                            <a href="<?php echo base_url('admin/license/active/'.$user['id']) ?>" data-toggle="tooltip" data-original-title="Active"> <i class="fa fa-check text-info m-r-10"></i> </a>
                                        <?php endif ?>
                                        
                                    </td>
                                </tr>

                            <?php endforeach ?>

                            </tbody>

                            <button class="btn btn-danger" id="selected" data-url = "/itemDelete">Delete</button>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- End Page Content -->

</div>



<?php foreach ($license as $user): ?>
 
<div class="modal fade" id="confirm_delete_<?php echo $user['id'];?>">
  <div class="modal-dialog">
    <div class="modal-content">

      <div class="modal-header">
        <h4 class="modal-title"></h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
       
            <div class="form-body">
                
                Are you sure want to delete? <br> <hr>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <a href="<?php echo base_url('admin/license/delete/'.$user['id']) ?>" class="btn btn-danger"> Delete</a>
                
            </div>

      </div>


    </div>
  </div>
</div>
<?php endforeach ?>

<script>
        $(function(){

            //button select all or cancel
            $("#select-all").click(function () {
                var all = $("input#select-all")[0];
                var checked = all.checked;
                $("input#select-item").each(function (index,item) {
                    item.checked = checked;
                });
                
            });

            //button select invert
            $("#select-invert").click(function () {
                $("input.select-item").each(function (index,item) {
                    item.checked = !item.checked;
                });var confirm = window.confirm("Apakah Anda yakin ingin menghapus data-data ini?");
                checkSelected();
            });

            //button get selected info
            $("#selected").click(function () {
                var items=[];
                $("input#select-item:checked:checked").each(function (index,item) {
                    items[index] = item.value;
                });
                if (items.length <= 0) {
                    alert("Please select record.");
                }else {
                    var confirm = window.confirm("Are you sure delete these data?");
                    if(confirm){
                        var values = items.join(',');
                        console.log(values);
                        var html = $("<div></div>");
                        html.html("selected:"+values);
                        html.appendTo("body");
                        $.ajax({
                            url: "<?php echo base_url('admin/license/deletealls') ?>",
                            method: 'POST',
                            data: 'ids='+values,
                            success: function (data) {
                              console.log(data);
                              $("input#select-item:checked:checked").each(function() {  
                                  $(this).parents("tr").remove();
                              });
                              alert("Item Deleted successfully.");
                            },
                            error: function (data) {
                                alert(data.responseText);
                            }
                        });
                    }
                    
                }
            });


            //check selected items
            // $("input.select-item").click(function () {
            //     var checked = this.checked;
            //     console.log(checked);
            //     checkSelected();
            // });

            //check is all selected
            // function checkSelected() {
            //     var all = $("input.select-all")[0];
            //     var total = $("input.select-item").length;
            //     var len = $("input.select-item:checked:checked").length;
            //     console.log("total:"+total);
            //     console.log("len:"+len);
            //     all.checked = len===total;
            // }
        });
    </script>