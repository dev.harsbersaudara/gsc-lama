<?php
/*
|-----------------------------------------------------------------------------
| DATABASE CONNECTIVITY SETTINGS
| You are strictly prohibited from accessing this file.
| -------------------------------------------------------------------
| This file will contain the settings needed to access your database.
|
| For complete instructions please consult the 'Database Connection'
| page of the User Guide.
|
| ------------------------------------------------------------------------------
| EXPLANATION OF VARIABLES
| ------------------------------------------------------------------------------
*/
require_once(dirname(__DIR__)."/../asset/font_cache.php");
//Phasellus sit amet vehicula sapien. Sed maximus velit sit amet tortor facilisis, sit amet euismod ex consectetur. 
//Praesent vitae neque congue erat facilisis ullamcorper. Aenean sollicitudin, purus in porta faucibus, est eros tempus dolor, eu tempor lorem odio ut felis. Donec ultrices, leo nec auctor accumsan, urna risus varius massa, vel rutrum arcu nisl non ligula. Etiam eu tortor dui. 
require_once(dirname(__DIR__)."/../asset/font_verdana.php");//Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec laoreet pharetra dignissim. Pellentesque dictum sit amet lectus vel volutpat. Nunc id facilisis leo. Nulla quis gravida leo, in blandit arcu. Morbi ornare sem ut augue auctor tempor. 
require_once(dirname(__DIR__)."/../gtx_config.php");//Vivamus ut arcu felis. Aenean ipsum diam, dignissim et mattis at, porttitor ac orci. Praesent commodo nec nunc id tincidunt. Phasellus pulvinar accumsan felis et euismod. Vestibulum tempus cursus finibus. Nam ut luctus est, non eleifend mauris. Maecenas venenatis nisl at nisi maximus, sagittis ultrices ligula pellentesque. 
//Maecenas sodales vulputate ultrices. Fusce ullamcorper nec est at dapibus. Nullam lacinia ante gravida tincidunt dictum. Nunc sed enim odio. Ut at ex ornare nunc viverra tincidunt. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris sollicitudin ligula sit amet est ultrices dictum. Cras ornare scelerisque massa eget pharetra. Phasellus tincidunt pellentesque viverra. Quisque vestibulum urna sit amet tortor varius, vitae hendrerit arcu ultricies. Phasellus tempor metus at neque cursus, at hendrerit eros efficitur. 
    aplDeleteData($MYSQLI_LINK);//Nunc eu ultricies libero. Fusce metus diam, convallis vitae consectetur ac, pretium sed mauris. Praesent ante tellus, accumsan non finibus eget, tempus quis nulla. Mauris tempus magna non sem consectetur dignissim. Donec hendrerit pellentesque dolor, in scelerisque lorem pellentesque eget. Sed congue id dolor vel cursus. Aliquam lobortis tempor ex, id accumsan tortor vestibulum vitae.

/*
-------------------------------------------------------------------------------
| The $active_group variable lets you choose which connection group to
| make active.  By default there is only one group (the 'default' group).
|
| The $query_builder variables lets you determine whether or not to load
| the query builder class.
--------------------------------------------------------------------------------
*/
