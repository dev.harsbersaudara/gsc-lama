<?php
defined('BASEPATH') OR exit('No direct script access allowed');require_once("../gtx_settings.php");
?>
<!DOCTYPE html>
<html>
    <head>
        
<title><?php echo "$name_apps"; ?> - Validate Driver <?php echo "$tittle";?></title>

 <meta content="template gotaxi" name="keywords">
 <meta content="go-taxi" name="author">
 <meta content="On Demand All in One App Services Android" name="description">
 <link rel="shortcut icon" href="/asset/images/favicon.png">
 <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
 <link rel="stylesheet" href="<?php echo base_url(); ?>bootstrap/css/bootstrap.min.css"> 
  <link rel="stylesheet" href="<?php echo $bootstrap; ?>">
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
 <link rel="stylesheet" href="<?php echo base_url(); ?>dist/css/skins/_all-skins.min.css">
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    
 <style type = "text/css">
 
 .label{display:inline;padding:.2em .6em .3em;font-size:75%;font-weight:700;line-height:1;color:#fff;text-align:center;white-space:nowrap;vertical-align:baseline;border-radius:.25em}a.label:focus,a.label:hover{color:#fff;text-decoration:none;cursor:pointer}.label:empty{display:none}.btn .label{position:relative;top:-1px}.label-default{background-color:#777}.label-default[href]:focus,.label-default[href]:hover{background-color:#5e5e5e}.label-primary{background-color:#337ab7}.label-primary[href]:focus,.label-primary[href]:hover{background-color:#286090}.label-success{background-color:#5cb85c}.label-success[href]:focus,.label-success[href]:hover{background-color:#449d44}.label-info{background-color:#5bc0de}.label-info[href]:focus,.label-info[href]:hover{background-color:#31b0d5}.label-warning{background-color:#f0ad4e}.label-warning[href]:focus,.label-warning[href]:hover{background-color:#ec971f}.label-danger{background-color:#d9534f}.label-danger[href]:focus,.label-danger[href]:hover{background-color:#c9302c}.
 </style>
 <link rel="stylesheet" href="/modules/css/animate.css"/>
 <script type="text/javascript" src="/modules/js/jquery-2.1.1.min.js"></script>
 <script type="text/javascript" src="/modules/js/jquery.countTo.js"></script>
 <script type="text/javascript" src="/modules/js/moment.min.js"></script>
 <script type="text/javascript" src="/modules/js/app.js"></script>
 </head>
 
 <body>
    
   <?php include 'header.php'; ?>  
   <div class="wrapper ">
	<?php include 'SIDEBAR.php'; ?>
	<div class="main">
		<div class="breadcrumb">
	<a href="<?php echo base_url(); ?>index.php/Listpelanggan"> Validate Applicant <?php echo "$tittle";?></a> </div>

<div class="content with-top-banner">
	<div class="content-header no-mg-top">
		<i class="fa fa-newspaper-o"></i>
		<div class="content-header-title">Perform registration validation <?php echo "$tittle";?></div>
	
</div>
	<div class="panel">
      <div class="row">
             	    <div class="col-md-12">
                 
                            <!-- TABLE: LATEST ORDERS -->
                            <div class="content-box">
                                
                                <div class="box-header">
                                    
                                </div>
                                <?php echo "$pesan"; ?>
                                <!-- /.box-header -->
                                <div class="box-body">
                                   
                                   
                                    <table id="example1" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                               <th>No</th>
                                                <th>Name</th>
                                                <th>Phone</th>
                                                <th>Email</th>
                                                <th>Date of birth</th>
                                                <th>Address</th>
                                                <th>Detil</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                            <?php
                                            $no = 1;
//                                            var_dump($pendaftar);
                                            foreach ($pendaftar as $d) {
                                                ?>
                                                <tr>
                                                    <td><p class="text-center"><?php echo $no;
                                            $no++ ?></p></td>
                                                    <td><?php echo "$d->nama_lengkap"; ?></td>
                                                    <td><?php echo "$d->nomor_telepon"; ?></td>
                                                    <td><?php echo "$d->email"; ?></td>
                                                    <td><?php echo "$d->tanggal_lahir"; ?></td>
                                                    <td><?php echo "$d->alamat_tinggal , $d->kecamatan , $d->kota "; ?></td>
                                                    <td><a href="<?php echo base_url() ?>index.php/Validatedriver/detilPelamarMmassage/<?php echo "$d->nomor"; ?>"><button type="button" class="btn btn-default btn-sm">View Detail</button></a></td>
                                                    <td>
                                                        <a href="<?php echo base_url() ?>index.php/Validatedriver/validasiMmassage/<?php echo "$d->nomor"; ?>"><button type="button" class="btn btn-primary btn-sm" onclick="return confirm('Are you sure to validate the registration ?')">Validation</button></a>
                                                        <a href="<?php echo base_url() ?>index.php/Validatedriver/tolakMmassage/<?php echo "$d->nomor"; ?>"><button type="button" class="btn btn-danger btn-sm" onclick="return confirm('Are you sure to cancel the registration ?')"><span class="fa fa-trash-o"></span></button></a>
                                                    </td>
                                                </tr>
<?php }
?>
                                        </tbody>
                                    </table>
                                    
                                    
                                    
                                </div>
                                <!-- /.box-body -->
                            </div>
                            <!-- /.box -->
                        </div>
                        <!-- /.col -->
                    </div>
                    
                    
                    <!-- /.row -->
		


</div>
<!-- /end panel-->







 </div>

</div>




  
       
        <!-- ./wrapper -->

        <!-- jQuery 2.2.3 -->
        <script src="<?php echo base_url(); ?>plugins/jQuery/jquery-2.2.3.min.js"></script>
        <!-- Bootstrap 3.3.6 -->
        <script src="<?php echo base_url(); ?>bootstrap/js/bootstrap.min.js"></script>
        <!-- DataTables -->
        <script src="<?php echo base_url(); ?>plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="<?php echo base_url(); ?>plugins/datatables/dataTables.bootstrap.min.js"></script>
        <!-- SlimScroll -->
        <script src="<?php echo base_url(); ?>plugins/slimScroll/jquery.slimscroll.min.js"></script>
        <!-- FastClick -->
        <script src="<?php echo base_url(); ?>plugins/fastclick/fastclick.js"></script>
        
        <script>
            $(function () {
                $("#example1").DataTable();
                $('#example2').DataTable({
                    "paging": true,
                    "lengthChange": false,
                    "searching": false,
                    "ordering": true,
                    "info": true,
                    "autoWidth": false
                });
            });
        </script>



</body>
</html>
