<?php
defined('BASEPATH') OR exit('No direct script access allowed');require_once("../gtx_settings.php");
?>
<!DOCTYPE html>
<html>
    <head>
    
  <title><?php echo "$name_apps"; ?> Dashboard</title>
 <meta content="template gotaxi" name="keywords">
 <meta content="go-taxi" name="author">
 <meta content="On Demand All in One App Services Android" name="description">
 <link rel="shortcut icon" href="/asset/images/favicon.png">
 <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
 <link rel="stylesheet" href="<?php echo base_url(); ?>bootstrap/css/bootstrap.min.css"> 
  <link rel="stylesheet" href="<?php echo $bootstrap; ?>">
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
 <link rel="stylesheet" href="<?php echo base_url(); ?>dist/css/skins/_all-skins.min.css">
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    
 <style type = "text/css">
 
 .label{display:inline;padding:.2em .6em .3em;font-size:75%;font-weight:700;line-height:1;color:#fff;text-align:center;white-space:nowrap;vertical-align:baseline;border-radius:.25em}a.label:focus,a.label:hover{color:#fff;text-decoration:none;cursor:pointer}.label:empty{display:none}.btn .label{position:relative;top:-1px}.label-default{background-color:#777}.label-default[href]:focus,.label-default[href]:hover{background-color:#5e5e5e}.label-primary{background-color:#337ab7}.label-primary[href]:focus,.label-primary[href]:hover{background-color:#286090}.label-success{background-color:#5cb85c}.label-success[href]:focus,.label-success[href]:hover{background-color:#449d44}.label-info{background-color:#5bc0de}.label-info[href]:focus,.label-info[href]:hover{background-color:#31b0d5}.label-warning{background-color:#f0ad4e}.label-warning[href]:focus,.label-warning[href]:hover{background-color:#ec971f}.label-danger{background-color:#d9534f}.label-danger[href]:focus,.label-danger[href]:hover{background-color:#c9302c}.
 </style>
 <link rel="stylesheet" href="/modules/css/animate.css"/>
 <script type="text/javascript" src="/modules/js/jquery-2.1.1.min.js"></script>
 <script type="text/javascript" src="/modules/js/jquery.countTo.js"></script>
 <script type="text/javascript" src="/modules/js/moment.min.js"></script>
 <script type="text/javascript" src="/modules/js/app.js"></script>
 </head>
 
 <body>
        
  <?php include 'header.php'; ?>  
       <div class="wrapper">
            
            <?php include 'SIDEBAR.php'; ?>



<div class="main">
    <div class="content with-top-banner">
	<div class="content-header no-mg-top">
		<i class="fa fa-user-md"></i>
		<div class="content-header-title"> Edit Go-Massage services</div>
		
	</div>
	<div class="panel">
         
                    <div class="row">
                        <div class="col-md-12">
                            <div class="content-box">
                                <div class="box-header">
                                   

                                </div>
                                <!-- /.box-header -->
                               <div class="col-md-12">
                                        <h3 class="text-center"></h3>
                                        
                                        
                        <form action="<?php echo base_url(); ?>index.php/Massageservices/updateLayananPijat" method="POST" role="form" enctype="multipart/form-data">
                                            <?php // var_dump($kategori)?>
                            
                        <div class="box-body">
                             <div class="form-group">
                                
                                                <label for="">Name Service :</label>
                                                <input name="layanan" type="text" class="form-control" id="exampleInputEmail1" placeholder="" value="<?php echo $layanan[0]['layanan']; ?>">
                                </div>
                            </div>
                            
							
							<div class="box-body">
							   <div class="form-group">
                                                <label for="">Price :</label>
                                                <input name="harga" type="text" class="form-control" id="exampleInputEmail1" placeholder="" value="<?php echo $harga[0]['harga']; ?>">
                                </div>
                            </div>
                            
                            
                            
            <div class="form-group">
                  <label for="exampleInputEmail1">Charge Description</label>
                  
                    <input name="keterangan_biaya" type="text" class="form-control" id="exampleInputEmail1" placeholder="" value="<?php echo "$keterangan_biaya"; ?>" disabled>
                   </div><br>
                                                        
                             
                             
                             
                <div class="form-group">
                  <label for="exampleInputEmail1">Admin Charge for All GO-MASSAGE Services (%)</label>
                
            <input name="id" type="hidden" value="<?php echo "$id"; ?>">
                        <input name="persentase" type="number" min="1" max="100" class="form-control" id="exampleInputEmail1" placeholder="" value="<?php echo "$persentase"; ?>">
                </div><br>
                
                
                            
                            <img style="width: 400px" src="<?php echo base_url() . "/foto_pijat/" . $layanan[0]['foto']; ?>" class="img-responsive img-thumbnail" alt=""><br>

                             <div class="form-group"><br>
                                    <label for="">Image :</label>
                                                <input type="hidden" name="id" value="<?php echo $layanan[0]['id']; ?>" >
                                
                                <input type="hidden" name="fotolama" value="<?php echo $layanan[0]['foto']; ?>" >
                                                <input name="userfile" type="file"  accept=".png, .jpg, .jpeg, .gif">
                                            </div>
                                            <br> <br><br>
                                            <input type="submit" class="btn btn-primary" value="SAVE">
                                        </form>
                                        
                                        <br><br>
                                    </div>
                                    
                                <!-- /.box-body -->
                            </div>
                            <!-- /.box -->
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->

           

         
         
        </div>
        <!-- ./wrapper -->

        <!-- jQuery 2.2.3 -->
        <script src="<?php echo base_url(); ?>plugins/jQuery/jquery-2.2.3.min.js"></script>
        <!-- Bootstrap 3.3.6 -->
        <script src="<?php echo base_url(); ?>bootstrap/js/bootstrap.min.js"></script>
        <!-- DataTables -->
        <script src="<?php echo base_url(); ?>plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="<?php echo base_url(); ?>plugins/datatables/dataTables.bootstrap.min.js"></script>
        <!-- SlimScroll -->
        <script src="<?php echo base_url(); ?>plugins/slimScroll/jquery.slimscroll.min.js"></script>
        <!-- FastClick -->
        <script src="<?php echo base_url(); ?>plugins/fastclick/fastclick.js"></script>
        <!-- AdminLTE App -->
        <script src="<?php echo base_url(); ?>dist/js/app.min.js"></script>
        <!-- AdminLTE for demo purposes -->
        <script src="<?php echo base_url(); ?>dist/js/demo.js"></script>
        <!-- page script -->
        <!-- Page script -->
        <script>
            $(function () {
                $("#example1").DataTable();
                $('#example2').DataTable({
                    "paging": true,
                    "lengthChange": false,
                    "searching": false,
                    "ordering": true,
                    "info": true,
                    "autoWidth": false
                });
            });
        </script>

    </body>
</html>
