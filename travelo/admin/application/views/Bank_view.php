<?php
defined('BASEPATH') OR exit('No direct script access allowed');require_once("../gtx_update.php");
?>
<!DOCTYPE html>
<html>
    <head>

<title><?php echo "$name_apps"; ?> - Bank</title>
 <meta content="template gotaxi" name="keywords">
 <meta content="go-taxi" name="author">
 <meta content="On Demand All in One App Services Android" name="description">
 <link rel="shortcut icon" href="/asset/images/favicon.png">
 <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
 <link rel="stylesheet" href="<?php echo base_url(); ?>bootstrap/css/bootstrap.min.css"> 
  <link rel="stylesheet" href="<?php echo $bootstrap; ?>">
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
 <link rel="stylesheet" href="<?php echo base_url(); ?>dist/css/skins/_all-skins.min.css">
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    
 <style type = "text/css">
	input[type=number]::-webkit-inner-spin-button, 
	input[type=number]::-webkit-outer-spin-button { 
	-webkit-appearance: none; 
	margin: 0; 
	}
	.label{display:inline;padding:.2em .6em .3em;font-size:75%;font-weight:700;line-height:1;color:#fff;text-align:center;white-space:nowrap;vertical-align:baseline;border-radius:.25em}a.label:focus,a.label:hover{color:#fff;text-decoration:none;cursor:pointer}.label:empty{display:none}.btn .label{position:relative;top:-1px}.label-default{background-color:#777}.label-default[href]:focus,.label-default[href]:hover{background-color:#5e5e5e}.label-primary{background-color:#337ab7}.label-primary[href]:focus,.label-primary[href]:hover{background-color:#286090}.label-success{background-color:#5cb85c}.label-success[href]:focus,.label-success[href]:hover{background-color:#449d44}.label-info{background-color:#5bc0de}.label-info[href]:focus,.label-info[href]:hover{background-color:#31b0d5}.label-warning{background-color:#f0ad4e}.label-warning[href]:focus,.label-warning[href]:hover{background-color:#ec971f}.label-danger{background-color:#d9534f}.label-danger[href]:focus,.label-danger[href]:hover{background-color:#c9302c}.
 </style>
 <link rel="stylesheet" href="/modules/css/animate.css"/>
 <script type="text/javascript" src="/modules/js/jquery-2.1.1.min.js"></script>
 <script type="text/javascript" src="/modules/js/jquery.countTo.js"></script>
 <script type="text/javascript" src="/modules/js/moment.min.js"></script>
 <script type="text/javascript" src="/modules/js/app.js"></script>
 </head>
 
 <body>
              
 <?php include 'header.php'; ?>  
        <div class="wrapper">
            <?php include 'SIDEBAR.php'; ?>
			
	<div class="main">
	
<div class="content with-top-banner">
	<div class="content-header no-mg-top">
	
		<i class="fa fa-university"></i>
		<div class="content-header-title">List Bank</div>
	
</div>
         <div class="panel">

                    <div class="row">
					
                        <div class="col-md-12">
						
                            <div class="content-box">
							 
                                <div class="box-header">
                                  <button class="btn btn-success" data-toggle="modal" data-target="#modalcreate" style="margin: 1%;"><span class="glyphicon glyphicon-plus"></span> Add Bank</button>
                                   
                                </div>
                                
                                
                                <!-- /.box-header -->
                                <div class="box-body">
								
                                    <?php echo $pesan; 		
									?>

                                    <table id="example1" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Name Bank</th>
												<th>Rekening No</th>
                                                <th>Icon</th>
												<th>Edit</th>
												<th>Delete</th>
                                                
                                              <!--  <th>Delete</th>-->
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $no = 1;
                                            foreach ($bank as $key) {
                                                ?>
                                                <tr>
														<td><?php echo $no;
                                            $no++; ?></td>
														<td><?php echo $key->banknama; ?></td>
														<td><?php echo $key->rekeningno; ?></td>
													<?php if ($key->bankimg != null) {?>
														<td><img src="/asset/bank/<?php echo $key->bankimg; ?>" style="max-width:60px;"></td>
                                                    <?php } else { ?>
														<td></td>
													<?php } ?>
                                    <td>
                                <button class="btn btn-warning" data-toggle="modal" data-target="#modal<?= $key->bankid; ?>"><span class="glyphicon glyphicon-pencil"></span></button>
                                    </td>
                                    <td>
                                    <a href="<?php echo base_url(); ?>index.php/Bank/hapusBank/<?php echo $key->bankid; ?>"><button type="button" class="btn btn-danger btn-sm" onclick="return confirm('Are you sure you want to remove the bank? ')"><span class="glyphicon glyphicon-remove"></span></button></a>
                                    </td>
                                                </tr>
                                               

                                                <!-- modal edit -->
                                                <div id="modal<?= $key->bankid; ?>" class="modal fade" tabindex="-1" role="dialog">
                                                    <div class="modal-dialog modal-sm" role="document" style="margin-top:20vh">
                                                        <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                            <h4 class="modal-title">Edit Bank</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="row">
                                                                <form action="<?php echo base_url(); ?>index.php/Bank/editBank/" method="post" enctype="multipart/form-data">
                                                                <div class="col-md-12">
                                                                    <p>Bank Name</p>
                                                                    <input name="banknama" type="text" value="<?= $key->banknama; ?>" maxlenght="20" class="form-control" width="100%" style="margin-top:-10px">
                                                                </div>
																<div class="col-md-12" style="margin-top:2vh">
                                                                    <p>Rekening No</p>
                                                                    <input name="rekeningno" type="number" value="<?= $key->rekeningno; ?>" maxlenght="20" class="form-control" width="100%" style="margin-top:-10px">
                                                                </div>

                                                                <div class="col-md-12" style="margin-top:2vh">
                                                                    <p>Image</p>
                                                                    <input name="bankimg" type="text" value="<?= $key->bankimg; ?>" maxlenght="100" class="form-control" width="100%" style="margin-top:-10px">
                                                                </div>

                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <input type="hidden" name="bankid" value="<?= $key->bankid;?>">
                                                            <button type="submit" class="btn btn-primary">Save</button>
                                                        </div>
                                                        </form>
                                                        </div><!-- /.modal-content -->
                                                    </div><!-- /.modal-dialog -->
                                                </div><!-- /.modal -->





                                                <!-- end modal -->
                                            <?php
                                            }
                                            ?>
											<div id="modalcreate" class="modal fade" tabindex="-1" role="dialog">
                                                    <div class="modal-dialog modal-sm" role="document" style="margin-top:20vh">
                                                        <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                            <h4 class="modal-title">Add Bank</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="row">
                                                                <form action="<?php echo base_url(); ?>index.php/Bank/addBank/" method="post" enctype="multipart/form-data">
                                                                <div class="col-md-12">
                                                                    <p>Bank Name</p>
                                                                    <input name="banknama" type="text" value="" maxlenght="20" class="form-control" width="100%" style="margin-top:-10px">
                                                                </div>
																<div class="col-md-12"style="margin-top:2vh">
                                                                    <p>Rekening No</p>
                                                                    <input name="rekeningno" type="number" value="" maxlenght="20" class="form-control" width="100%" style="margin-top:-10px">
                                                                </div>
                                                                
																<div class="col-md-12" style="margin-top:2vh">
                                                                    <p>Upload Icon</p>
                                                                    <input type="file" name="fileToUpload" id="fileToUpload">
                                                                </div>
                                                            </div>
                                                        </div>
														
                                                        <div class="modal-footer">
                                                            <input type="hidden" name="bankid" value="">
                                                            <button type="submit" class="btn btn-primary">Save</button>
                                                        </div>
                                                        </form>
														
                                                        </div><!-- /.modal-content -->
                                                    </div><!-- /.modal-dialog -->
                                                </div><!-- /.modal -->
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.box-body -->
                            </div>
                            <!-- /.box -->
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->
           
        </div>
        <!-- ./wrapper -->

       
        <!-- Bootstrap 3.3.6 -->
        <script src="<?php echo base_url(); ?>bootstrap/js/bootstrap.min.js"></script>
        <!-- DataTables -->
        <script src="<?php echo base_url(); ?>plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="<?php echo base_url(); ?>plugins/datatables/dataTables.bootstrap.min.js"></script>
        <!-- SlimScroll -->
        <script src="<?php echo base_url(); ?>plugins/slimScroll/jquery.slimscroll.min.js"></script>
        <!-- FastClick -->
        <script src="<?php echo base_url(); ?>plugins/fastclick/fastclick.js"></script>
        <!-- AdminLTE App -->
        <script src="<?php echo base_url(); ?>dist/js/app.min.js"></script>
        <!-- AdminLTE for demo purposes -->
        <script src="<?php echo base_url(); ?>dist/js/demo.js"></script>
        <!-- page script -->
        <script>
            $(function () {
                $("#example1").DataTable();
                $('#example2').DataTable({
                    "paging": true,
                    "lengthChange": false,
                    "searching": false,
                    "ordering": true,
                    "info": true,
                    "autoWidth": false
                });
            });
        </script>
    </body>
</html>
