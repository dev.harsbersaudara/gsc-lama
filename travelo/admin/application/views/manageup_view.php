<?php
/*==============================================================================
	Item Name: GoTaxi - On Demand All in One App Services Android
	Version: 1.0.4
	Author: Androgo Design
	Author URL: https://codecanyon.net/item/gotaxi-on-demand-all-in-one-app-services-android/22612350
	Attention: Don't modify this FILE because it will error the script.
================================================================================
*/
defined('BASEPATH') OR exit('No direct script access allowed');require_once("../gtx_config.php");require_once("../gtx_settings.php");require_once("../gtx_ver.php");

$version_notifications_array=ausGetVersion($version_number);
        if ($version_notifications_array['notification_data']['version_number']>$PRODUCT_VERSION) 
        {
            $ver_page_message="$PRODUCT_NAME version $latest_product_version available! See the full <a href='$PRODUCT_CHANGELOG_PAGE' target='_blank'>changelog</a> and update now for new features, bug fixes, and performance improvements! <br><br><b>Version number</b>: ".$version_notifications_array['notification_data']['version_number']."<br><b>Version release date</b>: ".$version_notifications_array['notification_data']['version_date'];
            $page_sumbit = "Attention: This update will overwrite the previous <b>File</b> script. Please click the 'Update Now' button to continue.<br><br><button class='btn btn-sm btn-primary' name='submit_ok'><i class='fa fa-check'></i> Update Now</button><br><br>";
     
     $demo_page_class="alert alert-warning";
 }
  else 
  {
  $ver_page_message="You have the latest $PRODUCT_NAME version installed! Once a new version is available, update option will appear below.";
    $page_sumbit = "Latest version is not yet available!<br><br>";

  $demo_page_class="alert alert-info alert-with-icon";
  }
  
 $update_notifications_array=aplVerifyUpdates($GLOBALS["mysqli"]);
 if ($update_notifications_array['notification_case']!="notification_license_ok") {
   echo "Your $PRODUCT_NAME license is not eligible for updates because of this reason: ".$update_notifications_array['notification_text'];
    exit();
 }

$page_title="Software Updates";
$page_message_instaled="$PRODUCT_NAME updated from version $PRODUCT_VERSION<br>If you find this update useful, <a href='$PRODUCT_REVIEW_PAGE' target='_blank'><b>let us know</b></a>! Your positive rating will help us to deliver even more free updates to you!";

if (!empty($_POST) && is_array($_POST))
    {
    extract(array_map("trim", $_POST), EXTR_SKIP); 
    }
    

if (isset($submit_ok))
 {
  
   $download_notifications_array=ausDownloadFile();
   $version_notifications_array=ausGetVersion($version_number); 
 
   if ($download_notifications_array['notification_case']=="notification_operation_ok")
    {
        
    if ($version_notifications_array['notification_case']=="notification_operation_ok") 
        {
        $ver_page_message=$version_notifications_array['notification_data']['product_title']." version information parsed successfully and is displayed below:<br><br><b>Version number</b>: ".$version_notifications_array['notification_data']['version_number']."<br><b>Version release date</b>: ".$version_notifications_array['notification_data']['version_date'];
        $demo_page_class="alert alert-success";
        }
    else 
        {
        $ver_page_message="Version $version_number information could not be parsed because of this reason: ".$version_notifications_array['notification_text'];
        $demo_page_class="alert alert-danger";
        }
    }
    else 
    {
    echo "Your installation could not be updated because of this reason: ".$download_notifications_array['notification_text'];
    }
   
 }    

?>

<!DOCTYPE html>
<html>
<head>

 <title><?php echo "$name_apps"; ?> - Software Updates</title>
 <meta content="template gotaxi" name="keywords">
 <meta content="go-taxi" name="author">
 <meta content="On Demand All in One App Services Android" name="description">
 <link rel="shortcut icon" href="/asset/images/favicon.png">
 <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
 <link rel="stylesheet" href="<?php echo base_url(); ?>bootstrap/css/bootstrap.min.css"> 
  <link rel="stylesheet" href="<?php echo $bootstrap; ?>">
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
 <link rel="stylesheet" href="<?php echo base_url(); ?>dist/css/skins/_all-skins.min.css">
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    
 <style type = "text/css">
 .label{display:inline;padding:.2em .6em .3em;font-size:75%;font-weight:700;line-height:1;color:#fff;text-align:center;white-space:nowrap;vertical-align:baseline;border-radius:.25em}a.label:focus,a.label:hover{color:#fff;text-decoration:none;cursor:pointer}.label:empty{display:none}.btn .label{position:relative;top:-1px}.label-default{background-color:#777}.label-default[href]:focus,.label-default[href]:hover{background-color:#5e5e5e}.label-primary{background-color:#337ab7}.label-primary[href]:focus,.label-primary[href]:hover{background-color:#286090}.label-success{background-color:#5cb85c}.label-success[href]:focus,.label-success[href]:hover{background-color:#449d44}.label-info{background-color:#5bc0de}.label-info[href]:focus,.label-info[href]:hover{background-color:#31b0d5}.label-warning{background-color:#f0ad4e}.label-warning[href]:focus,.label-warning[href]:hover{background-color:#ec971f}.label-danger{background-color:#d9534f}.label-danger[href]:focus,.label-danger[href]:hover{background-color:#c9302c}.
 </style>
 <link rel="stylesheet" href="/modules/css/animate.css"/>
 <script type="text/javascript" src="/modules/js/jquery-2.1.1.min.js"></script>
 <script type="text/javascript" src="/modules/js/jquery.countTo.js"></script>
 <script type="text/javascript" src="/modules/js/moment.min.js"></script>
 <script type="text/javascript" src="/modules/js/app.js"></script>
 <link href="/asset/css/now-ui-dashboard.min.css" rel="stylesheet" />
 
  <link href="/asset/css/style.min.css" rel="stylesheet">

 </head>
 <body>
        
        
<?php include 'header.php'; ?>
  <div class="wrapper">
      <?php include 'SIDEBAR.php'; ?>
      
 
 <div class="main">
     <div class="panel">
         <div class="col-md-12">
            <div class="card">
             
              <div class="card-body">
             <div class="content-header no-mg-top">
             <div class="card-header">      
              <h4 class="card-title" style="font-size:22px"><i class="fa fa-cloud-download"></i><strong>
                  <?php echo $page_title; ?></strong></h4>
              </div>
              </div>
              
              
                <div class="<?php echo $demo_page_class; ?>" role="alert" style="font-size:17px"><strong><?php echo $ver_page_message; ?></strong></div>
                
                <?php if (!isset($submit_ok)) { ?><?php } ?>
              </div>
            </div>
            
            <div class="card">
              <form action="<?php echo base_url(); ?>index.php/Dashboard/update_software" method="post">
                  
            <div class="card-header">      
              <h4 class="card-title" style="font-size:15px">
                  <?php echo $ORIGINAL_PRODUCT_NAME; ?></h4>
              </div> 
              
              <?php if ($demo_page_class!="alert alert-success") { ?>
             
             <div class="col-md-12">
               <?php echo $page_sumbit; ?>
              </div>
              
             <div class="row"> 
              <div class="col-md-12">
                  
                  <?php } ?> <?php if ($demo_page_class=="alert alert-success") { ?>
      
               <div class="h4">Parsing Version Information... Completed!</div>
                
                <div class="progress">
                  <div class="progress-bar progress-bar-striped bg-success" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width:100%"></div>
                </div>
                <br><br>
                 
                 
                  <p class="lead"><pre><?php print_r($version_notifications_array['notification_data']); ?></pre></p>
                  
                  <h4 class="card-title"><?php echo $page_message_instaled ?></h4>
                  
                 <h4 class="card-title"><a class="btn btn-sm btn-primary" href="<?php echo $PRODUCT_REVIEW_PAGE; ?>" target="_blank" role="button">RATE NOW</a></h4>
                 
             
             </div> 
        </div>
        
              <?php } ?>
              </form>
            </div>
         
        </div>
      </div>
 
 
 </div>

        <script src="<?php echo base_url(); ?>plugins/jQuery/jquery-2.2.3.min.js"></script>
        <script src="<?php echo base_url(); ?>bootstrap/js/bootstrap.min.js"></script>
        <!-- Select2 -->
        <script src="<?php echo base_url(); ?>plugins/select2/select2.full.min.js"></script>
        <!-- InputMask -->
        <script src="<?php echo base_url(); ?>plugins/input-mask/jquery.inputmask.js"></script>
        <script src="<?php echo base_url(); ?>plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
        <script src="<?php echo base_url(); ?>plugins/input-mask/jquery.inputmask.extensions.js"></script>
        <!-- date-range-picker -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
        <script src="<?php echo base_url(); ?>plugins/daterangepicker/daterangepicker.js"></script>
        <!-- bootstrap datepicker -->
        <script src="<?php echo base_url(); ?>plugins/datepicker/bootstrap-datepicker.js"></script>
        <!-- bootstrap color picker -->
        <script src="<?php echo base_url(); ?>plugins/colorpicker/bootstrap-colorpicker.min.js"></script>
        <!-- bootstrap time picker -->
        <script src="<?php echo base_url(); ?>plugins/timepicker/bootstrap-timepicker.min.js"></script>
        <!-- SlimScroll 1.3.0 -->
        <script src="<?php echo base_url(); ?>plugins/slimScroll/jquery.slimscroll.min.js"></script>
        <!-- iCheck 1.0.1 -->
        <script src="<?php echo base_url(); ?>plugins/iCheck/icheck.min.js"></script>
        <!-- FastClick -->
        <script src="<?php echo base_url(); ?>plugins/fastclick/fastclick.js"></script>
        <!-- AdminLTE App -->
        <script src="<?php echo base_url(); ?>dist/js/app.min.js"></script>
        <!-- AdminLTE for demo purposes -->
        <script src="<?php echo base_url(); ?>dist/js/demo.js"></script>
        <!-- Page script -->
        <script>
            $(function () {
                //Datemask dd/mm/yyyy
                $("#datemask").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
                //Datemask2 mm/dd/yyyy
                $("#datemask2").inputmask("mm/dd/yyyy", {"placeholder": "mm/dd/yyyy"});
                //Money Euro
                $("[data-mask]").inputmask();
            });
        </script>

    </body>
</html>