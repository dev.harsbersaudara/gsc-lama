<aside class="side-nav">

		<div class="user-side-profile">
			<div class="user-image">
				<div class="user-on"></div>
				<img alt="gotaxi" src="/asset/images/photo_male.jpg">
			</div>

			<div class="clear">
				<div class="user-name"><?php echo "$useradmin"; ?></div>
						<div class="user-group">Administrator</div>
						<ul class="user-side-menu animated bounceInUp">
					<li><a href="<?php echo base_url(); ?>index.php/manageadmin/nameprofile">Profile <div class="badge badge-red pull-right">new</div></a></li>
					<li><a href="<?php echo base_url(); ?>index.php/manageadmin/nameapps">Name Apps<div class="badge badge-red pull-right">new</div></a></li>
					<li><a href="<?php echo base_url(); ?>index.php/manageadmin">Change Password</a></li>
					<li><a href="<?php echo base_url(); ?>index.php/signout">Logout</a></li>
				</ul>
			</div>
		</div>


<!--- Print parent menu-->
<div class="main-menu-title">MAIN NAVIGATION</div>
		  <div class="main-menu">
			<ul>
			   <!--<li class="active">-->
				<li class="">
					<a href="<?php echo base_url(); ?>index.php/dashboard">
						<i class="fa fa-bars"></i>
						<span>Dashboard</span>
					</a>
				</li>
				<!-- Print parent menu -->
	           <li class="">
			    <a href="">
					 <i class="fa fa-car"></i>
				        <span>Approve Driver</span>

						 <div class="badge badge-red pull-right">4</div>

						 </a>
			      <ul class="treeview-menu">
                    <li><a href="<?php echo base_url(); ?>index.php/validatedriver/driverMotor"><i class="fa fa-motorcycle"></i> Go-Moto</a></li>

                    <li><a href="<?php echo base_url(); ?>index.php/validatedriver/mcar"><i class="fa fa-car"></i> Go-Cab</a></li>
                    <li><a href="<?php echo base_url(); ?>index.php/validatedriver/mbox"><i class="fa fa-truck"></i> Go-Box</a></li>

                    <li><a href="<?php echo base_url(); ?>index.php/validatedriver/mservice"><i class="fa fa-wrench"></i> Go-Service</a></li>

                   </ul>

	             </li>


	             <?php if ($this->session->userdata('level') == 1) { ?>
	             		<!-- Print parent menu -->
	             		 <li class="">
							    <a href="">
								 <i class="fa fa-user-plus"></i>
						        <span>Approve Partner</span>

								   <div class="badge badge-red pull-right">2</div>

								 </a>
					      <ul class="treeview-menu">

		                    <li><a href="<?php echo base_url(); ?>index.php/validatedriver/mmassage"><i class="fa fa-user-md"></i> Go-Massage</a></li>

		                    <li><a href="<?php echo base_url(); ?>index.php/validatedriver/mfood"><i class="fa fa-cutlery"></i> Restaurant</a></li>
		                   </ul>
			            </li>
	           <?php  } ?>
	             

	           	 <?php if ($this->session->userdata('level') == 1) { ?>

	             <li>
	                 <a href="<?php echo base_url(); ?>index.php/Listpelanggan">
	                     <i class="active fa fa-child"></i> <span>List Customer</span></a>
	             </li>

	             <?php  } ?>

	        <?php if ($this->session->userdata('level') == 1) { ?>     

             <li>
                <a href="<?php echo base_url(); ?>index.php/promotion">
                    <i class="active fa fa-paper-plane"></i> <span> App main page Promotion </span>
                </a>
            </li>
			<li>
                <a href="<?php echo base_url(); ?>index.php/slider">
                    <i class="active fa fa-picture-o"></i> <span> App Slider </span>
                </a>
            </li>
            <li>
                <a href="<?php echo base_url(); ?>index.php/helpcenter">
                    <i class="active fa fa-question-circle"></i> <span> Help Center</span>
                </a>
            </li>
             <?php  } ?>

		  </ul>
		</div>

		<?php if ($this->session->userdata('level') == 1) { ?>
		<div class="main-menu-title">MAIN SERVICES</div>
		  <div class="main-menu">
			<ul>
			 <li class="">
			     <a href="">
						 <i class="fa fa-motorcycle"></i>
				        <span>Moto</span>

				    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
				 </a>
				<ul class="treeview-menu">

			    <li><a href="<?php echo base_url(); ?>index.php/setcost/mride/Go-Moto"><i class="fa fa-motorcycle"></i> Setting Price Go-Moto</a></li>

				<li><a href="<?php echo base_url(); ?>index.php/setcost/mfood/Go-Food"><i class="fa fa-cutlery"></i> Setting Price Go-Food</a></li>

			    <li><a href="<?php echo base_url(); ?>index.php/setcost/msend/Go-Send"><i class="fa fa-archive"></i>  Setting Price Go-Send</a></li>

                <li><a href="<?php echo base_url(); ?>index.php/setcost/mmart/Go-Mart"><i class="fa fa-shopping-basket"></i>  Setting Price Mart</a></li>

			    <li><a href="<?php echo base_url(); ?>index.php/listdriver/driverMotor"><i class="fa fa-motorcycle"></i> List Driver</a></li>

			    <li><a href="<?php echo base_url(); ?>index.php/listdriver/driverMotorNonActive"><i class="fa fa-motorcycle"></i> List Driver Non Active</a></li>

			    <li><a href="<?php echo base_url(); ?>index.php/atribut"><i class="fa fa-gift"></i> Atribut</a></li>

				<li><a href="<?php echo base_url(); ?>index.php/beli"><i class="fa fa-gift"></i> Pembelian Atribut</a></li>

			   </ul>
            </li>

			<li class="">
			     <a href="">
					<i class="fa fa-taxi"></i>
				        <span>Cab</span>
					<span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>

						 </a>
					   <ul class="treeview-menu">

			    <li><a href="<?php echo base_url(); ?>index.php/setcost/mcar/Go-Cab"><i class="active fa fa-money"></i> Setting Price</a></li>

			    <li><a href="<?php echo base_url(); ?>index.php/listdriver/mcar"><i class="fa fa-car"></i> List Driver</a></li>
			    <li><a href="<?php echo base_url(); ?>index.php/listdriver/mcarnon"><i class="fa fa-car"></i> List Driver Non Active</a></li>

               </ul>
            </li>


            	<li class="">
			     <a href="">
					<i class="fa fa-wrench"></i>
				        <span>Service</span>
					<span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>

						 </a>
					   <ul class="treeview-menu">


             <li><a href="<?php echo base_url(); ?>index.php/setcost/mservice/Go-Service"><i class="active fa fa-money"></i> Setting Price</a></li>

            <li><a href="<?php echo base_url(); ?>index.php/listdriver/mservice"><i class="fa fa-wrench"></i> List Driver</a></li>

			   </ul>
            </li>




			 <li class="">
					    <a href="">
						 <i class="fa fa-user-md"></i>
				        <span>Massage</span>

						   <div class="badge badge-yellow pull-right">New V1.0.4</div>

						 </a>
			      <ul class="treeview-menu">


                   <li><a href="<?php echo base_url(); ?>index.php/listdriver/mmassage"><i class="fa fa-user-md"></i> List Partner</a></li>

                     <li><a href="<?php echo base_url(); ?>index.php/Massageservices"><i class="fa fa-user-md"></i> List Category</a></li>
                  </ul>
            </li>


		   <li class="">
					    <a href="">
						 <i class="fa fa-truck"></i>
				        <span>Box</span>

						   <div class="badge badge-green pull-right">New V1.0.4</div>

						 </a>
			      <ul class="treeview-menu">

                   <li><a href="<?php echo base_url(); ?>index.php/listdriver/mbox"><i class="fa fa-truck"></i> List Driver</a></li>

                     <li><a href="<?php echo base_url(); ?>index.php/boxservices"><i class="fa fa-truck"></i> List Category</a></li>
                  </ul>
            </li>

            <li class="">
					    <a href="">
						 <i class="fa fa-shopping-bag"></i>
				        <span>Grocery</span>

						   <div class="badge badge-green pull-right">New !</div>

						 </a>
			      <ul class="treeview-menu">

                   <li><a href="<?php echo base_url(); ?>index.php/grocery"><i class="fa fa-shopping-bag"></i> List Product</a></li>

                     <li><a href="<?php echo base_url(); ?>index.php/grocery"><i class="fa fa-shopping-bag"></i> List Category</a></li>
                  </ul>
            </li>


            <li class="">
					    <a href="">
						 <i class="fa fa-shopping-bag"></i>
				        <span>Insurance</span>

						   <div class="badge badge-green pull-right">New !</div>

						 </a>
			      <ul class="treeview-menu">

                   <li><a href="<?php echo base_url(); ?>index.php/grocery"><i class="fa fa-shopping-bag"></i> List Driver</a></li>

                     <li><a href="<?php echo base_url(); ?>index.php/grocery"><i class="fa fa-shopping-bag"></i> List Partner Insurance</a></li>
                  </ul>
            </li>


		  </ul>
		</div>

		 <?php  } ?>

		 <?php if ($this->session->userdata('level') == 1) { ?>
		<div class="main-menu-title">PARTNER RESTAURANTS</div>
		<div class="main-menu">
			<ul>
				 <!-- Print parent menu -->


	        <li>
                <a href="<?php echo base_url(); ?>index.php/listmitramfood">
                    <i class="fa fa-cutlery"></i> <span> List Restaurant</span>
                </a>
            </li>
            <li>
                <a href="<?php echo base_url(); ?>index.php/kategoriresto">
                    <i class="fa fa-cart-arrow-down"></i> <span> Restaurant Category</span>
                </a>
            </li>
            <li>
                <a href="<?php echo base_url(); ?>index.php/bannermfood">
                    <i class="fa fa-shopping-bag"></i> <span> Restaurant Promotion</span>
                </a>
            </li>
            <li>
				<a href="<?php echo base_url(); ?>index.php/setcost/mfood/Go-Food">
					<i class="active fa fa-money"></i> Setting Price Go-Food
				</a>
			</li>
         </ul>
	</div>
<?php } ?>



<!--- print menu utama -->

	<div class="main-menu-title">FINANCE</div>
		  <div class="main-menu">
			<ul>

			     <li>
                <a href="<?php echo base_url(); ?>index.php/Withdraw">
                    <i class="active fa fa-share-square-o"></i> <span> Driver Withdraw</span>
                </a>
            </li>
			    <li class="">
                <a href="#">
                    <i class="fa fa-group"></i>
                    <span>Manual Transaction</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo base_url(); ?>index.php/Manualtransaction/pelanggan"><i class="active fa fa-child"></i> Customers</a></li>
                    <li><a href="<?php echo base_url(); ?>index.php/Manualtransaction/driver"><i class="fa fa-car"></i> Driver</a></li>

                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-dollar"></i>
                    <span>Wallet Topup</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo base_url(); ?>index.php/usertopup"><i class="active fa fa-child"></i> Customer TopUp </a></li>
                    <li><a href="<?php echo base_url(); ?>index.php/drivertopup"><i class="fa fa-motorcycle"></i> Driver TopUp </a></li>
                </ul>
            </li>
			<li>
                <a href="<?php echo base_url(); ?>index.php/Bank">
                    <i class="fa fa-university"></i> <span>Bank</span>
                </a>
            </li>


		  </ul>
		</div>


<!--- print menu utama -->

		<div class="main-menu-title">VOUCHER</div>
		<div class="main-menu">
			<ul>

		  	<li>
                <a href="<?php echo base_url(); ?>index.php/Voucher">
                    <i class="fa fa-gift"></i> <span> Voucher Active</span>
                </a>
            </li>

		  </ul>
		</div>


<!-- pirnt menu utama -->

</aside>