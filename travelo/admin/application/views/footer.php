<footer class="footer">
    <div class="breadcrumb">
      <div class="pull-right hidden-xs">
                    <b>Version</b> <?php echo $PRODUCT_VERSION ?>
        </div>
                
         <strong>Copyright &copy; 2017 - <?php echo date("Y"); ?> | <a href="<?php echo $ORIGINAL_PRODUCT_URL; ?>" target='_blank'><?php echo $ORIGINAL_PRODUCT_NAME; ?></a>.</strong> All Rights Reserved.</strong> 
    </div>

</footer>