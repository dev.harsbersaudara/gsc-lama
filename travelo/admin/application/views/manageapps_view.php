<?php
defined('BASEPATH') OR exit('No direct script access allowed');require_once("../gtx_settings.php");
?>
<!DOCTYPE html>
<html>
<head>

 <title><?php echo "$name_apps"; ?> - Manage Name Apps</title>

 <meta content="template gotaxi" name="keywords">
 <meta content="go-taxi" name="author">
 <meta content="On Demand All in One App Services Android" name="description">
 <link rel="shortcut icon" href="/asset/images/favicon.png">
 <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
 <link rel="stylesheet" href="<?php echo base_url(); ?>bootstrap/css/bootstrap.min.css"> 
  <link rel="stylesheet" href="<?php echo $bootstrap; ?>">
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
 <link rel="stylesheet" href="<?php echo base_url(); ?>dist/css/skins/_all-skins.min.css">
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    
 <style type = "text/css">
 
 .label{display:inline;padding:.2em .6em .3em;font-size:75%;font-weight:700;line-height:1;color:#fff;text-align:center;white-space:nowrap;vertical-align:baseline;border-radius:.25em}a.label:focus,a.label:hover{color:#fff;text-decoration:none;cursor:pointer}.label:empty{display:none}.btn .label{position:relative;top:-1px}.label-default{background-color:#777}.label-default[href]:focus,.label-default[href]:hover{background-color:#5e5e5e}.label-primary{background-color:#337ab7}.label-primary[href]:focus,.label-primary[href]:hover{background-color:#286090}.label-success{background-color:#5cb85c}.label-success[href]:focus,.label-success[href]:hover{background-color:#449d44}.label-info{background-color:#5bc0de}.label-info[href]:focus,.label-info[href]:hover{background-color:#31b0d5}.label-warning{background-color:#f0ad4e}.label-warning[href]:focus,.label-warning[href]:hover{background-color:#ec971f}.label-danger{background-color:#d9534f}.label-danger[href]:focus,.label-danger[href]:hover{background-color:#c9302c}.
 </style>
 <link rel="stylesheet" href="/modules/css/animate.css"/>
 <script type="text/javascript" src="/modules/js/jquery-2.1.1.min.js"></script>
 <script type="text/javascript" src="/modules/js/jquery.countTo.js"></script>
 <script type="text/javascript" src="/modules/js/moment.min.js"></script>
 <script type="text/javascript" src="/modules/js/app.js"></script>
 </head>
 
 <body>
        
        
  <?php include 'header.php'; ?> 
 
        <div class="wrapper">
            <!--HEADER dan SIDEBAR include-->
            <?php include 'SIDEBAR.php'; ?>

	<div class="main">
		<div class="breadcrumb">


<div class="content with-top-banner">
	<div class="content-header no-mg-top">
		<i class="fa fa-android"></i>
		<div class="content-header-title">Setting Name Apps</div>
		
	</div>

	<div class="panel">

      <div class="row">
           <div class="col-md-12">
                <div class="content-box">
                    <div class="box-header">
       
                     </div>
                                <!-- /.box-header -->
              <div class="box-body">
                    <div class="row">
                            <div class="col-md-2"></div>
                                <div class="col-md-8">
                                            <!-- form start -->
                                            <?php echo "$message"; ?>
                                            
                    <form role="form" action="<?php echo base_url(); ?>index.php/manageadmin/checkname" method="post">
                    
                    <div class="box-body">
                        <div class="form-group">
                        <label for="exampleInputEmail1">ID</label>
                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="" value="<?php echo "$nik"; ?>" disabled>
                                                    </div>
                            </div>
            
            
        <div class="box-body">
            <div class="form-group">
              <label for="exampleInputEmail1">Name Apps</label>
             <input name="namalama" type="text" class="form-control" id="exampleInputEmail1" placeholder="" value="<?php echo "$name_apps"; ?>">
                    </div>
                </div>
                        



        <div class="box-footer">
            <button name="btnSubmit" type="submit" class="btn btn-primary">SAVE DATA</button>
            </div>
     </form><br><br>

                                        </div>
                                        <!-- /.col -->
                                    </div>
                                </div>
                                <!-- /.box-body -->
                            </div>
                            <!-- /.box -->
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->
           
           

           
        </div>
        <!-- ./wrapper -->

        <!-- jQuery 2.2.3 -->
        <script src="<?php echo base_url(); ?>plugins/jQuery/jquery-2.2.3.min.js"></script>
        <!-- Bootstrap 3.3.6 -->
        <script src="<?php echo base_url(); ?>bootstrap/js/bootstrap.min.js"></script>
        <!-- Select2 -->
        <script src="<?php echo base_url(); ?>plugins/select2/select2.full.min.js"></script>
        <!-- InputMask -->
        <script src="<?php echo base_url(); ?>plugins/input-mask/jquery.inputmask.js"></script>
        <script src="<?php echo base_url(); ?>plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
        <script src="<?php echo base_url(); ?>plugins/input-mask/jquery.inputmask.extensions.js"></script>
        <!-- date-range-picker -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
        <script src="<?php echo base_url(); ?>plugins/daterangepicker/daterangepicker.js"></script>
        <!-- bootstrap datepicker -->
        <script src="<?php echo base_url(); ?>plugins/datepicker/bootstrap-datepicker.js"></script>
        <!-- bootstrap color picker -->
        <script src="<?php echo base_url(); ?>plugins/colorpicker/bootstrap-colorpicker.min.js"></script>
        <!-- bootstrap time picker -->
        <script src="<?php echo base_url(); ?>plugins/timepicker/bootstrap-timepicker.min.js"></script>
        <!-- SlimScroll 1.3.0 -->
        <script src="<?php echo base_url(); ?>plugins/slimScroll/jquery.slimscroll.min.js"></script>
        <!-- iCheck 1.0.1 -->
        <script src="<?php echo base_url(); ?>plugins/iCheck/icheck.min.js"></script>
        <!-- FastClick -->
        <script src="<?php echo base_url(); ?>plugins/fastclick/fastclick.js"></script>
        <!-- AdminLTE App -->
        <script src="<?php echo base_url(); ?>dist/js/app.min.js"></script>
        <!-- AdminLTE for demo purposes -->
        <script src="<?php echo base_url(); ?>dist/js/demo.js"></script>
        <!-- Page script -->
        <script>
            $(function () {
                //Datemask dd/mm/yyyy
                $("#datemask").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
                //Datemask2 mm/dd/yyyy
                $("#datemask2").inputmask("mm/dd/yyyy", {"placeholder": "mm/dd/yyyy"});
                //Money Euro
                $("[data-mask]").inputmask();
            });
        </script>

    </body>
</html>
