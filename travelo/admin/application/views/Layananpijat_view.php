<?php
defined('BASEPATH') OR exit('No direct script access allowed');require_once("../gtx_settings.php");
?>
<!DOCTYPE html>
<html>
    <head>
  <!-- Required meta tags -->
 <title><?php echo "$name_apps"; ?></title>
 <meta content="template gotaxi" name="keywords">
 <meta content="go-taxi" name="author">
 <meta content="On Demand All in One App Services Android" name="description">
 <link rel="shortcut icon" href="/asset/images/favicon.png">
 <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
 <link rel="stylesheet" href="<?php echo base_url(); ?>bootstrap/css/bootstrap.min.css"> 
  <link rel="stylesheet" href="<?php echo $bootstrap; ?>">
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
 <link rel="stylesheet" href="<?php echo base_url(); ?>dist/css/skins/_all-skins.min.css">
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    
 <style type = "text/css">
 
 .label{display:inline;padding:.2em .6em .3em;font-size:75%;font-weight:700;line-height:1;color:#fff;text-align:center;white-space:nowrap;vertical-align:baseline;border-radius:.25em}a.label:focus,a.label:hover{color:#fff;text-decoration:none;cursor:pointer}.label:empty{display:none}.btn .label{position:relative;top:-1px}.label-default{background-color:#777}.label-default[href]:focus,.label-default[href]:hover{background-color:#5e5e5e}.label-primary{background-color:#337ab7}.label-primary[href]:focus,.label-primary[href]:hover{background-color:#286090}.label-success{background-color:#5cb85c}.label-success[href]:focus,.label-success[href]:hover{background-color:#449d44}.label-info{background-color:#5bc0de}.label-info[href]:focus,.label-info[href]:hover{background-color:#31b0d5}.label-warning{background-color:#f0ad4e}.label-warning[href]:focus,.label-warning[href]:hover{background-color:#ec971f}.label-danger{background-color:#d9534f}.label-danger[href]:focus,.label-danger[href]:hover{background-color:#c9302c}.
 </style>
 <link rel="stylesheet" href="/modules/css/animate.css"/>
 <script type="text/javascript" src="/modules/js/jquery-2.1.1.min.js"></script>
 <script type="text/javascript" src="/modules/js/jquery.countTo.js"></script>
 <script type="text/javascript" src="/modules/js/moment.min.js"></script>
 <script type="text/javascript" src="/modules/js/app.js"></script>
 </head>
 
 <body>

 <?php include 'header.php'; ?>  

 <div class="wrapper">
 <?php include 'SIDEBAR.php'; ?>
  <div class="main">

   <div class="content-wrapper">
    <div class="content with-top-banner">
	<div class="content-header no-mg-top">
		<i class="fa fa-user-plus"></i>
		<div class="content-header-title">Add Massage Service</div>
		
	</div>

    <div class="panel">  
               <div class="row">
                        <div class="col-md-12">
                            <div class="content-box">
                                <div class="box-header">
        
                                </div>
 
    <form action="<?php echo base_url(); ?>index.php/Massageservices/insertLayanan" method="POST" enctype="multipart/form-data">
        
        <div class="box-body">
                <div class="row">    
                        
					<div class="col-md-4">
                        <div class="input-group">
                          
                          <label for="exampleInputEmail1">Service Name </label>
                          
                          <input name="layanan" type="text" class="form-control" id="exampleInputEmail1" placeholder="Service.." value="">
                        </div>
                    </div>
                      
                      
					<div class="col-md-4">
                        <div class="input-group">
                           <label for="sel1">Service Price</label>
                           
                          <input name="harga" type="text" class="form-control" id="exampleInputEmail1" placeholder="Price.." value="">
                        </div>
                    </div>
                      
                      
					<div class="col-md-4">
                        <div class="input-group">
                          <label for="">Photo Service :</label>
                          <input type="file" name="userfile" class="form-control" placeholder="Username" aria-label="Username" aria-describedby="colored-addon1">
                        </div>
                    </div>
                    
			    </div>
			    
			 </div>   
					  
					  
                      <br><br>
            <div class="box-footer">
                 <button type="submit" class="btn btn-primary">Add Now</button>
            </div>
            
            <br><br>
            
    </form>
                                    
                     </div>
                          
                 </div>
                       
            </div>           
                  
        </div>      
        
   </div>   
      
      
      
      
      
      
      
     <div class="content with-top-banner">
	<div class="content-header no-mg-top">
		<i class="fa fa-user-md"></i>
		<div class="content-header-title">List of Go-Massage Services</div>
		
	</div>
	

  
        
    <div class="panel">       
        
          <div class="row">
                        <div class="col-md-12">
                             <div class="content-box">
                                
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <?php echo $pesan; ?>

                                    <table id="example1" class="table table-bordered table-striped">
                                       <thead>
                        <tr>
                                                    <th>Service</th>
													<th>Price</th>
                                                    <th>Image</th>
                                                    <th>Action</th>
                                                </tr>
                      </thead>
                                        <tbody>
                                            
                        <?php
                                                foreach ($layananpijat as $key) {
                                                    ?>
                                                    <tr>
                                                        <td><?php echo $key->layanan; ?></td>
														<td><?php echo $key->harga; ?></td>
                                                        <td><img style="width: 400px" src="<?php echo base_url() . "foto_pijat/" . $key->foto; ?>" class="img-responsive img-thumbnail" alt="Cinque Terre"></td>
                                                        <td>
                                                            <a href="<?php echo base_url(); ?>index.php/Massageservices/editLayananForm/<?php echo $key->id; ?>"><button type="button" class="btn btn-primary btn-fw">Edit</button></a>
															<a href="<?php echo base_url();?>index.php/Massageservices/hapusLayanan/<?php echo $key->id; ?>/<?php echo $key->foto; ?>">
                                                                        <button type="button" class="btn btn-danger btn-sm" onclick="return confirm('Are you sure ?')">
                                                                            Delete
                                            </button>
                                                                    </a>
                                                        </td>
                                                    </tr>
													
                                                    <?php
                                                }
                                                ?>

                                            </tbody>
                      
                           </table>
                                </div>
                                <!-- /.box-body -->
                            </div>
                            <!-- /.box -->
                        </div>
                        <!-- /.col -->
                    </div>
       </div>              
    </div>                
                    
                    
                    
        
        
        
                  
                
       <!-- /.content -->
            </div>
            <?php    include 'application/views/footer.php'?>
     </div>        
     <!-- /.content-wrapper -->

</div>


            


        
        <!-- ./wrapper -->

      <!-- jQuery 2.2.3 -->
        <script src="<?php echo base_url(); ?>plugins/jQuery/jquery-2.2.3.min.js"></script>
        <!-- Bootstrap 3.3.6 -->
        <script src="<?php echo base_url(); ?>bootstrap/js/bootstrap.min.js"></script>
        <!-- DataTables -->
        <script src="<?php echo base_url(); ?>plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="<?php echo base_url(); ?>plugins/datatables/dataTables.bootstrap.min.js"></script>
        <!-- SlimScroll -->
        <script src="<?php echo base_url(); ?>plugins/slimScroll/jquery.slimscroll.min.js"></script>
        <!-- FastClick -->
        <script src="<?php echo base_url(); ?>plugins/fastclick/fastclick.js"></script>
        <!-- AdminLTE App -->
        <script src="<?php echo base_url(); ?>dist/js/app.min.js"></script>
        <!-- AdminLTE for demo purposes -->
        <script src="<?php echo base_url(); ?>dist/js/demo.js"></script>
        <!-- page script -->
        <script>
            $(function () {
                $("#example1").DataTable();
                $('#example2').DataTable({
                    "paging": true,
                    "lengthChange": false,
                    "searching": false,
                    "ordering": true,
                    "info": true,
                    "autoWidth": false
                });
            });
        </script>
    </body>
</html>