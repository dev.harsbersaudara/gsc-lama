<?php
defined('BASEPATH') OR exit('No direct script access allowed');require_once("../gtx_settings.php");
?>
<!DOCTYPE html>
<html>
    <head>

 <title><?php echo "$name_apps"; ?></title>
 <meta content="template gotaxi" name="keywords">
 <meta content="go-taxi" name="author">
 <meta content="On Demand All in One App Services Android" name="description">
 <link rel="shortcut icon" href="/asset/images/favicon.png">
 <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
 <link rel="stylesheet" href="<?php echo base_url(); ?>bootstrap/css/bootstrap.min.css"> 
  <link rel="stylesheet" href="<?php echo $bootstrap; ?>">
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
 <link rel="stylesheet" href="<?php echo base_url(); ?>dist/css/skins/_all-skins.min.css">
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    
 <style type = "text/css">
 
 .label{display:inline;padding:.2em .6em .3em;font-size:75%;font-weight:700;line-height:1;color:#fff;text-align:center;white-space:nowrap;vertical-align:baseline;border-radius:.25em}a.label:focus,a.label:hover{color:#fff;text-decoration:none;cursor:pointer}.label:empty{display:none}.btn .label{position:relative;top:-1px}.label-default{background-color:#777}.label-default[href]:focus,.label-default[href]:hover{background-color:#5e5e5e}.label-primary{background-color:#337ab7}.label-primary[href]:focus,.label-primary[href]:hover{background-color:#286090}.label-success{background-color:#5cb85c}.label-success[href]:focus,.label-success[href]:hover{background-color:#449d44}.label-info{background-color:#5bc0de}.label-info[href]:focus,.label-info[href]:hover{background-color:#31b0d5}.label-warning{background-color:#f0ad4e}.label-warning[href]:focus,.label-warning[href]:hover{background-color:#ec971f}.label-danger{background-color:#d9534f}.label-danger[href]:focus,.label-danger[href]:hover{background-color:#c9302c}.
 </style>
 <link rel="stylesheet" href="/modules/css/animate.css"/>
 <script type="text/javascript" src="/modules/js/jquery-2.1.1.min.js"></script>
 <script type="text/javascript" src="/modules/js/jquery.countTo.js"></script>
 <script type="text/javascript" src="/modules/js/moment.min.js"></script>
 <script type="text/javascript" src="/modules/js/app.js"></script>
 </head>
 
 <body>
        
 <?php include 'header.php'; ?>  
        <div class="wrapper">
            <?php include 'SIDEBAR.php'; ?>

<div class="main">
    <div class="content with-top-banner">
	<div class="content-header no-mg-top">
		<i class="fa fa-newspaper-o"></i>
	<div class="content-header-title">Add <?php echo $tittle; ?></div>
	
</div>
     <div class="panel">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="content-box">
                                <div class="box-header">
                                    <h3 class="box-title">Add <?php echo $tittle; ?></h3>
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <?php echo $pesan; ?>
                                    <form role="form" action="<?php echo base_url(); ?>index.php/Voucher/insert<?php echo $insert; ?>" method="post">
                                        <div class="box-body">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Voucher Name</label>
                                                <input name="nama" type="text" class="form-control" id="exampleInputEmail1" placeholder="" value="">
                                            </div>
                                        </div>
                                        <div class="box-body">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Fitur</label><br>

                                                <table>
                                                    <tr>
                                                        <td><input name="fitur[]" style="margin : 10px;" type="checkbox"  value="1"> Go-Moto</td>
                                                        <td><input name="fitur[]" style="margin : 10px;"type="checkbox"  value="2"> Go-Cab</td>
                                                        <td><input name="fitur[]" style="margin : 10px;"type="checkbox"  value="3"> Go-Food</td>
                                                        <td><input name="fitur[]" style="margin : 10px;"type="checkbox"  value="4"> Go-Mart</td>
                                                    </tr>
                                                    <tr>
                                                        <td><input name="fitur[]" style="margin : 10px;" type="checkbox"  value="5"> Go-Send</td>
                                                        <td><input name="fitur[]" style="margin : 10px;"type="checkbox"  value="6"> Go-Massage</td>
                                                        <td><input name="fitur[]" style="margin : 10px;"type="checkbox"  value="7"> Go-Box</td>
                                                        <td><input name="fitur[]" style="margin : 10px;"type="checkbox" value="8"> Go-Service</td>
                                                    </tr>
                                                </table>


                                              <!--<input type="text" class="form-control" id="exampleInputEmail1" placeholder="" value="">-->
                                            </div>
                                        </div>
                                        <div class="box-body">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Value ($)</label>
                                                <input name ="nilai" type="number" class="form-control" id="exampleInputEmail1" placeholder="" value="">
                                            </div>
                                        </div>
                                        <div class="box-body">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Voucher Quota</label>
                                                <input name="kuota" type="text" class="form-control" id="exampleInputEmail1" placeholder="" value="">
                                            </div>
                                        </div>

                                        <div class="box-body">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Information</label>
                                                <textarea class="form-control" name="keterangan" rows="3"> </textarea>
                                            </div>
                                        </div>


                                        <div class="box-footer">
                                            <button name="btnSubmit" type="submit" class="btn btn-primary">Add Voucher</button>
                                        </div>
                                    </form>

                                </div>
                                <!-- /.box-body -->
                            </div>
                            <!-- /.box -->
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->
           
           
           
           
           
        </div>
        <!-- ./wrapper -->

        <!-- jQuery 2.2.3 -->
        <script src="<?php echo base_url(); ?>plugins/jQuery/jquery-2.2.3.min.js"></script>
        <!-- Bootstrap 3.3.6 -->
        <script src="<?php echo base_url(); ?>bootstrap/js/bootstrap.min.js"></script>
        <!-- DataTables -->
        <script src="<?php echo base_url(); ?>plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="<?php echo base_url(); ?>plugins/datatables/dataTables.bootstrap.min.js"></script>
        <!-- SlimScroll -->
        <script src="<?php echo base_url(); ?>plugins/slimScroll/jquery.slimscroll.min.js"></script>
        <!-- FastClick -->
        <script src="<?php echo base_url(); ?>plugins/fastclick/fastclick.js"></script>
        <!-- AdminLTE App -->
        <script src="<?php echo base_url(); ?>dist/js/app.min.js"></script>
        <!-- AdminLTE for demo purposes -->
        <script src="<?php echo base_url(); ?>dist/js/demo.js"></script>
        <!-- page script -->
        <script>
            $(function () {
                $("#example1").DataTable();
                $('#example2').DataTable({
                    "paging": true,
                    "lengthChange": false,
                    "searching": false,
                    "ordering": true,
                    "info": true,
                    "autoWidth": false
                });
            });
        </script>
    </body>
</html>
