<?php
defined('BASEPATH') OR exit('No direct script access allowed');require_once("../gtx_settings.php");
?>
<!DOCTYPE html>
<html>
    <head>
        
 <title><?php echo "$name_apps"; ?> | Edit Status Go-Service</title>
<meta content="template gotaxi" name="keywords">
 <meta content="go-taxi" name="author">
 <meta content="On Demand All in One App Services Android" name="description">
 <link rel="shortcut icon" href="/asset/images/favicon.png">
 <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
 <link rel="stylesheet" href="<?php echo base_url(); ?>bootstrap/css/bootstrap.min.css"> 
  <link rel="stylesheet" href="<?php echo $bootstrap; ?>">
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
 <link rel="stylesheet" href="<?php echo base_url(); ?>dist/css/skins/_all-skins.min.css">
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    
 <style type = "text/css">
 
 .label{display:inline;padding:.2em .6em .3em;font-size:75%;font-weight:700;line-height:1;color:#fff;text-align:center;white-space:nowrap;vertical-align:baseline;border-radius:.25em}a.label:focus,a.label:hover{color:#fff;text-decoration:none;cursor:pointer}.label:empty{display:none}.btn .label{position:relative;top:-1px}.label-default{background-color:#777}.label-default[href]:focus,.label-default[href]:hover{background-color:#5e5e5e}.label-primary{background-color:#337ab7}.label-primary[href]:focus,.label-primary[href]:hover{background-color:#286090}.label-success{background-color:#5cb85c}.label-success[href]:focus,.label-success[href]:hover{background-color:#449d44}.label-info{background-color:#5bc0de}.label-info[href]:focus,.label-info[href]:hover{background-color:#31b0d5}.label-warning{background-color:#f0ad4e}.label-warning[href]:focus,.label-warning[href]:hover{background-color:#ec971f}.label-danger{background-color:#d9534f}.label-danger[href]:focus,.label-danger[href]:hover{background-color:#c9302c}.
 </style>
 <link rel="stylesheet" href="/modules/css/animate.css"/>
 <script type="text/javascript" src="/modules/js/jquery-2.1.1.min.js"></script>
 <script type="text/javascript" src="/modules/js/jquery.countTo.js"></script>
 <script type="text/javascript" src="/modules/js/moment.min.js"></script>
 <script type="text/javascript" src="/modules/js/app.js"></script>
 </head>
 
 <body>
       
  <?php include 'header.php'; ?>  
       <div class="wrapper">
     
            <?php include 'SIDEBAR.php'; ?>
	<div class="main">
	    <div class="content-wrapper">
<div class="content with-top-banner">
	<div class="content-header no-mg-top">
		<i class="fa fa-newspaper-o"></i>
		<div class="content-header-title">Driver Detail Information</div>
	
</div>               

                <!-- Main content -->
    <div class="panel">
                  <div class="row">
                        <div class="col-md-12">
                             <div class="content-box">
                                <div class="box-header">
                                  

                                </div>
                                
                               <!-- form start -->
                                <form action="<?php echo base_url(); ?>index.php/Listdriver/editStatusMservice" method="POST" role="form" enctype="multipart/form-data">
                                    <!-- /.box-header -->
                                    <div class="box-body">
                                        <div class="row">
                                            <div class="col-md-2"></div>
                                            <div class="col-md-4">
                                                <br><br><br>
                                                <div class="box-body">
                                                    <div class="form-group">
                                                        <label for="">Driver ID</label>
                                                        <input name="idmservice" type="hidden" class="form-control" id="exampleInputEmail1" placeholder="" value="<?php echo $driver[0]['id']; ?>">
                                                        <input type="text" class="form-control" placeholder="" value="<?php echo $driver[0]['id']; ?>" disabled>
                                                    </div>
                                                </div>
                                                <div class="box-body">
                                                    <div class="form-group">
                                                        <label for="">Full Name</label>
                                                        <input name="namalengkap" type="text" class="form-control" id="exampleInputEmail1" placeholder="" value="<?php echo $driver[0]['nama_depan']; ?>">
                                                    </div>
                                                </div>
                                                <div class="box-body">
                                                    <div class="form-group">
                                                        <label for="">Identity Number</label>
                                                        <input name="ktp" type="text" class="form-control" id="exampleInputEmail1" placeholder="" value="<?php echo $driver[0]['no_ktp']; ?>">
                                                    </div>
                                                </div>
                                                <div class="box-body">
                                                    <div class="form-group">
                                                        <label for="">Place of Birth</label>
                                                        <input name="tempatlahir" type="text" class="form-control" placeholder="" value="<?php echo $driver[0]['tempat_lahir']; ?>">
                                                    </div>
                                                </div>
                                                <div class="box-body">
                                                    <div class="form-group">
                                                        <label for="">Date of birth (hh/bb/tttt)</label>
                                                        
                                                        <input name="DOB" class="form-control" type="date" data-date="" data-date-format="DD MMMM YYYY" value="<?php echo $driver[0]['tgl_lahir']; ?>">
                                                        
                                                    </div>
                                                </div>


                                                <div class="box-body">
                                                    <div class="form-group">
                                                        <label for="">Phone</label>
                                                        <input type="text" class="form-control" id="exampleInputEmail1" placeholder="" value="<?php echo $driver[0]['no_telepon']; ?>" disabled>
                                                    </div>
                                                </div>
                                                <div class="box-body">
                                                    <div class="form-group">
                                                        <label for="">Job</label>
                                                        <input type="text" class="form-control" id="exampleInputEmail1" placeholder="" value="<?php echo $driver[0]['driver_job']; ?>" disabled>
                                                    </div>
                                                </div>
                                                <div class="box-body">
                                                    <div class="form-group">
                                                        <label>Status</label>
                                                        <select name="status" class="form-control" id="sel1">
                                                            <option value="1">Active</option>
                                                            <option value="2">Non active</option>
                                                            <option value="3">Banned</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <!-- /.box-body -->
                                            </div>
                                            <div class="col-md-4">
                                              
                                                <img src="<?php echo base_url() . "fotomservice/" . $driver[0]['foto'] ?>" class="fa fa-newspaper-o" alt="">

                                                <br>
                                                
                                                
                 <div class="box-body">
                         <div class="form-group"><br>
                                         <label for="">Change Photos</label>
                    <input name="foto" type="hidden" value="<?php echo $driver[0]['foto']; ?>">
                        <input type="file" name="userfile" accept=".png, .jpg, .jpeg, .gif">
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /.col -->
                                        </div>
                                    </div>
                                    <!-- /.box-body -->

                                    <div class="box-footer">
                                        <div class="col-md-2"></div>
                                        <div class="col-md-4">
                                            <button type="submit" class="btn btn-primary">Save Data</button>
                                        </div>
                                </form>
                            </div>
                            <!-- /.box -->
                        </div>
                        <!-- /.col -->
                    </div>
                    
                    
                    
                    
                    
                    
                    
                    <!-- /.row -->
                </div><!-- section -->
                
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->
            

  </div>        
</div>
        <!-- ./wrapper -->

        <!-- jQuery 2.2.3 -->
        <script src="<?php echo base_url(); ?>plugins/jQuery/jquery-2.2.3.min.js"></script>
        <!-- Bootstrap 3.3.6 -->
        <script src="<?php echo base_url(); ?>bootstrap/js/bootstrap.min.js"></script>
        <!-- DataTables -->
        <script src="<?php echo base_url(); ?>plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="<?php echo base_url(); ?>plugins/datatables/dataTables.bootstrap.min.js"></script>
        <!-- SlimScroll -->
        <script src="<?php echo base_url(); ?>plugins/slimScroll/jquery.slimscroll.min.js"></script>
        <!-- FastClick -->
        <script src="<?php echo base_url(); ?>plugins/fastclick/fastclick.js"></script>
        <!-- AdminLTE App -->
        <script src="<?php echo base_url(); ?>dist/js/app.min.js"></script>
        <!-- AdminLTE for demo purposes -->
        <script src="<?php echo base_url(); ?>dist/js/demo.js"></script>
        <!-- page script -->
        <script>
            $(function () {
                $("#example1").DataTable();
                $('#example2').DataTable({
                    "paging": true,
                    "lengthChange": false,
                    "searching": false,
                    "ordering": true,
                    "info": true,
                    "autoWidth": false
                });
            });
        </script>
    </body>
</html>

