<?php
defined('BASEPATH') OR exit('No direct script access allowed');require_once("../gtx_update.php");
?>
<!DOCTYPE html>
<html>
    <head>

<title><?php echo "$name_apps"; ?> - Atribut</title>
 <meta content="template gotaxi" name="keywords">
 <meta content="go-taxi" name="author">
 <meta content="On Demand All in One App Services Android" name="description">
 <link rel="shortcut icon" href="/asset/images/favicon.png">
 <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
 <link rel="stylesheet" href="<?php echo base_url(); ?>bootstrap/css/bootstrap.min.css"> 
  <link rel="stylesheet" href="<?php echo $bootstrap; ?>">
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
 <link rel="stylesheet" href="<?php echo base_url(); ?>dist/css/skins/_all-skins.min.css">
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    
 <style type = "text/css">
	input[type=number]::-webkit-inner-spin-button, 
	input[type=number]::-webkit-outer-spin-button { 
	-webkit-appearance: none; 
	margin: 0; 
	}
	.label{display:inline;padding:.2em .6em .3em;font-size:75%;font-weight:700;line-height:1;color:#fff;text-align:center;white-space:nowrap;vertical-align:baseline;border-radius:.25em}a.label:focus,a.label:hover{color:#fff;text-decoration:none;cursor:pointer}.label:empty{display:none}.btn .label{position:relative;top:-1px}.label-default{background-color:#777}.label-default[href]:focus,.label-default[href]:hover{background-color:#5e5e5e}.label-primary{background-color:#337ab7}.label-primary[href]:focus,.label-primary[href]:hover{background-color:#286090}.label-success{background-color:#5cb85c}.label-success[href]:focus,.label-success[href]:hover{background-color:#449d44}.label-info{background-color:#5bc0de}.label-info[href]:focus,.label-info[href]:hover{background-color:#31b0d5}.label-warning{background-color:#f0ad4e}.label-warning[href]:focus,.label-warning[href]:hover{background-color:#ec971f}.label-danger{background-color:#d9534f}.label-danger[href]:focus,.label-danger[href]:hover{background-color:#c9302c}.
 </style>
 <link rel="stylesheet" href="/modules/css/animate.css"/>
 <script type="text/javascript" src="/modules/js/jquery-2.1.1.min.js"></script>
 <script type="text/javascript" src="/modules/js/jquery.countTo.js"></script>
 <script type="text/javascript" src="/modules/js/moment.min.js"></script>
 <script type="text/javascript" src="/modules/js/app.js"></script>
 </head>
 
 <body>
              
 <?php include 'header.php'; ?>  
        <div class="wrapper">
            <?php include 'SIDEBAR.php'; ?>
			
	<div class="main">
	
<div class="content with-top-banner">
	<div class="content-header no-mg-top">
	
		<i class="fa fa-university"></i>
		<div class="content-header-title">List Atribut</div>
	
</div>
         <div class="panel">

                    <div class="row">
					
                        <div class="col-md-12">
						
                            <div class="content-box">
							 
                                <div class="box-header">
                                  <button class="btn btn-success" data-toggle="modal" data-target="#modalcreate" style="margin: 1%;"><span class="glyphicon glyphicon-plus"></span> Add Atribut</button>
                                   
                                </div>
                                
                                
                                <!-- /.box-header -->
                                <div class="box-body">
								
                                    <?php echo $pesan; 		
									?>

                                    <table id="example1" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Name Atribut</th>
												<th>Price</th>
                                                <th>Image</th>
												<th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $no = 1;
                                            foreach ($atribut as $key) {
                                                ?>
                                                <tr>
														<td><?php echo $no;
                                            $no++; ?></td>
														<td><?php echo $key->atributnama; ?></td>
														<td><?php echo $key->atributharga; ?></td>
													<?php if ($key->atributimg != null) {?>
														<td><img src="/asset/atribut/<?php echo $key->atributimg; ?>" style="max-width:60px;"></td>
                                                    <?php } else { ?>
														<td></td>
													<?php } ?>
                                    <td>
                                <button class="btn btn-sm btn-warning" data-toggle="modal" data-target="#modal<?= $key->atributid; ?>"><span class="glyphicon glyphicon-pencil"> Ubah</span></button>
                                <a href="<?php echo base_url(); ?>index.php/Atribut/hapusAtribut/<?php echo $key->atributid; ?>"><button type="button" class="btn btn-danger btn-sm" onclick="return confirm('Are you sure you want to remove the atribut? ')"><span class="glyphicon glyphicon-trash"> Hapus</span></button></a>
                                    </td>
                                    
                                                </tr>
                                               

                                                <!-- modal edit -->
                                                <div id="modal<?= $key->atributid; ?>" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog modal-sm" role="document" style="margin-top:20vh">
                                                        <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                            <h4 class="modal-title">Ubah Atribut</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="row">
                                                                <form action="<?php echo base_url(); ?>index.php/Atribut/editAtribut/" method="post" enctype="multipart/form-data">
                                                                <div class="form-group" style="margin:0 10px">
                                                                    <label>Nama Atribut<br></label><p><br></p>
                                                                    <input name="atributnama" type="text" value="<?= $key->atributnama; ?>" maxlenght="20" class="form-control" width="100%" style="margin-top:-10px">
                                                                    <p><br></p>
                                                                </div>
																<div class="form-group" style="margin:0 10px">
                                                                    <label>Harga Atribut<br></label><p><br></p>
                                                                    <input name="atributharga" type="number" value="<?= $key->atributharga; ?>" maxlenght="20" class="form-control" width="100%" style="margin-top:-10px">
                                                                    <p><br></p>
                                                                </div>
                                                                <div class="form-group" style="margin:0 10px">
                                                                    <label>Upload Atribut<br></label><p><br></p>
                                                                    <input type="file" name="atributimg" id="atributimg">
                                                                    <p><br></p>
                                                                </div>

                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-info" data-dismiss="modal">Tutup</button>
                                                            <input type="hidden" name="atributid" value="<?= $key->atributid;?>">
                                                            <button type="submit" class="btn btn-primary">Simpan</button>
                                                        </div>
                                                        </form>
                                                        </div><!-- /.modal-content -->
                                                    </div><!-- /.modal-dialog -->
                                                </div><!-- /.modal -->





                                                <!-- end modal -->
                                            <?php
                                            }
                                            ?>
											<div id="modalcreate" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog modal-sm" role="document" style="margin-top:20vh">
                                                        <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                            <h4 class="modal-title">Tambah Atribut Baru</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="row">
                                                                <form action="<?php echo base_url(); ?>index.php/Atribut/addAtribut/" method="post" enctype="multipart/form-data">
                                                                <div class="form-group" style="margin:0 10px">
                                                                    <label>Nama Atribut<br></label><p><br></p>
                                                                    <input name="atributnama" type="text" value="" maxlenght="20" class="form-control" width="100%" style="margin-top:-10px">
                                                                    <p><br></p>
                                                                </div>
																<div class="form-group" style="margin:0 10px">
                                                                    <label>Harga<br></label><p><br></p>
                                                                    <input name="atributharga" type="number" value="" maxlenght="20" class="form-control" width="100%" style="margin-top:-10px">
                                                                    <p><br></p>
                                                                </div>
                                                                
																<div class="form-group" style="margin:0 10px">
                                                                    <label>Upload Foto Atribut<br></label><p><br></p>
                                                                    <input type="file" name="atributimg" id="atributimg">
                                                                    <p><br></p>
                                                                </div>
                                                            </div>
                                                        </div>
														
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-info" data-dismiss="modal">Tutup</button>
                                                            <input type="hidden" name="atributid" value="">
                                                            <button type="submit" class="btn btn-primary">Simpan</button>
                                                        </div>
                                                        </form>
														
                                                        </div><!-- /.modal-content -->
                                                    </div><!-- /.modal-dialog -->
                                                </div><!-- /.modal -->
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.box-body -->
                            </div>
                            <!-- /.box -->
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->
           
        </div>
        <!-- ./wrapper -->

       
        <!-- Bootstrap 3.3.6 -->
        <script src="<?php echo base_url(); ?>bootstrap/js/bootstrap.min.js"></script>
        <!-- DataTables -->
        <script src="<?php echo base_url(); ?>plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="<?php echo base_url(); ?>plugins/datatables/dataTables.bootstrap.min.js"></script>
        <!-- SlimScroll -->
        <script src="<?php echo base_url(); ?>plugins/slimScroll/jquery.slimscroll.min.js"></script>
        <!-- FastClick -->
        <script src="<?php echo base_url(); ?>plugins/fastclick/fastclick.js"></script>
        <!-- AdminLTE App -->
        <script src="<?php echo base_url(); ?>dist/js/app.min.js"></script>
        <!-- AdminLTE for demo purposes -->
        <script src="<?php echo base_url(); ?>dist/js/demo.js"></script>
        <!-- page script -->
        <script>
            $(function () {
                $("#example1").DataTable();
                $('#example2').DataTable({
                    "paging": true,
                    "lengthChange": false,
                    "searching": false,
                    "ordering": true,
                    "info": true,
                    "autoWidth": false
                });
            });
        </script>
    </body>
</html>
