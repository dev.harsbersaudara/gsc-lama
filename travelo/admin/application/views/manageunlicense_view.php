<?php
/*==============================================================================
	Item Name: GoTaxi - On Demand All in One App Services Android
	Author: Androgo Design
	Author URL: https://codecanyon.net/item/gotaxi-on-demand-all-in-one-app-services-android/22612350
	Attention: Don't modify this FILE because it will error the script.
================================================================================
*/

//Lorem ipsum dolor sit amet, consectetur adipiscing elit. In eget ultricies eros, quis viverra purus. Aliquam ut eros diam. Cras id felis lorem. Nam eget nibh molestie, lacinia metus eget, pharetra urna. Maecenas suscipit blandit mauris, a commodo turpis egestas ac. Etiam blandit ante a leo porta congue. Proin tincidunt, leo at ultricies vestibulum, odio purus vestibulum tellus, et tristique lacus leo in quam. 
require_once("../gtx_config.php");//Ut iaculis ligula eget porttitor dignissim. Fusce sollicitudin blandit orci, sit amet posuere neque molestie in. Integer aliquam tempor tellus eu ultricies. Morbi ac eleifend ligula. In lacinia neque at dolor efficitur, molestie vehicula velit feugiat. 
require_once("../gtx_settings.php");//Aenean aliquet ipsum nec est luctus euismod. Donec at volutpat eros. Nunc ultricies scelerisque orci, rutrum convallis metus gravida id. Nullam facilisis augue ligula, quis feugiat dolor sollicitudin eget. Fusce ultricies risus lobortis, vehicula nisi quis, mattis enim. Nulla egestas nulla vel justo pretium facilisis.
$demo_page_title="Uninstall License";//Proin vitae dui bibendum, porta elit vel, viverra quam. Integer vitae magna ac libero tincidunt suscipit. Etiam rutrum purus eu purus porta posuere. 
$demo_page_message="Uninstalls license, so user can re-install script on different domain. Script stops working immediately.<br><br><b>Attention!</b> If not configured otherwise, $PRODUCT_NAME will automatically use.";//Morbi sit amet turpis a purus suscipit interdum vitae id erat. Vestibulum dictum, diam sit amet lobortis consequat, odio mi ultricies sapien, nec iaculis ante sapien quis purus. Aenean ut massa eros. Nullam bibendum in sem vel dictum. Aliquam eleifend tellus ligula, ut volutpat enim tempus id.
$demo_page_class="alert alert-info alert-with-icon";
if (!empty($_POST) && is_array($_POST))
    {
    extract(array_map("trim", $_POST), EXTR_SKIP); //Curabitur sapien libero, mollis ut aliquam a, volutpat quis odio. Vestibulum porta elementum elit, eu feugiat sem tempus ac. Proin condimentum mauris eget maximus auctor. Etiam auctor luctus magna sit amet venenatis. Sed eleifend lacus quis convallis accumsan. 
    }
//Ut nunc turpis, vestibulum in eros eu, blandit iaculis nibh. Cras sit amet leo accumsan, lacinia nunc eu, accumsan orci. Nam at nulla varius, commodo tortor in, gravida enim. Nam maximus non sem a vehicula. Sed in leo in tellus volutpat vehicula. Duis lorem nisl, faucibus sit amet turpis non, sodales convallis massa.
if (isset($submit_ok))
    {
//Duis in rhoncus nibh, non tristique metus. Quisque sapien tortor, finibus in feugiat quis, pretium ac lacus. Etiam malesuada efficitur consequat. Nam hendrerit lorem lectus, vel iaculis tellus tincidunt id. Morbi at risus eu tortor laoreet accumsan auctor nec quam. Quisque luctus urna ac nulla commodo, quis vulputate dui sagittis.
    $license_notifications_array=aplUninstallLicense($GLOBALS["mysqli"]);

    if ($license_notifications_array['notification_case']=="notification_license_ok") 
        {
        $demo_page_message="Your $PRODUCT_NAME license was uninstalled! $PRODUCT_NAME will now stop working!<br>You can reinstall the gotaxi license on this domain or also move all script files including MySQL and reinstall on the new domain.";
        $demo_page_class="alert alert-success alert-with-icon";
        }
    else //Cras ut nibh ligula. Pellentesque eros nulla, condimentum nec erat vel, aliquam ullamcorper velit. Vestibulum fermentum odio nec sapien tincidunt bibendum. Nullam eget nisi fermentum, venenatis nunc id, tempus est. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Pellentesque at nibh dapibus, volutpat augue non, venenatis risus.
        {
        $demo_page_message="Your $PRODUCT_NAME license uninstallation failed because of this reason: ".$license_notifications_array['notification_text'];
        $demo_page_class="alert alert-danger alert-with-icon";
        }
    }//Donec pellentesque dignissim accumsan. Maecenas et pretium quam, a faucibus sapien. Ut sapien nulla, convallis vel magna sed, tempus interdum justo. Sed ac tellus pretium, hendrerit mi ac, pharetra mauris. Sed dictum massa tempus felis vestibulum, vitae placerat est volutpat. 
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title><?php echo "$demo_page_title | $ORIGINAL_PRODUCT_NAME"; ?></title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
    <link href="/asset/css/bootstrap-license.min.css" rel="stylesheet" />
    <link href="/asset/css/now-ui-dashboard.min.css" rel="stylesheet" />
</head>
<body>
    
    <div class="wrapper ">
        <div class="sidebar" data-color="orange">
            <div class="logo">
                <a href="<?php echo $ORIGINAL_PRODUCT_URL; ?>" class="simple-text logo-normal" target="_blank"><?php echo $PRODUCT_NAME; ?></a>
            </div>
            <div class="sidebar-wrapper">
                <ul class="nav">
                    <li><a href="<?php echo $HOST_URL_ROOT ?>/gtx_home.php"><i class="now-ui-icons media-1_button-play"></i><p>Home Page</p></a></li>
                    <li class="active"><a href="<?php echo $HOST_URL_ROOT ?>/uninstall.php"><i class="now-ui-icons media-1_button-power"></i><p>Uninstall License</p></a></li>
                </ul>
            </div>
        </div>
        
        <div class="main-panel">
            <nav class="navbar navbar-expand-lg navbar-transparent  navbar-absolute bg-primary fixed-top">
                <div class="container-fluid">
                    <div class="navbar-wrapper">
                        <div class="navbar-toggle">
                            <button type="button" class="navbar-toggler">
                                <span class="navbar-toggler-bar bar1"></span>
                                <span class="navbar-toggler-bar bar2"></span>
                                <span class="navbar-toggler-bar bar3"></span>
                            </button>
                        </div>
                    </div>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation"></button>
                </div>
            </nav>
            <div class="panel-header panel-header-sm">
            </div>
            <div class="content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h5 class="title"><?php echo $ORIGINAL_PRODUCT_NAME; ?></h5>
                            </div>
                            <div class="card-body">
                                <div class="<?php echo $demo_page_class; ?>" data-notify="container">
                                    <span data-notify="icon" class="now-ui-icons ui-1_bell-53"></span>
                                    <span data-notify="message"><?php echo $demo_page_message; ?></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
              
                
            </div>
            
            <footer class="footer">
                <div class="container-fluid">
                    <div class="copyright">
                        <?php echo "<a href='$ORIGINAL_PRODUCT_URL' title='$ORIGINAL_PRODUCT_NAME' target='_blank'>$ORIGINAL_PRODUCT_NAME</a> by <a href='http://gotaxi.biz' title='androgo' target='_blank'>Androgo</a>"; ?>
                    </div>
                </div>
            </footer>
        </div>
    </div>
</body>
<script src="/asset/js/core/jquery.min.js"></script>
<script src="/asset/js/core/popper.min.js"></script>
<script src="/asset/js/core/bootstrap.min.js"></script>
<script src="/asset/js/now-ui-dashboard.min.js"></script>
</html>
