<div class="top-nav">
	<div class="top-navigation-box">
		<div class="side-nav-mobile"><i class="fa fa-bars"></i></div>
		<div class="logo-wrapper">
			<div class="logo-box">
				<img alt="go-taxi" src="/asset/images/favicon.png">
				<a href="/">
					<div class="logo-title"><?php echo "$name_apps"; ?></div>
				</a>
			</div>
		</div>
		<div class="top-nav-content">
			<div class="top-navigation-box">
			    
				<div class="quick-link">
					<div class="link-icon"><i class="fa fa-bars"></i></div>
					<ul class="animated bounceInUp">
						<li><a href="<?php echo base_url(); ?>index.php/Listpelanggan"><i class="active fa fa-child"></i> List User</a></li>
						
						<li><a href="<?php echo base_url(); ?>index.php/helpcenter"><i class="active fa fa-question-circle"></i> Help Center</a></li>
						
						<li><a href="<?php echo base_url(); ?>index.php/Withdraw"><i class="active fa fa-share-square-o"></i>Driver Withdraw</a></li>
						
						<li><a href="<?php echo base_url(); ?>index.php/bannermfood"><i class="fa fa-shopping-bag"></i> Restaurant Promotion</a></li>
						
							<li><a href="<?php echo base_url(); ?>index.php/listmitramfood"><i class="fa fa-cutlery"></i>List Restaurant</a></li>
						
						<li><a href="<?php echo base_url(); ?>index.php/promotion"><i class="fa fa-paper-plane"></i> App Promotion</a></li>
					
					</ul>
				</div>
				
				
				<div class="global-search">
					<form class="form-inline">
					
						
					</form>
				</div>
				
				 
				<div class="top-notification">

					<div class="notification-icon">
						<div class="notification-badge bounceInDown animated timer" data-from="0" data-to="<?php echo $jumlahDriverOn[0]['jumlah']; ?>"></div>
					
					<i class="ion-android-map"></i>
						
						<div class="notification-wrapper animated bounceInUp">
							<div class="notification-header">Driver Online <span class="notification-count"><?php echo $jumlahDriverOn[0]['jumlah']; ?></span></div>
							
			
			
				<div class="notification-body">
				   <?php
                        foreach ($detildriveronline as $value) {
                         ?>
								<div class="notification-list">
									<div class="notification-image">
										<img alt="gotaxi" src="<?php echo base_url() . "/fotodriver/" . $value->foto; ?>" ?>
									</div>
								  
									<div class="notification-content">
										<div class="notification-text"><strong><?php echo $value->nama_depan . " " . $value->nama_belakang; ?></strong> </div>
							<div class="notification-time">
						Job :	<?php
          if ($value->driver_job == 'mride') {
                        echo 'Go-Moto';
                } 
                
                else if ($value->driver_job == 'mcar') {
                echo 'Go-Moto';
                }
                else if ($value->driver_job == 'mmassage') {
                echo 'Go-Massage';
                }
                
                else if ($value->driver_job == 'mbox') {
                echo 'Go-Box';
                }
            
                  else {
                  echo 'Go-Service';
                      }
                ?>
							</div>
									</div>
								</div>
						
			<?php
           }
        ?>			
	 </div> 
			
			
			
			<div class="notification-footer">
			<a href="<?php echo base_url(); ?>index.php/Dashboard/dirveronline">See all Driver Online</a>
			</div>
							
							
					
		</div>
	</div>
	
	
	
					<div class="notification-icon">
						<div class="notification-badge bounceInDown animated timer" data-from="0" data-to="<?php echo $jumlahDriverWork[0]['jumlah']; ?>"><?php echo $jumlahDriverWork[0]['jumlah']; ?></div>
						<i class="fa fa-bell"></i>
						
					
					
						<div class="notification-wrapper animated bounceInUp">
							<div class="notification-header">Driver Working <span class="notification-count"><?php echo $jumlahDriverWork[0]['jumlah']; ?></span></div>
					
						</div>
						
						
					</div>
				</div>
				
				
				<div class="user-top-profile">
					<div class="user-image">
						<div class="user-on"></div>
						<img alt="gotaxi" src="/asset/images/photo_male.jpg">
					</div>
					<div class="clear">
						<div class="user-name"><?php echo "$useradmin"; ?></div>
						<div class="user-group">Maintenance</div>
						<ul class="user-top-menu animated bounceInUp">
						    
							<li><a href="https://support.gotaxi.biz">Support Forums </a></li>
						<li><a href="<?php echo base_url(); ?>index.php/Dashboard/update">Software Updates</a></li>
						    <li><a href="<?php echo base_url(); ?>index.php/Dashboard/uninstall">Uninstall License</a></li>
							
							<li><a href="<?php echo base_url(); ?>index.php/signout">Logout</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="profile-nav-mobile"><i class="fa fa-cog"></i></div>
	</div>
</div>
