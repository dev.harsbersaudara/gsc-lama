<?php
defined('BASEPATH') OR exit('No direct script access allowed');require_once("../gtx_settings.php");
?>
<!DOCTYPE html>
<html>
    <head>

<title>RESTAURANT - List Partners <?php echo "$name_apps"; ?></title>
 <meta content="template gotaxi" name="keywords">
 <meta content="go-taxi" name="author">
 <meta content="On Demand All in One App Services Android" name="description">
 <link rel="shortcut icon" href="/asset/images/favicon.png">
 <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
 <link rel="stylesheet" href="<?php echo base_url(); ?>bootstrap/css/bootstrap.min.css"> 
  <link rel="stylesheet" href="<?php echo $bootstrap; ?>">
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
 <link rel="stylesheet" href="<?php echo base_url(); ?>dist/css/skins/_all-skins.min.css">
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    
 <style type = "text/css">
 
 .label{display:inline;padding:.2em .6em .3em;font-size:75%;font-weight:700;line-height:1;color:#fff;text-align:center;white-space:nowrap;vertical-align:baseline;border-radius:.25em}a.label:focus,a.label:hover{color:#fff;text-decoration:none;cursor:pointer}.label:empty{display:none}.btn .label{position:relative;top:-1px}.label-default{background-color:#777}.label-default[href]:focus,.label-default[href]:hover{background-color:#5e5e5e}.label-primary{background-color:#337ab7}.label-primary[href]:focus,.label-primary[href]:hover{background-color:#286090}.label-success{background-color:#5cb85c}.label-success[href]:focus,.label-success[href]:hover{background-color:#449d44}.label-info{background-color:#5bc0de}.label-info[href]:focus,.label-info[href]:hover{background-color:#31b0d5}.label-warning{background-color:#f0ad4e}.label-warning[href]:focus,.label-warning[href]:hover{background-color:#ec971f}.label-danger{background-color:#d9534f}.label-danger[href]:focus,.label-danger[href]:hover{background-color:#c9302c}.
 </style>
 <link rel="stylesheet" href="/modules/css/animate.css"/>
 <script type="text/javascript" src="/modules/js/jquery-2.1.1.min.js"></script>
 <script type="text/javascript" src="/modules/js/jquery.countTo.js"></script>
 <script type="text/javascript" src="/modules/js/moment.min.js"></script>
 <script type="text/javascript" src="/modules/js/app.js"></script>
 </head>
 
 <body>
   <?php include 'header.php'; ?>  
       <div class="wrapper">
            <?php include 'SIDEBAR.php'; ?>
          	<div class="main">
<div class="content with-top-banner">
	<div class="content-header no-mg-top">
		<i class="fa fa-cutlery"></i>
		<div class="content-header-title">LIST RESTAURANT </div>
	
</div>
         <div class="panel">
                    
                    
                    <div class="row">
                        <div class="col-md-12">
                            <div class="content-box">
                                
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <?php echo $pesan; ?>

                                    <table id="example1" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>ID Partners</th>
                                                <th>Restaurant Name</th>
                                                <th>Restaurant Address</th>
                                                <th>Owner Name</th>
                                                <th>Email</th>
                                                <th>Owner Address</th>
                                                <th>Owner Phone</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            foreach ($mitra as $key) {
                                                ?>
                                                <tr>
                                                    <td><?php echo $key->idmitra; ?></td>
                                                    <td><?php echo $key->nama_resto; ?></td>
                                                    <td><?php echo $key->alamat; ?></td>
                                                    <td><?php echo $key->nama_pemilik; ?></td>
                                                    <td><?php echo $key->email_penanggung_jawab; ?></td>
                                                    <td><?php echo $key->alamat_pemilik; ?></td>
                                                    <td><?php echo $key->telepon_penanggung_jawab; ?></td>

                                                    <td>
                                                        <?php
                                                        if ($key->is_valid == '1') {
                                                            echo '<span class="label label-success">Active</span>';
                                                        } else if ($key->is_valid == 2) {
                                                            echo '<span class="label label-danger">Non Active</span>';
                                                        } else {
                                                            echo '<span class="label label-danger">Banned</span>';
                                                        }
                                                        ?>
                                                    </td>
                                                    <td>
                                                        <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#status<?php echo $key->idmitra; ?>">Change status</button>
                                                    </td>
                                                </tr>

                                            <div class="modal fade" id="status<?php echo $key->idmitra; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <form method="POST" action="<?php echo base_url(); ?>index.php/Listmitramfood/editStatus">
                                                            
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                                <h4 class="modal-title" id="myModalLabel">Change Partner Status</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                ID Partners &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;= &nbsp;&nbsp;&nbsp;<b><?php echo $key->idmitra; ?><br></b>
                                                                Owner Name  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;= &nbsp;&nbsp;&nbsp;<b><?php echo $key->nama_pemilik ?>  </b>   
                                                                <br>
                                                                Current Status &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;= &nbsp;&nbsp;&nbsp;<b><?php
                                                    if ($key->is_valid == '1') {
                                                        echo 'Active';
                                                    } else if ($key->is_valid == 2) {
                                                        echo 'Non Active';
                                                    } else {
                                                        echo 'Banned';
                                                    }
                                                        ?>  </b>                                                                
                                                                <hr>
                                                                <input type="hidden" name="idmitra" value="<?php echo $key->idmitra; ?>">
                                                                <input type="hidden" name="idresto" value="<?php echo $key->idresto; ?>">
                                                                <div class="form-group">
                                                                    <label>Status</label>
                                                                    <select name="status" class="form-control" id="sel1">
                                                                        <option value="1">Active</option>
                                                                        <option value="2">Non Active</option>
                                                                        <option value="3">Delete</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                <input class="btn btn-primary" type="submit" value="Save">
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php
                                        }
                                        ?>

                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.box-body -->
                            </div>
                            <!-- /.box -->
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->
             

           
        </div>
        <!-- ./wrapper -->

        <!-- jQuery 2.2.3 -->
        <script src="<?php echo base_url(); ?>plugins/jQuery/jquery-2.2.3.min.js"></script>
        <!-- Bootstrap 3.3.6 -->
        <script src="<?php echo base_url(); ?>bootstrap/js/bootstrap.min.js"></script>
        <!-- DataTables -->
        <script src="<?php echo base_url(); ?>plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="<?php echo base_url(); ?>plugins/datatables/dataTables.bootstrap.min.js"></script>
        <!-- SlimScroll -->
        <script src="<?php echo base_url(); ?>plugins/slimScroll/jquery.slimscroll.min.js"></script>
        <!-- FastClick -->
        <script src="<?php echo base_url(); ?>plugins/fastclick/fastclick.js"></script>
        <!-- AdminLTE App -->
        <script src="<?php echo base_url(); ?>dist/js/app.min.js"></script>
        <!-- AdminLTE for demo purposes -->
        <script src="<?php echo base_url(); ?>dist/js/demo.js"></script>
        <!-- page script -->
        <script>
            $(function () {
                $("#example1").DataTable();
                $('#example2').DataTable({
                    "paging": true,
                    "lengthChange": false,
                    "searching": false,
                    "ordering": true,
                    "info": true,
                    "autoWidth": false
                });
            });
        </script>
    </body>
</html>
