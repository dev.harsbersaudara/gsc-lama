<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Slider_m extends CI_Model {

    function __construct() {
        $this->load->database();
        $this->load->library('session');
    }

    function getAllSlider() {
        $query = $this->db->query("
                SELECT a.sliderid, a.isi, a.img
                FROM `slider` a 
            ");
        return $query->result();
    }

    function hapusSlider($idSlider) {
        $query = $this->db->query("
                DELETE FROM `slider` WHERE `slider`.`sliderid` = '$idSlider'
            ");
    }

    function insertSlider($id,$nama,$img,$rekeningno) {
		
        $query = $this->db->query("
                INSERT INTO `slider` (`sliderid`, `isi`, `img`) 
                VALUES ($id,'$nama',LOWER('$img'))
            ");
    }

    function editSlider($id,$nama,$img,$rekeningno){
        // $this->db->update 
        $this->db->query("update slider set isi ='$nama', img = LOWER('$img') where sliderid=$id");
    }

}

?>
