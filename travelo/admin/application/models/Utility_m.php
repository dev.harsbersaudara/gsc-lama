<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Utility_m extends CI_Model {


    public function __construct() {
        parent::__construct();

    }


	public function GetDataFirebase($endpoint){
		$api_url = "https://gotrav-c9c70.firebaseio.com/";
		// $server_key = "Mid-server-b4ZqPAncCr_0DiRquWH1Zmtx";

		$api_endpoint = $api_url . $endpoint;


		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $api_endpoint);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json',
                                                   'Accept: application/json'));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        $result = array(
            'body' => curl_exec($ch),
            'http_code' => curl_getinfo($ch, CURLINFO_HTTP_CODE),
          );
		curl_close($ch);

		return $result;
	}

	public function SaveDataFirebase($endpoint,$data){
		$api_url = "https://gotrav-c9c70.firebaseio.com/";

		$api_endpoint = $api_url . $endpoint;
		$fields = json_encode($data);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $api_endpoint);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json',
                                                   'Accept: application/json'));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
		curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        $result = array(
            'body' => curl_exec($ch),
            'http_code' => curl_getinfo($ch, CURLINFO_HTTP_CODE),
		  );
		log_message("debug", json_encode($result));
		curl_close($ch);

		return $result;
	}

	public function UpdateDataFirebase($endpoint,$data){
		$api_url = "https://gotrav-c9c70.firebaseio.com/";

		$api_endpoint = $api_url . $endpoint;
		$fields = json_encode($data);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $api_endpoint);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json',
                                                   'Accept: application/json'));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PATCH');
		curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        $result = array(
            'body' => curl_exec($ch),
            'http_code' => curl_getinfo($ch, CURLINFO_HTTP_CODE),
		  );
		log_message("debug", json_encode($result));
		curl_close($ch);

		return $result;
	}


}
