<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class FeatureGotaxi_m extends CI_Model {

    function __construct() {
        $this->load->database();
        $this->load->library('session');
    }

    
//    GET FUNCTION
    function getMride() {
        $query = $this->db->query("SELECT * FROM `feature_gotaxi` WHERE fitur = 'Travelo Ojek'");
        return $query->result_array();
    }

    function getMcar() {
        $query = $this->db->query("SELECT * FROM `feature_gotaxi` WHERE fitur = 'Go-Cab'");
        return $query->result_array();
    }

    function getMfood() {
        $query = $this->db->query("SELECT * FROM `feature_gotaxi` WHERE fitur = 'Go-Food'");
        return $query->result_array();
    }

    function getMmart() {
        $query = $this->db->query("SELECT * FROM `feature_gotaxi` WHERE fitur = 'Belanja'");
        return $query->result_array();
    }

    function getMsend() {
        $query = $this->db->query("SELECT * FROM `feature_gotaxi` WHERE fitur = 'Go-Send'");
        return $query->result_array();
    }

    function getMmassage() {
        $query = $this->db->query("SELECT * FROM `feature_gotaxi` WHERE fitur = 'Go-Massage'");
        return $query->result_array();
    }

    function getMbox() {
        $query = $this->db->query("SELECT * FROM `feature_gotaxi` WHERE fitur = 'Go-Box'");
        return $query->result_array();
    }

    function getMservice() {
        $query = $this->db->query("SELECT * FROM `feature_gotaxi` WHERE fitur = 'Go-Service'");
        return $query->result_array();
    }
    
//    UPDATE FUNCTION Product GoTaxi

    function updateMride($biaya, $biayaminimum) {
        $this->db->query("UPDATE `feature_gotaxi` SET `biaya` = '$biaya' , `biaya_minimum` = '$biayaminimum'  WHERE fitur = 'Travelo Ojek'");
    }

    function updateMcar($biaya, $biayaminimum) {

        $this->db->query("UPDATE `feature_gotaxi` SET `biaya` = '$biaya' , `biaya_minimum` = '$biayaminimum' WHERE fitur = 'Go-Cab'");
    }

    function updateMfood($biaya, $biayaminimum) {

        $this->db->query("UPDATE `feature_gotaxi` SET `biaya` = '$biaya' , `biaya_minimum` = '$biayaminimum' WHERE fitur = 'Go-Food'");
    }

    function updateMmart($biaya, $biayaminimum) {

        $this->db->query("UPDATE `feature_gotaxi` SET `biaya` = '$biaya' , `biaya_minimum` = '$biayaminimum' WHERE fitur = 'Belanja'");
    }

    function updateMsend($biaya, $biayaminimum) {

        $this->db->query("UPDATE `feature_gotaxi` SET `biaya` = '$biaya' , `biaya_minimum` = '$biayaminimum' WHERE fitur = 'Go-Send'");
    }

    function updateMmassage($biaya, $biayaminimum) {

        $this->db->query("UPDATE `feature_gotaxi` SET `biaya` = '$biaya' , `biaya_minimum` = '$biayaminimum' WHERE fitur = 'Go-Massage'");
    }

    function updateMbox($biaya, $biayaminimum) {

        $this->db->query("UPDATE `feature_gotaxi` SET `biaya` = '$biaya' , `biaya_minimum` = '$biayaminimum' WHERE fitur = 'Go-Box'");
    }

    function updateMservice($biaya, $biayaminimum) {
    $this->db->query("UPDATE `feature_gotaxi` SET `biaya` = '$biaya' , `biaya_minimum` = '$biayaminimum' WHERE fitur = 'Go-Service'");
    }

}

?>
