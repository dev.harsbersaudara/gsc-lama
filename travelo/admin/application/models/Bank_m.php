<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Bank_m extends CI_Model {

    function __construct() {
        $this->load->database();
        $this->load->library('session');
    }

    function getAllBank() {
        $query = $this->db->query("
                SELECT a.bankid, a.banknama, a.bankimg, a.rekeningno
                FROM `bank` a 
            ");
        return $query->result();
    }

    function hapusBank($idBank) {
        $query = $this->db->query("
                DELETE FROM `bank` WHERE `bank`.`bankid` = '$idBank'
            ");
    }

    function insertBank($id,$nama,$img,$rekeningno) {
		
        $query = $this->db->query("
                INSERT INTO `bank` (`bankid`, `banknama`, `bankimg`, `rekeningno`) 
                VALUES ($id,'$nama',LOWER('$img'),'$rekeningno')
            ");
    }

    function editBank($id,$nama,$img,$rekeningno){
        // $this->db->update 
        $this->db->query("update bank set banknama ='$nama', bankimg = LOWER('$img') ,rekeningno = '$rekeningno' where bankid=$id");
    }

}

?>
