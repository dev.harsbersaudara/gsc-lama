<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class DataBeli_m extends CI_Model {

    function __construct() {
        $this->load->database();
        $this->load->library('session');
    }

    function getAllDataPendaftarDriver() {
        $query = $this->db->query("
                SELECT * FROM berkas_lamaran_kerja a, kendaraan b, jenis_kendaraan c, driver_job d
                WHERE a.kendaraan = b.id 
                AND b.jenis = c.id
                AND a.job = d.id
                AND a.is_valid = 'no'
            ");
        return $query->result();
    }


function getPembelian() {
        $query = $this->db->query("
                SELECT b.*,a.* FROM beli a
				INNER JOIN driver b ON a.driverid=b.id
				ORDER BY a.waktu desc
            ");
        return $query->result();
    }
	
	 
	 function updateBeli($id, $status) {
		 $sql="UPDATE `beli` SET `status` = '$status' WHERE `beli`.`beliid` = $id;";
        $this->db->query($sql);
			//return $query->result();
       
			
    }
	
	 
	 function tolakBeli($id) {
		 $sql="DELETE FROM `beli`  WHERE `beli`.`beliid` = $id;";
        $this->db->query($sql);
			//return $query->result();
			
    }
   

}

?>
