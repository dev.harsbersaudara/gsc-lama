<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Atribut_m extends CI_Model {

    function __construct() {
        $this->load->database();
        $this->load->library('session');
    }

    function getAllAtribut() {
        $query = $this->db->query("
                SELECT a.atributid, a.atributnama, a.atributimg, a.atributharga
                FROM `atribut` a 
            ");
        return $query->result();
    }

    function hapusAtribut($idAtribut) {
        $query = $this->db->query("
                DELETE FROM `atribut` WHERE `atribut`.`atributid` = '$idAtribut'
            ");
    }

    function insertAtribut($id,$nama,$img,$atributharga) {
		
        $query = $this->db->query("
                INSERT INTO `atribut` (`atributnama`, `atributimg`, `atributharga`) 
                VALUES ('$nama',LOWER('$img'),'$atributharga')
            ");
    }

    function editAtribut($id,$nama,$img,$atributharga){
        // $this->db->update 
        $this->db->query("update atribut set atributnama ='$nama', atributimg = LOWER('$img') ,atributharga = '$atributharga' where atributid=$id");
    }

}

?>
