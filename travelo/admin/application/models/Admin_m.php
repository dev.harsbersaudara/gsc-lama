<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_m extends CI_Model {

    public function login($post){
        $passno = ($post['pass']);
        $pass = hash('sha256',$passno);
        
        $this->db->select('*');
        $this->db->from('admin');
        $this->db->where('email', $post['email']);
        $this->db->where('password',  $pass);
        $query = $this->db->get();
        return $query;
        
    }
}