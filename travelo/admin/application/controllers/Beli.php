<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Beli extends CI_Controller {
    
/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
 */
 

    public $datakirim;
    public $pesan = "";

    public function __construct() {
        parent::__construct();
        require_once("../gtx_config.php");
        

        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => SMTP_HOST_EMAIL,
            'smtp_port' => SMTP_PORT_EMAIL,
            'smtp_user' => EMAIL_NAME_HOST,
            'smtp_pass' => SMTP_PASS_EMAIL,
            'mailtype' => 'html',
            'charset' => 'iso-8859-1'
        );

        $this->load->library('email', $config);
    }

     public function index() {
		$this->load->model('manageadmin_model');
         $model = $this->manageadmin_model;
         $this->datakirim = array("currency" => "$model->currency", "name_apps" => "$model->name_apps", "useradmin" => "$model->useradmin",);
         
         
      //  echo 'validate driver';exit;

        $this->load->model('DataBeli_m');
        $this->datakirim['pendaftar'] = $this->DataBeli_m->getPembelian();
        $this->datakirim['pesan'] = $this->pesan;
        $this->datakirim['tittle'] = 'Pemesanan Atribut';

        $this->load->view('beli_view', $this->datakirim);
    }

	
	 public function validasiBeli($idBeli) {
		 
		
        $this->load->model('DataBeli_m');
      	
        $res = $this->DataBeli_m->updateBeli($idBeli,1);


        $this->pesan = "<p style=\"color:green\" class=\"text-center\">Data Applicants Successfully approve</p> <br>";
        $this->index(); 
    }
	
	 public function validasiKirim($idBeli) {
        $this->load->model('DataBeli_m');
      	
       $res =  $this->DataBeli_m->updateBeli($idBeli,2);


			$this->pesan = "<p style=\"color:green\" class=\"text-center\">Data Applicants Successfully send</p> <br>";
			  $this->index(); 
               
    }
	
	    public function tolakBeli($idbank) {
//        echo $idbank;
        $this->load->model('DataBeli_m');
        $this->DataBeli_m->tolakBeli($idbank);

        $this->pesan = "<p style=\"color:green\" class=\"text-center\">Data successfully deleted</p> <br>";
        $this->index();

//        var_dump($mride);
    }

}