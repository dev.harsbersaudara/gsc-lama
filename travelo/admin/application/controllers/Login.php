<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
    
/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
 */
    
    public $pesanerror = array("msg" => "");

    //Session 
    public function __construct() {
        parent::__construct();
        require_once BASEPATH.'core/Construct.php'; 
        $this->load->library('session');
        
        }

/**
 * System Initialization File
 *
 * Loads the base classes and executes the request.
 *
 * @package		CodeIgniter
 * @subpackage	CodeIgniter
 * @category	Front-controller
 * @link		https://codeigniter.com/user_guide/
 */
// parent::__construct();
/**
 * CodeIgniter Version
 *
 * @var	string
 *
 */
    public function index() {
        if ($this->session->userdata('email') == NULL) {
            $this->load->view('login_view', $this->pesanerror);
        } else {
           
            header('Location: ' . base_url() . "index.php/dashboard");
        }
        
    }

    public function check() {
        $email = $_POST['email'];
        $passno = ($_POST['pass']);
        $pass = hash('sha256',$passno);

//      echo "$email | $pass";
        $this->load->database();
        $data = $this->db->query("select * from admin where email = '$email'");

        if ($d = $data->result_array()) {
            $emailDB = $d[0]['email'];
            $passDB = $d[0]['password'];
           // var_dump($d);die();
            if ($passDB != $pass) {
                $this->pesanerror = array(
                    "msg" => "Password wrong"
                );
                $this->load->view('login_view', $this->pesanerror);
            } else {
                // echo $d[0]['email'];die();

                $this->session->set_userdata('id', $d[0]['id']);
                $this->session->set_userdata('nik', $d[0]['nik']);
                $this->session->set_userdata('email', $d[0]['email']);
                $this->session->set_userdata('name_apps', $d[0]['name_apps']);
                $this->session->set_userdata('currency', $d[0]['currency']);
                $this->session->set_userdata('useradmin', $d[0]['useradmin']);
                $this->session->set_userdata('level', $d[0]['level']);


                header('Location: ' . base_url() . "index.php/dashboard");
            }
        } else {
            $this->pesanerror = array(
                "msg" => "Admin not listed"
            );
            $this->load->view('login_view', $this->pesanerror);
        }
    }

}