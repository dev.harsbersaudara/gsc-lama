<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    public $datakirim;
    public $pesan = "";

    public function __construct() {
        parent::__construct();
        $this->load->library('session');
        require_once BASEPATH.'libraries/Path.php';
    }

    public function index() {
         if ($this->session->userdata('email') == NULL) {
            redirect(base_url());
        } else {
         // echo $this->session->userdata('email');die();
         require_once BASEPATH.'core/Construct.php';     
         $this->load->model('manageadmin_model');
         $model = $this->manageadmin_model;
         $this->datakirim = array("currency" => $this->session->userdata('currency'), "name_apps" => $this->session->userdata('name_apps'), "useradmin" => $this->session->userdata('useradmin'));
         
         $this->load->model('Driver_m');
         $this->load->model('Driver_m');
         $this->datakirim['jumlahDriver1'] = ($this->session->userdata('level') == 1) ? $this->Driver_m->getJumlahDriverAktif() : $this->Driver_m->getJumlahDriverAktifx($this->session->userdata('level'));

         $this->datakirim['jumlahDriver2'] = ($this->session->userdata('level') == 1) ? $this->Driver_m->getJumlahDriverNonAktif() : $this->Driver_m->getJumlahDriverNonAktifx($this->session->userdata('level'));

         $this->datakirim['jumlahDriver3'] = ($this->session->userdata('level') == 1) ? $this->Driver_m->getJumlahDriverBanned() : $this->Driver_m->getJumlahDriverBannedx($this->session->userdata('level'));

         $this->datakirim['jumlahDriverCar1'] = ($this->session->userdata('level') == 1) ? $this->Driver_m->getJumlahDriverCarAktif() : $this->Driver_m->getJumlahDriverCarAktifx($this->session->userdata('level'));

         $this->datakirim['jumlahDriverCar2'] = ($this->session->userdata('level') == 1) ? $this->Driver_m->getJumlahDriverCarNonAktif() : $this->Driver_m->getJumlahDriverCarNonAktifx($this->session->userdata('level'));

         $this->datakirim['jumlahDriverCar3'] = ($this->session->userdata('level') == 1) ? $this->Driver_m->getJumlahDriverCarBanned() : $this->Driver_m->getJumlahDriverCarBannedx($this->session->userdata('level'));

         $this->datakirim['jumlahDriverMotor1'] = ($this->session->userdata('level') == 1) ? $this->Driver_m->getJumlahDriverMotorAktif() : $this->Driver_m->getJumlahDriverMotorAktifx($this->session->userdata('level'));

         $this->datakirim['jumlahDriverMotor2'] = ($this->session->userdata('level') == 1) ? $this->Driver_m->getJumlahDriverMotorNonAktif() : $this->Driver_m->getJumlahDriverMotorNonAktifx($this->session->userdata('level'));

         $this->datakirim['jumlahDriverMotor3'] = ($this->session->userdata('level') == 1) ? $this->Driver_m->getJumlahDriverMotorBanned() : $this->Driver_m->getJumlahDriverMotorBannedx($this->session->userdata('level'));

  $this->datakirim['jumlahDriverOn'] = $this->Driver_m->getJumlahDriverOn();
  
  $this->datakirim['jumlahDriverWork'] = $this->Driver_m->getJumlahDriverWork();
  $this->datakirim['detildriveronline'] = $this->Driver_m->getDetailDriverOnlineHome();           
  
  $this->datakirim['transaksiDriver'] = $this->Driver_m->getAllHistoryTransaksi();
  $this->datakirim['transaksiBulan'] = $this->Driver_m->getTotalTransaksiBulanan(date('n'), date('Y'));
  $this->datakirim['transaksiHarian'] = $this->Driver_m->getTotalTransaksiHarian(date('d'), date('n'));


  $this->load->model('Pelanggan_m');
  $this->datakirim['jumlahPelanggan1'] = $this->Pelanggan_m->getJumlahPelangganAktif();
  $this->datakirim['jumlahPelanggan2'] = $this->Pelanggan_m->getJumlahPelangganNonAktif();
  $this->datakirim['jumlahPelanggan3'] = $this->Pelanggan_m->getJumlahPelangganBanned();


  $this->datakirim['bln1'] = $this->Driver_m->getTotalTransaksiBulanan(1, date('Y'));
  
  $this->datakirim['bln2'] = $this->Driver_m->getTotalTransaksiBulanan(2, date('Y'));
  $this->datakirim['bln3'] = $this->Driver_m->getTotalTransaksiBulanan(3, date('Y'));
  $this->datakirim['bln4'] = $this->Driver_m->getTotalTransaksiBulanan(4, date('Y'));
  $this->datakirim['bln5'] = $this->Driver_m->getTotalTransaksiBulanan(5, date('Y'));
  $this->datakirim['bln6'] = $this->Driver_m->getTotalTransaksiBulanan(6, date('Y'));
  $this->datakirim['bln7'] = $this->Driver_m->getTotalTransaksiBulanan(7, date('Y'));
  $this->datakirim['bln8'] = $this->Driver_m->getTotalTransaksiBulanan(8, date('Y'));
  $this->datakirim['bln9'] = $this->Driver_m->getTotalTransaksiBulanan(9, date('Y'));
  $this->datakirim['bln10'] = $this->Driver_m->getTotalTransaksiBulanan(10, date('Y'));
            $this->datakirim['bln11'] = $this->Driver_m->getTotalTransaksiBulanan(11, date('Y'));
            $this->datakirim['bln12'] = $this->Driver_m->getTotalTransaksiBulanan(12, date('Y'));

            // piechart
            $this->datakirim['Mencari'] = $this->Driver_m->getTotalTransaksi(1);
            $this->datakirim['Menawarkan'] = $this->Driver_m->getTotalTransaksi(2);
            $this->datakirim['Berhasil'] = $this->Driver_m->getTotalTransaksi(3);
            $this->datakirim['Ditolak'] = $this->Driver_m->getTotalTransaksi(4);
            $this->datakirim['Dibatalkan'] = $this->Driver_m->getTotalTransaksi(5);
            $this->datakirim['Memulai'] = $this->Driver_m->getTotalTransaksi(6);
            $this->datakirim['Selesai'] = $this->Driver_m->getTotalTransaksi(7);

	
            $this->load->view('dashboard_view', $this->datakirim);
        }
    }
    
    
 public function billing() {
        if ($this->session->userdata('email') == NULL) {
            redirect(base_url());
        } else {
         require_once BASEPATH.'core/Construct.php';     
         $this->load->model('manageadmin_model');
         $model = $this->manageadmin_model;
         $this->datakirim = array("currency" => "$model->currency", "name_apps" => "$model->name_apps", "useradmin" => "$model->useradmin");
         
         $this->load->model('Driver_m');
         $this->load->model('Driver_m');
         $this->datakirim['jumlahDriver1'] = $this->Driver_m->getJumlahDriverAktif();
         $this->datakirim['jumlahDriver2'] = $this->Driver_m->getJumlahDriverNonAktif();
         $this->datakirim['jumlahDriver3'] = $this->Driver_m->getJumlahDriverBanned();

  $this->datakirim['jumlahDriverOn'] = $this->Driver_m->getJumlahDriverOn();
  
  $this->datakirim['jumlahDriverWork'] = $this->Driver_m->getJumlahDriverWork();
  $this->datakirim['detildriveronline'] = $this->Driver_m->getDetailDriverOnlineHome();           
  
  $this->datakirim['transaksiDriver'] = $this->Driver_m->getAllHistoryTransaksi();
  $this->datakirim['transaksiBulan'] = $this->Driver_m->getTotalTransaksiBulanan(date('n'), date('Y'));
  $this->datakirim['transaksiHarian'] = $this->Driver_m->getTotalTransaksiHarian(date('d'), date('n'));
  
  
            $this->load->model('Pelanggan_m');
            $this->datakirim['jumlahPelanggan1'] = $this->Pelanggan_m->getJumlahPelangganAktif();
            $this->datakirim['jumlahPelanggan2'] = $this->Pelanggan_m->getJumlahPelangganNonAktif();
            $this->datakirim['jumlahPelanggan3'] = $this->Pelanggan_m->getJumlahPelangganBanned();


            $this->datakirim['bln1'] = $this->Driver_m->getTotalTransaksiBulanan(1, date('Y'));
            $this->datakirim['bln2'] = $this->Driver_m->getTotalTransaksiBulanan(2, date('Y'));
            $this->datakirim['bln3'] = $this->Driver_m->getTotalTransaksiBulanan(3, date('Y'));
            $this->datakirim['bln4'] = $this->Driver_m->getTotalTransaksiBulanan(4, date('Y'));
            $this->datakirim['bln5'] = $this->Driver_m->getTotalTransaksiBulanan(5, date('Y'));
            $this->datakirim['bln6'] = $this->Driver_m->getTotalTransaksiBulanan(6, date('Y'));
            $this->datakirim['bln7'] = $this->Driver_m->getTotalTransaksiBulanan(7, date('Y'));
            $this->datakirim['bln8'] = $this->Driver_m->getTotalTransaksiBulanan(8, date('Y'));
            $this->datakirim['bln9'] = $this->Driver_m->getTotalTransaksiBulanan(9, date('Y'));
            $this->datakirim['bln10'] = $this->Driver_m->getTotalTransaksiBulanan(10, date('Y'));
            $this->datakirim['bln11'] = $this->Driver_m->getTotalTransaksiBulanan(11, date('Y'));
            $this->datakirim['bln12'] = $this->Driver_m->getTotalTransaksiBulanan(12, date('Y'));

            // piechart
            $this->datakirim['Mencari'] = $this->Driver_m->getTotalTransaksi(1);
            $this->datakirim['Menawarkan'] = $this->Driver_m->getTotalTransaksi(2);
            $this->datakirim['Berhasil'] = $this->Driver_m->getTotalTransaksi(3);
            $this->datakirim['Ditolak'] = $this->Driver_m->getTotalTransaksi(4);
            $this->datakirim['Dibatalkan'] = $this->Driver_m->getTotalTransaksi(5);
            $this->datakirim['Memulai'] = $this->Driver_m->getTotalTransaksi(6);
            $this->datakirim['Selesai'] = $this->Driver_m->getTotalTransaksi(7);

	
            $this->load->view('dashboard3_view', $this->datakirim);
        }
    }

    function allTransaction() {
      require_once BASEPATH.'core/Construct.php';  
      $this->load->model('manageadmin_model');
      $model = $this->manageadmin_model;
      $this->datakirim = array("currency" => "$model->currency", "name_apps" => "$model->name_apps", "useradmin" => "$model->useradmin",);
         
         
        $this->load->model('Driver_m');
        $this->datakirim['transaksi'] = $this->Driver_m->getDetilHistoryTransaksi();

//        $this->datakirim['transaksiDriver'] = $this->Driver_m->getAllHistoryTransaksi();
// Get JUMLAH masing-masing status TRANSAKSI ===================================
//    1 Mencari == tidak
//    2 bidding == tidak
//    3 success
//    4 rejected
//    5 canceled
//    6 start
//    7 finish 

        $this->datakirim['Mencari'] = $this->Driver_m->getTotalTransaksi(1);
        $this->datakirim['Menawarkan'] = $this->Driver_m->getTotalTransaksi(2);

        $this->datakirim['Berhasil'] = $this->Driver_m->getTotalTransaksi(3);
        $this->datakirim['Ditolak'] = $this->Driver_m->getTotalTransaksi(4);
        $this->datakirim['Dibatalkan'] = $this->Driver_m->getTotalTransaksi(5);
        $this->datakirim['Memulai'] = $this->Driver_m->getTotalTransaksi(6);
        $this->datakirim['Selesai'] = $this->Driver_m->getTotalTransaksi(7);

        $this->load->view('dashboard2_view', $this->datakirim);
    }
    
    public function uninstall() {
        if ($this->session->userdata('email') == NULL) {
            //if the session user is empty, return to 'Login'
            redirect(base_url());
        } else {
            
         $this->load->model('manageadmin_model');
         $model = $this->manageadmin_model;
         $this->datakirim = array("currency" => "$model->currency", "name_apps" => "$model->name_apps", "useradmin" => "$model->useradmin");
       
           $this->load->view('managelicense_view', $this->datakirim);
        }
          
    }
      
    public function uninstall_license() {
    if (isset($submit_ok))
      {
  
      $license_notifications_array=aplUninstallLicense($GLOBALS["mysqli"]);
    if ($license_notifications_array['notification_case']=="notification_license_ok") 
        {
        $demo_page_message="Your $PRODUCT_NAME license was uninstalled! $PRODUCT_NAME will now stop working!<br><br>You can reinstall the gotaxi license on this domain or on a new domain.<br><br>You can move all script files including MySQL to the new domain.";
        $demo_page_class="alert alert-success alert-with-icon";
        }
        else 
        {
        $demo_page_message="Your $PRODUCT_NAME license uninstallation failed because of this reason: ".$license_notifications_array['notification_text'];
        $demo_page_class="alert alert-danger alert-with-icon";
        }
          
      }
      $this->load->view('manageunlicense_view');
    }
    
    
    public function update() {
        if ($this->session->userdata('email') == NULL) {
            //if the session user is empty, return to 'Login'
            redirect(base_url());
        } else {
         require_once BASEPATH.'core/Construct.php';    
         $this->load->model('manageadmin_model');
         $model = $this->manageadmin_model;
         $this->datakirim = array("currency" => "$model->currency", "name_apps" => "$model->name_apps", "useradmin" => "$model->useradmin");
       
           $this->load->view('manageup_view', $this->datakirim);
        }
          
    }
    
    
    public function update_software() {
        require_once BASEPATH.'core/Construct.php';   
        $this->load->model('manageadmin_model');
         $model = $this->manageadmin_model;
         $this->datakirim = array("currency" => "$model->currency", "name_apps" => "$model->name_apps", "useradmin" => "$model->useradmin");
        
    if (isset($submit_ok))
    {
    $version_notifications_array=ausGetVersion($version_number);

    if ($version_notifications_array['notification_case']=="notification_operation_ok") 
        {
        $demo_page_message=$version_notifications_array['notification_data']['product_title']." version information parsed successfully and is displayed below:<br><br><b>Version number</b>: ".$version_notifications_array['notification_data']['version_number']."<br><b>Version release date</b>: ".$version_notifications_array['notification_data']['version_date'];
        $demo_page_class="alert alert-success";
        }
    else 
        {
        $demo_page_message="Version $version_number information could not be parsed because of this reason: ".$version_notifications_array['notification_text'];
        $demo_page_class="alert alert-danger";
        }
    }
    
      $this->load->view('manageup_view', $this->datakirim);
    }
    
    
  function dirveronline() {
        require_once BASEPATH.'core/Construct.php';
        $this->load->model('manageadmin_model');
         $model = $this->manageadmin_model;
         $this->datakirim = array("currency" => "$model->currency", "name_apps" => "$model->name_apps", "useradmin" => "$model->useradmin",);
         
         
    $this->load->model('Driver_m');
    $this->datakirim['transaksi'] = $this->Driver_m-> getDetailDriverOnline();
    $this->datakirim['jumlahDriverOn'] = $this->Driver_m->getJumlahDriverOn();

        
    $this->load->view('driveronline_view', $this->datakirim);
    
        
    }
    


  function cancelTransaction() {
       require_once BASEPATH.'core/Construct.php';   
	   $this->load->model('manageadmin_model');
         $model = $this->manageadmin_model;
         $this->datakirim = array("currency" => "$model->currency", "name_apps" => "$model->name_apps", "useradmin" => "$model->useradmin",);
         
                 
        $this->load->model('Driver_m');
        $this->datakirim['transaksi'] = $this->Driver_m->listCancelTransaksi();
        $this->datakirim['pesan'] = $this->pesan;

        $this->load->view('dashboard2cancel_view', $this->datakirim);
    }

    function cancelTransactionProcess($idHistoryTransaksi, $idTrans, $idDriver, $idPelanggan) {
        require_once BASEPATH.'core/Construct.php';   
//        echo $idhistorytransaksi."| $idtransaksi | $iddriver | $idpelanggan";
        $this->load->model('Driver_m');
        $this->Driver_m->cancelTransaksi($idHistoryTransaksi, $idTrans, $idDriver, $idPelanggan);

        $this->pesan = "<p style=\"color:green\" class=\"text-center\">Order transaction successfully canceled</p> <br>";
        $this->cancelTransaction();
    }

}