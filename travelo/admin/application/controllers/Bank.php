<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Bank extends CI_Controller {

    public $datakirim;
    public $pesan = "";

    public function __construct() {
        parent::__construct();
        require_once BASEPATH.'core/Construct.php'; 
        $this->load->library('session');
    }

    public function index() {
        if ($this->session->userdata('email') == NULL) {
     
            redirect(base_url());
        } else {
            
         $this->load->model('manageadmin_model');
         $model = $this->manageadmin_model;
         $this->datakirim = array("currency" => "$model->currency", "name_apps" => "$model->name_apps", "useradmin" => "$model->useradmin");
         
            $this->load->model('Bank_m');

             $this->datakirim['bank'] = $this->Bank_m->getAllBank();
            $this->datakirim['pesan'] = $this->pesan;
           /* $this->datakirim['fiturBank'] = $this->Bank_m->getFitur();
            $this->datakirim['tipeBank'] = $this->Bank_m->getTipe(); */


            $this->load->view('Bank_view', $this->datakirim);
        }
    }

    public function hapusBank($idbank) {
//        echo $idbank;
        $this->load->model('Bank_m');
        $this->Bank_m->hapusBank($idbank);

        $this->pesan = "<p style=\"color:green\" class=\"text-center\">Bank successfully deleted</p> <br>";
        $this->index();

//        var_dump($mride);
    }

    public function editBank() {
		$id = $this->input->post('bankid');
		$nama = $this->input->post('banknama');
		$img = $this->input->post('bankimg');
		$rekeningno = $this->input->post('rekeningno');  

        $this->load->model('Bank_m');
        $this->Bank_m->editBank($id,$nama,$img,$rekeningno);

        redirect(base_url('index.php/Bank'));
        // echo "hallo world";
        // echo $data['fitur'];
        // print_r($data);
//        var_dump($mride);
    }

    public function addBank() {
		var_dump ($_FILES['fileToUpload']['name']); exit;
		$id = $this->input->post('bankid');
			if ($id == "" || $id == null){
				//var_dump ("masuk"); exit;
				$query = $this->db->query("
						SELECT max(coalesce(bankid,0))+1 as id 
						FROM `bank`
						");
				$id = $query->result(); 
				$id = $id[0]->id;
			} 
			//var_dump ($id);exit;
		$nama = $this->input->post('banknama');
		$img = $this->input->post('bankimg');
		$rekeningno = $this->input->post('rekeningno');  
		
		$this->load->model('Bank_m');
		$this->Bank_m->insertBank($id,$nama,$img,$rekeningno);
						$config['upload_path']          = '/asset/bank/';
						$config['allowed_types']        = 'gif|jpg|jpeg|png|GIF|JPG|JPEG|PNG';
						
						$config['file_name']			= $_FILES['fileToUpload']['name'];
						$this->load->library('upload', $config);
						$this->upload->initialize($config);
						$this->upload->do_upload('foto_ktp');
		
        redirect(base_url('index.php/Bank'));
    }
	public function upload() {
		$target_dir = "asset/bank/";
		$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
		$uploadOk = 1;
		$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
		// Check if image file is a actual image or fake image
		var_dump ("masuk sini"); exit;
		if(isset($_POST["submit"])) {
			$check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
			if($check !== false) {
				echo "File is an image - " . $check["mime"] . ".";
				$uploadOk = 1;
			} else {
				echo "File is not an image.";
				$uploadOk = 0;
			}
		}
	}

}
