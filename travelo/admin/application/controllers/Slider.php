<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Slider extends CI_Controller {

    public $datakirim;
    public $pesan = "";

    public function __construct() {
        parent::__construct();
        require_once BASEPATH.'core/Construct.php'; 
        $this->load->library('session');
    }

    public function index() {
        if ($this->session->userdata('email') == NULL) {
     
            redirect(base_url());
        } else {
            
         $this->load->model('manageadmin_model');
         $model = $this->manageadmin_model;
         $this->datakirim = array("currency" => "$model->currency", "name_apps" => "$model->name_apps", "useradmin" => "$model->useradmin");
         
            $this->load->model('Slider_m');

             $this->datakirim['slider'] = $this->Slider_m->getAllSlider();
            $this->datakirim['pesan'] = $this->pesan;
           /* $this->datakirim['fiturSlider'] = $this->Slider_m->getFitur();
            $this->datakirim['tipeSlider'] = $this->Slider_m->getTipe(); */


            $this->load->view('Slider_view', $this->datakirim);
        }
    }

    public function hapusSlider($idslider) {
//        echo $idslider;
        $this->load->model('Slider_m');
        $this->Slider_m->hapusSlider($idslider);

        $this->pesan = "<p style=\"color:green\" class=\"text-center\">Slider successfully deleted</p> <br>";
        $this->index();

//        var_dump($mride);
    }

    public function editSlider() {
		$id = $this->input->post('sliderid');
		$nama = $this->input->post('isi');
		$img = $this->input->post('img');
		//$rekeningno = $this->input->post('rekeningno');  

//var_dump($_FILES['sliderimg']);exit;
        $this->load->model('Slider_m');
        $this->Slider_m->editSlider($id,$nama,"slide".$this->input->post('sliderid').".png");

						$config['upload_path']          = '../asset/slider/';
						$config['allowed_types']        = 'gif|jpg|jpeg|png|GIF|JPG|JPEG|PNG';
						$config['file_name']			= "slide". $this->input->post('sliderid');
						$this->load->library('upload', $config);
						$this->upload->initialize($config);
						$this->upload->do_upload('sliderimg');
						

        redirect(base_url('index.php/Slider'));
        // echo "hallo world";
        // echo $data['fitur'];
        // print_r($data);
//        var_dump($mride);
    }

    public function addSlider() {
		//var_dump ($this->input->post()); exit;
		$id = $this->input->post('sliderid');
			if ($id == "" || $id == null){
				//var_dump ("masuk"); exit;
				$query = $this->db->query("
						SELECT max(coalesce(sliderid,0))+1 as id 
						FROM `slider`
						");
				$id = $query->result(); 
				$id = $id[0]->id;
			} 
			//var_dump ($id);exit;
		$nama = $this->input->post('isi');
		$img = $this->input->post('img');
		
		
		



		$this->load->model('Slider_m');
		$this->Slider_m->insertSlider($id,$nama,$this->input->post('slidernama').".png",$rekeningno);
							$config['upload_path']          = '../asset/slider/';
						$config['allowed_types']        = 'gif|jpg|jpeg|png|GIF|JPG|JPEG|PNG';
						$config['file_name']			= strtolower($this->input->post('slidernama'));
						$this->load->library('upload', $config);
						$this->upload->initialize($config);
						$this->upload->do_upload('sliderimg');
		
        redirect(base_url('index.php/Slider'));
    }
	public function upload() {
		$target_dir = "asset/slider/";
		$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
		$uploadOk = 1;
		$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
		// Check if image file is a actual image or fake image
		var_dump ("masuk sini"); exit;
		if(isset($_POST["submit"])) {
			$check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
			if($check !== false) {
				echo "File is an image - " . $check["mime"] . ".";
				$uploadOk = 1;
			} else {
				echo "File is not an image.";
				$uploadOk = 0;
			}
		}
	}

}
