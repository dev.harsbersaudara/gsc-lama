<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Atribut extends CI_Controller {

    public $datakirim;
    public $pesan = "";

    public function __construct() {
        parent::__construct();
        require_once BASEPATH.'core/Construct.php';
        $this->load->library('session');
    }

    public function index() {
        if ($this->session->userdata('email') == NULL) {

            redirect(base_url());
        } else {

         $this->load->model('manageadmin_model');
         $model = $this->manageadmin_model;
         $this->datakirim = array("currency" => "$model->currency", "name_apps" => "$model->name_apps", "useradmin" => "$model->useradmin");

            $this->load->model('Atribut_m');

             $this->datakirim['atribut'] = $this->Atribut_m->getAllAtribut();
            $this->datakirim['pesan'] = $this->pesan;
           /* $this->datakirim['fiturAtribut'] = $this->Atribut_m->getFitur();
            $this->datakirim['tipeAtribut'] = $this->Atribut_m->getTipe(); */


            $this->load->view('Atribut_view', $this->datakirim);
        }
    }

    public function hapusAtribut($idatribut) {
//        echo $idatribut;
        $this->load->model('Atribut_m');
        $this->Atribut_m->hapusAtribut($idatribut);

        $this->pesan = "<p style=\"color:green\" class=\"text-center\">Atribut successfully deleted</p> <br>";
        $this->index();

//        var_dump($mride);
    }

    public function editAtribut() {
		// print_r($_FILES['atributimg']); exit;
		$id = $this->input->post('atributid');
		$nama = $this->input->post('atributnama');
		$img = $this->input->post('atributimg');
		$atributharga = $this->input->post('atributharga');
		$ext = pathinfo($_FILES['atributimg']['name'], PATHINFO_EXTENSION);

//var_dump($_FILES['atributimg']);exit;
        $this->load->model('Atribut_m');
        $this->Atribut_m->editAtribut($id,$nama,date("YmdHis").".".$ext,$atributharga);

		$config['upload_path']          = '../asset/atribut/';
		$config['allowed_types']        = 'gif|jpg|jpeg|png|GIF|JPG|JPEG|PNG';
		$config['file_name']			= date("YmdHis");
		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		$this->upload->do_upload('atributimg');


        redirect(base_url('index.php/Atribut'));
        // echo "hallo world";
        // echo $data['fitur'];
        // print_r($data);
//        var_dump($mride);
    }

    public function addAtribut() {
		// print_r($_FILES['atributimg']['name']); exit;
		$id = $this->input->post('atributid');
			if ($id == "" || $id == null){
				//var_dump ("masuk"); exit;
				$query = $this->db->query("
						SELECT max(coalesce(atributid,0))+1 as id
						FROM `atribut`
						");
				$id = $query->result();
				$id = $id[0]->id;
			}

			//var_dump ($id);exit;
		$nama = $this->input->post('atributnama');
		$img = $_FILES['atributimg']['name'];
		$atributharga = $this->input->post('atributharga');
		$ext = pathinfo($_FILES['atributimg']['name'], PATHINFO_EXTENSION);



		$this->load->model('Atribut_m');
		$this->Atribut_m->insertAtribut($id,$nama,date("YmdHis").".".$ext,$atributharga);
		$config['upload_path']          = '../asset/atribut/';
		$config['allowed_types']        = 'gif|jpg|jpeg|png|GIF|JPG|JPEG|PNG';
		$config['file_name']			= date("YmdHis");
		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		$this->upload->do_upload('atributimg');

        redirect(base_url('index.php/Atribut'));
    }
	public function upload() {
		$target_dir = "asset/atribut/";
		$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
		$uploadOk = 1;
		$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
		// Check if image file is a actual image or fake image
		var_dump ("masuk sini"); exit;
		if(isset($_POST["submit"])) {
			$check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
			if($check !== false) {
				echo "File is an image - " . $check["mime"] . ".";
				$uploadOk = 1;
			} else {
				echo "File is not an image.";
				$uploadOk = 0;
			}
		}
	}

}
