<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Drivertopup extends CI_Controller
{

    public $datakirim;
    public $pesan = "";

    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
    }

    public function index()
    {
        if ($this->session->userdata('email') == null) {
            //bila session user kosong balik ke 'Login'
            redirect(base_url());
        } else {


            $this->load->model('manageadmin_model');
            $model = $this->manageadmin_model;
            $this->datakirim = array("currency" => "$model->currency", "name_apps" => "$model->name_apps", "useradmin" => "$model->useradmin");



            $this->load->model("Topup_m");
            $this->datakirim['topupdriver'] = $this->Topup_m->getDriverTopupTpay();
            $this->datakirim['pesan'] = $this->pesan;

            // print_r($this->datakirim['topupdriver']);exit;

            $this->load->view('Drivertopuptpay_view', $this->datakirim);
        }
    }

}
