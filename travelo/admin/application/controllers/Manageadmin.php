<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Manageadmin extends CI_Controller {

    public $datakirim = array();

    public function __construct() {
        parent::__construct();
        $this->load->library('session');
    }

    public function index() {
        if ($this->session->userdata('email') == NULL) {
            //bila session user kosong balik ke 'Login'
            redirect(base_url());
        } else {

            $this->load->model('manageadmin_model');
            $model = $this->manageadmin_model;

            $this->datakirim = array( "nik" => "$model->nik",
                "email" => "$model->email","currency" => "$model->currency", "name_apps" => "$model->name_apps", "useradmin" => "$model->useradmin", "pesan" => "");

            $this->load->view('manageadmin_view', $this->datakirim);
        }
    }
s

    public function check() {
        $this->load->model('manageadmin_model');
        $model = $this->manageadmin_model;
        $nik = $model->nik;
        $passDB = $model->password;

        $passlamano = ($_POST['passlama']);
        $passno = ($_POST['passbaru']);
        $repassbaruno = ($_POST['repassbaru']);

        $passlama = hash("sha256", $passlamano);
        $passbaru = hash("sha256", $passno);
        $repassbaru = hash("sha256", $repassbaruno);


       if ($passDB != $passlama) {
       $this->datakirim = array(
             "pesan" => "<p style=\"color:red\" class=\"text-center\">The old   password you have submitted is incorrect</p> <br>",
          "nik" => "$model->nik",
          "email" => "$model->email",
           "name_apps" => "$model->name_apps",
         "useradmin" => "$model->useradmin"
        );

           $this->load->view('manageadmin_view', $this->datakirim);


       } else


        if ($passbaru != $repassbaru) {
            $this->datakirim = array(
                "pesan" => "<p style=\"color:red\" class=\"text-center\">The new password you entered does not match</p> <br>",
                "nik" => "$model->nik",
                "email" => "$model->email",
                "name_apps" => "$model->name_apps",
                "useradmin" => "$model->useradmin"

            );

            $this->load->view('manageadmin_view', $this->datakirim);
        } else {
            $model->setData($nik, $passbaru);
            $this->datakirim = array(
                "pesan" => "<p style=\"color:green\" class=\"text-center\">Password successfully updated</p> <br>",
                "nik" => "$model->nik",
                "email" => "$model->email",
                "name_apps" => "$model->name_apps",
                "useradmin" => "$model->useradmin"
            );

            $this->load->view('manageadmin_view', $this->datakirim);
        }
    }



     public function nameapps() {
     if ($this->session->userdata('email') == NULL) {

            redirect(base_url());
        } else {
            $this->load->model('manageadmin_model');
            $model = $this->manageadmin_model;

            $this->datakirim = array(
                "message" => "",
                "nik" => "$model->nik",
                "name_apps" => "$model->name_apps",
                "useradmin" => "$model->useradmin"
            );

            $this->load->view('manageapps_view', $this->datakirim);
        }
    }


     public function checkname() {
        $this->load->model('manageadmin_model');
        $model = $this->manageadmin_model;

        $nik = $model->nik;
        $namalama = $_POST['namalama'];

        $model->setDataNameWeb($nik, $namalama);

        $this->datakirim = array(
                "nik" => "$model->nik",
                "useradmin" => "$model->useradmin",
                "name_apps" => "$namalama",
                "message" => "<p style=\"color:green\" class=\"text-center\">Name Apps successfully updated</p> <br>",

            );

            $this->load->view('manageapps_view', $this->datakirim);
    }

    public function nameprofile() {
     if ($this->session->userdata('email') == NULL) {

            redirect(base_url());
        } else {
            $this->load->model('manageadmin_model');
            $model = $this->manageadmin_model;

            $this->datakirim = array(
                "message" => "",
                "nik" => "$model->nik",
                "name_apps" => "$model->name_apps",
                "useradmin" => "$model->useradmin"
            );

            $this->load->view('manageprofile_view', $this->datakirim);
        }
    }


     public function checkprofile() {
        $this->load->model('manageadmin_model');
        $model = $this->manageadmin_model;

        $nik = $model->nik;
        $namalama = $_POST['namalama'];

        $model->setDataNameProfile($nik, $namalama);

        $this->datakirim = array(
                "nik" => "$model->nik",
                "name_apps" => "$model->name_apps",
                "useradmin" => "$namalama",
                "message" => "<p style=\"color:green\" class=\"text-center\">Name Profile successfully updated</p> <br>",

            );

            $this->load->view('manageprofile_view', $this->datakirim);
    }



}
