<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Massageservices extends CI_Controller {

    public $datakirim;
    public $pesan = "";

    public function __construct() {
        parent::__construct();
        require_once BASEPATH.'core/Construct.php'; 
        $this->load->library('session');
    }

    public function index() {
        if ($this->session->userdata('email') == NULL) {
            //bila session user kosong balik ke 'Login'
            redirect(base_url());
        } else {
        // view name apps
		 $this->load->model('manageadmin_model');
         $model = $this->manageadmin_model;
         $this->datakirim = array(
                "currency" => "$model->currency",
                "name_apps" => "$model->name_apps",
                "useradmin" => "$model->useradmin",
            );
            
            
            $this->load->model('Layananpijat_m');

            $this->datakirim['layananpijat'] = $this->Layananpijat_m->getAllLayananPijat();
            $this->datakirim['pesan'] = $this->pesan;

            $this->load->view('Layananpijat_view', $this->datakirim);
        }
    }

    public function insertLayanan() {
        $layanan = $this->input->post('layanan');
		$harga = $this->input->post('harga');
        $namafoto = time() . "_" . $_FILES["userfile"]['name'];

        $pathfilesave = $_SERVER['DOCUMENT_ROOT'] . "/admin/foto_pijat/";

        echo "$namafoto";

        // UPLOAD FILE ke server
        if ($_FILES["userfile"]['name'] != NULL) {
            //            hapus foto lama 
            //            unlink("$pathfiledelete");
            //upload foto
            $config['file_name'] = $namafoto;
            $config['upload_path'] = $pathfilesave;
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size'] = '1000';

            $this->load->library('upload', $config);
            $this->upload->do_upload();
        }

        // Isert ke database
        $this->load->model('Layananpijat_m');
        $this->Layananpijat_m->insertLayanan($layanan, $harga, $namafoto);

        $this->pesan = "<p style=\"color:green\" class=\"text-center\">Successful service added</p> <br>";
        $this->index();
    }

    public function editLayananForm($idLayanan) {
        	// view name apps
		 $this->load->model('manageadmin_model');
         $model = $this->manageadmin_model;
         $this->datakirim = array(
                "currency" => "$model->currency",
                "name_apps" => "$model->name_apps",
                "useradmin" => "$model->useradmin",
            );
         
        $this->load->model('Layananpijat_m');
         $this->load->model('FeatureGotaxi_m');
        $d = $this->FeatureGotaxi_m->getMmassage();
        $this->biaya = $d[0]['biaya'];
        $this->biayaminimum = $d[0]['biaya_minimum'];
        $this->keterangan_biaya = $d[0]['keterangan_biaya'];
        
        

//   set persentase
        $this->load->model('ProporsiBiaya_m');
        $proporsi = $this->ProporsiBiaya_m->getPersentaseDriverBox($d[0]['id']);
        $persentase = 100 - $proporsi[0]['persentase_driver'];
        
        $this->datakirim['id'] = $d[0]['id'];
        $this->datakirim['persentase'] = "$persentase";
		$this->datakirim['keterangan_biaya'] = "$this->keterangan_biaya";
		
		
		
        $this->datakirim['layanan'] = $this->Layananpijat_m->getLayanan($idLayanan);
		$this->datakirim['harga'] = $this->Layananpijat_m->getLayanan($idLayanan);

        $this->load->view('Layananpijat2_view', $this->datakirim);
    }
	
	public function hapusLayanan($id, $foto) {

//        echo $id."|".$foto;
//        hapus entry database 
        $this->load->model('Layananpijat_m');

//        hapus file 
        $pathfiledelete = $_SERVER['DOCUMENT_ROOT'] . "/admin/foto_pijat/$foto";
        if (unlink("$pathfiledelete")) {
            $this->Layananpijat_m->hapusLayanan($id);
            $this->pesan = "<p style=\"color:green\" class=\"text-center\">Data successfully deleted</p> <br>";
            $this->index();
        } else {
            $this->pesan = "<p style=\"color:red\" class=\"text-center\">Service data and photos failed to delete, please contact Admin</p> <br>";
            $this->index();
        }
    }

    public function updateLayananPijat() {
        $id = $this->input->post('id');
        $layanan = $this->input->post('layanan');
		$harga = $this->input->post('harga');
        $namafotolama = $this->input->post('fotolama');
        $namafotobaru = time() . "_" . $_FILES["userfile"]['name'];

        $pathfiledelete = $_SERVER['DOCUMENT_ROOT'] . "/admin/foto_pijat/$namafotolama";
        $pathfilesave = $_SERVER['DOCUMENT_ROOT'] . "/admin/foto_pijat/";
        
        
        

        // UPLOAD FILE ke server
        if ($_FILES["userfile"]['name'] != NULL) {
            
         
            unlink("$pathfiledelete");
            
            //upload foto
            $config['file_name'] = $namafotobaru;
            $config['upload_path'] = $pathfilesave;
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size'] = '1000';

            $this->load->library('upload', $config);
            $this->upload->do_upload();
            
            
            
        // baru 1-5-2019    
        $ids = 6; // id for GO-MASSAGE
        $persentase = 100 - $_POST['persentase'];
        $this->load->model('ProporsiBiaya_m');
        $this->ProporsiBiaya_m->updateProportsiDriverMassage($ids, $persentase);
        

            // Update database
            $this->load->model('Layananpijat_m');
            $this->Layananpijat_m->updateLayananPijat1($id, $layanan, $harga, $namafotobaru);

            $this->pesan = "<p style=\"color:green\" class=\"text-center\">Successfully edited categories</p> <br>";
            $this->index();
            
            
        } else {
            // Update database
            $this->load->model('Layananpijat_m');
            $this->Layananpijat_m->updateLayananPijat2($id, $layanan, $harga);
            
       // baru 1-5-2019    
        $ids = 6; // id for GO-MASSAGE
        $persentase = 100 - $_POST['persentase'];
        $this->load->model('ProporsiBiaya_m');
        $this->ProporsiBiaya_m->updateProportsiDriverMassage($ids, $persentase);
        

            $this->pesan = "<p style=\"color:green\" class=\"text-center\">Successfully edited categories</p> <br>";
            $this->index();
        } 
    }

}