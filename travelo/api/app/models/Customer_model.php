<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Customer_model extends MY_Model {


    function __construct() {
        parent::__construct();
    }

    public function check_phone_number($number) {
        $cek = $this->db->query("SELECT id FROM customer where phone='$number'");
        if ($cek->num_rows() == 1) {
            return true;
        } else {
            return false;
        }
    }

    public function check_exist($email) {
        $cek = $this->db->query("SELECT id FROM customer where email='$email'");
        if ($cek->num_rows() == 1) {
            return true;
        } else {
            return false;
        }
    }

    public function check_banned($email) {
        $stat =  $this->db->query("SELECT id FROM customer WHERE status='3' AND email='$email'");
        if($stat->num_rows() == 1){
            return true;
        }else{
            return false;
        }
    }

    public function get_data_customer($condition){
        $this->db->select('customer.*, wallet.wallet');
        $this->db->from('customer');
        $this->db->join('wallet', 'customer.id = wallet.id_user');
        $this->db->where($condition);
        return $this->db->get();
    }

    public function signup($data_signup){
        $signup = $this->db->insert('customer', $data_signup);
        
        $dataIns = array(
            'id_user'=>$data_signup['id'],
            'wallet'=>0
        );
        $inswallet = $this->db->insert('wallet', $dataIns);
        return $signup;
    }

    public function edit_profile($data, $email){
        
        $this->db->where('email', $email);
        $edit = $this->db->update('customer', $data);
        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;        
        }
    }
    
    public function check_password($condition){
        $this->db->select('id');
        $this->db->from('customer');
        $this->db->where($condition);
        $cek = $this->db->get();
        if($cek->num_rows() > 0){
            return true;
        }else{
            return false;
        }
    }

    public function count_user(){
        $this->db->select('count(id) as count');
        $this->db->from('customer');
        return $this->db->get();
    }

    public function logout($id){
        $data = array(
            'reg_id' => '0'
        );
        
        $this->db->where('id', $id);
        $logout = $this->db->update('customer', $data);
        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;        
        }
    }
    
    public function get_wallet($id){
        $this->db->select('wallet');
        $this->db->from('wallet');
        $this->db->where('id_user', $id);
        $wallet = $this->db->get();
        return $wallet;
    }

    public function check_available_voucher($cond_voucher){
        $this->db->select('*');
        $this->db->from('voucher');
        $this->db->where($cond_voucher);
        $cek = $this->db->get();
        return $cek;
    }
    
    public function redeem_voucher($cond_voucher){
        $this->db->select('number');
        $this->db->from('redeemed_voucher');
        $this->db->where($cond_voucher);
        $cek = $this->db->get();
        if($cek->num_rows() == 0){
            $red_voucher = $this->db->insert('redeemed_voucher', $cond_voucher);
            if($this->db->affected_rows() == 1){
                $get_data = $this->get_data_redeem($cond_voucher);
                return array(
                        'status'=> true,
                        'data' => $get_data
                    );
            }else{
                return array(
                        'status'=> false,
                        'data' => []
                    );
            }
        }else{
            return array(
                    'status'=> false,
                    'data' => 'has redeemed'
                );
        }
    }
    
    public function get_data_redeem($red_voucher){
        $this->db->select(
                'redeemed_voucher.id_customer,'
                .'redeemed_voucher.id_voucher,'
                .'voucher.voucher,'
                .'voucher.tanggal_expired,'
                .'voucher.keterangan');
        $this->db->from('redeemed_voucher');
        $this->db->join('voucher', 'redeemed_voucher.id_voucher = voucher.id');
        $this->db->where($red_voucher);
        $cek = $this->db->get();
        return $cek;
    }
    
    public function get_banner_promotion(){
        
        $url_photo = base_url().'../admin/photopromotion/';
        
        $this->db->select(''
                . 'id,'
                . 'feature_promotion,'
                . "CONCAT('$url_photo', photo, '') as photo");
        $this->db->from('promotion');
        $this->db->where('is_show', 'yes');
        $promo = $this->db->get();
        return $promo;
    }
    
    public function verify_topup($data_topup){
        $verify = $this->db->insert('topup', $data_topup);
        if($this->db->affected_rows() == 1){
            return true;
        }else{
            return false;
        }
    }
    
    
    public function verify_pulsa($data_pulsa){
        $verify = $this->db->insert('userpulsa', $data_pulsa);
        if($this->db->affected_rows() == 1){
            return true;
        }else{
            return false;
        }
    }    
    
    
    public function get_history_complete($data_user){
        $url_photo = base_url().'../admin/photodriver/';
        $this->db->select('transaction.id as id_transaction,'
                . 'driver.id as id_driver,'
                . 'feature_gotaxi.feature as order_feature,'
                . 'transaction.start_latitude,'
                . 'transaction.start_longitude,'
                . 'transaction.end_latitude,'
                . 'transaction.end_longitude,'
                . 'transaction.waktu_order,'
                . 'transaction.waktu_selesai,'
                . 'transaction.waktu_selesai,'
                . 'transaction.alamat_asal,'
                . 'transaction.alamat_tujuan,'
                . 'transaction.harga,'
                . 'transaction.jarak,'
                . 'status_transaction.status_transaction as status,'
                . 'driver.nama_depan as nama_depan_driver,'
                . 'driver.nama_belakang as nama_belakang_driver,'
                . 'driver.no_telepon,'
                . "CONCAT('$url_photo', driver.photo, '') as photo,"
                . 'driver.rating,'
                . 'kendaraan.*');
        $this->db->join('history_transaction', 'transaction.id = history_transaction.id_transaction');
        $this->db->join('status_transaction', 'status_transaction.id = history_transaction.status');
        $this->db->join('driver', 'driver.id = history_transaction.id_driver');
        $this->db->join('feature_gotaxi', 'feature_gotaxi.id = transaction.order_feature');
        $this->db->join('kendaraan', 'driver.kendaraan = kendaraan.id');
        $this->db->where($data_user);
        $this->db->order_by('history_transaction.number DESC');
        $this->db->where("(history_transaction.status = '5' OR history_transaction.status = '7' OR history_transaction.status = '4')", NULL, FALSE);
        $history = $this->db->get('transaction', 10, 0);
        return $history;
    }
    
    public function get_history_incomplete($data_user){
        $url_photo = base_url().'../admin/photodriver/';
        
        $this->db->select('transaction.id as id_transaction,'
                . 'driver.id as id_driver,'
                . 'feature_gotaxi.feature as order_feature,'
                . 'transaction.start_latitude,'
                . 'transaction.start_longitude,'
                . 'transaction.end_latitude,'
                . 'transaction.end_longitude,'
                . 'transaction.waktu_order,'
                . 'transaction.waktu_selesai,'
                . 'transaction.waktu_selesai,'
                . 'transaction.alamat_asal,'
                . 'transaction.alamat_tujuan,'
                . 'transaction.harga,'
                . 'transaction.jarak,'
                . 'status_transaction.status_transaction as status,'
                . 'driver.nama_depan as nama_depan_driver,'
                . 'driver.nama_belakang as nama_belakang_driver,'
                . 'driver.no_telepon,'
                . "CONCAT('$url_photo', driver.photo, '') as photo,"
                . 'driver.rating,'
                . 'driver.reg_id,'
                . 'kendaraan.*');
        $this->db->join('history_transaction', 'transaction.id = history_transaction.id_transaction');
        $this->db->join('status_transaction', 'status_transaction.id = history_transaction.status');
        $this->db->join('driver', 'driver.id = history_transaction.id_driver');
        $this->db->join('feature_gotaxi', 'feature_gotaxi.id = transaction.order_feature');
        $this->db->join('kendaraan', 'driver.kendaraan = kendaraan.id');
        $this->db->where($data_user);
        $this->db->where("(history_transaction.status = '1' OR history_transaction.status = '3' OR history_transaction.status = '6')", NULL, FALSE);
        $this->db->order_by('transaction.id', 'DESC');
        $history = $this->db->get('transaction', 10, 0);
        return $history;
    }
    
    function get_biaya(){
        $this->db->select('id as id_feature,'
                . 'feature,'
                . 'biaya,'
                . 'keterangan_biaya,'
                . 'biaya_minimum,'
                . 'keterangan');
        
        $biaya = $this->db->get('feature_gotaxi');
        $tempBiayaAll = array();
        $tempBiaya = array();
        $queryDiskon = $this->diskon_mpay();
        
        $arrDiskon = array();
        $arrBiayaAkhir = array();
        foreach($queryDiskon->result() as $row){
            $dis = (100.0 - $row->nilai)/100;
            if($dis == 1){
                $dis = "1.0";
            }
            array_push($arrDiskon, $row->nilai."%");
            array_push($arrBiayaAkhir, $dis);
        }
            
        $i = 0;
        foreach($biaya->result() as $row){
            $tempBiaya = array(
                'id_feature' => $row->id_feature,
                'feature' => $row->feature,
                'biaya' => $row->biaya,
                'biaya_minimum' => $row->biaya_minimum,
                'keterangan_biaya' => $row->keterangan_biaya,
                'keterangan' => $row->keterangan,
                'diskon' => $arrDiskon[$i],
                'biaya_akhir' => $arrBiayaAkhir[$i]
            );
            array_push($tempBiayaAll, $tempBiaya);
            $i++;
        }
        
        $this->db->select('nilai');
        $this->db->where('voucher', 'MPAYDISKON1');
        $diskon = $this->db->get('voucher');
        $disk = array(
            'diskon'=> $diskon->row('nilai')."%",
            'biaya_akhir'=> (100 - $diskon->row('nilai'))/100
        );
        
        $this->db->select('*');
        $this->db->from('biaya_promo');
        $this->db->where('feature', '3');
        $promo_mfood = $this->db->get('voucher');
        $promo = array(
            'id_feature'=> $promo_mfood->row('feature'),
            'biaya'=> $promo_mfood->row('biaya'),
            'biaya_minimum'=> $promo_mfood->row('biaya_minimum'),
            'keterangan_biaya'=> $promo_mfood->row('keterangan_biaya'),
            'diskon'=> $arrDiskon[8],
            'biaya_akhir'=>$arrBiayaAkhir[8]
        );
        $data = array(
            'feature' => $tempBiayaAll,
            'promo_mfood' => $promo,
            'diskon' => $disk
        );
        
        return $data;
    }
    
    function diskon_mpay(){
//        $disk = $this->db->query("SELECT * FROM voucher WHERE voucher LIKE 'MPAYDISKON%'");
        $this->db->select('*');
        $this->db->from('voucher');
        $this->db->where("voucher LIKE 'MPAYDISKON%'");
        $this->db->order_by('id', 'asc');
        $disk = $this->db->get();
        
        $arrDisk = array();
        
        foreach($disk->result() as $row){
            $diskmpay = array(
                'feature' => $row->untuk_feature,
                'diskon' => $row->nilai."%",
                'biaya_akhir' => (100 - $row->nilai)/100
            );
            array_push($arrDisk, $diskmpay);
        }
        return $disk;
    }
    
    function insert_help($data){
        $ins = $this->db->insert('help_customer', $data);
        return $ins;
    }
    
    function getVersions(){
        $this->db->select('version');
        $this->db->where('application', 0);
        $this->db->from('app_versions');
        $getV = $this->db->get();
        return $getV->row('version');
    }
}
