<?php
/*==============================================================================
	Item Name: GoTaxi App - On Demand All in One App Services Android
	Version: 1.0.6
	Author: Androgo Design
	Author URL: https://codecanyon.net/item/gotaxi-on-demand-all-in-one-app-services-android/22612350
	Attention: Don't modify this FILE because it will error the script.
================================================================================
*/

//Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce semper consequat ante non pretium. Nunc pellentesque consequat lorem, eget pulvinar quam varius quis. Quisque vitae semper mauris, a venenatis mauris. Etiam quis porttitor nisi, sed ultricies dolor. Aliquam quis risus eu lectus eleifend tristique nec non dui. Fusce vestibulum placerat turpis. Curabitur ac convallis enim, malesuada consectetur augue.
require_once("gtx_config.php");
require_once("gtx_settings.php");

//Suspendisse vitae laoreet ipsum. Praesent eu tortor non odio sollicitudin scelerisque. Nulla rutrum magna at magna bibendum convallis. Donec fringilla convallis justo et euismod. Phasellus dui ex, cursus eget metus sit amet, iaculis viverra nibh. Nullam sed feugiat justo. Integer sem nibh, egestas nec aliquet vitae, lobortis sed dui. Suspendisse fermentum volutpat sagittis. Integer varius justo in laoreet lobortis.
$home_title="Home Page";
$page_title="Start Page";
$demo_page_message="$PRODUCT_NAME is a web-based android application powered by Codeigniter. You can use the script $PRODUCT_NAME but before continuing, you must verify the purchase code that is forwarded to the envato center.<br><br>Please note: You may not enter the same purchase code or the wrong purchase code 5 times, because your domain will be blocked.<br><br>Click of links Install License to get started.";
$demo_page_class="alert alert-info alert-with-icon";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title><?php echo "$page_title | $ORIGINAL_PRODUCT_NAME"; ?></title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
    <link rel="preload" href="asset/css/bootstrap-license.min.css" as="style" />
    <link rel="preload" href="asset/css/now-ui-dashboard.min.css" as="style" />
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />

</head>
<body>
    <div class="wrapper ">
        <div class="sidebar" data-color="orange">
            <div class="logo">
                <a href="<?php echo $ORIGINAL_PRODUCT_URL; ?>" class="simple-text logo-normal" target="_blank"><?php echo $PRODUCT_NAME; ?></a>
            </div>
            <div class="sidebar-wrapper">
                <ul class="nav">
                    <li class="active"><a href="gtx_home.php"><i class="now-ui-icons media-1_button-play"></i><p>Start Page</p></a></li>
                    <li><a href="install.php"><i class="now-ui-icons objects_key-25"></i><p>Install License</p></a></li>
                </ul>
            </div>
        </div>
        <div class="main-panel">
            <nav class="navbar navbar-expand-lg navbar-transparent  navbar-absolute bg-primary fixed-top">
                <div class="container-fluid">
                    <div class="navbar-wrapper">
                        <div class="navbar-toggle">
                            <button type="button" class="navbar-toggler">
                                <span class="navbar-toggler-bar bar1"></span>
                                <span class="navbar-toggler-bar bar2"></span>
                                <span class="navbar-toggler-bar bar3"></span>
                            </button>
                        </div>
                    </div>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation"></button>
                </div>
            </nav>
            <div class="panel-header panel-header-sm">
            </div>
            <div class="content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h5 class="title"><?php echo $ORIGINAL_PRODUCT_NAME; ?></h5>
                            </div>
                            <div class="card-body">
                                <div class="<?php echo $demo_page_class; ?>" data-notify="container">
                                    <span data-notify="icon" class="now-ui-icons ui-1_bell-53"></span>
                                    <span data-notify="message"><?php echo $demo_page_message; ?></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card card-chart">
                            <div class="card-header">
                                <h4 class="card-title"><?php echo $page_title; ?></h4>
                            </div>
                            <div class="card-body">
                                <a href="install.php"><b>Install License</b></a> &raquo; Installs script for licensed users.<br><br><br>
                                
                                <h4 class="card-title"><?php echo $home_title; ?></h4>
                                <li><a href="index.php" target='_blank'><b>Frontend</b></a> &raquo; This is the main web address that can be seen by visitors.</li><br>
                                
                                 <li><a href="admin/index.php" target='_blank'><b>Admin Panel</b></a>  &raquo; This is the main web address that can be seen by admin.</li><br>
                                
                                 <li><a href="partner/index.php" target='_blank'><b>Restaurant admin panel</b></a> &raquo; This is the main web address that can be seen by restaurant owner.</li><br><br><br>
                                
                                
                                To <b>integrate</b> <?php echo $ORIGINAL_PRODUCT_NAME; ?>  into in your web and android, refer to '<b>/DOCUMENTATION</b>' directory inside download package for step-by-step instructions.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <footer class="footer">
                <div class="container-fluid">
                    <div class="copyright">
                        <?php echo "<a href='$ORIGINAL_PRODUCT_URL' title='$ORIGINAL_PRODUCT_NAME' target='_blank'>$ORIGINAL_PRODUCT_NAME</a> by <a href='https://gotaxi.biz' title='Androgo' target='_blank'>Androgo</a>"; ?>
                    </div>
                </div>
            </footer>
        </div>
    </div>
</body>
<script src="asset/js/core/jquery.min.js"></script>
<script src="asset/js/core/popper.min.js"></script>
<script src="asset/js/core/bootstrap.min.js"></script>
<script src="asset/js/now-ui-dashboard.min.js"></script>
</html>
