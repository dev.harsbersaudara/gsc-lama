<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Tpay extends CI_Controller {

    public $datakirim;
    public $namaresto;
    public $idmitra;
    public $idresto;

    function __construct() {
        parent::__construct();
       
        $this->load->library('session');
        $this->load->library('Pdf');
        
        // $this->load->model('Makanan');
        $this->load->model('Tpay_m');

        // var_dump($this->session->userdata);

        $this->idmitra = $this->session->userdata('idmitra');
        $this->idresto = $this->session->userdata('idresto');

        $namaresto = $this->session->userdata('nama');
        $this->namaresto = $namaresto;
    }

    public function index() {
        if ($this->idmitra != NULL) {
            $this->datakirim['namaresto'] = $this->namaresto;
            $this->datakirim['pesan'] = $this->pesan;
            
            //load model
            $this->load->model('Makanan');
            $this->load->model('Tpay_m');
            // var_dump($this->idmitra);exit;

            // data transaction tpay
            $tpay = $this->Tpay_m->getTransactionList("R".$this->idmitra);
            // var_dump($tpay);exit;
            $data = $this->db->query("select * from mitra_mmart_mfood a, restoran b, kategori_resto c WHERE a.lapak = b.id AND b.kategori_resto = c.id AND a.id = ".$this->idmitra);
            $d = $data->result_array();
            // data saldo tpay
            $saldo = $this->Tpay_m->getSaldo();
            // var_dump($saldo);exit;
            $saldo->saldo = number_format((float)$saldo->saldo, 2, '.', '');
            $this->datakirim = array(
                "nama_resto" => $d[0]['nama_resto'],
                "kategori" => $d[0]['kategori'],
                "alamat" => $d[0]['alamat'],
                "jam_buka" => $d[0]['jam_buka'],
                "jam_tutup" => $d[0]['jam_tutup'],
                "deskripsi_resto" => $d[0]['deskripsi_resto'],
                "kontak_telepon" => $d[0]['kontak_telepon'],
                "foto" => $d[0]['foto_resto'],
                "namaresto" => $namaresto,
                "jumlahmakanan" => $jumlahmakanan[0]['jumlah'],
                "saldo" => $saldo->saldo,
                "tpay_no" => $d[0]['telepon_penanggung_jawab'],
                "tpay" => $tpay,
                "telepon_penanggung_jawab" => $d[0]['telepon_penanggung_jawab']
            );

            $this->load->view('tpay_view', $this->datakirim);
        } else {
            header('Location: ' . base_url());
        }
    }

    public function print_qr(){
        if ($this->idmitra != NULL) {
            $this->datakirim['namaresto'] = $this->namaresto;
            $this->datakirim['pesan'] = "$this->pesan";
            // var_dump($this->datakirim);exit;
            $data = $this->db->query("select * from mitra_mmart_mfood a, restoran b, kategori_resto c WHERE a.lapak = b.id AND b.kategori_resto = c.id AND a.id = ".$this->idmitra);
            $d = $data->result_array();
            $this->datakirim = array(
                "nama_resto" => $d[0]['nama_resto'],
                "kategori" => $d[0]['kategori'],
                "alamat" => $d[0]['alamat'],
                "jam_buka" => $d[0]['jam_buka'],
                "jam_tutup" => $d[0]['jam_tutup'],
                "deskripsi_resto" => $d[0]['deskripsi_resto'],
                "kontak_telepon" => $d[0]['kontak_telepon'],
                "foto" => $d[0]['foto_resto'],
                "namaresto" => $namaresto,
                "jumlahmakanan" => $jumlahmakanan[0]['jumlah'],
                "saldo" => $saldo->saldo,
                "tpay" => $tpay,
                "telepon_penanggung_jawab" => $d[0]['telepon_penanggung_jawab']
            );
            // $qr_code = "https://chart.googleapis.com/chart?cht=qr&chs=200x200&chl=tpay|merchant|".$kontak_telepon;
            // print_r("dasdsaad");exit;
            $this->load->view('tpay_print_qr_view', $this->datakirim);
        } else {
            header('Location: ' . base_url());
        }
        
    }

}
