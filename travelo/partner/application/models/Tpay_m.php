<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Tpay_m extends CI_Model {

    function __construct() {
        $this->load->database();
        $this->load->library('session');
    }

    function getSaldo() {
        
        $idresto = $this->session->userdata('idresto');

        $query = $this->db->select("*")
                        ->from("saldo")
                        ->where(['id_user'=>'R'.$idresto]);
        return $query->get()->row();
    }

    public function get_customer($param){
        $this->db->select('*');
        $this->db->from('pelanggan');
        $this->db->where($param);
        $this->db->limit(1);
        return $this->db->get()->row();
    }

    public function get_driver($param){
        $this->db->select('*');
        $this->db->from('driver');
        $this->db->where($param);
        $this->db->limit(1);
        return $this->db->get()->row();
    }

    function getTransactionList($userid, $limit = 100, $page = 1){
        $offset = ($page - 1) * $limit;

        $this->db->select('trx.*')
                ->from('transaksi_tpay trx')
                ->where('trx.user_sender' , $userid)
                ->or_where('trx.user_receiver' , $userid)
                ->where('trx.is_deleted' , 0)
                ->limit($limit, $offset)
                ->order_by('trx.tanggal_transaksi', 'ASC');

        $data = $this->db->get()->result();

        $result;
        foreach ($data as $key => $value) {
            $result[$key]->id = $value->id;
            $result[$key]->tipe_transaksi_id = $value->tipe_transaksi_id;
            $result[$key]->tanggal_transaksi = $value->tanggal_transaksi;
            $result[$key]->no_transaksi = $value->no_transaksi;
            $result[$key]->nama_transaksi = $value->nama_transaksi;
            
            if ($value->user_sender == $userid) {
                $result[$key]->amount =  -$value->amount;
                if ($value->tipe_transaksi_id == 13) {
                    $cond = array('id'=>$value->user_receiver);
                    $subject = $this->get_customer($cond);
                    $result[$key]->subject = 'ke ' . ucwords($subject->nama_depan) . ' ' . ucwords($subject->nama_belakang);
                } else if ($value->tipe_transaksi_id == 15) {
                    $cond = array('id'=>$value->user_receiver);
                    $subject = $this->get_customer($cond);
                    $result[$key]->subject = 'ke ' . ucwords($subject->nama_depan) . ' ' . ucwords($subject->nama_belakang);
                } else if ($value->tipe_transaksi_id == 16) {
                    $cond = array('merchant.id'=>str_replace("R","",$value->user_receiver));
                    $subject = $this->get_merchant($cond);
                    $result[$key]->subject = 'ke ' . ucwords($subject->nama_resto);
                }
            } else {    
                $result[$key]->amount = +$value->amount;
                if ($value->tipe_transaksi_id == 13) {
                    $cond = array('id'=>$value->user_sender);
                    $subject = $this->get_customer($cond);
                    $result[$key]->subject = 'dari ' . ucwords($subject->nama_depan) . ' ' . ucwords($subject->nama_belakang);
                } else if ($value->tipe_transaksi_id == 15) {
                    $cond = array('id'=>$value->user_sender);
                    $subject = $this->get_driver($cond);
                    $result[$key]->subject = 'dari ' . ucwords($subject->nama_depan) . ' ' . ucwords($subject->nama_belakang);
                } else if ($value->tipe_transaksi_id == 16) {
                    $cond = array('id'=>$value->user_sender);
                    $subject = $this->get_customer($cond);
                    if (empty($subject)){
                        $subject = $this->get_driver($cond);
                    }
                    $result[$key]->subject = 'dari ' . ucwords($subject->nama_depan) . ' ' . ucwords($subject->nama_belakang);
                }
            }  
                
        }

       

        return $result;
    }

}

?>
