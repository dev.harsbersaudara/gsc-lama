<?php
    $pdf = new Pdf('P', 'mm', 'A6', true, 'UTF-8', false);
    $pdf->SetCreator(PDF_CREATOR);
    // $pdf->SetMargins(0,20,0,0);
    $pdf->SetTopMargin(20);
    // $pdf->SetRightMargin(0);
    
    // $pdf->setFooterMargin(0);
    $pdf->SetAuthor('Author');
    $pdf->SetDisplayMode('real', 'default');
    $no_telp = $telepon_penanggung_jawab;
    $nama_merchant = $nama_resto;
    
    $pdf->SetPrintHeader(false);
    $pdf->AddPage();
    // $pdf->Image('https://travelo.live/asset/images/bg_doodle.webp', 0, 0,190.5 ,353 , 'WEBP');
    $pdf->SetPrintFooter(false);

    $html = '
    
        <table style="width: 100%; border-collapse: collapse;">
            <tbody>
                <tr>
                    <td colspan="3" style="height: 70px;text-align: center;">
                        <img src="https://travelo.live/asset/images/logo.png" width="323" height="74" />
                    </td>
                </tr>
    
                <tr>
                    <td colspan="3">
                        <h2 class="text-left" style="text-align: center; ">Scan melalui wallet TPay<br>yang bisa diakses melalui aplikasi Travelo&nbsp;
                        </h2>
                    </td>
                </tr>
    
                <tr>
                    <td colspan="3">
                        <p style="text-align: center;"><strong><img style="text-align: center; padding: 20px;" src="https://chart.googleapis.com/chart?cht=qr&chs=200x200&chl=tpay|merchant|'.$no_telp.'" alt="" width="300" height="300" /></strong></p>
                    </td>
                </tr>
    
                <tr>
                    <td style="padding-bottom: 30px;" colspan="3">
                        <h1 class="text-left" style="text-align: center;">'.$nama_merchant.'</h1><br><br><br>
                    </td>
                </tr>
    
                <tr style="background-color: #f44336; min-height: 80px;color:#ffffff; padding:50px">
                    <td style="width: 33%; text-align: center;  font-family:MV Boli; font"><br><br>1. Scan<br></td>
                    <td style="width: 32%; text-align: center; "><br><br>2. Masukkan Nominal<br></td>
                    <td style="width: 33%; text-align: center; "><br><br>3. Bayar<br></td>
                </tr>
    
                <tr style="background-color: #f44336; color:#ffffff;">
                    <td style="text-align: center;"><img style="display: block; margin-left: auto; margin-right: auto;" src="https://travelo.live/asset/images/qr-code.png" width="63" height="63" /></td>
                    <td style="text-align: center;"><img style="display: block; margin-left: auto; margin-right: auto;" src="https://travelo.live/asset/images/input.png" width="63" height="63" /></td>
                    <td style="text-align: center;"><img style="display: block; margin-left: auto; margin-right: auto;" src="https://travelo.live/asset/images/sucess.png" width="63" height="63" /></td>
                </tr>
                <tr style="background-color: #f44336;  height: 60px;color:#ffffff">
                    <td style="text-align: center;padding-top: 25px;" colspan="3"><br><br><br>Copyright &copy; 2019 Travelo. All Rights Reserved<br>
                    </td>
                </tr>
            </tbody>
        </table>
    ';
    $pdf->writeHTML($html, true, false, true, false,'');
    $pdf->lastPage();
    $pdf->Output($nama_merchant.'-print.pdf', 'I');
?>