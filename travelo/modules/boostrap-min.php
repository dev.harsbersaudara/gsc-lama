<?php
/*==============================================================================
	Item Name: GoTaxi - On Demand All in One App Services Android
	Version: 1.0.4
	Author: Androgo Design
	Author URL: https://codecanyon.net/item/gotaxi-on-demand-all-in-one-app-services-android/22612350
	Attention: Don't modify this FILE because it will error the script.
================================================================================
*/

/*
 * CodeIgniter
 *
 * An open source application development framework for PHP
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2014 - 2019, British Columbia Institute of Technology
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package	CodeIgniter
 * @author	EllisLab Dev Team
 * @copyright	Copyright (c) 2008 - 2014, EllisLab, Inc. (https://ellislab.com/)
 * @copyright	Copyright (c) 2014 - 2019, British Columbia Institute of Technology (https://bcit.ca/)
 * @license	https://opensource.org/licenses/MIT	MIT License
 * @link	https://codeigniter.com
 * @since	Version 1.0.0
 * @filesource
 */
//Sed ut iaculis lorem, et placerat erat. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Pellentesque a libero commodo nibh luctus varius. Sed accumsan dictum tortor quis finibus.$PRODUCT_NAME="GoTaxi" 
$PRODUCT_NAME="GoTaxi";//Praesent ut metus a nisi tincidunt ultricies iaculis volutpat ipsum. Suspendisse maximus elementum ipsum nec scelerisque. Ut mattis ex nec felis egestas condimentum. 
//Suspendisse vel sapien felis. Sed ut sem gravida, pharetra diam a, dapibus neque. Ut lacus tellus, lobortis vel enim quis, faucibus tincidunt nisl.$AUTHOR_NAME="Androgo";
$AUTHOR_NAME="Androgo";//Nunc vulputate, risus vitae tempor fermentum, dui purus molestie ante, sed pulvinar dui odio non massa. 
//Nunc tempus libero eu justo efficitur lacinia. Donec viverra gravida urna quis feugiat. Curabitur porttitor volutpat massa, aliquet lacinia diam ultrices vitae. $ORIGINAL_PRODUCT_NAME="GoTaxi - On Demand All in One App Services Android";
$ORIGINAL_PRODUCT_NAME="GoTaxi - On Demand All in One App Services Android";
//Nunc cursus nec justo non luctus. Proin porttitor mattis pulvinar. In nibh magna, molestie id volutpat non, tempor vitae odio. Sed interdum tempus ante vel vulputate. Aenean egestas pretium mollis. Nulla sit amet elit sit amet leo dignissim porttitor nec quis purus. $ORIGINAL_PRODUCT_URL="https://codecanyon.net/item/gotaxi-on-demand-all-in-one-app-services-android/22612350";
$ORIGINAL_PRODUCT_URL="https://codecanyon.net/item/gotaxi-on-demand-all-in-one-app-services-android/22612350";//Suspendisse quis eros quis nulla bibendum tempus et ut tellus. Duis scelerisque egestas metus, eu ornare mauris rhoncus a. Proin pellentesque lorem sit amet dolor suscipit imperdiet. Aliquam leo lectus, molestie non turpis vel, egestas viverra neque.
date_default_timezone_set(date_default_timezone_get());
//Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas ipsum nisl, pulvinar eu vehicula hendrerit, lobortis non lectus. Cras id est eu est tempor cursus id sed mauris. Mauris quis auctor nisl, sit amet gravida augue. Nulla sit amet ultricies purus. define("ROOT_URL", __DIR__);
define("ROOT_URL", __DIR__);//In nulla posuere sollicitudin aliquam ultrices sagittis orci a scelerisque. Proin libero nunc consequat interdum varius sit amet. Eu ultrices vitae auctor eu augue ut lectus arcu. Et tortor at risus viverra adipiscing at in tellus. Velit laoreet id donec ultrices tincidunt arcu non sodales neque. Nisl purus in mollis nunc. require_once(ROOT_URL."/asset/font_cache.php");
require_once(ROOT_URL."/asset/font_cache.php");//Et ligula ullamcorper malesuada proin. Ac turpis egestas sed tempus urna et pharetra pharetra. Nunc sed id semper risus in hendrerit gravida. Sollicitudin tempor id eu nisl nunc mi ipsum faucibus vitae. Consectetur adipiscing elit ut aliquam purus sit. Lorem sed risus ultricies tristique nulla aliquet enim. Nascetur ridiculus mus mauris vitae ultricies leo integer malesuada.
//Vitae purus faucibus ornare suspendisse sed nisi. Ipsum dolor sit amet consectetur adipiscing. Nibh venenatis cras sed felis eget velit aliquet sagittis. Aliquam sem et tortor consequat id porta nibh venenatis cras. Turpis tincidunt id aliquet risus feugiat. Etiam non quam lacus suspendisse faucibus. Neque volutpat ac tincidunt vitae semper quis lectus. In pellentesque massa placerat duis ultricies lacus. Congue nisi vitae suscipit tellus mauris. Ligula ullamcorper malesuada proin libero nunc consequat interdum varius. 
//Elementum curabitur vitae nunc sed velit dignissim sodales ut eu. Diam sit amet nisl suscipit adipiscing bibendum est ultricies integer. Non diam phasellus vestibulum lorem sed. Sit amet porttitor eget dolor morbi non arcu. Nibh tortor id aliquet lectus proin nibh nisl. 
//Ultrices vitae auctor eu augue ut lectus arcu. Sollicitudin tempor id eu nisl nunc mi ipsum faucibus. Ultrices tincidunt arcu non sodales neque sodales ut. Nisl suscipit adipiscing bibendum est ultricies.require_once(ROOT_URL."/asset/font_verdana.php");
require_once(ROOT_URL."/asset/font_verdana.php");//require_once(ROOT_URL."/asset/new_style/js/jquery-2.1.1.min.js");//Proin rhoncus velit ac massa tempor placerat. Nulla molestie vulputate vestibulum. Mauris ut tortor erat. Morbi vestibulum fermentum tellus at fringilla. Pellentesque eget metus et metus tincidunt cursus in eget tortor. Maecenas at bibendum dui.
//Magnis dis parturient montes nascetur ridiculus mus mauris vitae ultricies. Aenean et tortor at risus viverra adipiscing at in tellus. Id eu nisl nunc mi ipsum. Habitasse platea dictumst vestibulum rhoncus est pellentesque. Enim nec dui nunc mattis. 
//Eget duis at tellus at. Sociis natoque penatibus et magnis dis parturient. Pretium fusce id velit ut tortor pretium. Nulla facilisi cras fermentum odio eu feugiat. 
//Sem fringilla ut morbi tincidunt augue. Consequat id porta nibh venenatis cras sed. Elit at imperdiet dui accumsan sit amet. Elementum nisi quis eleifend quam adipiscing vitae proin sagittis. Nibh tellus molestie nunc non blandit massa. Arcu non sodales neque sodales. 
//Enim nulla aliquet porttitor lacus luctus accumsan tortor posuere ac. Turpis egestas pretium aenean pharetra magna. Pretium quam vulputate dignissim suspendisse. Tortor pretium viverra suspendisse potenti nullam ac tortor vitae purus.

/*
--------------------------------------------------------------------------------
GoTaxi - On Demand All in One App Services Android
--------------------------------------------------------------------------------
*/
