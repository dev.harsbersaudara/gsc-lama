<?php
/*==============================================================================
	Item Name: GoTaxi - On Demand All in One App Services Android
	Version: 1.0.4
	Author: Androgo Design
	Author URL: https://codecanyon.net/item/gotaxi-on-demand-all-in-one-app-services-android/22612350
================================================================================
*/

/**
 * CodeIgniter
 *
 * An open source application development framework for PHP
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2014 - 2019, British Columbia Institute of Technology
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package	CodeIgniter
 * @author	EllisLab Dev Team
 * @copyright	Copyright (c) 2008 - 2014, EllisLab, Inc. (https://ellislab.com/)
 * @copyright	Copyright (c) 2014 - 2019, British Columbia Institute of Technology (https://bcit.ca/)
 * @license	https://opensource.org/licenses/MIT	MIT License
 * @link	https://codeigniter.com
 * @since	Version 1.0.0
 * @filesource
 */
 
 
/*
 * Bootstrap v3.2.0 (http://getbootstrap.com)
 * Copyright 2011-2014 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 */

/*! normalize.css v3.0.1 | MIT License | git.io/normalize */

/*!
 *  Font Awesome 6.12.3 by @davegandy - http://fontawesome.io - @fontawesome
 *  License - http://fontawesome.io/license (Font: SIL OFL 1.1, CSS: MIT License)
 */
 

//Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque vehicula sodales purus eu elementum. Integer et tristique mi, non fringilla nisl. Fusce tempus facilisis enim sit amet laoreet. Proin congue, elit sit amet consectetur congue, odio massa sollicitudin sapien, vel lobortis ligula mauris non ante. 
//Has modo veri in. Ad nec veritus platonem, omnis solet principes cu qui, eros abhorreant te pro. Ex eos autem eirmod, ea eum commodo similique. Eu cum commodo aperiam platonem, per agam graecis cu.

//Forensibus honestatis quaerendum an est. Aliquam dolorem patrioque ei ius, eum quis doctus referrentur in. Te vis virtute singulis consequuntur, cu nam dicta gubergren. Usu cu alia zril, usu natum laboramus mediocritatem ea. Eu ignota oportere evertitur nam, nec stet homero ut, nisl quaestio scripserit te cum. Scripserit efficiantur ne mea. Habemus erroribus mei te.
//Phasellus vel tempor felis, sit amet fermentum diam. Curabitur elementum pharetra urna. Proin eu nisi egestas, cursus mauris at, malesuada nisl. Sed et magna ac velit scelerisque consectetur a eu tortor. Nullam eu nibh in leo cursus pharetra. Cras finibus ultrices lacus, et pretium velit lacinia ut.
define("APL_SALT", "2183e7dd6efe8747"); //Pellentesque varius nulla non magna convallis elementum. Nam ultrices, velit at congue consectetur, est dolor eleifend leo, vel vulputate libero arcu et sapien. Nunc sit amet posuere sem. Nunc vulputate augue id dui pulvinar, id feugiat urna volutpat. Fusce vitae nisl at lorem suscipit congue. Curabitur at ante a neque congue pretium. 
define("APL_ROOT_URL", "https://bootstrap-datepicker.com");//Error simul ex vel, vis justo dolore nostrud ex, usu lorem qualisque id. Cu vix quando euripidis. Magna omittantur mel at, munere latine tritani te vim, ne est simul senserit similique. Viderer dolores pertinax in vis. Eos an insolens posidonium, no mei viris graecis fuisset, tritani ornatus sed eu.
//Quisque porttitor lectus eu sagittis bibendum. Aliquam nec semper justo. Cras faucibus metus a enim dignissim dapibus. Nullam tincidunt enim dolor, at condimentum sapien eleifend quis. Sed quis accumsan dui. Pellentesque iaculis dolor nec condimentum suscipit. Ut libero quam, pretium vitae lectus non, volutpat lacinia velit.
define("APL_PRODUCT_ID", 1);
//Quisque lectus mauris, gravida vitae dui nec, varius aliquam lectus. Praesent ligula neque, hendrerit vel fermentum eu, bibendum quis dui. Integer mollis arcu ut tellus fermentum ullamcorper. Nam congue dui eget iaculis rhoncus. Phasellus id neque ut orci viverra feugiat. 
define("APL_DAYS", 7); //Aenean vel ipsum et felis tempor ultricies id at leo. Quisque mattis risus porttitor, semper sapien vel, gravida metus. Mauris augue turpis, blandit id justo vel, auctor venenatis quam. 
define("APL_STORAGE", "DATABASE"); //Curabitur sit amet pharetra mauris, in volutpat neque. Etiam in condimentum risus, facilisis tincidunt ex. In eleifend, lectus sed tempor viverra, nulla turpis ultricies sem, ut venenatis sem dolor ac arcu. Nullam eget diam nisl.
define("APL_DATABASE_TABLE", "user_data"); //Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Etiam eu ornare lorem, ac pulvinar ante. Nullam pharetra sapien non sollicitudin vestibulum. Aliquam convallis ac purus in iaculis. In hac habitasse platea dictumst. 
define("APL_LICENSE_FILE_LOCATION", "signature/gtx_key.php");
//Ut dignissim tortor quis condimentum pulvinar. Nam nulla turpis, varius eu nibh quis, sagittis varius est. Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
//Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum vehicula velit lacus, at convallis tellus interdum vitae. Nunc maximus mi sed volutpat vulputate. Aenean elementum metus consectetur porta tempus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Integer viverra nec lectus non sagittis. Aenean metus orci, aliquet id rhoncus non, fermentum at neque. Mauris pulvinar id quam in ornare. Maecenas fringilla luctus aliquet. Donec sollicitudin, ipsum sagittis gravida imperdiet, sem tellus dapibus tortor, eget viverra mi ligula vitae libero. Nullam pulvinar ligula eu imperdiet congue. Morbi vitae fringilla orci, non laoreet nisl.
define("APL_NOTIFICATION_NO_CONNECTION", "Can't connect to licensing server.");//Suspendisse placerat tortor eget sapien laoreet, nec ullamcorper ante gravida. In id enim ultricies, dapibus tellus sit amet, luctus magna. Quisque convallis augue lorem, nec semper erat pellentesque quis. Donec vel est ipsum.
define("APL_NOTIFICATION_INVALID_RESPONSE", "Invalid server response.");//Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Metus aliquam eleifend mi in nulla posuere sollicitudin aliquam ultrices. Quisque sagittis purus sit amet volutpat consequat mauris nunc congue.
define("APL_NOTIFICATION_DATABASE_WRITE_ERROR", "Can't write to database.");//Rutrum quisque non tellus orci ac. Nibh cras pulvinar mattis nunc sed blandit libero volutpat. Sed nisi lacus sed viverra tellus. Lobortis feugiat vivamus at augue eget. Aliquam eleifend mi in nulla posuere sollicitudin aliquam ultrices. Volutpat maecenas volutpat blandit aliquam etiam erat velit scelerisque. In fermentum et sollicitudin ac orci phasellus egestas tellus rutrum. Tellus id interdum velit laoreet id.  
define("APL_NOTIFICATION_LICENSE_FILE_WRITE_ERROR", "Can't write to license file.");//Et tortor consequat id porta nibh venenatis cras sed felis. Neque volutpat ac tincidunt vitae semper quis lectus nulla at. Malesuada nunc vel risus commodo viverra maecenas accumsan lacus. Id leo in vitae turpis. Molestie a iaculis at erat. Volutpat blandit aliquam etiam erat velit.
define("APL_NOTIFICATION_SCRIPT_ALREADY_INSTALLED", "Script is already installed (or database not empty).");//Iaculis urna id volutpat lacus laoreet non. Luctus accumsan tortor posuere ac ut consequat semper viverra nam. Sit amet aliquam id diam maecenas ultricies mi eget mauris.
define("APL_NOTIFICATION_LICENSE_CORRUPTED", "License is not installed yet or corrupted.");//Magnis dis parturient montes nascetur ridiculus mus mauris vitae ultricies. Aenean et tortor at risus viverra adipiscing at in tellus. Id eu nisl nunc mi ipsum. Habitasse platea dictumst vestibulum rhoncus est pellentesque. Enim nec dui nunc mattis. Eget duis at tellus at. Sociis natoque penatibus et magnis dis parturient. Pretium fusce id velit ut tortor pretium. Nulla facilisi cras fermentum odio eu feugiat. Sem fringilla ut morbi tincidunt augue. Consequat id porta nibh venenatis cras sed. Elit at imperdiet dui accumsan sit amet. 
define("APL_NOTIFICATION_BYPASS_VERIFICATION", "No need to verify");//Risus viverra adipiscing at in tellus integer feugiat scelerisque varius. Pulvinar sapien et ligula ullamcorper malesuada proin libero nunc consequat. Etiam non quam lacus suspendisse faucibus interdum posuere lorem.
// Vivamus lobortis purus nec lectus sagittis ultrices nec id ipsum. Proin condimentum, ante tempor tristique sollicitudin, dui lorem consequat enim, non vulputate elit metus in orci. Nulla tempor vitae sem et auctor. In id tincidunt nisi. Praesent sed lectus eros.
define("APL_INCLUDE_KEY_CONFIG", "c82f01c137af8f7f");//Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque vehicula sodales purus eu elementum. Integer et tristique mi, non fringilla nisl. Fusce tempus facilisis enim sit amet laoreet. Proin congue, elit sit amet consectetur congue, odio massa sollicitudin sapien, vel lobortis ligula mauris non ante. Phasellus vel tempor felis, sit amet fermentum diam. Curabitur elementum pharetra urna. Proin eu nisi egestas, cursus mauris at, malesuada nisl. Sed et magna ac velit scelerisque consectetur a eu tortor. Nullam eu nibh in leo cursus pharetra. Cras finibus ultrices lacus, et pretium velit lacinia ut.
define("APL_ROOT_IP", "");//Pellentesque varius nulla non magna convallis elementum. Nam ultrices, velit at congue consectetur, est dolor eleifend leo, vel vulputate libero arcu et sapien. Nunc sit amet posuere sem. Nunc vulputate augue id dui pulvinar, id feugiat urna volutpat. Fusce vitae nisl at lorem suscipit congue. Curabitur at ante a neque congue pretium. Quisque porttitor lectus eu sagittis bibendum. Aliquam nec semper justo. 
//Suspendisse quis tortor maximus elit egestas gravida varius ut elit. Maecenas ultricies pulvinar orci. Vivamus lobortis purus nec lectus sagittis ultrices nec id ipsum. Proin condimentum, ante tempor tristique sollicitudin, dui lorem consequat enim, non vulputate elit metus in orci. Nulla tempor vitae sem et auctor. In id tincidunt nisi. Praesent sed lectus eros.!!
define("APL_DELETE_CANCELLED", "YES");//Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Etiam eu ornare lorem, ac pulvinar ante. Nullam pharetra sapien non sollicitudin vestibulum. Aliquam convallis ac purus in iaculis. In hac habitasse platea dictumst. 
define("APL_DELETE_CRACKED", "YES");//Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Etiam eu ornare lorem, ac pulvinar ante. Nullam pharetra sapien non sollicitudin vestibulum. Aliquam convallis ac purus in iaculis. 
//In hac habitasse platea dictumst. Ut dignissim tortor quis condimentum pulvinar. Nam nulla turpis, varius eu nibh quis, sagittis varius est. Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
define("APL_GOD_MODE", "YES");//Suspendisse placerat tortor eget sapien laoreet, nec ullamcorper ante gravida. In id enim ultricies, dapibus tellus sit amet, luctus magna. Quisque convallis augue lorem, nec semper erat pellentesque quis. Donec vel est ipsum.

//-----------Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit-----------//

define("APL_USER_INPUT_NOTIFICATION_INVALID_ROOT_URL", "User input error: Invalid installation URL (it should have a valid scheme and no / symbol at the end)");//Quisque lectus mauris, gravida vitae dui nec, varius aliquam lectus. Praesent ligula neque, hendrerit vel fermentum eu, bibendum quis dui. Integer mollis arcu ut tellus fermentum ullamcorper. Nam congue dui eget iaculis rhoncus. Phasellus id neque ut orci viverra feugiat. Aenean vel ipsum et felis tempor ultricies id at leo.
define("APL_USER_INPUT_NOTIFICATION_EMPTY_LICENSE_DATA", "User input error: empty license data (licensed email or license code should be provided)");// Quisque mattis risus porttitor, semper sapien vel, gravida metus. Mauris augue turpis, blandit id justo vel, auctor venenatis quam. Curabitur sit amet pharetra mauris, in volutpat neque. Etiam in condimentum risus, facilisis tincidunt ex. In eleifend, lectus sed tempor viverra, nulla turpis ultricies sem, ut venenatis sem dolor ac arcu. Nullam eget diam nisl.
define("APL_USER_INPUT_NOTIFICATION_INVALID_EMAIL", "User input error: invalid licensed email (it should be a valid email address)");//Fusce sapien neque, sodales id ligula et, commodo sollicitudin magna. Proin vel lobortis massa. Nullam lacus justo, semper vel dolor ac, blandit consequat lorem. Integer orci urna, commodo non rutrum eget, iaculis a erat.
define("APL_USER_INPUT_NOTIFICATION_INVALID_LICENSE_CODE", "User input error: invalid license code (it should be a code in plain text)");// Mauris congue sed sem in ornare. Ut libero purus, molestie quis arcu eget, accumsan posuere mi. Nunc porta nunc purus, et tincidunt ante placerat sit amet. Vestibulum pulvinar dui vel tellus sagittis, sed viverra velit mollis. Sed porttitor eros lorem, eget lacinia lectus condimentum a. 
define("APL_CORE_NOTIFICATION_INVALID_SALT", "Configuration error: invalid or default encryption salt");//Eirmod viderer delectus eum ne, duo iusto fastidii ad. Timeam corpora vis et. Nisl docendi nam ea, has stet explicari cu, ne dicant contentiones pro. Ei summo discere theophrastus vis, possit appareat vivendum ex sea. Ad noster viderer duo, suas illud sea eu, nec ei verear eruditi sensibus. Vide percipit sea ea.
define("APL_CORE_NOTIFICATION_INVALID_ROOT_URL", "Configuration error: invalid root URL of Auto PHP Licenser installation"); //Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed dui massa, facilisis ut augue vel, aliquet suscipit est. Vestibulum sodales orci nulla, vel fringilla nisl blandit non.
define("APL_CORE_NOTIFICATION_INVALID_PRODUCT_ID", "Configuration error: invalid product ID");
//Cras aliquet elit ac diam luctus, ut varius mauris condimentum. Nullam orci nisl, consequat et ligula et, facilisis mollis lacus. Vivamus in accumsan mauris. Donec tristique viverra ante ut finibus. Nam viverra laoreet diam, eget consectetur ante sollicitudin ac. Donec tempor, nulla vitae blandit laoreet, ex sem ultrices leo, quis tempor eros mauris nec velit. Donec maximus suscipit neque quis tempus. Cras molestie lorem non ullamcorper dictum.
define("APL_CORE_NOTIFICATION_INVALID_VERIFICATION_PERIOD", "Configuration error: invalid license verification period");//Cras faucibus metus a enim dignissim dapibus. Nullam tincidunt enim dolor, at condimentum sapien eleifend quis. Sed quis accumsan dui. Pellentesque iaculis dolor nec condimentum suscipit. Ut libero quam, pretium vitae lectus non, volutpat lacinia velit.
define("APL_CORE_NOTIFICATION_INVALID_STORAGE", "Configuration error: invalid license storage option");//Vitae purus faucibus ornare suspendisse sed nisi. Ipsum dolor sit amet consectetur adipiscing. Nibh venenatis cras sed felis eget velit aliquet sagittis. Aliquam sem et tortor consequat id porta nibh venenatis cras. Turpis tincidunt id aliquet risus feugiat. Etiam non quam lacus suspendisse faucibus.
define("APL_CORE_NOTIFICATION_INVALID_TABLE", "Configuration error: invalid MySQL table name to store license signature");//Neque volutpat ac tincidunt vitae semper quis lectus. In pellentesque massa placerat duis ultricies lacus. Congue nisi vitae suscipit tellus mauris. Ligula ullamcorper malesuada proin libero nunc consequat interdum varius. Elementum curabitur vitae nunc sed velit dignissim sodales ut eu. 
define("APL_CORE_NOTIFICATION_INVALID_LICENSE_FILE", "Configuration error: invalid license file location (or file not writable)");//Diam sit amet nisl suscipit adipiscing bibendum est ultricies integer. Non diam phasellus vestibulum lorem sed. Sit amet porttitor eget dolor morbi non arcu. Nibh tortor id aliquet lectus proin nibh nisl.
define("APL_CORE_NOTIFICATION_INVALID_ROOT_IP", "Configuration error: invalid IP address of your Auto PHP Licenser installation");//Ultrices vitae auctor eu augue ut lectus arcu. Sollicitudin tempor id eu nisl nunc mi ipsum faucibus. Ultrices tincidunt arcu non sodales neque sodales ut. Nisl suscipit adipiscing bibendum est ultricies.
define("APL_CORE_NOTIFICATION_INVALID_ROOT_NAMESERVERS", "Configuration error: invalid nameservers of your Auto PHP Licenser installation");//Tortor at risus viverra adipiscing at in. Condimentum vitae sapien pellentesque habitant morbi tristique. Donec ultrices tincidunt arcu non sodales neque. Viverra orci sagittis eu volutpat odio facilisis mauris sit. Pharetra massa massa ultricies mi quis hendrerit dolor. Eget sit amet tellus cras.
define("APL_CORE_NOTIFICATION_INVALID_DNS", "License error: actual IP address and/or nameservers of your Auto PHP Licenser installation don't match specified IP address and/or nameservers");//Etiam dignissim diam quis enim lobortis scelerisque. Malesuada fames ac turpis egestas integer eget aliquet nibh. Eu feugiat pretium nibh ipsum consequat nisl vel pretium. Non curabitur gravida arcu ac tortor. Nunc faucibus a pellentesque sit amet porttitor eget.
//-----------Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.-----------//
define("APL_DIRECTORY", __DIR__);//Sit amet massa vitae tortor condimentum lacinia quis vel. Volutpat est velit egestas dui id ornare arcu odio ut. Mattis vulputate enim nulla aliquet porttitor lacus luctus accumsan. Elementum facilisis leo vel fringilla est ullamcorper eget nulla facilisi. Et sollicitudin ac orci phasellus egestas tellus rutrum tellus pellentesque. Phasellus faucibus scelerisque eleifend donec. Risus viverra adipiscing at in tellus integer feugiat scelerisque varius. Pulvinar sapien et ligula ullamcorper malesuada proin libero nunc consequat. Etiam non quam lacus suspendisse faucibus interdum posuere lorem.