

(function ($) {	$(document).ready(function(){
// Do not delete above line
/****************************************************************/
/****************************************************************/


/***************************************************************
* (9) Rotate Testimonials *
****************************************************************/

$(".testi_slide").each(function(){

	var $this				= $(this),
		animationType		= $this.attr("data-animationType") || 'fade',
		autoSlideshow		= $this.attr("data-autoSlideshow") || true,
		smoothHeight		= $this.attr("data-smoothHeight") || true,
		pauseHover			= $this.attr("data-pauseHover") || true,
		displayNavigation	= $this.attr("data-displayNavigation") || true,
		slideshowSpeed		= parseInt($this.attr("data-slideshowSpeed") || 500),
		slideshowInterval	= parseInt($this.attr("data-slideshowInterval") || 4000);

	if(autoSlideshow == "true") { autoSlideshow = true; } else { autoSlideshow = false; }
	if(smoothHeight == "true") { smoothHeight = true; } else { smoothHeight = false; }
	if(pauseHover == "true") { pauseHover = true; } else { pauseHover = false; }
	if(displayNavigation == "true") { displayNavigation = true; } else { displayNavigation = false; }

	$(this).bxSlider({
		mode: animationType,
		auto:autoSlideshow,
		autoHover:pauseHover,
		adaptiveHeight: smoothHeight,
		adaptiveHeightSpeed:500,
		speed:slideshowSpeed,
		pause:slideshowInterval,
		controls:displayNavigation
	});
});

/****************************************************************/
/****************************************************************/
}); })(jQuery);
// Do not delete above line