<?php ob_start(); 
if (basename($_SERVER['SCRIPT_FILENAME']) == basename(__FILE__)){
echo "<p align=center><br><br><br><br><br><br><font size=\"6\" color=\"#FF0000\">ILLEGAL ACCESS !!";
echo "<meta http-equiv=\"refresh\" content=\"2; url=../index.php\">";
exit();} 
if (empty($_SERVER['HTTPS']) || $_SERVER['HTTPS'] === 'off') {
     $protocol = 'http://'; } else { $protocol = 'https://'; }
	 $base_url = $protocol . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
     $bsurl=base64_encode($base_url);	
if($db->config("maintenance") == 1){ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script>
     (adsbygoogle = window.adsbygoogle || []).push({
          google_ad_client: "ca-pub-8939934716039947",
          enable_page_level_ads: true
     });
</script>
    
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php echo $db->config("title"); ?></title>
<link href="images/banner/<?php echo $db->config("fcon"); ?>" rel="SHORTCUT ICON" />
<link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
<style>
 html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 70vh;
                margin: 0;
            }

            .full-height {
                height: 80vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 36px;
                padding: 20px;
            }
			.linetext {
                font-size: 18px;
				font-weight:bold;
				line-height:160%;
				margin-left:20px;
				margin-right:20px;
            }
			</style>
</head>
<body>
<div class="flex-center position-ref full-height">
<div class="content">
<img src="images/maintenance.png" style="max-width:932px; width:100%;">
<div class="linetext">
<?php echo $db->config("maintenance_info"); ?>
</div>
</div>
</div>
</body>
</html>
<?php } else { ?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="copyright" content="Copyright (c) <?php echo date('Y'); ?> | <?php echo $domain; ?>" />
<meta name="author" content="<?php echo $domain; ?>"/>
<meta name="description" content="<?php echo $descriptionweb; ?>" />
<meta name="keywords" content="<?php echo $keywordweb; ?>" />
<meta name="robots" content="all,index,follow" />
<link href="./images/banner/<?php echo $db->config("fcon"); ?>" rel="SHORTCUT ICON" />
<title><?php echo $db->config("title"); ?></title>
<meta charset="utf-8" /><meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />

<link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" />
<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet" />
<link href="css_gate/font-awesome.min6434.css?180503" rel="stylesheet" />
<link href="css_gate/bootstrap.min6434.css?180503" rel="stylesheet" />
<link href="css_gate/common6434.css?180503" rel="stylesheet" />
<link href="scss/offcanvas-menu.min6434.css?180503" rel="stylesheet" />
<link href="scss/submenu.min6434.css?180503" rel="stylesheet" />
<link href="scss/master.min6434.css?180503" rel="stylesheet" />

    <link href="css_gate/animate.min.css" rel="stylesheet" />
    <link href="css_gate/imagehover.min.css" rel="stylesheet" />
    <script src="js/noframework.waypoints.min.js"></script>
<script src='https://www.google.com/recaptcha/api.js'></script>

    <style>
        /* BOF: Extra small devices (portrait phones, less than 576px) */
		
        .carousel-top-margin {
            margin-top: 0 !important;
        }

        body {
            background-color: #fff;
            font-size: 1em;
        }

        .title-carousel {
            font-size: 1.2em;
            font-weight: bold;
        }

        .title-carousel2 {
            font-size: .6em;
        }

        .ximg {
            flex-shrink: 0;
            min-width: 100%;
            height: auto;
            background: rgba(0, 0, 0, 0.1);
        }

        .xcard {
            height: 50vh;
            width: 100%;
            cursor: pointer;
        }

        .xtext-xs {
            position: absolute;
            background-color: rgba(0, 0, 100, 0.30);
            color: white;
            font-size: 1em;
            text-align: center;
            text-decoration: underline;
            text-transform: uppercase;
            white-space: nowrap;
            padding: 2rem;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
            -ms-transform: translate(-50%, -50%);
            text-align: center;
        }

        .btn-highlight {
            background-color: #006600;
            border-radius: 2rem;
            border-color: #006600;
            color: white;
            font-size: .7em;
            padding: 1rem;
            width: 35vw;
            text-align: center;
            padding-top: 0.525rem;
        }

        .company-box {
            background-size: cover;
            height: 150px;
            cursor: pointer;
        }

        /** card zoom **/
        .xzoom {
            transition: transform .2s;
            margin: 0 auto;
        }

            .xzoom:hover {
                transform: scale(1.1); /* (150% zoom - Note: if the zoom is too large, it will go outside of the viewport) */
            }

        .shadow {
            box-shadow: 0 2px 1px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
        }

        .xintro {
            font-size: .8em;
        }

        .highlight-title {
            padding-top: 1rem;
            font-size: 1.3em;
            font-weight: bold;
            color: #134e72;
        }

        .bg-img:before {
            content: '';
            position: absolute;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            background-image: linear-gradient(to bottom right, #000, #000);
            opacity: .5;
        }

        .xCardEventlLabel {
            font-size: 1.3em;
            font-weight: bold;
        }

        /* figure caption */
        [class*=" imghvr-"] figcaption, [class^=imghvr-] figcaption {
            background-color: rgba(0, 0, 0, 0.45) !important;
        }

        .xborder {
            border-left: none !important;
            border-right: none !important;
        }


        .xcard-date {
            border: 1px solid white;
            width: 4rem;
            text-align: center;
            height: 6rem;
            background-color: rgba(0, 28, 99, 0.55);
        }


        .xcontainer {
            position: relative;
        }

        .ximage {
            opacity: 1;
            display: block;
            width: 100%;
            height: auto;
            transition: .5s ease;
            backface-visibility: hidden;
            object-fit: cover; /* Do not scale the image */
            object-position: center; /* Center the image within the element */
            cursor: pointer;
        }

        .xmiddle {
            transition: .5s ease;
            opacity: 0.9;
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
            -ms-transform: translate(-50%, -50%);
            text-align: center;
        }

        .xcontainer:hover .ximage {
            opacity: 0.3;
        }

        .xcontainer:hover .xmiddle {
            opacity: 1;
        }

        .xtext {
            background-color: rgba(0, 0, 100, 0.30);
            color: white;
            font-size: 2em;
            text-align: center;
            text-decoration: underline;
            text-transform: uppercase;
            position: relative;
            white-space: nowrap;
            text-shadow: 0 0 3px #000;
            font-weight: bold;
            padding: 2rem;
        }

            .xtext a {
                text-decoration: none;
                color: white;
            }


        .xtext-xs a {
            text-decoration: none;
            color: white;
        }

        .center-cropped {
            object-fit: cover; /* Do not scale the image */
            object-position: center; /* Center the image within the element */
        }

        .centered {
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
        }

        .xborder {
            border-left: 1px solid #e6e6e6 !important;
            border-right: 1px solid #e6e6e6 !important;
        }

        #caro-home {
            width: 100%;
            padding-left: 0;
            padding-right: 0;
            margin-left: 0;
            margin-right: 0;
        }
        /* EOF: Extra small devices (portrait phones, less than 576px) */


        /* BOF: Small devices (landscape phones, 576px and up) */
        @media (min-width: 576px) and (orientation : portrait) {

            .carousel-top-margin {
                margin-top: 8vh;
            }

            .xmiddle {
                transition: .5s ease;
                opacity: 1;
                position: absolute;
                top: 50%;
                left: 50%;
                transform: translate(-50%, -50%);
                -ms-transform: translate(-50%, -50%);
                text-align: center;
            }

            .xtext {
                background-color: rgba(0, 0, 100, 0.30);
                color: white;
                font-size: 1.1em;
                text-align: center;
                text-decoration: underline;
                text-transform: uppercase;
                position: relative;
                white-space: nowrap;
                padding: 1rem;
            }

            .company-box {
                background-size: cover;
                height: 350px;
                cursor: pointer;
            }

            .xborder {
                border-left: none !important;
                border-right: none !important;
            }
        }
        /* EOF: Small devices (landscape phones, 576px and up) */


        /*BOF: iphone x landscape */
        @media (width: 812px) and (orientation : landscape) {
            #caro-home {
                width: 75vw;
            }
        }


        /* BOF:  Medium devices (tablets, 768px and up) portrait */
        @media (min-width: 768px) and (orientation:portrait) {
            .carousel-top-margin {
                margin-top: 0 !important;
                padding-top: 0 !important;
            }


            body {
                /*padding-top: 6rem;*/
                background-color: #fff;
                font-size: 1em;
            }

            .title-carousel {
                font-size: 2em;
                font-weight: bold;
            }

            .title-carousel2 {
                font-size: 1em;
            }

            .ximg {
                flex-shrink: 0;
                min-width: 100%;
                height: 30vh;
            }

            .xcard {
                height: 50vh;
                width: 100%;
                cursor: pointer;
            }

            .btn-highlight {
                background-color: #006600;
                border-radius: 2rem;
                border-color: #006600;
                color: white;
                font-size: 0.8em;
                padding: 1rem;
                width: 15vw;
                text-align: center;
                padding-top: 0.525rem;
            }

            .company-box {
                background-size: cover;
                height: 350px;
                cursor: pointer;
            }

            .xborder {
                border-left: 1px solid #e6e6e6 !important;
                border-right: 1px solid #e6e6e6 !important;
            }


            #caro-home {
                width: 80%;
                padding-left: 0;
                padding-right: 0;
                margin-left: 0;
                margin-right: 0;
            }
        }
        /* BOF:  Medium devices (tablets, 768px and up) portrait */


        /* BOF:  Medium devices (tablets, 768px and up) landscape */
        @media (min-width: 768px) and (orientation:landscape) {
            .carousel-top-margin {
                margin-top: 0 !important;
                padding-top: 0 !important;
            }


            body {
                /*padding-top: 6rem;*/
                background-color: #fff;
                font-size: 1em;
            }

            .title-carousel {
                font-size: 2em;
                font-weight: bold;
            }

            .title-carousel2 {
                font-size: 1em;
            }

            .ximg {
                flex-shrink: 0;
                min-width: 100%;
                height: 30vh;
            }

            .xcard {
                height: 50vh;
                width: 100%;
                cursor: pointer;
            }

            .btn-highlight {
                background-color: #006600;
                border-radius: 2rem;
                border-color: #006600;
                color: white;
                font-size: 0.8em;
                padding: 1rem;
                width: 15vw;
                text-align: center;
                padding-top: 0.525rem;
            }

            .company-box {
                background-size: cover;
                height: 350px;
                cursor: pointer;
            }

            .xborder {
                border-left: 1px solid #e6e6e6 !important;
                border-right: 1px solid #e6e6e6 !important;
            }
        }
        /* BOF:  Medium devices (tablets, 768px and up) landscape*/



        /* BOF: Large devices (desktops, 992px and up) */
        @media (min-width: 992px) {

            .carousel-top-margin {
                margin-top: 0 !important;
                padding-top: 0 !important;
            }


            body {
                font-size: 1em;
            }

            .title-carousel {
                font-size: 3.2em;
                font-weight: bold;
            }

            .title-carousel2 {
                font-size: 1.3em;
            }

            .ximg {
                flex-shrink: 0;
                min-width: 100%;
                height: 45vh;
            }

            .xcard {
                height: 45vh;
                min-width: 100%;
                cursor: pointer;
                margin-left: 1rem;
                margin-right: 1rem;
            }

            .btn-highlight {
                background-color: #006600;
                border-radius: 2rem;
                border-color: #006600;
                color: white;
                font-size: 0.8em;
                padding: 1rem;
                width: 15vw;
                text-align: center;
                padding-top: 0.525rem;
            }

            .company-box {
                background-size: cover;
                height: 350px;
                cursor: pointer;
            }
        }
        /* EOF: Large devices (desktops, 992px and up) */



        /*// Extra large devices (large desktops, 1200px and up)*/
        @media (min-width: 1200px) {
            .carousel-top-margin {
                margin-top: 0 !important;
                padding-top: 0 !important;
            }


            body {
                font-size: 1em;
            }

            .title-carousel {
                font-size: 3.2em;
                font-weight: bold;
            }

            .title-carousel2 {
                font-size: 1.3em;
            }

            .ximg {
                flex-shrink: 0;
                min-width: 100%;
                height: 45vh;
            }

            .xcard {
                height: 45vh;
                min-width: 100%;
                cursor: pointer;
                margin-left: 1rem;
                margin-right: 1rem;
            }

            .btn-highlight {
                background-color: #006600;
                border-radius: 2rem;
                border-color: #006600;
                color: white;
                font-size: 0.8em;
                padding: 1rem;
                width: 15vw;
                text-align: center;
                padding-top: 0.525rem;
            }

            .company-box {
                background-size: cover;
                height: 350px;
                cursor: pointer;
            }
        }
    </style>
</head>
<body style="font-family:Montserrat;">
   
<div class="aspNetHidden">
</div>

<div class="aspNetHidden">

</div>

        <!-- BOF: mobile device menu-->
        <div class="navbar navbar-dark p-0 m-0  d-block d-lg-none" style="color: black;">
            <div class="row p-0 m-0 w-100">
                <div class="col-12  ">
                    <div class="d-flex align-items-center m-2">
                        <a href="index.html">
                            
                            <img src="images/home/logo.png" class="img-fluid" style="max-height: 25px;" />
                        </a>
                        <div class="w-100 text-right">
                           
                            <div class="d-inline-block align-items-center  align-self-center p-1" id="icon-hamburger">
                                <img src="images/home/icon-mobile-circlemenu.png" class="img-fluid" style="height: 30px;" />
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- EOF: mobile device menu-->

         
<!-- TradingView Widget BEGIN -->
<div class="tradingview-widget-container">
  <div class="tradingview-widget-container__widget"></div>
  <div class="tradingview-widget-copyright">
  <script type="text/javascript" src="https://s3.tradingview.com/external-embedding/embed-widget-tickers.js" async>
  {
  "symbols": [
    {
      "title": "S&P 500",
      "proName": "INDEX:SPX"
    },
    {
      "title": "Nasdaq 100",
      "proName": "INDEX:IUXX"
    },
    {
      "title": "EUR/USD",
      "proName": "FX_IDC:EURUSD"
    },
    {
      "title": "BTC/USD",
      "proName": "BITFINEX:BTCUSD"
    },
    {
      "title": "ETH/USD",
      "proName": "BITFINEX:ETHUSD"
    }
  ],
  "locale": "id"
}
  </script>
</div>
<!-- TradingView Widget END -->
        <!-- BOF: marque -->
        <div class="d-none d-lg-block">
            <div class="container-fluid p-0 m-0">
                <div style="border-top: 1px solid #006600; border-bottom: 1px solid #006600; height: 24px;"
                    class="p-0 m-0">
                    <iframe height="25" scrolling="no" src="https://www.dailyforex.com/forex-widget/widget/25250" style="width: 100%; height:25px; display: block;border:0px;overflow:hidden;" width="100%"></iframe><span style="position:relative;display:block;text-align:center;color:#333333;width:100%;font-family:Tahoma,sans-serif;font-size:10px;"></span>
                </div>
            </div>
        </div>
        <!-- EOF: marque -->


        <!-- BOF: top main lg menu -->
        <div class="id-main-menu justify-content-around d-none d-lg-block">
            <div class="container">
 <div class="d-inline-block align-items-center  align-self-center">
                    <a href="index.php"><strong><img src="images/home/logo.png" width="154" height="25"></strong></a>
              </div>
                 <div class="d-inline-block align-items-center  align-self-center">
                    <a href="index.php"><strong>Home</strong></a>
                </div>
				<div class="d-inline-block align-items-center  align-self-center">
                    <a href="page.php?p=tentang-kami"><strong>Tentang GSC</strong></a>
                </div>
				<div class="d-inline-block align-items-center  align-self-center">
                    <a href="page.php?p=system"><strong>System Plan</strong></a>
                </div>
				<div class="d-inline-block align-items-center  align-self-center">
                    <a href="page.php?p=berita"><strong>Berita</strong></a>
                </div>
				<div class="d-inline-block align-items-center  align-self-center">
                    <a href="page.php?p=testimonial"><strong>Testimony</strong></a>
                </div>
				<div class="d-inline-block align-items-center  align-self-center">
                    <a href="page.php?p=kontak"><strong>Kontak kami</strong></a>
                </div>
				<div class="d-inline-block align-items-center  align-self-center">
                    <a href="./login.php"><strong>Login</strong></a>
                </div>
				 
                <div class="d-inline-block align-items-center  align-self-center mainmenu-roundborder">
                    <a href="page.php?p=terms"><strong>Register</strong></a>
                </div>

            </div>
        </div>
        <!-- EOF: top main lg menu -->





        <!-- BOF: mobile right push menu -->
        <nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-right" id="cbp-spmenu-s1"
            style="z-index: 9999999; font-size: 14px; !important; background-color: #185b83; font-family:Verdana, Geneva, sans-serif;">

            <div style="height: 2rem;">
                <button type="button" class="close" aria-label="Close" id="btnRightMenuClose">
                    <span aria-hidden="true" style="color: #fff; cursor: pointer; font-size: 1.5em !important; padding-right: 1rem;">&times;</span>
                </button>
            </div>



            <nav class="navigation">
                <ul class="mainmenu">
				 <li>
                        <a href="index.php">Home</a>
                  </li>
					<li>
                        <a href="page.php?p=tentang-kami">Tentang GSC</a>
                    </li>
					<li>
                        <a href="page.php?p=system">System Plan</a>
                    </li>
					<li>
                        <a href="page.php?p=berita">Berita</a>
                    </li>
					<li>
                        <a href="page.php?p=testimonial">Testimony</a>
                    </li>
					<li>
                        <a href="page.php?mpkontak">Kontak kami</a>
                    </li>
					<li>
                        <a href="./login.php">Login</a>
                    </li>
                     
                    <li style="margin-bottom: 50px;">
                        <a href="page.php?p=terms">
                            <div class="mainmenu-roundborder p-1" style="width: auto; display: inline-block; padding-left: 0.4rem !important;">Register</div>
                  </a></li>

                </ul>
            </nav>


        </nav>
        <!-- EOF: left push menu -->


        <!-- BOF:  image banner -->
        
        <!-- EOF: image banner -->


        <!-- blue bottom menu-->
        

        <!-- EOF: blue submenu top -->

        
	
    <div class="container-fluid pl-0 pr-0 carousel-top-margin">

        

    </div>

    <!-- info -->
    <a id="wp-sec-events" />
    <div class="container">

        <div class="row my-5">

         
         
              <?php 
					if(isset($_REQUEST['p'])){$p = $_REQUEST['p'];}
					if (empty($p)) $p = '';
					// PROSES OPEN ACCOUNT **************************************
					if 		($p=='')
								{ include("./dt_page/dt_home.php"); }
					else if ($p=='home')
								{ include("./dt_page/dt_home.php"); }
					else if ($p=='tentang-kami')
								{ include("./dt_page/dt_abouts.php"); }
					else if ($p=='system')
								{ include("./dt_page/dt_plan.php"); }
					else if ($p=='berita')
								{ include("./dt_page/dt_news.php"); }
					else if ($p=='detailberita')
								{ include("./dt_page/dt_detailberita.php"); }
					else if ($p=='album')
								{ include("./dt_page/dt_foto.php"); }
					else if ($p=='testimonial')
								{ include("./dt_page/dt_testi.php"); }
					else if ($p=='kontak')
								{ include("./dt_page/dt_contact.php"); }
					else if ($p=='faq')
								{ include("./dt_page/dt_faq.php"); }
					else if ($p=='terms')
								{ include("./dt_page/dt_terms.php"); }
					else if ($p=='join')
								{ include("./dt_page/dt_reg.php"); }
					else if ($p=='login')
								{ include("./dt_page/dt_login.php"); }
					else if ($p=='seminar')
								{ include("./dt_page/dt_seminar.php"); }
					else if ($p=='legal')
								{ include("./dt_page/dt_legal.php"); }
					else if ($p=='store')
								{ include("./dt_page/dt_produk.php"); }
					else if ($p=='checkout')
								{ include("./dt_page/dt_checkout.php"); }
								
					else 		{ include("./dt_page/dt_redr.php");  }
					?>
         
         
         
         
         
         
        </div>

    </div>

    <!-- end news -->


     
 <div align="center" style="margin-top:30px; margin-bottom:-80px;">
  <hr />
  <a href="#" onclick="window.open('https://www.sitelock.com/verify.php?site=gatesolutionsclub.com','SiteLock','width=600,height=600,left=160,top=170');" ><img class="img-responsive" alt="SiteLock" title="SiteLock" src="//shield.sitelock.com/shield/gatesolutionsclub.com" /></a>
  </div>
    
        <!--BOF: app -->
        <div style="background-color: #4396E6; color: #fff; margin-top: 8rem;">
             
        </div>
        <!-- EOF: app -->


        <!-- BOF: footer -->
        <div style="background-color: #006600; color: #fff;" class="mt-0">
            <div class="container">
                <div class="row d-flex  justify-content-between    pl-0 pt-3 pb-3">
                    <div class="col-12 col-sm-12 col-md-8 align-self-center d-flex">
                          <img src="images/gslogo.png" />
                        <div class="col-10 col-sm-8 d-inline-block align-self-center">
                            <span style="font-size: 0.9em; color: #8ca6c2; text-align: justify;">Bertransaksi forex dan komoditi memiliki resiko yang tinggi dan mungkin tidak sesuai untuk semua orang.<br>Sponsor Anda : <?php echo $db->dataku("nama", $_SESSION["sponsor"]);?> (<?php echo $_SESSION["sponsor"];?>)
                            </span>

                            <div style="font-size: 0.8em; color: #8ca6c2; margin-top: 10px">
                                Copyright &copy;  <?php echo $footer; ?>
                            </div>

                        </div>
                  </div>
                    <div class="col-12 col-sm-12 col-md-4 d-flex pr-0">
                        <div class="align-self-center w-100 text-right tnc d-none d-sm-none d-lg-block">
                        </div>

                  </div>
                </div>



            </div>
        </div>
        <!-- EOF: footer -->


        <!-- BOF: modal login-->
     

    <script src="js/jquery-3.3.1.min6434.js?180503"></script>
    <script src="js/popper.min6434.js?180503"></script>
    <script src="js/bootstrap.min6434.js?180503"></script>
    <script src="js/classie.es5.min6434.js?180503"></script>
    <script>       


        $(function () {

            var menuRight = document.getElementById('cbp-spmenu-s1'),
                // menuRight = document.getElementById('cbp-spmenu-s2'),
                btnHamburger = document.getElementById('icon-hamburger'),
                body = document.body,
                hh = document.getElementById('header');

            $('#icon-hamburger').click(function () {
                classie.toggle(this, 'active');
                classie.toggle(body, 'cbp-spmenu-push-toleft');
                classie.toggle(menuRight, 'cbp-spmenu-open');

                // reset all submenu color and drop down
                $('.mainmenu a').not($(this).closest('li').find('a')).css('background-color', '#185b83');
                $('#id-lang .dropdown-menu').removeClass('show');
                return false;
            });

            $('#btnRightMenuClose').click(function () {
                classie.toggle(this, 'active');
                classie.toggle(body, 'cbp-spmenu-push-toleft');
                classie.toggle(menuRight, 'cbp-spmenu-open');
                return false;
            });

           


            // sub menu
            $('.dropdown-menu a.dropdown-toggle').on('click', function (e) {
                if (!$(this).next().hasClass('show')) {
                    $(this).parents('.dropdown-menu').first().find('.show').removeClass("show");
                }
                var $subMenu = $(this).next(".dropdown-menu");
                $subMenu.toggleClass('show');
                $(this).parents('li.nav-item.dropdown.show').on('hidden.bs.dropdown', function (e) {
                    $('.dropdown-submenu .show').removeClass("show");
                });
                return false;
            });


            $("#id-lang").on("show.bs.dropdown", function (event) {
                if (classie.has(this, 'active show')) {
                    classie.removeClass(this, 'active');
                    $(this).closest('li').find('.submenu').css("display", "none");
                    $(this).closest('li').find('.submenu').css("max-height", "0");

                    $(this).closest('li').find('.dropdown-toggle').css('width', '240px');

                } else {
                    classie.addClass(this, 'active');
                    // change selected submenu bg
                    $(this).find('a').css('background-color', '#0a1f41');
                    // expand selected sub menu          
                    $(this).closest('li').find('.submenu').css("display", "block");
                    $(this).closest('li').find('.submenu').css("max-height", "100%");

                    $(this).closest('li').find('.dropdown-toggle').css('width', '240px');

                }

                // reset those not been selected
                $('.submenu').not($(this).closest('li').find('.submenu')).css('display', 'none');
                $('.mainmenu a').not($(this).closest('li').find('a')).css('background-color', '#185b83');
            });


            // menu clicked          
            $('.mainmenu li').click(function () {

                if ($(this).attr('id') == 'id-lang') {

                    //if (classie.has(this, 'active show')) {
                    //    classie.removeClass(this, 'active');
                    //    $(this).closest('li').find('.submenu').css("display", "none");
                    //    $(this).closest('li').find('.submenu').css("max-height", "0");

                    //    $(this).closest('li').find('.dropdown-toggle').css('width', '240px');

                    //} else {
                    //    classie.addClass(this, 'active');
                    //    // change selected submenu bg
                    //    $(this).find('a').css('background-color', '#0a1f41');
                    //    // expand selected sub menu          
                    //    $(this).closest('li').find('.submenu').css("display", "block");
                    //    $(this).closest('li').find('.submenu').css("max-height", "100%");

                    //    $(this).closest('li').find('.dropdown-toggle').css('width', '240px');

                    //}

                    //// reset those not been selected
                    //$('.submenu').not($(this).closest('li').find('.submenu')).css('display', 'none');
                    //$('.mainmenu a').not($(this).closest('li').find('a')).css('background-color', '#185b83');

                } else {
                    if (classie.has(this, 'active')) {
                        classie.removeClass(this, 'active');
                        $(this).closest('li').find('.submenu').css("display", "none");
                        $(this).closest('li').find('.submenu').css("max-height", "0");
                        $(this).closest('li').find('i').removeClass("fa-chevron-down").addClass("fa-chevron-rigt");
                    } else {
                        classie.addClass(this, 'active');
                        // change selected submenu bg
                        $(this).find('a').css('background-color', '#0a1f41');
                        // expand selected sub menu          
                        $(this).closest('li').find('.submenu').css("display", "block");
                        $(this).closest('li').find('.submenu').css("max-height", "100%");
                        // change chevron icon
                        $(this).closest('li').find('i').removeClass("fa-chevron-right").addClass("fa-chevron-down");
                    }
                    // reset those not been selected
                    $('.submenu').not($(this).closest('li').find('.submenu')).css('display', 'none');
                    $('.mainmenu a').not($(this).closest('li').find('a')).css('background-color', '#185b83');

                }

                // reset all submenu color and drop down
                $('.mainmenu a').not($(this).closest('li').find('a')).css('background-color', '#185b83');
                $('#id-lang .dropdown-menu').removeClass('show');



            });

            // nav positioning
            $('#cbp-spmenu-s1').css('margin-top', $('#header-up').outerHeight() - 1);

            // main menu clicked
            $('.id-main-menu .dropdown').click(function () {

                $(this).on('shown.bs.dropdown', function (e) {
                    $(this).css('color', '#7dd7ff');
                });

                $(this).on('hidden.bs.dropdown', function (e) {
                    $(this).css('color', '#fff');
                });
            });


            // marque                      
            function getIndexFeed() {
                $.get("/worker/wkrFeed.ashx?cmd=1", function (data, status) {
                    //console.log('index feed');
                    $('#id-stock-index').fadeOut('slow', function () {
                        var a = [{ o: $('#idxSnp .stock_value'), d: data[2] },
                        { o: $('#idxDjia .stock_value'), d: data[0] },
                        { o: $('#idxNasdaq .stock_value'), d: data[1] }];
                        a.forEach(function (i) {
                            i.o.text(i.d.price);
                            i.o.next('div').text(i.d.change_value + (' ( ' + i.d.change_percent + ' %)'));
                            if (i.d.change_value < 0) {
                                i.o.next('div').removeClass('stock_increase').addClass('stock_decrease');
                            }
                            else {
                                i.o.next('div').removeClass('stock_decrease').addClass('stock_increase');
                            }
                        });
                        $('#id-stock-index').fadeIn('slow');
                    });

                });
            }

            function getMarqueFeed() {
                $.get("/worker/wkrFeed.ashx?cmd=2", function (d, status) {
                    // console.log('marqueue feed');
                    var html = '';
                    var x = '';
                    var isFirst = 1;
                    var isFirstCrypt = 1;
                    var isFirstCommodity = 1;
                    d.forEach(function (i) {
                        if (isFirst == 1) {
                            isFirst = 0;
                            switch (i.symbol) {
                                case '1': x = 'US STOCK PRE-MARKET'; break;
                                case '2': x = 'US STOCK MARKET OPEN'; break;
                                case '3': x = 'US STOCK AFTER MARKET'; break;
                                case '4': x = 'US STOCK MARKET CLOSE'; break;
                                default: return;
                            }
                            // $('#market-status').html(x);
                            html += '<div class="px-2 d-inline font-weight-bold">' + x;
                            html += '</div>'
                        } else {

                            if (i.assettype == 3 && isFirstCrypt) {
                                isFirstCrypt = 0;
                                html += ' |   <b>CRYPTOCURRENTCY</b> ';
                            } else if (i.assettype == 4 && isFirstCommodity) {
                                isFirstCommodity = 0;
                                html += ' |   <b>COMMODITY</b> ';
                            }

                            html += '<div class="px-2 d-inline">';
                            var isdown = i.change_value < 0;
                            var xc = isdown ? 'xstock-red' : 'xstock-green';
                            html += '<span class="xstock-black"> ' + i.symbol + ' ' + i.xprice + ' </span>';

                            if (i.change_value < 0) {
                                html += ' <i class="fa fa-arrow-down ' + xc + '"></i> ';
                                html += '<span class="' + xc + '"> ' + i.change_percent + ' % </span>';
                            }
                            else if (i.change_value > 0) {
                                html += ' <i class="fa fa-arrow-up ' + xc + '"></i> ';
                                html += '<span class="' + xc + '"> ' + i.change_percent + ' % </span>';
                            }
                            //else {
                            //    html += ' <b class="' + xc + '" style="font-size:1.2em;">=</b>';
                            //}

                            if (i.assettype == 1) {
                                html += '<span class="' + xc + '"> ' + i.change_percent + ' % </span>';
                            }

                            html += '</div>';
                        }


                    });
                    $('#xmarquee').fadeOut('slow', function () {
                        document.getElementById("xmarquee").innerHTML = html;
                        $('#xmarquee').fadeIn('slow');
                    });

                });
            }
            var marqueeClock = 0;
            if ($(window).width() > 992) {
                setInterval(function () {
                    if (marqueeClock % 10 == 0) getIndexFeed();
                    if (marqueeClock % 30 == 0) getMarqueFeed();
                    marqueeClock++;
                    if (marqueeClock > 120) marqueeClock = 1;
                }, 1000);
            }
            getIndexFeed();
            getMarqueFeed();


        });
    </script>

<script>
var width = $('.g-recaptcha').parent().width();
if (width < 302) {
	var scale = width / 302;
	$('.g-recaptcha').css('transform', 'scale(' + scale + ')');
	$('.g-recaptcha').css('-webkit-transform', 'scale(' + scale + ')');
	$('.g-recaptcha').css('transform-origin', '0 0');
	$('.g-recaptcha').css('-webkit-transform-origin', '0 0');
}
</script>
<?php if($stchat == 1) { echo $lchats; }?>
</body>
</html>
<?php } ob_flush(); ?>