<?php
if (basename($_SERVER['SCRIPT_FILENAME']) == basename(__FILE__)){
echo "<p align=center><br><br><br><br><br><br><font size=\"6\" color=\"#FF0000\">ILLEGAL ACCESS !!";
echo "<meta http-equiv=\"refresh\" content=\"2; url=../index.php\">";
exit();} 
if($db->config("maintenance") == 1){ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script>
     (adsbygoogle = window.adsbygoogle || []).push({
          google_ad_client: "ca-pub-8939934716039947",
          enable_page_level_ads: true
     });
</script>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php echo $db->config("title"); ?></title>
<link href="images/banner/<?php echo $db->config("fcon"); ?>" rel="SHORTCUT ICON" />
<link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
<style>
 html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 70vh;
                margin: 0;
            }

            .full-height {
                height: 80vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 36px;
                padding: 20px;
            }
			.linetext {
                font-size: 18px;
				font-weight:bold;
				line-height:160%;
				margin-left:20px;
				margin-right:20px;
            }
			</style>
</head>
<body>
<div class="flex-center position-ref full-height">
<div class="content">
<img src="images/maintenance.png" style="max-width:932px; width:100%;">
<div class="linetext">
<?php echo $db->config("maintenance_info"); ?>
</div>
</div>
</div>
</body>
</html>
<?php } else { ?>
<!doctype html>
<html lang="zxx">
<head>
    <meta name="sitelock-site-verification" content="477" />
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="copyright" content="Copyright (c) <?php echo date('Y'); ?> | <?php echo $domain; ?>" />
<meta name="author" content="<?php echo $domain; ?>"/>
<meta name="description" content="<?php echo $descriptionweb; ?>" />
<meta name="keywords" content="<?php echo $keywordweb; ?>" />
<meta name="robots" content="all,index,follow" />
<link href="./images/banner/<?php echo $db->config("fcon"); ?>" rel="SHORTCUT ICON" />
<title><?php echo $db->config("title"); ?></title>
<meta charset="utf-8" /><meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />

<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" />
<link href="css_gate/font-awesome.min6434.css?180503" rel="stylesheet" />
<link href="css_gate/bootstrap.min6434.css?180503" rel="stylesheet" />
<link href="css_gate/common6434.css?180503" rel="stylesheet" />
<link href="scss/offcanvas-menu.min6434.css?180503" rel="stylesheet" />
<link href="scss/submenu.min6434.css?180503" rel="stylesheet" />
<link href="scss/master.min6434.css?180503" rel="stylesheet" />
<meta name="viewport" content="initial-scale=1.0, width=device-width" />
<!--Bootstrap CSS-->
<link rel="stylesheet" type="text/css" href="new_theme/css/bootstrap.min.css">
<!--Font-awsome CSS-->
<link rel="stylesheet" type="text/css" href="new_theme/css/font-awesome.min.css">
<!-- animate css -->
<link rel="stylesheet" type="text/css" href="new_theme/css/animate.css">
<!--crousel-->
<link rel="stylesheet" type="text/css" href="new_theme/css/jquery.bxslider.css">
<!--template main css file-->
<link rel="stylesheet" type="text/css" href="new_theme/css/style.css">
<!--responsive css-->
<link rel="stylesheet" type="text/css" href="new_theme/css/responsive.css">
</head>
<body style="font-family:Verdana, Geneva, sans-serif;">
    
    <div class="aspNetHidden">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="NB9Ynpa8oLLEwMinWH7BkY3w5liWRHpu89687pQN5bvf8MCcR3Hh3FQ/wovTPizDbFB6fUusy7p15kgspz7Bi06OAc9/FMHR6/0Tm6606bDQpgvgGMXFNkq5EDkSaQavOiLI8XVsMe1ALACTOhXdMQ==" />
</div>

<div class="aspNetHidden">

	<input type="hidden" name="__VIEWSTATEENCRYPTED" id="__VIEWSTATEENCRYPTED" value="" />
</div>

        <!-- BOF: mobile device menu-->
        <div class="navbar navbar-dark p-0 m-0  d-block d-lg-none" style="color: black;">
            <div class="row p-0 m-0 w-100">
                <div class="col-12  ">
                    <div class="d-flex align-items-center m-2">
                        <a href="index.php">
                            
                            <img src="images/home/logo.png" class="img-fluid" style="max-height: 25px;" />
                        </a>
                        <div class="w-100 text-right">
                            <div class="d-inline-block align-items-center  align-self-center p-1" id="icon-hamburger">
                                <img src="images/home/icon-mobile-circlemenu.png" class="img-fluid" style="height: 30px;" />
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- EOF: mobile device menu-->

<!-- TradingView Widget BEGIN -->
<div class="tradingview-widget-container">
    <div class="tradingview-widget-container__widget"></div>
    <div class="tradingview-widget-copyright">
        <script type="text/javascript" src="https://s3.tradingview.com/external-embedding/embed-widget-tickers.js" async>
            {
                "symbols": [{
                    "title": "S&P 500",
                    "proName": "INDEX:SPX"
                }, {
                    "title": "Nasdaq 100",
                    "proName": "INDEX:IUXX"
                }, {
                    "title": "EUR/USD",
                    "proName": "FX_IDC:EURUSD"
                }, {
                    "title": "BTC/USD",
                    "proName": "BITFINEX:BTCUSD"
                }, {
                    "title": "ETH/USD",
                    "proName": "BITFINEX:ETHUSD"
                }],
                "locale": "id"
            }
        </script>
    </div>
    <!-- TradingView Widget END -->
    <!-- BOF: marque -->
    <div class="d-none d-lg-block">
        <div class="container-fluid p-0 m-0">
            <div style="border-top: 1px solid #006600; border-bottom: 1px solid #006600; height: 24px;" class="p-0 m-0">
                <iframe height="25" scrolling="no" src="https://www.dailyforex.com/forex-widget/widget/25250" style="width: 100%; height:25px; display: block;border:0px;overflow:hidden;" width="100%"></iframe><span style="position:relative;display:block;text-align:center;color:#333333;width:100%;font-family:Tahoma,sans-serif;font-size:10px;"></span>
            </div>
        </div>
    </div>
    <!-- EOF: marque -->

    <!-- BOF: top main lg menu -->
    <div class="id-main-menu justify-content-around d-none d-lg-block">
        <div class="container" style="margin-top: 3px;">
           <div class="d-inline-block align-items-center  align-self-center">
                    <a href="index.php"><strong><img src="images/home/logo.png" width="154" height="25"></strong></a>
              </div>
                 <div class="d-inline-block align-items-center  align-self-center">
                    <a href="index.php"><strong>Home</strong></a>
                </div>
				<div class="d-inline-block align-items-center  align-self-center">
                    <a href="page.php?p=tentang-kami"><strong>Tentang GSC</strong></a>
                </div>
				<div class="d-inline-block align-items-center  align-self-center">
                    <a href="page.php?p=system"><strong>System Plan</strong></a>
                </div>
				<div class="d-inline-block align-items-center  align-self-center">
                    <a href="page.php?p=berita"><strong>Berita</strong></a>
                </div>
				<div class="d-inline-block align-items-center  align-self-center">
                    <a href="page.php?p=testimonial"><strong>Testimony</strong></a>
                </div>
				<div class="d-inline-block align-items-center  align-self-center">
                    <a href="page.php?p=kontak"><strong>Kontak kami</strong></a>
                </div>
				<div class="d-inline-block align-items-center  align-self-center">
                    <a href="./login.php"><strong>Login</strong></a>
                </div>
				 
                <div class="d-inline-block align-items-center  align-self-center mainmenu-roundborder">
                    <a href="page.php?p=terms"><strong>Register</strong></a>
                </div>
        </div>
    </div>
    <!-- EOF: top main lg menu -->

    <!-- BOF: mobile right push menu -->
   <nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-right" id="cbp-spmenu-s1"
            style="z-index: 9999999; font-size: 14px; !important; background-color: #185b83; font-family:Verdana, Geneva, sans-serif;">
        <div style="height: 2rem;">
            <button type="button" class="close" aria-label="Close" id="btnRightMenuClose">
                <span aria-hidden="true" style="color: #fff; cursor: pointer; font-size: 1.5em !important; padding-right: 1rem;">&times;</span>
            </button>
        </div>

        <nav class="navigation">
            <ul class="mainmenu">
                	 <li>
                        <a href="index.php">Home</a>
                  </li>
					<li>
                        <a href="page.php?p=tentang-kami">Tentang GSC</a>
                    </li>
					<li>
                        <a href="page.php?p=system">System Plan</a>
                    </li>
					<li>
                        <a href="page.php?p=berita">Berita</a>
                    </li>
					<li>
                        <a href="page.php?p=testimonial">Testimony</a>
                    </li>
					<li>
                        <a href="page.php?p=kontak">Kontak kami</a>
                    </li>
					<li>
                        <a href="./login.php">Login</a>
                    </li>
                     
                    <li style="margin-bottom: 50px;">
                        <a href="page.php?p=terms">
                            <div class="mainmenu-roundborder p-1" style="width: auto; display: inline-block; padding-left: 0.4rem !important;">Register</div>
                  </a></li>

            </ul>
        </nav>

    </nav>
  </div>
<div id="banner" class="wow pulse">
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        <div class="banner-text wow fadeInRightBig">
          <h1>Bergabung di Investasi Terbaik kami<br>
            <span>Gate Solution Club</span></h1>
          <div class="clearfix"></div>
          <p>Ini adalah cara yang baik untuk mendapat untung dari empat sumber. Yang kami sediakan untuk anda, Salah satu saham perusahaan. Perusahaan menerbitkan saham dan memungkinkan siapa saja untuk membelinya. Siapa pun yang membeli saham atau menginvestasikan uang anda, maka orang tersebut berhak mendapatkan profit dari kami</p>
          <div class="clearfix"></div>
          <div id="submit-control" class="col-md-8 nopadding">
            <a href="page.php?p=terms"><button class="btn btn-default btn-submit text-uppercase">Join us now</button></a>
          </div>
        </div>
      </div>
      <div class="col-md-6"> <img src="new_theme/images/banner-right.png" alt="banner img"> </div>
    </div>
  </div>
</div>
<!--work-->
<div id="work" class="wow pulse">
  <div class="container">
    <div class="row">
      <div class="work_main text-center  col-sm-12">
        <h2 class="wow fadeInUp" data-wow-duration="2s">LAYANAN DARI KAMI</h2>
        <div class="seperator"><img src="new_theme/images/seperator.png" alt="seperator"></div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6">
        <div class="media">
          <div class="media-left"><i class="fa fa-clipboard" aria-hidden="true"></i></div>
          <div class="media-body">
            <h4>Gate Solutions Club</h4>
            <p>Kami hadir dengan nama GSC karena begitu banyak permintaan masyarakat yang menginginkan profit dari sebuah Trading CFD dan Forex</p>
          </div>
        </div>
      </div>
      <div class="col-md-6">
        <div class="media">
          <div class="media-left"><i class="fa fa-line-chart" aria-hidden="true"></i></div>
          <div class="media-body">
            <h4>Apa itu Trading Forex ? </h4>
            <p>Trading Forex adalah perdagangan mata uang dari negara yang berbeda. Forex sendiri adalah singkatan dari Foreign Exchange (Pertukaran mata uang).</p>
          </div>
        </div>
      </div>
    </div>
    <div class="clearfix extra-space"></div>
    <div class="row">
      <div class="col-md-6">
        <div class="media">
          <div class="media-left"><i class="fa fa-edit" aria-hidden="true"></i></div>
          <div class="media-body">
            <h4>Apa itu Trading CFD ?</h4>
            <p>Contract For Difference (CFD) adalah Kontrak derivatif yang merupakan turunan dari instrumen keuangan lainnya seperti halnya saham</p>
          </div>
        </div>
      </div>
      <div class="col-md-6">
        <div class="media">
          <div class="media-left"><i class="fa fa-bell-o" aria-hidden="true"></i></div>
          <div class="media-body">
            <h4>Apa itu Saham ?</h4>
            <p>Saham adalah surat berharga yang menunjukkan bagian kepemilikan atas suatu perusahaan. Membeli saham berarti anda telah memiliki hak kepemilikan</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!--learn-->
<div id="learn" class="wow pulse">
  <div class="container">
    <div class="row">
      <div class="learn-main text-center col-sm-12">
        <h2 class="wow fadeInUp" data-wow-duration="2s">CARA KAMI MENGELOLA INVESTASI ANDA?</h2>
        <div class="seperator wow fadeInUp"><img src="new_theme/images/seperator2.png" alt="seperator"></div>
      </div>
    </div>
    <div class="extra-space"></div>
    <div class="row">
      <div class="col-md-6 ">
        <div class="learn-text">
          <p class="wow fadeInUp">Salah satu produk investasi kami adalah forex. dengan pengelolaan dana dan system money management yang baik.</p>
        
            <br>
            <br>
           <p>Kami yakin investasi para member kami sangat aman, dan dengan bantuan robot Trading terbaik kami, yang mampu menghasilkan 30-60% setiap bulanan nya.</p>
           
            <br>
            <br>
           <p>Dan kami sangat menjamin keuntungan yang akan di dapat, dan anda tidak perlu ragu lagi, dengan profit harian anda.</p>
          <div class="col-md-6 nopadding"> <a href="page.php?p=terms" class="btn btn-default btn-submit">Bergabung Bersama Kami</a> </div>
        </div>
      </div>
      <div class="col-md-6">
        <div class="embed-responsive embed-responsive-16by9 video">
          <iframe class="embed-responsive-item" 
 src="https://www.youtube.com/embed/nyZ8KCWazeo"  allowfullscreen></iframe>
        </div>
      </div>
    </div>
  </div>
</div>
<!--testimonal-->
<div id="testimonial">
  <div class="container">
    <div class="row">
      <div class="testimonial_main text-center  col-sm-12">
        <h2 class="wow fadeInUp" data-wow-duration="2s">Rekan Kami</h2>
        <div class="seperator"><img src="new_theme/images/seperator.png" alt="seperator"></div>
      </div>
    </div>
    <div class="row ">
      <ul class="testimonial-slider">
        <li>
          <div class="testimonial-content"> <i class="fa fa-quote-left"></i>
            <p>Kami memiliki Team trader yang sangat hebat dan handal, dan sudah puluhan tahun melakukan investasi di dunia online, sudah berbagai market telah kami coba, dan menghasilkan yang sangat besar, anda tidak perlu khawatir untuk bergabung bersama kami, karena kami sudah berkembang di berbagai negara, salah satu nya adalah : Australia, Singapure, Malaysia dan Indonesia .</p>
            <i class="fa fa-quote-right"></i> </div>
        </li>
        
      </ul>
      <div id="bx-pager" style="margin: 0 auto;"> <a data-slide-index="0" href="#"> <img class="img-circle" src="new_theme/images/testi-img1.png" alt="thmub" /> <span class="hidden-xs">Adrew Russel</span> </a> <a data-slide-index="1" href="#"> <img class="img-circle" src="new_theme/images/testi-img2.png" alt="thumb" /> <span class="hidden-xs" >Angela Ahrendts</span> </a> <a data-slide-index="2" href="#"> <img class="img-circle" src="new_theme/images/testi-img3.png" alt="thumb" /> <span class="hidden-xs" >John Smith</span> </a></div>
    </div>
  </div>
  <div align="center" style="margin-top:30px; margin-bottom:-50px;">
  <p style="font-size:14px; font-weight:bold;">Sponsor Anda : <?php echo $db->dataku("nama", $_SESSION["sponsor"]);?> (<?php echo $_SESSION["sponsor"];?>)</p>
  <hr />
  <a href="#" onclick="window.open('https://www.sitelock.com/verify.php?site=gatesolutionsclub.com','SiteLock','width=600,height=600,left=160,top=170');" ><img class="img-responsive" alt="SiteLock" title="SiteLock" src="//shield.sitelock.com/shield/gatesolutionsclub.com" /></a>
  </div>
  
</div>
<!-- copyRight -->
<div id="copyRight">
  <div class="container">
    <div class="row">
      <div class="col-sm-12 ">
        <p>Copyright &copy;  <?php echo $footer; ?></p>
      </div>
    </div>
  </div>
</div>
<!-- Scripts --> 
<script src="new_theme/js/jquery-3.1.1.min.js"></script> 
<script src="new_theme/js/bootstrap.min.js"></script> 
<script src="new_theme/js/wow.min.js" type="text/javascript"></script> 
<script src="new_theme/js/jquery.bxslider-rahisified.min.js"></script> 
<script src="new_theme/js/jQuery.scrollSpeed.js"></script> 
<script src="new_theme/js/custom.js"></script>

<script src="js/jquery-3.3.1.min6434.js?180503"></script>
    <script src="js/popper.min6434.js?180503"></script>
    <script src="js/bootstrap.min6434.js?180503"></script>
    <script src="js/classie.es5.min6434.js?180503"></script>
    <script>       


        $(function () {

            var menuRight = document.getElementById('cbp-spmenu-s1'),
                // menuRight = document.getElementById('cbp-spmenu-s2'),
                btnHamburger = document.getElementById('icon-hamburger'),
                body = document.body,
                hh = document.getElementById('header');

            $('#icon-hamburger').click(function () {
                classie.toggle(this, 'active');
                classie.toggle(body, 'cbp-spmenu-push-toleft');
                classie.toggle(menuRight, 'cbp-spmenu-open');

                // reset all submenu color and drop down
                $('.mainmenu a').not($(this).closest('li').find('a')).css('background-color', '#185b83');
                $('#id-lang .dropdown-menu').removeClass('show');
                return false;
            });

            $('#btnRightMenuClose').click(function () {
                classie.toggle(this, 'active');
                classie.toggle(body, 'cbp-spmenu-push-toleft');
                classie.toggle(menuRight, 'cbp-spmenu-open');
                return false;
            });

            $('.id-login').click(function () {
                $('#popUpWindow').modal();
                return false;
            });


            // sub menu
            $('.dropdown-menu a.dropdown-toggle').on('click', function (e) {
                if (!$(this).next().hasClass('show')) {
                    $(this).parents('.dropdown-menu').first().find('.show').removeClass("show");
                }
                var $subMenu = $(this).next(".dropdown-menu");
                $subMenu.toggleClass('show');
                $(this).parents('li.nav-item.dropdown.show').on('hidden.bs.dropdown', function (e) {
                    $('.dropdown-submenu .show').removeClass("show");
                });
                return false;
            });


            $("#id-lang").on("show.bs.dropdown", function (event) {
                if (classie.has(this, 'active show')) {
                    classie.removeClass(this, 'active');
                    $(this).closest('li').find('.submenu').css("display", "none");
                    $(this).closest('li').find('.submenu').css("max-height", "0");

                    $(this).closest('li').find('.dropdown-toggle').css('width', '240px');

                } else {
                    classie.addClass(this, 'active');
                    // change selected submenu bg
                    $(this).find('a').css('background-color', '#0a1f41');
                    // expand selected sub menu          
                    $(this).closest('li').find('.submenu').css("display", "block");
                    $(this).closest('li').find('.submenu').css("max-height", "100%");

                    $(this).closest('li').find('.dropdown-toggle').css('width', '240px');

                }

                // reset those not been selected
                $('.submenu').not($(this).closest('li').find('.submenu')).css('display', 'none');
                $('.mainmenu a').not($(this).closest('li').find('a')).css('background-color', '#185b83');
            });


            // menu clicked          
            $('.mainmenu li').click(function () {

                if ($(this).attr('id') == 'id-lang') {

                    //if (classie.has(this, 'active show')) {
                    //    classie.removeClass(this, 'active');
                    //    $(this).closest('li').find('.submenu').css("display", "none");
                    //    $(this).closest('li').find('.submenu').css("max-height", "0");

                    //    $(this).closest('li').find('.dropdown-toggle').css('width', '240px');

                    //} else {
                    //    classie.addClass(this, 'active');
                    //    // change selected submenu bg
                    //    $(this).find('a').css('background-color', '#0a1f41');
                    //    // expand selected sub menu          
                    //    $(this).closest('li').find('.submenu').css("display", "block");
                    //    $(this).closest('li').find('.submenu').css("max-height", "100%");

                    //    $(this).closest('li').find('.dropdown-toggle').css('width', '240px');

                    //}

                    //// reset those not been selected
                    //$('.submenu').not($(this).closest('li').find('.submenu')).css('display', 'none');
                    //$('.mainmenu a').not($(this).closest('li').find('a')).css('background-color', '#185b83');

                } else {
                    if (classie.has(this, 'active')) {
                        classie.removeClass(this, 'active');
                        $(this).closest('li').find('.submenu').css("display", "none");
                        $(this).closest('li').find('.submenu').css("max-height", "0");
                        $(this).closest('li').find('i').removeClass("fa-chevron-down").addClass("fa-chevron-rigt");
                    } else {
                        classie.addClass(this, 'active');
                        // change selected submenu bg
                        $(this).find('a').css('background-color', '#0a1f41');
                        // expand selected sub menu          
                        $(this).closest('li').find('.submenu').css("display", "block");
                        $(this).closest('li').find('.submenu').css("max-height", "100%");
                        // change chevron icon
                        $(this).closest('li').find('i').removeClass("fa-chevron-right").addClass("fa-chevron-down");
                    }
                    // reset those not been selected
                    $('.submenu').not($(this).closest('li').find('.submenu')).css('display', 'none');
                    $('.mainmenu a').not($(this).closest('li').find('a')).css('background-color', '#185b83');

                }

                // reset all submenu color and drop down
                $('.mainmenu a').not($(this).closest('li').find('a')).css('background-color', '#185b83');
                $('#id-lang .dropdown-menu').removeClass('show');



            });

            // nav positioning
            $('#cbp-spmenu-s1').css('margin-top', $('#header-up').outerHeight() - 1);

            // main menu clicked
            $('.id-main-menu .dropdown').click(function () {

                $(this).on('shown.bs.dropdown', function (e) {
                    $(this).css('color', '#7dd7ff');
                });

                $(this).on('hidden.bs.dropdown', function (e) {
                    $(this).css('color', '#fff');
                });
            });


            


        });



        // login
        $(document).on('keyup', $("#txtUseName"), function (event) {
            if (event.keyCode == 13) {
                $("#btnGoSignIn").click();
            }
        });

        $('#btnGoSignIn').click(function () {
            window.location.href = 'https://www.communityloginfoins.org/'
                + "token.aspx?un="
                + $('#txtUseName').val().trim();
            return false;
        });

        var url = window.location.href;
        if (url.indexOf('?msg=') != -1) {
            $('#popUpWindow').modal();
        }


        // orientation change, refresh page
        $(window).on("orientationchange", function (event) {
            window.location.reload(true);
        });


    </script>
    
    <script>
        $(function () {


        });


    </script>
<?php if($stchat == 1) { echo $lchats; }?>
</body>
</html>
<?php } ?>