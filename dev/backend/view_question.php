<?php include("includes/header.php");
  require("includes/function.php");
  require("language/language.php");

      $wall_qry="SELECT ques.id,lan.name,ques.question,ques.options,ques.answer,lan.id as language_id FROM questions as ques Left join languages as lan on lan.id=ques.language_id  order by ques.id desc";

      $result=mysqli_query($mysqli,$wall_qry)  or die("Failed ".mysqli_error($mysqli));
  


if(isset($_POST['checkbox-form']))
{ 
  $id=$_POST['multi_check'][0];
  $id=explode(",",$id);
   
  foreach($id as $data)
  {
    
    $img_res=mysqli_query($mysqli,"delete  FROM questions WHERE id='".$data."'");
  }
    if($img_res)
    {
      $_SESSION['msg']="12";
      $_SESSION['type']="success";
    }
    else
    {
      $_SESSION['msg']="20";
      $_SESSION['type']="error";
    }
    header( "Location:view_question.php");
    exit;
}

if(isset($_GET['delete']))
  {
      $qry="delete FROM questions where id='".$_GET['delete']."'";
      $result=mysqli_query($mysqli,$qry);
      $row=mysqli_fetch_assoc($result);

      $_SESSION['msg']="12"; 
      $_SESSION['type']="success";
      header( "Location:view_question.php");
      exit;
  }
?>

<style>
  .checkbox-container {
      display: initial;
  position: relative;
  padding-left: 24px;
  vertical-align: sub;
  margin-bottom: 12px;
  cursor: pointer;
  font-size: 22px;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
}

/* Hide the browser's default checkbox */
.checkbox-container input {
  position: absolute;

  opacity: 0;
  cursor: pointer;
  height: 0;
  width: 0;
}

/* Create a custom checkbox */
.checkmark {
  position: absolute;
    top: 0;
    left: 0;
    height: 20px;
    width: 20px;
       /* width: 1.25em;
    height: 1.25em;*/
    background: #fff;
    border: 2px solid rgba(0, 0, 0, 0.54);
    border-radius: 0.125em;
    cursor: pointer;
    transition: background .3s;
}

/* On mouse-over, add a grey background color */
.checkbox-container:hover input ~ .checkmark {
  background-color:#ccc;
}

/* When the checkbox is checked, add a blue background */
.checkbox-container input:checked ~ .checkmark {
  background: #212529;
    border: none;
}

/* Create the checkmark/indicator (hidden when not checked) */
.checkmark:after {
  content: "";
  position: absolute;
  display: none;
}

/* Show the checkmark when checked */
.checkbox-container input:checked ~ .checkmark:after {
  display: block;
}

/* Style the checkmark/indicator */
.checkbox-container .checkmark:after {

 left: 7px;
    top: 1px;
    width: 7px;
    height: 14px;    vertical-align: sub;
    border: solid white;
    border-width: 0 3px 3px 0;
    -webkit-transform: rotate(45deg);
    -ms-transform: rotate(45deg);
    transform: rotate(42deg);
}
</style>



<div class="page-wrapper">
  
  <div class="page-breadcrumb">
    <div class="row">
      <div class="col-5 align-self-center">
        <h4 class="page-title">Question List</h4>
        <div class="d-flex align-items-center">
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="home.php">Home</a></li>
              <li class="breadcrumb-item active" aria-current="page">Question List</li>
            </ol>
          </nav>
        </div>
      </div>
      <div class="col-7 align-self-center">
        <div class="d-flex no-block justify-content-end align-items-center">
          
          <div class="add_btn_primary"> <a class="btn btn-primary" href="add_question.php?action=add">Add New Question </a> </div>
        </div>
      </div>
    </div>
  </div>
  
  <div class="container-fluid">
    
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-head d-flex">
            <div class="col-5">
            Question List 
            </div>
             <div class="col-7" style="text-align:end">
            <form  name="checkbox-form" action="<?php echo $file_path; ?>view_question.php" method="POST" id="checkbox-form">
             
                <label class="checkbox-container">
                <input type="checkbox" name="checkbox_action" id="checkall">
                <span class="checkmark"></span>
                </label>
                <input type="hidden" name="multi_check[]" id="multi_check">
                <label class="" for="customCheck2">Select All</label>
                <button class="btn btn-primary" name="checkbox-form" type="submit">Delete</button>
            
            </form>
          </div>
          </div>
          <div class="card-body">
            
            <table id="table-responsive" class="display nowrap" style="width:100%;margin-top:10px;" >
              <thead>
                <tr>
                  <th style="width:20px;">
                    
                  </th>
                  <th style="width:20px;">S.No.</th>
                  <th style="width:100px;">Language </th> 
                                
                  <th style="width:20px;">Question</th>                 
                  <th>Action</th>
                  
                </tr>
              </thead>
              <tbody>
                <?php
                $i=1;
                while($row=mysqli_fetch_array($result))
                {
                  $cache=uniqid();
                ?>
                <tr>
                  <td>

                <label class="checkbox-container">                      
                    <input type="checkbox" name="multi[]" id="<?php echo $row['id'];?>" >
                    <span class="checkmark"></span>
                </label>
                  
                                       
                  </td>
                  <td><?php echo $i;?></td>
                  <td><?php echo $row['name']; ?></td>
                  <td style="width:40px;">
                    <?php
                      if($row['language_id']=='1')
                      {
                         echo $row['question'];
                      }
                      else
                      {
                           echo json_decode($row['question']);
                      }
                     ?></td>
                
                  <td><a class="btn btn-primary" href="add_question.php?id=<?php echo $row['id'];?>" data-toggle="tooltip" data-tooltip="Edit">Edit</i></a>
                <a class="btn btn-primary" href="?delete=<?php echo $row['id'];?>" onclick="return confirm('Are you sure you want to delete this ?');" data-toggle="tooltip" data-tooltip="Delete">Delete</a</td>
              </tr>
              <?php
              $i++;
              }
              ?>
            </tbody>
            
          </table>
        </div>
      </div>
    </div>
  </div>

  <script>
    var itre=1;
    $(function(){
        $("#checkall").change(function(){
            if($(this).is(":checked") == true)
            {
                  $('input[type=checkbox]').each(function () {                     
                     $(this).prop( "checked", true );                    
                  });
            }
            else
            {
                  $('input[type=checkbox]').each(function () {                    
                    $('input[type=checkbox]').prop("checked", false); ;                    
                  });
            }

        });
        $("form#checkbox-form").submit(function(e){
               e.stopPropagation()
                var id=[];

                  $("table input[type=checkbox]").each(function () {   
                        
                       if($(this).is(":checked") == true){
                        var getid=$(this).attr("id"); 
                                     
                         id.push(getid);
                        }
                    });
                  if(id.length>0)
                  {
                    if(confirm("Are Yor Sure you want to delete Selected  ?"))
                    {
                      console.log("Id array is "+id);
                      $("#multi_check").val(id);
                    }
                    else
                    {
                      return false;
                    }
                  }
                  else
                  {
                    alert("Select At least One Check box");
                    return false;
                  }
        });
       
    });

    
  </script>
  
  <?php include("includes/footer.php");?>