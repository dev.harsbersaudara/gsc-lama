<?php include("includes/header.php");

	require("includes/function.php");
	require("language/language.php");

	if(isset($_POST['submit']) and isset($_GET['action']))
	{
		if($_GET['action']=="add")
		{
		   	$qry="SELECT * FROM languages where name='".$_POST['name']."'";
			$result=mysqli_query($mysqli,$qry);
			
			$rowCount=mysqli_num_rows($result);

			if($rowCount==0)
			{
					$data = array( 
					    'name'  =>  trim($_POST['name'])				    
					    );		

					$qry = Insert('languages',$data);


					$_SESSION['msg']="10";
					$_SESSION['type']="success";
			 
					header( "Location:language_view.php");
					exit;	
			}
			else
			{
				$_SESSION['msg']="19";
				$_SESSION['type']="error";
		 
				header( "Location:language_view.php");
				exit;
			}
		} 
		if($_GET['action']=="edit"){

			$data = array(
			'name'  =>  $_POST['name']
			);

			$category_edit=Update('languages', $data, "WHERE id = '".$_POST['id']."'");

			$_SESSION['msg']="11"; 
			$_SESSION['type']="success";
			header( "Location:language_view.php");
			exit;
		}	
	}

	
	
	// Edit Data 
	if(isset($_GET['id']))
	{
			 
			$qry="SELECT * FROM languages where id='".$_GET['id']."'";
			$result=mysqli_query($mysqli,$qry);
			$row=mysqli_fetch_assoc($result);

	}
	


?>

<div class="page-wrapper">
  
  <div class="page-breadcrumb">
    <div class="row">
      <div class="col-5 align-self-center">
        <h4 class="page-title">Language</h4>
        <div class="d-flex align-items-center">
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="home.php">Home</a></li>
              <li class="breadcrumb-item active" aria-current="page">Language</li>
            </ol>
          </nav>
        </div>
      </div>
      <div class="col-7 align-self-center">
        <div class="d-flex no-block justify-content-end align-items-center">
          
          <div class="add_btn_primary"> <a class="btn btn-primary" href="language_view.php">View Languages</a> </div>
        </div>
      </div>
    </div>
  </div>
  
  <div class="container-fluid">
    
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-head">
           <?php if(isset($_GET['id'])){?>Edit<?php }else{?>Add<?php }?> Language
          </div>
          <div class="card-body">
          	<?php if(isset($_GET['action'])=="add") { ?>
          	<form action="" name="addeditcategory" method="post" class="form form-horizontal" enctype="multipart/form-data">
          	<?php } else { ?>

          		<form action="?action=edit" name="addeditcategory" method="post" class="form form-horizontal" enctype="multipart/form-data">
          	<?php } ?>
            	<input  type="hidden" name="id" value="<?php echo $_GET['id'];?>" />

              <div class="section">
                <div class="section-body">
                  <div class="form-group">
                    <label class="col-md-3 control-label">Language Name :-</label>
                    <div class="col-md-6">
                      <input type="text" name="name" id="name" value="<?php if(isset($_GET['id'])){echo $row['name'];}?>" class="form-control" required>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-md-9">
                      <button type="submit" name="submit" class="btn btn-primary">Save</button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
            
	      </div>
	    </div>
	  </div>
	</div>

        

       
<?php include("includes/footer.php");?>       
