<?php include("includes/header.php");

?>



<div class="page-wrapper">
  
  <div class="page-breadcrumb">
    <div class="row">
      <div class="col-5 align-self-center">
        <h4 class="page-title">API Urls</h4>
        <div class="d-flex align-items-center">
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="home.php">Home</a></li>
              <li class="breadcrumb-item active" aria-current="page">API Urls</li>
            </ol>
          </nav>
        </div>
      </div>
      <div class="col-7 align-self-center">
        
      </div>
    </div>
  </div>
  
  <div class="container-fluid">
    
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-head">
           ALL API Urls
          </div>
          <div class="card-body">
           
          <div class="card">
            
                  <div class="card-body no-padding">
              
                 <pre><code class="html"><b>Users Register ( METHOD : POST ) Parameter: email,name,mobile,password,onesignalToken,device_id</b><br><?php echo $file_path."api.php?register"?><br><br><b>Users Login ( METHOD : POST ) Parameter: email,password,onesignalToken</b><br><?php echo $file_path."api.php?login"?><br><br><b>Settings ( METHOD : GET )</b><br><?php echo $file_path."api.php?action=settings"?><br><br><b>Question ( METHOD : GET )</b><br><?php echo $file_path."api.php?question&language=1&device_id=123456789"?><br><br><b>Refer  ( METHOD : POST ) Parameter: refer_from,refer_to</b><br><?php echo $file_path."api.php?refer"?><br><br><b>Refer Points to Score Board ( METHOD : GET ) </b><br><?php echo $file_path."api.php?ReferPointScoreUpdate&refer_code=1234564 "?><br><br><b>Score Update  ( METHOD : POST ) Parameter: user_id,contest_id,score_point,questions(array)</b><br><?php echo $file_path."api.php?score_update"?><br><br><b>Get Scoreboard Details( METHOD : GET )</b><br><?php echo $file_path."api.php?score_board&contest_id=2"?><br><br><b>Get User Info( METHOD : GET )</b><br><?php echo $file_path."api.php?userinfo&id=2"?><br><br><b>Contact Api( METHOD : POST ) Parameter: user_id,query</b><br><?php echo $file_path."api.php?contact"?>
                 </code></pre>
            
                </div>
              </div>
          </div>
    </div>
  </div>
</div>

        
<?php include("includes/footer.php");?>       
