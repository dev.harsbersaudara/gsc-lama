<?php include("includes/header.php");

	require("includes/function.php");
	require("language/language.php");

	require 'vendor/autoload.php';
	
	use PhpOffice\PhpSpreadsheet\Spreadsheet;
	use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

	if(isset($_POST['submit']) )
	{	
		if($_GET['action']=="export")
		{	
			$get;
			
			$language_id=$_POST['language_id'];
			if($language_id=="all")
			{
				$query="SELECT * from questions";
				$get=mysqli_query($mysqli,$query)  or die("Failed ".mysqli_error($mysqli));
				
			}
			else
			{
				$query="SELECT * from questions where language_id='{$language_id}' ";
				$get=mysqli_query($mysqli,$query)  or die("Failed ".mysqli_error($mysqli));
				
			}
				
				$spreadsheet = new Spreadsheet();
				$sheet = $spreadsheet->getActiveSheet();
				
				$styleArray = array(
				  'font' => array(
				    'bold' => true,
				  ),
				  'alignment' => array(
				    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
				    'vertical'   => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
				  ),
				  'borders' => array(
				      'bottom' => array(
				          'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
				          'color' => array('rgb' => '333333'),
				      ),
				  ),
				  'fill' => array(
				    'type'       => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
				    'rotation'   => 90,
				    'startcolor' => array('rgb' => '0d0d0d'),
				    'endColor'   => array('rgb' => 'f2f2f2'),
				  ),
				);

				
				$spreadsheet->getActiveSheet()->getStyle('A1:G1')->applyFromArray($styleArray);
				
				foreach(range('A', 'G') as $columnID) {
				  $spreadsheet->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
				}
				
				  $sheet->setCellValue('A1', 'Id');
				  $sheet->setCellValue('B1', 'Question');
				  $sheet->setCellValue('C1', 'Options a');
				  $sheet->setCellValue('D1', 'Options b');
				  $sheet->setCellValue('E1', 'Options c');
				  $sheet->setCellValue('F1', 'Options d');
				  $sheet->setCellValue('G1', 'answer');
				  
				$x = 2;
				while($row=mysqli_fetch_assoc($get)){
					$option=json_decode($row['options'],true);
				
				    if($row['language_id']==1)
				    {
					    $sheet->setCellValue('A'.$x,$row['id']);
					   
					    $sheet->setCellValue('B'.$x,$row['question']);
					    $sheet->setCellValue('C'.$x,$option['A']);
					    $sheet->setCellValue('D'.$x,$option['B']);
					    $sheet->setCellValue('E'.$x,$option['C']);
					    $sheet->setCellValue('F'.$x,$option['D']);
					    $sheet->setCellValue('G'.$x,$row['answer']);
					}else
					{
						$sheet->setCellValue('A'.$x,$row['id']);
					   
					    $sheet->setCellValue('B'.$x,json_decode($row['question']));
					    $sheet->setCellValue('C'.$x,$option['A']);
					    $sheet->setCellValue('D'.$x,$option['B']);
					    $sheet->setCellValue('E'.$x,$option['C']);
					    $sheet->setCellValue('F'.$x,$option['D']);	
					    $sheet->setCellValue('G'.$x,json_decode($row['answer']));	

					}
				   
				  $x++;
				}
				
				$filename2 ='excel'.time().'.xlsx';
				header('Content-Type: application/vnd.openxmlformats- 
				officedocument.spreadsheetml.sheet');
				header('Content-Disposition: attachment;filename="'.$filename2 .'"'); 
				
				header('Cache-Control: max-age=0');

				ob_end_clean();


				$writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
				$writer->save('php://output');
				exit;
				
		}

		if($_GET['action']=="add" || $_GET['action']=="edit")
		{
				$language_id=$_POST['language_id'];
				
				$spreadsheet = new Spreadsheet();

				$inputFileType = 'Xlsx';
				$inputFileName = $_FILES['file']['tmp_name'];

				
				$spreadSheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($inputFileName);
				$highestRow=$spreadSheet->getActiveSheet()->getHighestRow();
				$data=array();
				for($i=2,$j=0;$i<=$highestRow;$i++,$j++)
				{	
					if(!empty($spreadSheet->getActiveSheet()->getCell('B'.$i)->getValue()))
					{	
						$data[$j]['id']=$spreadSheet->getActiveSheet()->getCell('A'.$i)->getValue();
						$data[$j]['question']=$spreadSheet->getActiveSheet()->getCell('B'.$i)->getValue();
						$data[$j]['options']['A']=$spreadSheet->getActiveSheet()->getCell('C'.$i)->getValue();
						$data[$j]['options']['B']=$spreadSheet->getActiveSheet()->getCell('D'.$i)->getValue();
						$data[$j]['options']['C']=$spreadSheet->getActiveSheet()->getCell('E'.$i)->getValue();
						$data[$j]['options']['D']=$spreadSheet->getActiveSheet()->getCell('F'.$i)->getValue();
						$data[$j]['answer']=$spreadSheet->getActiveSheet()->getCell('G'.$i)->getValue();
						$data[$j]['options']=json_encode($data[$j]['options']);

						if(!empty($data[$j]['id']))
						{

							if($language_id=='1')
							{
								$store = array(
									'language_id'=>$language_id,
									'question'  =>  $data[$j]['question'],
									'options'  =>  $data[$j]['options'],
									'answer'=>$data[$j]['answer']
								);
							}
							else
							{
								$store = array(
									'language_id'=>$language_id,
									'question'  => json_encode($data[$j]['question']),
									'options'  =>  $data[$j]['options'],
									'answer'=>json_encode($data[$j]['answer'])
								);
							}

							$category_edit=Update('questions', $store, "WHERE id = '".$data[$j]['id']."'");

							$_SESSION['msg']="11"; 
							$_SESSION['type']="success";
							
						}
						else
						{	
							if($language_id=='1')
							{	
								$store = array( 
									'language_id'=>$language_id,
								    'question'  =>  $data[$j]['question'],
									'options'  =>  $data[$j]['options'],
									'answer'=>$data[$j]['answer']				    
								    );
							}
							else
							{
								$store = array(
									'language_id'=>$language_id,
									'question'  => json_encode($data[$j]['question']),
									'options'  =>  $data[$j]['options'],
									'answer'=>json_encode($data[$j]['answer'])
								);
							}
			    		

							$qry = Insert('questions',$store);


							$_SESSION['msg']="10";
							$_SESSION['type']="success";
					 
								

						}

					}
				}
		}
		header( "Location:bulk.php?action=add");
					exit;
		

	}

	
	
	// Edit Data 
	if(isset($_GET['id']))
	{
			 
			$qry="SELECT * FROM game_level where id='".$_GET['id']."'";
			$result=mysqli_query($mysqli,$qry);
			$row=mysqli_fetch_assoc($result);

	}
	


?>

<div class="page-wrapper">
  
  <div class="page-breadcrumb">
    <div class="row">
      <div class="col-5 align-self-center">
        <h4 class="page-title">Level Question</h4>
        <div class="d-flex align-items-center">
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="home.php">Home</a></li>
              <li class="breadcrumb-item active" aria-current="page">Question</li>
            </ol>
          </nav>
        </div>
      </div>
      <div class="col-7 align-self-center">
        <div class="d-flex no-block justify-content-end align-items-center">
          
          <div class="add_btn_primary"> <a class="btn btn-primary" href="view_question.php">View Question</a> </div>
        </div>
      </div>
    </div>
  </div>
  
  <div class="container-fluid">
    
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-head">
           Import Excel
          </div>
          <div class="card-body">
          	<?php if(isset($_GET['action'])=="add") { ?>
          	<form action="?action=add" name="addeditcategory" method="post" class="form form-horizontal" enctype="multipart/form-data">
          	<?php } else { ?>

          		<form action="?action=edit" name="addeditcategory" method="post" class="form form-horizontal" enctype="multipart/form-data">
          	<?php } ?>
            	<input  type="hidden" name="id" value="<?php echo $_GET['id'];?>" />
			<?php 
				$Language_query="SELECT id,name,status FROM languages ";
				$lan_result=mysqli_query($mysqli,$Language_query);	
			?>
              <div class="section">
                <div class="section-body">
				<div class="form-group">
                    <label class="col-md-3 control-label">Select Language :-</label>
                    <div class="col-md-6">
                    	<select name="language_id" onchange="language(this.value)" id="language_id"  class="form-control" required>
                    		<option value="">Select Language</option>

                    	<?php while($data=mysqli_fetch_array($lan_result)) { ?>
							<option value="<?php echo $data['id']; ?>" <?php if(isset($row['language_id'])){ echo ($row['language_id']==$data['id'])?"selected":"";} ?> ><?php echo $data['name']; ?></option>
                    	<?php } ?>
                    	</select>
                      
                    </div>
                  </div>

                  
                  <div class="form-group">
                    <label class="col-md-3 control-label">Upload Excel :-</label>
                    <div class="col-md-6">
                      <input type="file" name="file" id="file"  class="form-control" required>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-md-9">
                      <button type="submit" name="submit" class="btn btn-primary">Save</button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
            
	      </div>
	    </div>
	  </div>
	</div>
	 <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-head">
           Export Excel
          </div>
          <div class="card-body">
          
          	<form action="?action=export" name="addeditcategory" method="post" class="form form-horizontal" enctype="multipart/form-data">
          	
			<?php 
				$Language_query="SELECT id,name,status FROM languages ";
				$lan_result=mysqli_query($mysqli,$Language_query);	
			?>
              <div class="section">
                <div class="section-body">
				<div class="form-group">
                    <label class="col-md-3 control-label">Select Language :-</label>
                    <div class="col-md-6">
                    	<select name="language_id"  id="language_id"  class="form-control" required>
                    		<option value="">Select Language</option>
							<option value="all">ALL</option>
                    	<?php while($data=mysqli_fetch_array($lan_result)) { ?>
							<option value="<?php echo $data['id']; ?>" <?php if(isset($row['language_id'])){ echo ($row['language_id']==$data['id'])?"selected":"";} ?> ><?php echo $data['name']; ?></option>
                    	<?php } ?>
                    	</select>
                      
                    </div>
                  </div>

                  <div class="form-group">
                    <div class="col-md-9">
                      <button type="submit" name="submit" class="btn btn-primary">Export</button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
            
	      </div>
	    </div>
	  </div>
	</div>


       
<?php include("includes/footer.php");?>       
