<?php include("includes/connection.php");
 	  include("includes/function.php"); 	

	$permitted_chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';

	function generate_string($input, $strength = 6) {
		global $mysqli;
		$input_length = strlen($input);
		$random_string = '';
		for($i = 0; $i < $strength; $i++) {
			$random_character = $input[mt_rand(0, $input_length - 1)];
			$random_string .= $random_character;
		}
		$result=refer_code($random_string);  

		return ($result==true)?$random_string:generate_string($permitted_chars,$strength++);	
	}
	function refer_code($refer_code)
	{	
		global $mysqli;
		$check_refer_code="select email,refer_code from users  where  refer_code='{$refer_code}'";
		$refer_code = mysqli_query($mysqli,$check_refer_code) or die(mysqli_error($mysqli));
		$QueryCount=mysqli_num_rows($status);
		return ($QueryCount==0)?true:false; 
	}
 
	if(isset($_GET['register']))
 	{	
 		if($_POST)
 		{	 			
			$email=mysqli_real_escape_string($mysqli,$_POST['email']);
			$password=md5(mysqli_real_escape_string($mysqli,$_POST['password']));	
			$onesignalToken=mysqli_real_escape_string($mysqli,$_POST['onesignalToken']);
			$name=mysqli_real_escape_string($mysqli,$_POST['name']);			
			$mobile=$_POST['mobile'];	
				

			$refer_code=generate_string($permitted_chars);
				
			$pop_msg="";
			$device_id=mysqli_real_escape_string($mysqli,$_POST['device_id']);
			
			if(!empty($email) && !empty($name) && !empty($mobile) && !empty($password) && !empty($onesignalToken) && !empty($device_id) && !empty($refer_code) )
			{
				$check_query="select email from users  where  email='{$email}'";
				$status = mysqli_query($mysqli,$check_query) or die(mysqli_error($mysqli));
				$QueryCount=mysqli_num_rows($status);

				$device_id_query="select id from users where device_id='{$device_id}' ";
				$device_result = mysqli_query($mysqli,$device_id_query)or die(mysqli_error($mysqli));				
				$device_result_entry=mysqli_num_rows($device_result);
				
				if($device_result_entry>=1)
				{

						$pass=array(
						"status"=>false,
						"message"=>"Device id is already Exists"
						);
					
					header('Content-Type: application/json; charset=utf-8');
					echo $val= str_replace('\\/', '/', json_encode($pass,JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT));
					die();

				}
				
				if($QueryCount==0)
				{
					$getTag_query="insert into users  set email='{$email}', name='{$name}',mobile='{$mobile}',password='{$password}',onesignalToken='{$onesignalToken}',refer_code='{$refer_code}',  device_id='{$device_id}', question='' ";

					$result = mysqli_query($mysqli,$getTag_query)or die(mysqli_error($mysqli));
						$data=array();
					if($result)
					{
						$data=array(
							"status"=>true,
							"message"=>"Success"
						);
					}
					else
					{
						$data=array(
							"status"=>false,
							"message"=>mysqli_error($mysqli)
							
						);	
					}
				}
				else
				{
						$data=array(
							"status"=>false,
							"message"=>"Email already Exists"
						);	
				}
			}
			else
			{
				$data=array(
					"status"=>false,
					"message"=>"required field"
				);
			}	

			header( 'Content-Type: application/json; charset=utf-8' );
		    echo $val= str_replace('\\/', '/', json_encode($data,JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT));
			die();
		}
		else
		{

			$data=array(
			"message"=>"Request Not allow"
			);
			header( 'Content-Type: application/json; charset=utf-8' );
			echo $val= str_replace('\\/', '/', json_encode($data,JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT));
			die();
		}
 	}
 	else if(isset($_GET['login']))
 	{
 		if($_POST)
 		{	
			$email=mysqli_real_escape_string($mysqli,$_POST['email']);
			$password=md5(mysqli_real_escape_string($mysqli,$_POST['password']));	
			$onesignalToken=mysqli_real_escape_string($mysqli,$_POST['onesignalToken']);

 			if(!empty($email) && !empty($password) )
			{
	 			if(!empty($onesignalToken))
	 			{
		 			$checkOnesignalToken="select id,email,onesignalToken from users where email='{$email}' ";
					$token_result = mysqli_query($mysqli,$checkOnesignalToken)or die(mysqli_error($mysqli));
					$token_data=mysqli_fetch_assoc($token_result);
					if($token_data['onesignalToken']!=$onesignalToken)
					{
						$OnesignalTokenUpdate="update users set onesignalToken='{$onesignalToken}' where email='{$email}' ";
						mysqli_query($mysqli,$OnesignalTokenUpdate)or die(mysqli_error($mysqli));
					}
				}	
				
				$checkLogin="select id,email,password,name,mobile,onesignalToken from users where email='{$email}' ";
				$result = mysqli_query($mysqli,$checkLogin)or die(mysqli_error($mysqli));
				$checkCount=mysqli_num_rows($result);

				if($checkCount>=1)
				{		
						$row=$result->fetch_assoc();
						$data=array();
						if($row['password']===$password)
						{
							$data=array(
							"status"=>true,
							"message"=>"Success",
							"data"=>$row
							);
						}
						else
						{
							$data=array(
							"status"=>false,
							"message"=>"password doesn't match"

							);	
						}
				}
				else
				{
					$data=array(
							"status"=>false,
							"message"=>"Email not register"

							);
				}		
				header( 'Content-Type: application/json; charset=utf-8' );
			echo $val= str_replace('\\/', '/', json_encode($data,JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT));
			die();
			}
			else
			{
				$data=array(
					"status"=>false,
					"message"=>"required field"
				);
			}
			header( 'Content-Type: application/json; charset=utf-8' );
			echo $val= str_replace('\\/', '/', json_encode($data,JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT));
			die();
 		}
		else
		{
			$data=array(
			"message"=>"Request Not allow"
			);
			header( 'Content-Type: application/json; charset=utf-8' );
			echo $val= str_replace('\\/', '/', json_encode($data,JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT));
			die();
		}

 	}
 	 
 	else if(isset($_GET['question']) && isset($_GET['language']) && isset($_GET['device_id']))
 	{	
 		$device_id=$_GET['device_id'];

 		$user_query="SELECT id,question FROM users where device_id='{$device_id}' ";
		$user_res = mysqli_query($mysqli,$user_query)or die(mysqli_error($mysqli));
		$user_array=mysqli_fetch_assoc($user_res);

		$total_question_count_query="SELECT question FROM questions where language_id='{$_GET['language']}'";
		$total_res = mysqli_query($mysqli,$total_question_count_query)or die(mysqli_error($mysqli));
		$total_array=mysqli_fetch_assoc($total_res);
		$total_count=mysqli_num_rows($total_res);
		$db_question_num="";
		
		$quesion_array=array();

		if(!empty($user_array['question']))
		{
		 $db_question_num=count(json_decode($user_array['question'],true));
		}

		if($db_question_num>=$total_count)
		{
			$update_query="update users set question='' where device_id='{$device_id}'";
			mysqli_query($mysqli,$update_query)or die(mysqli_error($mysqli));

			$user_query="SELECT id,question FROM users where device_id='{$device_id}'";
			$user_res = mysqli_query($mysqli,$user_query)or die(mysqli_error($mysqli));
			$user_array=mysqli_fetch_assoc($user_res);
		}

		if(empty($user_array['question']))
		{
						
	 		$query2="SELECT * FROM questions where language_id='{$_GET['language']}'  ORDER BY rand() limit 15 ";
			$sql2 = mysqli_query($mysqli,$query2)or die(mysqli_error($mysqli));
		}else
		{
			$quesion_array=$arr=json_decode($user_array['question'],true);
			$arr=join(",",$arr);
			
	 		$query2="SELECT * FROM questions where language_id='{$_GET['language']}' and id not in ({$arr})  ORDER BY rand() limit 15 ";
			$sql2 = mysqli_query($mysqli,$query2)or die(mysqli_error($mysqli));
		}

		$pass=array();
	
	
		$question_id="";
		$j=0;
		while ($row = mysqli_fetch_assoc($sql2))
		{	
			if($row['language_id']=='1')
			{	
				$opt=json_decode($row['options'],true);
				shuffle($opt);
				$pass[$j]['id']=(int)$row['id'];
				$pass[$j]['question']=$row['question'];	
				$pass[$j]['options']=$opt;	
				$pass[$j]['answer']=$row['answer'];	
			}
			else
			{	
				$opt=json_decode($row['options'],true);
				shuffle($opt);
				$pass[$j]['id']=(int)$row['id'];
				$pass[$j]['question']=json_decode($row['question']);	
				$pass[$j]['options']=$opt;	
				$pass[$j]['answer']=json_decode($row['answer']);	
			}
			$j++;			
		}	
		if(count($pass)<15)
		{	
			$userQues_query="SELECT id,question FROM users where device_id='{$device_id}'";
			$userQues_result = mysqli_query($mysqli,$userQues_query)or die(mysqli_error($mysqli));
			$userQues_data=mysqli_fetch_assoc($userQues_result);

			if(!empty($userQues_data['question']))
			{
				$question=json_decode($userQues_data['question'],true);
				$question_pass=implode(",",$question);
				$getQues_query="SELECT id,question,options,answer,language_id FROM questions where id IN ({$question_pass}) ORDER BY rand()";
				$getQues_result = mysqli_query($mysqli,$getQues_query)or die(mysqli_error($mysqli));
				$temp=array();

				while ($row = mysqli_fetch_assoc($getQues_result))
				{	
				
					if($row['language_id']=='1')
					{	
						$opt=json_decode($row['options'],true);
						shuffle($opt);
						$pass[$j]['id']=(int)$row['id'];
						$pass[$j]['question']=$row['question'];	
						$pass[$j]['options']=$opt;	
						$pass[$j]['answer']=$row['answer'];	
					}
					else
					{	
						$opt=json_decode($row['options'],true);
						shuffle($opt);
						$pass[$j]['id']=(int)$row['id'];
						$pass[$j]['question']=json_decode($row['question']);	
						$pass[$j]['options']=$opt;	
						$pass[$j]['answer']=json_decode($row['answer']);	
					}
						$j++;			
				}
				$pass=array_slice($pass,0,14);
			}

		}	
			$jsonObj['status']=true;
			$jsonObj['message']="success";
			$jsonObj['data']=$pass;

		header('Content-Type: application/json; charset=utf-8');
		echo $val= str_replace('\\/', '/', json_encode($jsonObj,JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT));
		die();
 	} 
 	else if(isset($_GET['score_update']))
 	{			
 			$json=json_decode(file_get_contents("php://input"), true);
 			 			
 				if(isset($_POST))
				{	
					
					$user_id=$json['user_id'];
					$contest_id=$json['contest_id'];
					$score_point=$json['score'];

					$get_ques_query="SELECT id,question FROM users where id='{$user_id}'";
					$get_ques_query_result = mysqli_query($mysqli,$get_ques_query)or die(mysqli_error($mysqli));
					$old_ques_data=mysqli_fetch_assoc($get_ques_query_result);

					$ques_id=array();
					$old_ques_data['question']=json_decode($old_ques_data['question']);
					if(count($old_ques_data['question'])>=1)
					{	
						$ques_id=json_encode(array_unique(array_merge($old_ques_data['question'],$json['questions'])));
					}
					else
					{
						$ques_id=json_encode($json['questions']);
					}

					$query2="select id,score_point,user_id from score_board where user_id='{$user_id}' and contest_id='{$contest_id}' ";
					$sql2 = mysqli_query($mysqli,$query2)or die(mysqli_error($mysqli));
					$data=mysqli_fetch_assoc($sql2);
					$check_entry=mysqli_num_rows($sql2);
				
					$get_point=$data['score_point'];
					$total_point=$get_point+$score_point;
					if($check_entry==0)
					{
						$query3="insert into score_board set score_point='{$total_point}',user_id='{$user_id}',contest_id='{$contest_id}' ";
						$sql3 = mysqli_query($mysqli,$query3)or die(mysqli_error($mysqli));
						
				    }
				    else
				    {
				    	$query3="update score_board set score_point='{$total_point}' where user_id='{$user_id}' and contest_id='{$contest_id}' ";
						$sql3 = mysqli_query($mysqli,$query3)or die(mysqli_error($mysqli));
				    	
				    }
					$pass = array();

					//Question update
					if(count($ques_id)!=0)
					{
					$user_ques_query="update users set question='{$ques_id}' where id='{$user_id}' ";
					$user_ques_query_result = mysqli_query($mysqli,$user_ques_query)or die(mysqli_error($mysqli));
					}	


					if($sql2)
					{
						$pass=array(
						"status"=>true,
						"message"=>"success"
						);
					}
					else
					{
						$pass=array(
						"status"=>false,
						"message"=>"error"
						);
					}

					header('Content-Type: application/json; charset=utf-8');
					echo $val= str_replace('\\/', '/', json_encode($pass,JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT));
					die();
	   			}
 	}
 	
 	else if(isset($_GET['score_board']))
 	{	
 		$contest_id=$_GET['contest_id'];	
  		$query="SELECT sb.score_point+sb.refer_point as score_point,sb.user_id ,users.name FROM score_board  sb inner join users on users.id=sb.user_id where sb.contest_id='{$contest_id}' and (sb.score_point+sb.refer_point !=0 ) order by score_point desc,sb.updated_at ASC ";
		$sql = mysqli_query($mysqli,$query) or die(mysqli_error($mysqli));
		$jsonObj=array();
		$i=0;
		$rankCount=1;
		$jsonObj['status']=true;
		$jsonObj['message']="success";
		$rank="";
		$user_id=$_GET['user_id'];
		$temp=array();
		while($data = mysqli_fetch_assoc($sql))
		{				
			$temp[$i]['score_point']=(int)$data['score_point'];
			$temp[$i]['user_id']=(int)$data['user_id'];
			$temp[$i]['name']=$data['name'];
			if($data['user_id']==$user_id)
			{
				$rank=$rankCount;
			}
			$rankCount++;
			$i++;						
		}
		$jsonObj['data']['rank']=$rank;
		$jsonObj['data']['score_board']=$temp;

		header('Content-Type: application/json; charset=utf-8');
		echo $val= str_replace('\\/', '/', json_encode($jsonObj,JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT));
		die();
	   			
 	}
 	
 	else if(isset($_GET['ReferPointScoreUpdate']))
 	{
 		if(!empty($_GET['refer_code']))
 		{	
 		
 			$code=$_GET['refer_code'];
			$refer_from_query="select id,refer_from_point,refer_to_point,refer_from,refer_to from referral_info where refer_from='{$code}' ";
			$refer_from_result = mysqli_query($mysqli,$refer_from_query) or die(mysqli_error($mysqli));
		
			$refer_from_count;
			$refer_to_count;
			$total;
			while($row = mysqli_fetch_assoc($refer_from_result))
			{
				$refer_from_count+=	$row['refer_from_point'];
			}

			$refer_to_query="select id,refer_from_point,refer_to_point,refer_from,refer_to from referral_info where refer_to='{$code}' ";
			$refer_to_result = mysqli_query($mysqli,$refer_to_query) or die(mysqli_error($mysqli));

			while($row = mysqli_fetch_assoc($refer_to_result))
			{
				$refer_to_count+=$row['refer_to_point'];
			}
			$total=$refer_from_count+$refer_to_count;

			// Get Contest id
			$contest_query="select id,start_date,end_date,playing_status from contest";
			$contest_result = mysqli_query($mysqli,$contest_query) or die(mysqli_error($mysqli));
			
			$date=date('Y-m-d');
			$dateStr=strtotime($date);
			$contestId="";
			while($row = mysqli_fetch_assoc($contest_result))
			{	
				$startDate=$row['start_date'];
				$endDate=$row['end_date'];

				$startDateStr=strtotime($row['start_date']);
				$endDateStr=strtotime($row['end_date']);

				if(($dateStr>=$startDateStr)&&($dateStr<=$endDateStr))
				{
					$contestId=$row['id'];
				}
			}
			// Get User Id
			$user_id_query="select id,email from users where refer_code='{$code}' ";
			$user_id_result = mysqli_query($mysqli,$user_id_query) or die(mysqli_error($mysqli));
			$user_id_data = mysqli_fetch_assoc($user_id_result);
			$user_id=$user_id_data['id'];


			$query2="select * from score_board where user_id='{$user_id}' and contest_id='{$contestId}' ";
			$query2_result = mysqli_query($mysqli,$query2) or die(mysqli_error($mysqli));
			$query2_data = mysqli_fetch_assoc($query2_result);
			$query2_count = mysqli_num_rows($query2_result);
			
			$scoreBoardId=$query2_data['id'];
			$preValue=$query2_data['refer_point'];
			$total=$total+$preValue;

			
			if($query2_count>=1)
			{	
				$query3="update score_board set refer_point='{$total}' where id ='{$scoreBoardId}' ";
				$query3_result = mysqli_query($mysqli,$query3) or die(mysqli_error($mysqli));
				
			}
			else
			{	
				$query3="insert into score_board set user_id='{$user_id}',refer_point='{$total}',contest_id='{$contestId}' ";
				$query3_result = mysqli_query($mysqli,$query3) or die(mysqli_error($mysqli));
			}	

			if($query3_result)
			{

				$refer_from_query="update referral_info set refer_from_point='0' where refer_from='{$code}' ";
				$refer_from_result = mysqli_query($mysqli,$refer_from_query) or die(mysqli_error($mysqli));	


				$refer_to_query="update  referral_info set refer_to_point='0' where refer_to='{$code}' ";
				$refer_to_result = mysqli_query($mysqli,$refer_to_query) or die(mysqli_error($mysqli));	
				
				$refer_from_query="select id,refer_from_point,refer_to_point,refer_from,refer_to from referral_info where refer_from='{$code}' ";
				$refer_from_result = mysqli_query($mysqli,$refer_from_query) or die(mysqli_error($mysqli));	

				$refer_from_count;
				$refer_to_count;
				$total;
				while($row = mysqli_fetch_assoc($refer_from_result))
				{
				$refer_from_count+=	$row['refer_from_point'];
				}

				$refer_to_query="select id,refer_from_point,refer_to_point,refer_from,refer_to from referral_info where refer_to='{$code}' ";
				$refer_to_result = mysqli_query($mysqli,$refer_to_query) or die(mysqli_error($mysqli));

				while($row = mysqli_fetch_assoc($refer_to_result))
				{
				$refer_to_count+=$row['refer_to_point'];
				}
				$total=$refer_from_count+$refer_to_count;

				$pass=array(
					"status"=>true,
					"message"=>"success",
					"data"=>array(
						"response"=>"updated referral points",
						'refer_point'=>$total
						)
				);
				
			}
			else
			{

				$pass=array(
						"status"=>false,
				"message"=>"error",
				"data"=>array(
					"response"=>mysqli_error($mysqli),
					)
				);
				
			}
			header('Content-Type: application/json; charset=utf-8');
				echo $val= str_replace('\\/', '/', json_encode($pass,JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT));
				die();

 		}
 		else
 		{
 			$pass=array(
 					"status"=>false,
				"message"=>"Refer Code invalid",
				"data"=>""
			);
			header('Content-Type: application/json; charset=utf-8');
			echo $val= str_replace('\\/', '/', json_encode($pass,JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT));
			die();

 		}
 	}
 	else if(isset($_GET['refer']) )
 	{	
 		$refer_from=$_POST['refer_from'];
 		$refer_to=$_POST['refer_to'];

 		// select point 
 		$point_query="select refer_from,refer_to from tbl_settings";

		$point_result = mysqli_query($mysqli,$point_query) or die(mysqli_error($mysqli));
		$point_data = mysqli_fetch_assoc($point_result);

		$refer_from_point=$point_data['refer_from'];
		$refer_to_point=$point_data['refer_to'];

		
			
		$refer_code_query="select id,email from users where refer_code='{$refer_from}' ";
		$refer_code_result = mysqli_query($mysqli,$refer_code_query) or die(mysqli_error($mysqli));
		$refer_code_data = mysqli_fetch_assoc($refer_code_result);
		$refer_code_length =  mysqli_num_rows($refer_code_result);
		
		if($refer_code_length==0)
		{	
			$pass=array(
				"status"=>false,
				"message"=>"Refer Code is invalid",
				"data"=>""
			);
			header('Content-Type: application/json; charset=utf-8');
			echo $val= str_replace('\\/', '/', json_encode($pass,JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT));
			die();	
		}


		$refer_to_code_query="select id,email from users where refer_code='{$refer_to}' ";
		$refer_to_code_result = mysqli_query($mysqli,$refer_to_code_query) or die(mysqli_error($mysqli));
		$refer_to_code_data = mysqli_fetch_assoc($refer_to_code_result);
		$refer_to_code_length =  mysqli_num_rows($refer_to_code_result);
		
		if($refer_to_code_length==0)
		{	
			$pass=array(
				"status"=>false,
				"message"=>"Refer Code is invalid",
				"data"=>""
			);
			header('Content-Type: application/json; charset=utf-8');
			echo $val= str_replace('\\/', '/', json_encode($pass,JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT));
			die();	
		}


		//Check Point on refer code
		$check_query="select id,refer_from_point,refer_to_point,refer_from,refer_to from referral_info where (refer_from='{$refer_from}' and refer_to='{$refer_to}') or  (refer_from='{$refer_to}' and refer_to='{$refer_from}')";
		$check_result = mysqli_query($mysqli,$check_query) or die(mysqli_error($mysqli));
		$check_data = mysqli_fetch_assoc($check_result);
		$check_length =  mysqli_num_rows($check_result);

		if($check_length>0)
		{	
			$pass=array(
				"status"=>false,
				"message"=>"already redeem",
				"data"=>"already redeem"
			);
			header('Content-Type: application/json; charset=utf-8');
			echo $val= str_replace('\\/', '/', json_encode($pass,JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT));
			die();	
		}
		if($refer_from===$refer_to)
		{	
			$pass=array(
				"status"=>false,
				"message"=>"invalid referal link",
				"data"=>"invalid referal link"
			);
			header('Content-Type: application/json; charset=utf-8');
			echo $val= str_replace('\\/', '/', json_encode($pass,JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT));
			die();	
		}

		$query2="insert into referral_info set refer_from='{$refer_from}',refer_to='{$refer_to}',refer_from_point='{$refer_from_point}',refer_to_point='{$refer_to_point}' ";
		$sql2 = mysqli_query($mysqli,$query2) or die(mysqli_error($mysqli));
		$pass = array();
		if($sql2)
		{	

			$code=$refer_to;

			$refer_from_query="select id,refer_from_point,refer_to_point,refer_from,refer_to from referral_info where refer_from='{$code}' ";
			$refer_from_result = mysqli_query($mysqli,$refer_from_query) or die(mysqli_error($mysqli));

			$refer_from_count;
			$refer_to_count;
			$total;
			while($row = mysqli_fetch_assoc($refer_from_result))
			{
			$refer_from_count+=	$row['refer_from_point'];
			}

			$refer_to_query="select id,refer_from_point,refer_to_point,refer_from,refer_to from referral_info where refer_to='{$code}' ";
			$refer_to_result = mysqli_query($mysqli,$refer_to_query) or die(mysqli_error($mysqli));

			while($row = mysqli_fetch_assoc($refer_to_result))
			{
			$refer_to_count+=$row['refer_to_point'];
			}
			$total=$refer_from_count+$refer_to_count;
			$arr=array("refer_point"=>$total);
			$pass=array(
				"status"=>true,
			"message"=>"success",
			"data"=>$arr
			);
			
		}
		else
		{
			$pass=array(
				"status"=>false,
			"message"=>"error"
			);
		}

		header('Content-Type: application/json; charset=utf-8');
		echo $val= str_replace('\\/', '/', json_encode($pass,JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT));
		die();
	    
 	}
	else if(isset($_GET['action'])=="settings")
	{
		$jsonObj= array();	
		$arr=array();
		$con=array();

		$lan_query="SELECT id,name FROM languages ";
		$lan = mysqli_query($mysqli,$lan_query)or die(mysqli_error($mysqli));
		$i=0;
		while($row = mysqli_fetch_assoc($lan))
		{
			$arr[$i]['id']=(int)$row['id'];	
			$arr[$i]['language_name']=$row['name'];		
			$arr[$i]['isActive'];
			$check_query="select * from questions where language_id='{$row['id']}' ";
			$check_result = mysqli_query($mysqli,$check_query) or die(mysqli_error($mysqli));
			$check_data = mysqli_fetch_assoc($check_result);
			$check_length =  mysqli_num_rows($check_result);

			$arr[$i]['isActive']=($check_length<15)?false:true;


			$i++;	
		}
		$date= date('Y-m-d');

		$contest_query="SELECT * FROM contest ";
		$contest = mysqli_query($mysqli,$contest_query)or die(mysqli_error($mysqli));
		$i=0;
		while($row = mysqli_fetch_assoc($contest))
		{	

			if((strtotime($date)>=strtotime($row['start_date'])) && (strtotime($date)<=strtotime($row['end_date'])) )
			{	
				$con[$i]['id']=(int)$row['id'];	
				$con[$i]['start_date']=date("d-M-Y",strtotime($row['start_date']));
				$con[$i]['end_date']=date("d-M-Y",strtotime($row['end_date']));
				$con[$i]['playing_status']=(bool)$row['playing_status'];
				$con[$i]['msg']=$row['msg'];
				$i++;
			}		
		}

		$query="SELECT * FROM tbl_settings WHERE id='1'";
		$sql = mysqli_query($mysqli,$query)or die(mysqli_error($mysqli));

		while($data = mysqli_fetch_assoc($sql))
		{
 			$row['adShown'] = ($data['banner_ad']=="true")?true:false;
 			$row['interstital_ad_id'] = $data['interstital_ad_id']; 			
 			$row['reward_ad_id'] = $data['reward_ad_id']; 			
 			$row['banner_ad_id'] = $data['banner_ad_id'];
 			$row['refer_from'] = (int)$data['refer_from'];
 			$row['refer_to'] = (int)$data['refer_to']; 			
 			$row['update_title'] = $data['update_title'];
 			$row['update_version'] = (int)$data['update_version'];
 			$row['update_url'] = $data['update_url'];
 			$row['update_msg'] = $data['update_msg'];
 			
 			$row['force_update'] = ($data['force_update']=="true")?true:false; 			
 			$row['playing_allowed'] = ($data['playing_allowed']=="true")?true:false; 
 			$row['playing_msg'] = $data['playing_msg'];			
 			$row['referral_msg'] = $data['referral_msg'];
 			$row['languages'] = $arr;
 			$row['contest'] = $con;

			$jsonObj=$row;		
		}			
		header( 'Content-Type: application/json; charset=utf-8' );
	    echo $val= str_replace('\\/', '/', json_encode($jsonObj,JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT));
		die();	
	}	
	
	else if(isset($_GET['userinfo']))
	{
		$jsonObj= array();	
		$id=mysqli_real_escape_string($_GET['id']);

		$query="SELECT * FROM users WHERE id='".$_GET['id']."' ";

		$user_info=$sql = mysqli_query($mysqli,$query)or die(mysqli_error($mysqli));

		$data = mysqli_fetch_assoc($user_info);			
		
		$code=$data['refer_code'];
		
		$refer_from_query="select id,refer_from_point,refer_to_point,refer_from,refer_to from referral_info where refer_from='{$code}' ";
		$refer_from_result = mysqli_query($mysqli,$refer_from_query) or die(mysqli_error($mysqli));

		$refer_from_count;
		$refer_to_count;
		$total;
		while($row = mysqli_fetch_assoc($refer_from_result))
		{
			$refer_from_count+=	$row['refer_from_point'];
		}

		$refer_to_query="select id,refer_from_point,refer_to_point,refer_from,refer_to from referral_info where refer_to='{$code}' ";
		$refer_to_result = mysqli_query($mysqli,$refer_to_query) or die(mysqli_error($mysqli));

		while($row = mysqli_fetch_assoc($refer_to_result))
		{
			$refer_to_count+=$row['refer_to_point'];
		}
		$total=$refer_from_count+$refer_to_count;

		$blocked_status;
		if($data['blocked_status']=="Y")
		{
			$blocked_status=true;	
		}
		else{
			$blocked_status=false;
		}	
	    $temp['id']=(int)$data['id'];
	    $temp['email']=$data['email'];
	    $temp['name']=$data['name'];
	    $temp['mobile']=$data['mobile'];
	    $temp['onesignalToken']=$data['onesignalToken'];
	    $temp['refer_code']=$data['refer_code'];
	    $temp['device_id']=$data['device_id'];
	    $temp['blocked_status']=$blocked_status;
	    $temp['refer_point']=$total;
		
		array_push($jsonObj,$temp);
		
		header( 'Content-Type: application/json; charset=utf-8' );
	    echo $val= str_replace('\\/', '/', json_encode($jsonObj,JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT));
		die();	
	}
	else if(isset($_GET['contact']))
	{

		if(isset($_POST))
 		{			

			$jsonObj= array();	
			$id=mysqli_real_escape_string($mysqli,$_POST['user_id']);
			$query=mysqli_real_escape_string($mysqli,$_POST['query']);

			$query="insert into contact set user_id='{$id}' , query='{$query}' ";
			$sql = mysqli_query($mysqli,$query)or die(mysqli_error($mysqli));

			if($sql)
			{
				$jsonObj=array(
				"status"=>true,
				"message"=>"success"
				);
			}
			else
			{
				$jsonObj=array(
				"status"=>false,
				"message"=>mysqli_error($mysqli)
				);
			}
				
			header( 'Content-Type: application/json; charset=utf-8' );
		    echo $val= str_replace('\\/', '/', json_encode($jsonObj,JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT));
			die();	
	    }
	    else
	    {	
	    	$pass=array(
	    		"message"=>"Request not allow"
	    	);
	    	header('Content-Type: application/json; charset=utf-8');
			echo $val= str_replace('\\/', '/', json_encode($pass,JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT));
			die();
	    }
	}
	else
	{
		$jsonObj=array(
			"message"=>"Url Not allow"
		);
		header( 'Content-Type: application/json; charset=utf-8' );
	    echo $val= str_replace('\\/', '/', json_encode($jsonObj,JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT));
		die();
	}	
	 
	 
?>