<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class Api_model extends CI_Model{
	
	function __construct(){
		parent::__construct();
		$this->load->database();
		 date_default_timezone_set('Asia/Jakarta');

	}

	public function save_session($sessionID){
		$save = array();
		$save['session_id'] = $sessionID;
		$save['status'] = 1;
		$save['datetime'] = date('Y-m-d H:i:s');

		return $this->db->insert('dji_session_data', $save);
	}

	public function get_data_produk($tipe){
		$this->db->where('tipe_produk', $tipe);
		return $this->db->get('dji_produk')->result();
	}

	public function create_session_id($param){
		$method = '/auth/Sign-On';
		$result = new stdClass();
		$response = $this->Func_model->request_curl($param, $method);
		if($response['http_code'] == 200){
            $res = json_decode($response['body']);
            $result->resp = $response['body'];

            $data_update = array(
            	'status'=> 9
            );

            $this->db->update('dji_session_data', $data_update);

            $session_data = array(
            	'session_id' => $res->SessionID,
            	'status' => 1,
            	'datetime' => date('Y-m-d H:i:s')
            );

            $save = $this->db->insert('dji_session_data', $session_data);
            if($this->db->affected_rows() > 1){
            	$result->error = "Gagal memproses data(1)";
            	return $result;
            }

        }else{
        	$result->error = $response['body'];
        	return $result;
        }

        return $result;

	}

	public function get_session_id(){
		$this->db->select('session_id');
        $this->db->from('dji_session_data');
        $this->db->where('status', '1');
 
		return $this->db->get()->row('session_id');
	}

	public function request_inquiry_services($param){
		$method = '/Services/Inquiry';
		$result = new stdClass();

		// echo $this->Api_model->get_session_id();die();

		$request = array(
			'sessionID' => $this->Api_model->get_session_id(),
			'merchantID' => 'DJI000358',
			'productID' => $param->productID,
			'customerID' => $param->customerID,
			'referenceID' => '1234567789'
		);

		// echo $param->productID;die();
		$response = $this->Func_model->request_curl($request, $method);
		if($response['http_code'] == 200){
            $res = json_decode($response['body']);
            // $result->resp = $response['body'];

              	// $decode = json_decode($res);
		        if($res->rc == '00'){
	            	$tampung['rc'] = $res->rc;
			        $tampung['jmlTagihan']= $res->data->jmlTagihan;
			        $tampung['tagihan']= intval($res->data->tagihan);
			        $tampung['admin']= $res->data->admin;
			        $tampung['denda']= $res->data->denda;
			        $tampung['total']= $res->data->total;
			        $tampung['rincian'] = $res->data->rincian;

			        $result = $tampung;

	            }else{
	            	$tampung['rc'] = $res->rc;
	            	$tampung['description'] = $res->description;
	            	$result = $tampung;
	            }

            $insert_data = array(
            	'tipe' => 1,
            	'request' => json_encode($request),
            	'respon' => json_encode($res),
            	'datetime' => date('Y-m-d H:i:s')
            );

            $save = $this->db->insert('dji_log_data', $insert_data);
            if($this->db->affected_rows() > 1){
            	$result->error = "Gagal memproses data(1)";
            	return $result;
            }

        }else{
        	$result->error = $response['body'];
        	return $result;
        }

        return $result;

	}

	public function request_payemnt_services($param){
		$method = '/Services/Payment';
		$result = new stdClass();

		// echo $this->Api_model->get_session_id();die();

		$request = array(
			'sessionID' => $this->Api_model->get_session_id(),
			'merchantID' => 'DJI000358',
			'productID' => $param->productID,
			'customerID' => $param->customerID,
			'referenceID' => '1234567789',
			'tagihan' => $param->tagihan,
			'admin' => $param->admin,
			'total' => $param->total
		);

		// echo $param->productID;die();
		$response = $this->Func_model->request_curl($request, $method);
		if($response['http_code'] == 200){
            $res = json_decode($response['body']);

            $tampung = array();
            if($res->rc == '00'){
            	$tampung['rc'] = $res->rc;
		        $tampung['jmlTagihan']= $res->data->jmlTagihan;
		        $tampung['tagihan']= intval($res->data->tagihan);
		        $tampung['admin']= $res->data->admin;
		        $tampung['denda']= $res->data->denda;
		        $tampung['total']= $res->data->total;
		        $tampung['rincian'] = $res->data->rincian;

		        $result = $tampung;

            }else{
            	$tampung['rc'] = $res->rc;
            	$tampung['description'] = $res->description;
            	$result = $tampung;
            }
		      
            $insert_data = array(
            	'tipe' => 2,
            	'request' => json_encode($request),
            	'respon' => json_encode($res),
            	'datetime' => date('Y-m-d H:i:s')
            );

            $save = $this->db->insert('dji_log_data', $insert_data);
            if($this->db->affected_rows() > 1){
            	$result->error = "Gagal memproses data(1)";
            	return $result;
            }

        }else{
        	$result->error = $response['body'];
        	return $result;
        }

        return $result;

	}
}

?>