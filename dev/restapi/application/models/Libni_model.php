<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class Libni_model extends CI_Model{

	
	function __construct(){
		parent::__construct();
		$this->load->model('Account_model');
		

	}


	public function create_jwt_token($data){
		// Create token header as a JSON string
		$header = JSON_encode([
		  'alg' => 'HS256',
		  'typ' => 'JWT'
		  
		]);

		$apiSecret = $this->Account_model->get_api_secret();
		$clientId =  $this->Account_model->get_client_id();
		// Create token payload as a JSON string
		$data = json_decode($data);

		$payload = JSON_encode([
		  'clientId' => $clientId,
		  'accountNo' => $data->accountNo
		]);
		// Encode Header to Base64Url String
		$base64UrlHeader = $this->base64url_encode($header);
		// Encode Payload to Base64Url String
		$base64UrlPayload = $this->base64url_encode($payload);
		// Create Signature Hash
		$signature = hash_hmac(
		  'sha256', $base64UrlHeader.".".$base64UrlPayload, $apiSecret, true
		);
		// Encode Signature to Base64Url String
		// $base64UrlSignature = str_replace(
		//   ['+', '/', '='], ['-', '_', ''], base64_encode($signature)
		// );

		$base64UrlSignature = $this->base64url_encode($signature);
		// Create JWT
		$jwt = $base64UrlHeader.".".$base64UrlPayload.".".$base64UrlSignature;
		return $jwt;

	}

	public function create_access_token(){
        $api_url = 'https://apidev.bni.co.id:8067/api/oauth/token';

		$username = $clientId =  $this->Account_model->get_client_name();
		$password = $clientId =  $this->Account_model->get_name_secret();

		$fields = "grant_type=client_credentials";

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $api_url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded',
        	'Authorization: Basic ' . base64_encode($username . ':' . $password)));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        $result = array(
				'body' => curl_exec($ch),
				'http_code' => curl_getinfo($ch, CURLINFO_HTTP_CODE)
			);

			curl_close($ch);


		return $result;
	}

	public function curl_bni($data, $token, $method){
		$url = "https://apidev.bni.co.id:8067/";
		$api_url = $url. $method. '?'.'access_token='.$token;

		// echo json_decode($data);die();

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $api_url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json',
        	'x-api-key:959378cf-ee8d-491e-beeb-4e18be1964fb'));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS,json_encode($data));
		// curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        $result = array(
				'body' => curl_exec($ch),
				'http_code' => curl_getinfo($ch, CURLINFO_HTTP_CODE)
			);

		curl_close($ch);
		 return $result;
		

	}

	 function base64url_encode($data) {
		return rtrim(strtr(base64_encode($data), '+/', '-_'), '=');
	}

	 function base64url_decode($data) {
		return base64_decode(str_pad(strtr($data, '-_', '+/'), strlen($data) % 4, '=', STR_PAD_RIGHT));
	}
	
}

?>