<?php

defined('BASEPATH') OR exit('No direct script access allowed');



/**

 * 

 */

class Member_model extends CI_Model{

	

	function __construct(){

		parent::__construct();

		$this->load->database();

		 date_default_timezone_set('Asia/Jakarta');



	}



	public function get_data_member($condition){

		$this->db->select('*, getJumlahDeposit(username) as gateshare,

							getJumlahDownline(username) as downline');

        $this->db->from('member');

        $this->db->where($condition);

        return $this->db->get();

	}

	

	public function get_detail_member($condition){

		$this->db->select('getJumlahDeposit(username) as gateshare,

							getJumlahDownline(username) as downline,

							HitungWalletBalance(username) as balance');

		$this->db->from('member');

        $this->db->where($condition);

        return $this->db->get();

	}

	public function get_serial_card($condition){
		$this->db->select('serial, pin, status, pasif, idmlm');
		$this->db->from('card');
		$this->db->where($condition);
		return $this->db->get();
	}

	public function get_konfigurasi_sms(){
		$this->db->select('userkey, passkey, api_sms');
		$this->db->from('configuration');
		return $this->db->get();
	}

	public function insert_otp_reg($data){
		$status = $this->db->insert('otpreg', $data);
		return $status;
	}

	public function get_provinsi(){
		$this->db->select('*');
		$this->db->from('provinsi');
		return $this->db->get();
	}

	public function get_kota($conds){
		$this->db->select('*');
		$this->db->from('kota');
		$this->db->where($conds);
		return $this->db->get();
	}

}



?>