<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class Bank_model extends CI_Model{
	
	function __construct(){
		parent::__construct();
		$this->load->database();
       $this->load->model('Libni_model');
       $this->load->model('Account_model');

	}


	public function request_get_balance($param){
		$result = new stdClass();
		$method = 'H2H/v2/getbalance';
		$jwtToken = $this->Libni_model->create_jwt_token($param);

		$req = json_decode($param);
		$request = array(
            'clientId' => $this->Account_model->get_client_id(),
            'accountNo' => $req->accountNo,
            'signature' => $jwtToken

        );
        $resToken = $this->Libni_model->create_access_token();
        $res = json_decode($resToken['body']);
        // echo json_encode($res);die();
        $token = $res->access_token;

        $result = $this->Libni_model->curl_bni($request, $token, $method);

        if(!empty($data->error)){
            $respon['message'] = $data->error;
            $respon['data'] = null;
        }

        $rex = json_decode($result['body']);
        $data_result['res_code'] = $rex->getBalanceResponse->parameters->responseCode;
        $data_result['res_message'] = $rex->getBalanceResponse->parameters->responseMessage;

        if($data_result['res_code'] == '0001'){

            $data_result['res_time'] = $rex->getBalanceResponse->parameters->responseTimestamp;
            $data_result['res_nama'] = $rex->getBalanceResponse->parameters->customerName;
            $data_result['res_currency'] = $rex->getBalanceResponse->parameters->accountCurrency;
            $data_result['res_balance'] = $rex->getBalanceResponse->parameters->accountBalance;


            $respon['message'] = 'OK';
            $respon['data'] = $data_result;
            $result = $respon;
        }else{
            $data_result['error'] = $rex->getBalanceResponse->parameters->errorMessage;
            $respon['message'] = 'Error';
            $respon['data'] = $data_result;
            $result = $respon;
        }

        // process to log

        $insert_data = array(
            	'tipe' => 1,
            	'request' => json_encode($request),
            	'respon' => json_encode($rex),
            	'datetime' => date('Y-m-d H:i:s')
         );

         $save = $this->db->insert('bni_log_data', $insert_data);
         if($this->db->affected_rows() > 1){
            	$result->error = "Gagal memproses data(1)";
            	return $result;
         }

        return $result;


	}


	public function request_inhouse_inq($param){
		$result = new stdClass();
		$method = 'H2H/v2/getinhouseinquiry';
		$jwtToken = $this->Libni_model->create_jwt_token($param);
		
		$req = json_decode($param);
		$request = array(
            'clientId' => $this->Account_model->get_client_id(),
            'accountNo' => $req->accountNo,
            'signature' => $jwtToken

        );
        $resToken = $this->Libni_model->create_access_token();
        $res = json_decode($resToken['body']);
        $token = $res->access_token;

        $result = $this->Libni_model->curl_bni($request, $token, $method);

        if(!empty($data->error)){
            $respon['message'] = $data->error;
            $respon['data'] = null;
        }

        $rex = json_decode($result['body']);
        $data_result['res_code'] = $rex->getInHouseInquiryResponse->parameters->responseCode;
        $data_result['res_message'] = $rex->getInHouseInquiryResponse->parameters->responseMessage;

        if($data_result['res_code'] == '0001'){

            $data_result['res_time'] = $rex->getInHouseInquiryResponse->parameters->responseTimestamp;
            $data_result['res_nama'] = $rex->getInHouseInquiryResponse->parameters->customerName;
            $data_result['res_currency'] = $rex->getInHouseInquiryResponse->parameters->accountCurrency;
            $data_result['res_accnumber'] = $rex->getInHouseInquiryResponse->parameters->accountNumber;
            $data_result['res_status'] = $rex->getInHouseInquiryResponse->parameters->accountStatus;
            $data_result['res_status'] = $rex->getInHouseInquiryResponse->parameters->accountType;


            $respon['message'] = 'OK';
            $respon['data'] = $data_result;
            $result = $respon;
        }else{
            $data_result['error'] = $rex->getInHouseInquiryResponse->parameters->errorMessage;
            $respon['message'] = 'Error';
            $respon['data'] = $data_result;
            $result = $respon;
        }

        // process to log

        $insert_data = array(
            	'tipe' => 1,
            	'request' => json_encode($request),
            	'respon' => json_encode($rex),
            	'datetime' => date('Y-m-d H:i:s')
         );

         $save = $this->db->insert('bni_log_data', $insert_data);
         if($this->db->affected_rows() > 1){
            	$result->error = "Gagal memproses data(1)";
            	return $result;
         }

        return $result;


	}


	public function request_dopayment($param){
		$result = new stdClass();
		$method = 'H2H/v2/dopayment';
	}

	



	

	
}

?>