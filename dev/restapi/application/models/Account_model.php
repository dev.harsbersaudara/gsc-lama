<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class Account_model extends CI_Model{
	
	function __construct(){
		parent::__construct();
		$this->load->model('Libni_model');

	}

	function get_one_info( $conds = array()) {

		$sql = "SELECT * FROM bni_api WHERE `id` = '" . $conds['id'] . "' ";

		$query = $this->db->query($sql);

		return $query;
	}

	function get_client_id(){
		$conds['id'] = 1;
		$api_info = $this->get_one_info($conds)->result();
		$clientId = 'IDBNI'. $this->Libni_model->base64url_encode(ltrim($api_info[0]->name));
		return $clientId;
	}

	function get_api_secret(){
		$conds['id'] = 1;
		$api_info = $this->get_one_info($conds)->result();
		$apiSecret = ltrim($api_info[0]->key_secret);
		return $apiSecret;
	}

	function get_client_name(){
		$conds['id'] = 1;
		$api_info = $this->get_one_info($conds)->result();
		$name = ltrim($api_info[0]->name);
		return $name;
	}

	function get_name_secret(){
		$conds['id'] = 1;
		$api_info = $this->get_one_info($conds)->result();
		$secret = ltrim($api_info[0]->name_secret);
		return $secret;
	}
	
}

?>