<?php

defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . 'libraries/Rest_Controller.php';
use Restserver\Libraries\Rest_Controller;
class Gsc extends Rest_Controller{
		  /**
     * Get All Data from this method.
     *
     * @return Response
    */

    public function __construct() {

       parent::__construct();

       $this->load->database();

       $this->load->model('Member_model');
       $this->load->model('Func_model');

      ini_set('display_errors', 1);

    }
    /**
     * Get All Data from this method.
     *
     * @return Response
    */

	public function index_get(){

         $msg = array(
            "msg"=>"Ok :)"
        );

        // $this->response($msg, 200);

	}


    public function login_post(){
        $data = file_get_contents("php://input");
        $decoded_data = json_decode($data);
        $condition = array(
            'pass' => md5($decoded_data->password),
            'username' => $decoded_data->username

        );

        $cek_login = $this->Member_model->get_data_member($condition);
        $message = array();
        if ($cek_login->num_rows() > 0){
            $get_pelanggan = $this->Member_model->get_data_member($condition);
            $message = array(
                    'status' => true,
                    'message' => 'found',
                    'data' => $get_pelanggan->result()
            );

            // $this->response($message, 200);

        } else {
            $message = array(
                    'status' => false,
                    'message' => 'not found',
                    'data' => []
            );

            // $this->response($message, 200);

        }

        echo json_encode($message);

    }

    public function detail_member_post(){
        $data = file_get_contents("php://input");
        $decoded_data = json_decode($data);
        $condition = array(

            'username' => $decoded_data->username

        );

        $get_data = $this->Member_model->get_detail_member($condition);
        $message = array();

        if ($get_data->num_rows() > 0){
            $data = $this->Member_model->get_detail_member($condition);
            $message = array(
                    'status' => true,
                    'message' => 'found',
                    'data' => $data->result()
            );

        } else {

            $message = array(
                    'status' => false,
                    'message' => 'not found',
                    'data' => []
            );

        }

        echo json_encode($message);

    }


     public function check_card_post(){
        $data = file_get_contents("php://input");
        $decoded_data = json_decode($data);
        $hp = $decoded_data->mobile;
        $condition = array(
            'serial' => $decoded_data->serial,
            'pin' => $decoded_data->pin
        );

        $check = $this->Member_model->get_serial_card($condition);
        if ($check->num_rows() > 0){
            $row = $check->row();
            $idmlmnya = $row->idmlm;
            $config = $this->Member_model->get_konfigurasi_sms();
            $row1 = $config->row();
            $userkey = $row1->userkey;
            $passkey = $row1->passkey;
            $apikey = $row1->api_sms;
            $cdvr = substr(str_shuffle(str_repeat("1234567898765432145875263985675412532897548693256875482", 64)), 0, 6);
            $isipesan = $idmlmnya.", Kode OTP Registrasi Anda ".$cdvr.".";


            $otpreg = array(
                'username'=>$idmlmnya,
                'tgl' => date('Y-m-d H:i:s'),
                'phone' => $hp,
                'email' => '',
                'time' => time(),
                'sess' => $decoded_data->serial,
                'batas' => 0
            );

            $save = $this->Member_model->insert_otp_reg($otpreg);
            if($save){
                $sms = $this->Func_model->smssend($userkey, $passkey, $apikey, $hp, $isipesan);
                $result = array(
                    'status' => true,
                    'message' => 'Kode OTP anda sudah dikirim ke nomor '.$hp. 'Gunakan kode itu untuk lanjutkan proses registrasi anda.',
                    'data'=> $otpreg
                );

            }else{
                $result = array(
                    'status' => false,
                    'message' => 'Failed'

                );
            }

        }else{
            $result = array(
                    'status' => false,
                    'message' => 'Serial card / Pin salah'
                );
        }

        echo json_encode($result);

    }


    public function provinsi_get(){
        $data = $this->Member_model->get_provinsi();
        if($data->num_rows() > 0){
            $result = array(
                'result' => true,
                'message' => 'found',
                'data' => $data->result()
            );
        }else{
            $result = array(
                'result' => false,
                'message' => 'not found'
            );
        }
        echo json_encode($result);
    }


   

      

    /**

     * Get All Data from this method.

     *

     * @return Response

    */



}

?>