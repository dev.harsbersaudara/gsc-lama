﻿/*!
* classie - class helper functions
* from bonzo https://github.com/ded/bonzo
* 
* classie.has( elem, 'my-class' ) -> true/false
* classie.add( elem, 'my-new-class' )
* classie.remove( elem, 'my-unwanted-class' )
* classie.toggle( elem, 'my-class' )
*/
(function(n){"use strict";function u(n){return new RegExp("(^|\\s+)"+n+"(\\s+|$)")}function f(n,u){var f=t(n,u)?r:i;f(n,u)}var t,i,r;"classList"in document.documentElement?(t=function(n,t){return n.classList.contains(t)},i=function(n,t){n.classList.add(t)},r=function(n,t){n.classList.remove(t)}):(t=function(n,t){return u(t).test(n.className)},i=function(n,i){t(n,i)||(n.className=n.className+" "+i)},r=function(n,t){n.className=n.className.replace(u(t)," ")});n.classie={hasClass:t,addClass:i,removeClass:r,toggleClass:f,has:t,add:i,remove:r,toggle:f}})(window);