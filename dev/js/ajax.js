	/* Ajak Default */
	var xmlHttp = buatObjekXmlHttp ();
	function buatObjekXmlHttp () {
		var obj=null;
		if (window.ActiveXObject)
			obj=new ActiveXObject("Microsoft.XMLHTTP");
		else
			if (window.XMLHttpRequest)
				obj=new XMLHttpRequest();
		
		if (obj==null) 
			document.write("Browser tidak mendukung XMLHttpRequest");
		return obj;
	}
	function ambilData (sumber_data, id_elemen) {
		if (xmlHttp!=null) {
			var obj=document.getElementById(id_elemen);
			xmlHttp.open("GET", sumber_data);
			xmlHttp.onreadystatechange=function () {
				if (xmlHttp.readyState==4 && xmlHttp.status==200) {
					obj.innerHTML=xmlHttp.responseText;
				}
			}
			xmlHttp.send(null);
		}
	}
	
	/* Cari */
	function cekKeyword() {
		var keyword=document.getElementById('keyword').value;
		if(keyword=='Pencarian'){ alert('Masukan Keyword Pencarian!'); return false; } 
	}
	
	/* Cek Komentar Artikel */
	function cekkomentar() {
		var nama=document.getElementById('nama').value;
		var emailmu=document.getElementById('emailmu').value;
		var komentar=document.getElementById('komentar').value;
		if(nama==''){ alert('Masukan Nama Lengkap Anda !'); return false; }
		else if(emailmu==''){ alert('Masukan Email Anda !'); return false; }
		else if(komentar==''){ alert('Masukan Komentar Anda !'); return false; }
	}

	/* Cek Login */
	function ceklogin() {
		var username=document.getElementById('username').value;
		var password=document.getElementById('password').value;
		if(username==''){ alert('Masukan Username !'); return false; } 
		else if(password==''){ alert('Masukkan Password !'); return false; }
	}


	function cekKodesms(){
		var kodesms=document.getElementById('kodesms').value;
		if(kodesms==''){ alert('Masukan kode yang dikirim melalui SMS ke nomor telepon / handphone Anda!'); return false; }
	}


	function cekprofil() {
		var nama=document.getElementById('nama').value;
		var telepon=document.getElementById('telepon').value;
		if(nama==''){ alert('Masukan Nama Anda !'); return false; } 
		else if(telepon==''){ alert('Masukan Telepon Anda !'); return false; } 
		return confirm ('Apakah Anda Yakin Akan Mengubah Profil ?');
	}


	function cekpassword() {
		var pwordlama=document.getElementById('pwordlama').value;
		var pwordbaru=document.getElementById('pwordbaru').value;
		var pwordbaru2=document.getElementById('pwordbaru2').value;
		if(pwordlama==''){ alert('Masukkan Password Lama Anda !'); return false; } 
		else if(pwordbaru==''){ alert('Masukkan Password Baru Anda !'); return false; } 
		else if(pwordbaru2==''){ alert('Masukkan Password Baru Anda Sekali Lagi!'); return false; } 
		return confirm ('Apakah Anda Yakin Akan Mengubah Rekening Bank ?');	
	}


	function cekrekening() {
		var nama_bank=document.getElementById('nama_bank').value;
		var nomor_rekening=document.getElementById('nomor_rekening').value;
		var pemilik_rekening=document.getElementById('pemilik_rekening').value;
		if(nama_bank==''){ alert('Masukan Nama Bank !'); return false; } 
		else if(nomor_rekening==''){ alert('Masukan Nomor Rekening Bank !'); return false; } 
		else if(pemilik_rekening==''){ alert('Masukan Nama Pemilik Rekening Bank !'); return false; } 
		return confirm ('Apakah Anda Yakin Akan Mengubah Rekening Bank ?');
	}


	function cekregister(){
		var username=document.getElementById('username').value;
		var password=document.getElementById('password').value;
		var nama=document.getElementById('nama').value;
		var kota=document.getElementById('kota').value;
		var telepon=document.getElementById('telepon').value;
		var nama_bank=document.getElementById('nama_bank').value;
		var nomor_rekening=document.getElementById('nomor_rekening').value;
		var pemilik_rekening=document.getElementById('pemilik_rekening').value;
		if(username==''){ alert('Masukan Username Anggota Baru !'); return false; }
		else if(password==''){ alert('Masukan Password Anggota Baru !'); return false; }
		else if(nama==''){ alert('Masukan Nama Lengkap Anggota Baru !'); return false; }
		else if(kota==''){ alert('Masukan Kota / Kabupaten Anggota Baru !'); return false; }
		else if(telepon==''){ alert('Masukan Nomor Telepon Anggota Baru !'); return false; }
		else if(nama_bank=='-'){ alert('Pilih Nama Bank !'); return false; }
		else if(nomor_rekening==''){ alert('Masukan Nomor Rekening !'); return false; }
		else if(pemilik_rekening==''){ alert('Masukan Nama Pemilik Rekening !'); return false; }
	}

	function ceknilaiberi() {
		var nilai=document.getElementById('nilai').value;
		if(nilai==''){ alert('Pilih Nilai Bantuan!'); return false; } 
	}

	function cekberitransfer() {
		var tanggal_transfer=document.getElementById('tanggal_transfer').value;
		var metode_transfer=document.getElementById('metode_transfer').value;
		var keterangan=document.getElementById('keterangan').value;
		if(tanggal_transfer==''){ alert('Pilih Tanggal Transfer !'); return false; } 
		else if(metode_transfer=='-'){ alert('Pilih Metode Transfer !'); return false; } 
		else if(keterangan==''){ alert('Masukkan Keterangan !'); return false; } 
	}
	
	
	function cekpesan() {
		var isi=document.getElementById('isi').value;
		if(isi==''){ alert('Tulis Isi Pesan Anda !'); return false; } 
	}
	
	
	function cekberilaporkan() {
		var isi=document.getElementById('isi').value;
		if(isi==''){ alert('Tulis Isi Laporan Anda !'); return false; } 
	}
	
	
	function ceknilaiminta() {
		var nilai=document.getElementById('nilai').value;
		if(nilai=='-'){ alert('Pilih Nilai Sedekah!'); return false; } 
	}
