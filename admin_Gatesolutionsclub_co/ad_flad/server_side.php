<?php
ob_start();
error_reporting(0);

session_start();
session_regenerate_id(true);

$table = 'member';
// Table's primary key
$primaryKey = 'id';

// Array of database columns which should be read and sent back to DataTables.
// The `db` parameter represents the column name in the database, while the `dt`
// parameter represents the DataTables column identifier. In this case simple
// indexes
$columns = array(
    array('db' => 'username', 'dt' => 0),
    array('db' => 'nama',  'dt' => 1),
    array('db' => 'GetBalanceAll("username")',   'dt' => 2),
    array('db' => 'nama',     'dt' => 3),
    array('db' => 'nama',     'dt' => 4),
    // array(
    //     'db'        => 'start_date',
    //     'dt'        => 4,
    //     'formatter' => function ($d, $row) {
    //         return date('jS M y', strtotime($d));
    //     }
    // ),
    // array(
    //     'db'        => 'salary',
    //     'dt'        => 5,
    //     'formatter' => function ($d, $row) {
    //         return '$' . number_format($d);
    //     }
    // )
);

// SQL server connection information
$sql_details = array(
    'user' => 'root',
    'pass' => 'Gates112233!!!',
    'db'   => 'gatesolutionsclub',
    'host' => 'https://gatesolutionsclub.co/'
    //     pass : Gates112233!!!
    // database : gatesolutionsclub
);


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */

require('ssp.class.php');

echo json_encode(
    SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns)
);
