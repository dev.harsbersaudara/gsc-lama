/**
 * @license Copyright (c) 2003-2016, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
	
	config.skin = 'moonocolor';
	config.width = '950';
	config.height = '300';
	config.filebrowserBrowseUrl = '/gate_s74392/ckeditor/kcfinder/browse.php?type=files';
   config.filebrowserImageBrowseUrl = '/gate_s74392/ckeditor/kcfinder/browse.php?type=images';
   config.filebrowserFlashBrowseUrl = '/gate_s74392/ckeditor/kcfinder/browse.php?type=flash';
   config.filebrowserUploadUrl = '/gate_s74392/ckeditor/kcfinder/upload.php?type=files';
   config.filebrowserImageUploadUrl = '/gate_s74392/ckeditor/kcfinder/upload.php?type=images';
   config.filebrowserFlashUploadUrl = '/gate_s74392/ckeditor/kcfinder/upload.php?type=flash';
	
};
