<?php include("includes/header.php");

  require("includes/function.php");
  require("language/language.php");

 
  if(isset($_POST['submit']))
  {
     $bigPictureUrl=$_POST['big_picture_url'];
     $largeIconUrl=$_POST['large_icon_url'];      
     $notification_for=$_POST['notification_for'];   
      if($notification_for==1)
      {
              if($_FILES['big_picture']['name']!="")
          {   

              $big_picture=rand(0,99999)."_".$_FILES['big_picture']['name'];
              $tpath2='images/'.$big_picture;
              move_uploaded_file($_FILES["big_picture"]["tmp_name"], $tpath2);

              $file_path = 'http://'.$_SERVER['SERVER_NAME'] . dirname($_SERVER['REQUEST_URI']).'/images/'.$big_picture;
                
              $content = array(
                               "en" => $_POST['notification_msg']                                                 
                               );

              $fields = array(
                              'app_id' => ONESIGNAL_APP_ID,
                              'included_segments' => array('All'),                                            
                              'data' =>  NULL,
                              'headings'=> array("en" => $_POST['notification_title']),
                              'contents' => $content,
                              'big_picture' =>$bigPictureUrl,                        
                              'large_icon'=>$largeIconUrl,
                                              
                              );


              $fields = json_encode($fields);
             

              $ch = curl_init();
              curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
              curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
                                                         'Authorization: Basic '.ONESIGNAL_REST_KEY));
              curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
              curl_setopt($ch, CURLOPT_HEADER, FALSE);
              curl_setopt($ch, CURLOPT_POST, TRUE);
              curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
              curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

              $response = curl_exec($ch);
              curl_close($ch);

              
          }
          else
          {

       
              $content = array(
                               "en" => $_POST['notification_msg']
                                );

              $fields = array(
                              'app_id' => ONESIGNAL_APP_ID,
                              'included_segments' => array('All'),                                      
                              'data' => NULL,
                              'headings'=> array("en" => $_POST['notification_title']),
                              'contents' => $content,
                              'big_picture' =>$bigPictureUrl,                        
                              'large_icon'=>$largeIconUrl,
                            
                              );

              $fields = json_encode($fields);
             
              $ch = curl_init();
              curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
              curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
                                                         'Authorization: Basic '.ONESIGNAL_REST_KEY));
              curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
              curl_setopt($ch, CURLOPT_HEADER, FALSE);
              curl_setopt($ch, CURLOPT_POST, TRUE);
              curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
              curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

              $response = curl_exec($ch);
              
              
              
              curl_close($ch);


          }
      }
      else if($notification_for==2)
      {     
        $notification_player=$_POST['notification_player'];
         
          if($_FILES['big_picture']['name']!="")
          {   

              $big_picture=rand(0,99999)."_".$_FILES['big_picture']['name'];
              $tpath2='images/'.$big_picture;
              move_uploaded_file($_FILES["big_picture"]["tmp_name"], $tpath2);

              $file_path = 'http://'.$_SERVER['SERVER_NAME'] . dirname($_SERVER['REQUEST_URI']).'/images/'.$big_picture;
                
              $content = array(
                               "en" => $_POST['notification_msg']                                                 
                               );

              $fields = array(
                              'app_id' => ONESIGNAL_APP_ID,
                              'include_player_ids' =>$notification_player,                                          
                              'data' =>  NULL,
                              'headings'=> array("en" => $_POST['notification_title']),
                              'contents' => $content,
                              'big_picture' =>$bigPictureUrl,                        
                              'large_icon'=>$largeIconUrl,
                                              
                              );


              $fields = json_encode($fields);
             

              $ch = curl_init();
              curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
              curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
                                                         'Authorization: Basic '.ONESIGNAL_REST_KEY));
              curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
              curl_setopt($ch, CURLOPT_HEADER, FALSE);
              curl_setopt($ch, CURLOPT_POST, TRUE);
              curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
              curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

              $response = curl_exec($ch);
              curl_close($ch);

              
          }
          else
          {

       
              $content = array(
                               "en" => $_POST['notification_msg']
                                );

              $fields = array(
                              'app_id' => ONESIGNAL_APP_ID,
                              'include_player_ids' =>$notification_player,                                      
                              'data' => NULL,
                              'headings'=> array("en" => $_POST['notification_title']),
                              'contents' => $content,
                              'big_picture' =>$bigPictureUrl,                        
                              'large_icon'=>$largeIconUrl,
                            
                              );

              $fields = json_encode($fields);
             
              $ch = curl_init();
              curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
              curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
                                                         'Authorization: Basic '.ONESIGNAL_REST_KEY));
              curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
              curl_setopt($ch, CURLOPT_HEADER, FALSE);
              curl_setopt($ch, CURLOPT_POST, TRUE);
              curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
              curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

              $response = curl_exec($ch);
              
              
              
              curl_close($ch);


          }


      }


        
        $_SESSION['msg']="16";
        $_SESSION['type']="success";
     
        header( "Location:send_notification.php");
        exit; 
     
     
  }
  
  $qry="SELECT * FROM users";
      $sql=mysqli_query($mysqli,$qry) or die("error ".mysql_error($mysqli));
      
   

?>






<div class="page-wrapper">
  
  <div class="page-breadcrumb">
    <div class="row">
      <div class="col-5 align-self-center">
        <h4 class="page-title">Send Notification</h4>
        <div class="d-flex align-items-center">
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="home.php">Home</a></li>
              <li class="breadcrumb-item active" aria-current="page"> Send Notification</li>
            </ol>
          </nav>
        </div>
      </div>
      <div class="col-7 align-self-center">
        
      </div>
    </div>
  </div>
  
  <div class="container-fluid">
    
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-head">
           Send Notification
          </div>
          <div class="card-body">
          
            <form action="" name="addeditcategory" method="post" class="form form-horizontal" enctype="multipart/form-data">
               
              <div class="section">
                <div class="section-body">

                  <div class="form-group">
                    <label class="col-md-12 control-label">Title :-</label>
                    <div class="col-md-12">
                      <input type="text" placeholder="Enter Notification Title" name="notification_title" id="notification_title" class="form-control" value=""  required>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-12 control-label">Message :-</label>
                    <div class="col-md-12">
                        <textarea name="notification_msg" placeholder="Enter Notification Message" id="notification_msg" class="form-control" required></textarea>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-12 control-label">Large Icon Url:-</label>
                    <div class="col-md-12">
                      <input type="url" placeholder="Enter Large Icon Url" name="large_icon_url" id="large_icon" class="form-control" >
                       
                    </div>
                  </div>
                   <div class="form-group">
                    <label class="col-md-12 control-label">Big Picture Url :-</label>
                    <div class="col-md-12">
                      <input type="url" placeholder="Enter Big Picture Url" name="big_picture_url" id="big_picture" class="form-control" >
                       
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-12 control-label">Notification For</label>
                    <div class="col-md-12">
                      <select name="notification_for"  class="form-control"  id="notification_for" required>
                        <option value="1">All Player</option>
                        <option value="2">Select Player</option>
                      </select>                       
                    </div>
                  </div>
                  <div id="sel-user">
                    
                  </div>
                  <div id="get-option" style="display: none">
                     <?php while($data = mysqli_fetch_assoc($sql)) { ?>
            <option value='<?php echo $data['onesignalToken']; ?>'><?php echo $data['name'].' ( '.$data['email'].' ) ';?></option>
              <?php } ?> 
                  </div>
                 
             
                  <div class="form-group" style="padding-top:10px">
                    <div class="col-md-9 col-md-offset-3">
                      <button type="submit" name="submit" class="btn btn-primary">Send</button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
      </div>
    </div>
  </div>
</div>

<script>
  $(function(){
    $("#notification_for").on('change',function(){
        var notification=$(this).val();
        if(notification==2)
        {
          var html="<div class='form-group'>";
              html+="<label class='col-md-12 control-label'>Selected User</label>";
              html+="<div class='col-md-12'>";
              html+="<select class='select2 form-control' name='notification_player[]' multiple='multiple' style='height: 36px;width: 100%;'>";
             html+=$('#get-option').html();
              html+="</select> </div> </div>";

              $("#sel-user").append(html);
              $(".select2").select2();


        }else
        {
            $("#sel-user").empty();
        }

    });
  })
</script>



<?php include("includes/footer.php");?>       
