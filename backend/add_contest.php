<?php include("includes/header.php");

	require("includes/function.php");
	require("language/language.php");


	if(isset($_POST['submit']) && isset($_GET['action']))
	{	
		
		if($_GET['action']=="add")
		{	
				$start_date_qry="SELECT * FROM contest order by id  desc limit 1 ";
				$start_date_result=mysqli_query($mysqli,$start_date_qry);
				$Start_data=mysqli_fetch_assoc($start_date_result);

				
				
				$start_date	=date("Y-m-d",strtotime($Start_data['end_date']."+1 days"));
				$end_date	=date("Y-m-d",strtotime($_POST['end_date']));

				$start_date_str=strtotime($start_date);
				$end_date_str=strtotime($end_date);

				if($end_date_str<=$start_date_str)
				{

					$_SESSION['msg']="21";
					$_SESSION['type']="error";
			 
					header( "Location:view_contest.php");
					exit;
				}
					$data = array( 
					    'start_date'  => $start_date,				    
					    'end_date'  =>  $end_date,				    
					    'playing_status'  =>  trim($_POST['playing_status']),				    
					    'msg'  =>  trim($_POST['msg']),				    
					    );		
				
					$qry = Insert('contest',$data);


					$_SESSION['msg']="10";
					$_SESSION['type']="success";
			 
					header( "Location:view_contest.php");
					exit;	
			
		} 
		if($_GET['action']=="edit"){
			
				$end_date	=date("Y-m-d",strtotime($_POST['end_date']));
				$data = array( 					    		    
					    'end_date'  => $end_date,				    
					    'msg'  =>  trim($_POST['msg']),				    
					    );

			$category_edit=Update('contest', $data, "WHERE id = '".$_POST['id']."'");

			$_SESSION['msg']="11"; 
			$_SESSION['type']="success";
			header( "Location:view_contest.php");
			exit;
		}	
	}

	
	
	// Edit Data 
	if(isset($_GET['id']))
	{
			 
			$qry="SELECT * FROM contest where id='".$_GET['id']."'";
			$result=mysqli_query($mysqli,$qry);
			$row=mysqli_fetch_assoc($result);

	}
	


?>

<div class="page-wrapper">
  
  <div class="page-breadcrumb">
    <div class="row">
      <div class="col-5 align-self-center">
        <h4 class="page-title">Game Contest</h4>
        <div class="d-flex align-items-center">
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="home.php">Home</a></li>
              <li class="breadcrumb-item active" aria-current="page">Game Contest</li>
            </ol>
          </nav>
        </div>
      </div>
      <div class="col-7 align-self-center">
        <div class="d-flex no-block justify-content-end align-items-center">
          
          <div class="add_btn_primary"> <a class="btn btn-primary" href="view_contest.php">View Game Contest</a> </div>
        </div>
      </div>
    </div>
  </div>
  
  <div class="container-fluid">
    
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-head">
           <?php if(isset($_GET['id'])){?>Edit<?php }else{?>Add<?php }?> Contest
          </div>
          <div class="card-body">
          	<?php if(isset($_GET['action'])=="add") { ?>
          	<form action="" name="addeditcategory" method="post" class="form form-horizontal" enctype="multipart/form-data">
          	<?php } else { ?>

          		<form action="?action=edit" name="addeditcategory" method="post" class="form form-horizontal" enctype="multipart/form-data">
          	<?php } ?>
            	<input  type="hidden" name="id" value="<?php echo $_GET['id'];?>" />

            	
			<?php 
				$Language_query="SELECT id,name,status FROM languages ";
				$lan_result=mysqli_query($mysqli,$Language_query);
			

			?>
              <div class="section">
                <div class="section-body">
				
                  <div class="form-group">
                    <label class="col-md-3 control-label">End Date:-</label>
                    <div class="col-md-6">
                      <input type="text" placeholder="End Date" name="end_date" id="end_date" value="<?php if(isset($_GET['id'])){echo date("d/m/Y",strtotime($row['end_date']));}?>" autocomplete="off" class="form-control mydatepicker" required>
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <label class="col-md-3 control-label">Playing Status:-</label>
                    <div class="col-md-6">
                    	<select name="playing_status" id="playing_status" class="form-control" required>
                    		<option value="Yes" <?php if(isset($row['playing_status'])){ echo ($row['start_date']=="Yes")?"selected":"" ;}?> >Yes</option>
                    		<option value="No" <?php if(isset($row['playing_status'])){ echo ($row['start_date']=="No")?"selected":"" ;}?> >No</option>
                    		
                    	</select>
                      
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-3 control-label">Message:-</label>
                    <div class="col-md-6">
                     <textarea placeholder="Message" name="msg" id="msg" value="<?php if(isset($_GET['id'])){echo $row['msg'];}?>" class="form-control"  cols="10" rows="3"></textarea>
                    </div>
                  </div>

                  <div class="form-group">
                    <div class="col-md-9">
                      <button type="submit" name="submit" class="btn btn-primary">Save</button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
            
	      </div>
	    </div>
	  </div>
	</div>

        

       
<?php include("includes/footer.php");?>       
