<?php include("includes/header.php");

	require("includes/function.php");
	require("language/language.php");
	 
	
	if(isset($_SESSION['id']))
	{
			 
		$qry="select * from tbl_admin where id='".$_SESSION['id']."'";
		 
		$result=mysqli_query($mysqli,$qry);
		$row=mysqli_fetch_assoc($result);

	}
	if(isset($_POST['submit']))
	{
		if($_FILES['image']['name']!="")
		 {		


				$img_res=mysqli_query($mysqli,'SELECT * FROM tbl_admin WHERE id='.$_SESSION['id'].'');
			    $img_res_row=mysqli_fetch_assoc($img_res);
			 
        if(!is_dir("images"))
        {
          mkdir("images",0777);
         
        }

			    if($img_res_row['image']!="")
		        {
					 
					     unlink('images/'.$img_res_row['image']);
			     }



 				   $image="profile.png";
				   $pic1=$_FILES['image']['tmp_name'];
				   $tpath1='images/'.$image;
					
					copy($pic1,$tpath1);
 
					$data = array( 
							    'username'  =>  $_POST['username'],
							    'password'  =>  $_POST['password'],
							    'email'  =>  $_POST['email'],
							    'image'  =>  $image
							    );
					
					$channel_edit=Update('tbl_admin', $data, "WHERE id = '".$_SESSION['id']."'"); 

		 }
		 else
		 {
					$data = array( 
							    'username'  =>  $_POST['username'],
							    'password'  =>  $_POST['password'],
							    'email'  =>  $_POST['email'] 
							    );
					
					$channel_edit=Update('tbl_admin', $data, "WHERE id = '".$_SESSION['id']."'"); 
		}

		$_SESSION['msg']="11"; 
		header( "Location:profile.php");
		exit;
		 
	}


?>


<div class="page-wrapper">
  
  <div class="page-breadcrumb">
    <div class="row">
      <div class="col-5 align-self-center">
        <h4 class="page-title">Admin Profile</h4>
        <div class="d-flex align-items-center">
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="home.php">Home</a></li>
              <li class="breadcrumb-item active" aria-current="page">Admin Profile</li>
            </ol>
          </nav>
        </div>
      </div>
    </div>
  </div>
  
  <div class="container-fluid">
    
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-head">
            Profile
          </div>
          <div class="card-body">
              
            <form action="" name="editprofile" method="post" class="form form-horizontal" enctype="multipart/form-data">
              <div class="section">
                <div class="section-body">
                  <div class="row d-flex">
                  <div class="col-xs-12 col-sm-6">
                  <div class="form-group">
                    <label class="col-md-12 control-label">Username :-</label>
                    <div class="col-md-12">
                      <input type="text" name="username" id="username" value="<?php echo $row['username'];?>" class="form-control" required autocomplete="off">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-12 control-label">Password :-</label>
                    <div class="col-md-12">
                      <input type="password" name="password" id="password" value="<?php echo $row['password'];?>" class="form-control" required autocomplete="off">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-12 control-label">Email :-</label>
                    <div class="col-md-12">
                      <input type="text" name="email" id="email" value="<?php echo $row['email'];?>" class="form-control" required autocomplete="off">
                    </div>
                  </div>
                  </div>
                  <div class="col-xs-12 col-sm-6">
                    <div class="col-xs-12 ">
                      <center>
                      <div class="img-box"> 
                        <?php if($row['image']!="") {?>
                        <div class="el-element-overlay">
                          <div class="el-card-item">
                              <div class="el-card-avatar el-overlay-1"> 
                                <img class="img-responsive" src="<?php echo $file_path; ?>images/<?php echo $row['image'];?>?<?php echo uniqid();?>" style="width:200px;height: 200px; object-fit: cover;" alt="Image NOt Found" />
                                  <div class="el-overlay">
                                      <ul class="list-style-none el-info">
                                          <li class="el-item"><a class="btn default btn-outline image-popup-vertical-fit el-link" href="<?php echo $file_path; ?>images/<?php echo $row['image'];?>?<?php echo uniqid();?>"><i class="icon-magnifier"></i></a></li>
                                          
                                      </ul>
                                  </div>
                              </div>                                
                          </div>
                        </div>
                        <?php } ?>
                      </div>
                    </center>
                    </div>
                  <div class="form-group">
                    <center>
                    <label class="col-md-3 control-label">Profile Image :-</label>
                    <div class="col-md-6">
                      <div class="fileupload_block">
                        <input type="file" name="image" id="fileupload">  
                          
                      </div>
                    </div>
                    </center>
                  </div>
          </div>      
          </div>           
                  <div class="form-group">
                    <div class="col-md-9 col-md-offset-3">
                      <button type="submit" name="submit" class="btn btn-primary">Save</button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
      </div>
    </div>
  </div>
</div>
 
	 
<?php include("includes/footer.php");?>       
