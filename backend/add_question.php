<?php include("includes/header.php");

	require("includes/function.php");
	require("language/language.php");
	
	if(isset($_POST['submit']) && isset($_GET['action']))
	{	
		
		if($_GET['action']=="add")
		{
		   	
				$data = array( 
					    'language_id'  =>  trim($_POST['language_id']),					   				    
					    'question'  =>  trim($_POST['question']),				    
					    'options'  => json_encode(array("A"=>$_POST['options_a'],"B"=>$_POST['options_b'],"C"=>$_POST['options_c'],"D"=>$_POST['options_d'])),
					    	'answer'=>trim($_POST['answer'])				    
					    );	
					 
					$qry = Insert('questions',$data);

						$_SESSION['msg']="10";
						$_SESSION['type']="success";
					
					header( "Location:view_question.php");
					exit;	
			
		} 
		if($_GET['action']=="edit"){
			
			$id=$_POST['id'];
			$data = array( 
					    'language_id'  =>  trim($_POST['language_id']),					 				    
					    'question'  =>  trim($_POST['question']),				    
					    'options'  => json_encode(array("A"=>$_POST['options_a'],"B"=>$_POST['options_b'],"C"=>$_POST['options_c'],"D"=>$_POST['options_d'])),
					    	'answer'=>trim($_POST['answer'])				    
					    );	

			$category_edit=Update('questions', $data, "WHERE id = '".$id."'");

			$_SESSION['msg']="11"; 
			$_SESSION['type']="success";
			header( "Location:view_question.php");
			exit;
		}	
	}

	
	
	// Edit Data 
	if(isset($_GET['id']))
	{
			 
			$qry="SELECT * FROM questions where id='".$_GET['id']."'";
			$result=mysqli_query($mysqli,$qry);
			$row=mysqli_fetch_assoc($result);

	}
	


?>

<div class="page-wrapper">
  
  <div class="page-breadcrumb">
    <div class="row">
      <div class="col-5 align-self-center">
        <h4 class="page-title">Add Question </h4>
        <div class="d-flex align-items-center">
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="home.php">Home</a></li>
              <li class="breadcrumb-item active" aria-current="page">Add Question</li>
            </ol>
          </nav>
        </div>
      </div>
      <div class="col-7 align-self-center">
        <div class="d-flex no-block justify-content-end align-items-center">
          
          <div class="add_btn_primary"> <a class="btn btn-primary" href="view_question.php">View Question</a> </div>
        </div>
      </div>
    </div>
  </div>
  
  <div class="container-fluid">
    
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-head">
           <?php if(isset($_GET['id'])){?>Edit<?php }else{?>Add<?php }?> Question
          </div>
          <div class="card-body">
          	<?php if(isset($_GET['action'])=="add") { ?>
          	<form action="" name="addeditcategory" method="post" class="form form-horizontal" enctype="multipart/form-data">
          	<?php } else { ?>

          		<form action="?action=edit" name="addeditcategory" method="post" class="form form-horizontal" enctype="multipart/form-data">
          			<input  type="hidden" name="id" value="<?php echo $_GET['id'];?>" />
          	<?php } ?>
            	
			<?php 
				$Language_query="SELECT id,name,status FROM languages ";
				$lan_result=mysqli_query($mysqli,$Language_query);
			

			?>
              <div class="section">
                <div class="section-body">
					<div class="row">
								<div class="col-md-12 col-xs-12">
								<div class="form-group">
									<label class="col-md-12 control-label">Select Language :-</label>
									<div class="col-md-12">
										<select name="language_id" onchange="language(this.value)" id="language_id"  class="form-control" required>
											<option value="">Select Language</option>

										<?php while($data=mysqli_fetch_array($lan_result)) { ?>
											<option value="<?php echo $data['id']; ?>"   ><?php echo $data['name']; ?></option>
										<?php } ?>
										</select>
									  
									</div>
								</div>
							</div>
							
					</div>
					<div class="row">
						<?php if(isset($_GET['id'])){ ?>

							<?php  if($row['language_id'] != '1') { ?>
							<div class="col-md-12 col-xs-12">
								<div class="form-group">
									<label class="col-md-12 control-label">Question :-</label>
									<div class="col-md-12">
										<input type="text" name="question" id="question" placeholder="Enter Question " value="<?php  echo json_decode($row['question']);  ?>" class="form-control" required  >								  
									</div>
								</div>
							</div>
							<?php } else {?>
								<div class="col-md-12 col-xs-12">
								<div class="form-group">
									<label class="col-md-12 control-label">Question :-</label>
									<div class="col-md-12">
										<input type="text" name="question" id="question" placeholder="Enter Question " value="<?php if(isset($row['question'])){ echo $row['question']; } ?>" class="form-control" required  >								  
									</div>
								</div>
							</div>

							<?php } ?>
							
						<?php } else { ?>
							<div class="col-md-12 col-xs-12">
								<div class="form-group">
									<label class="col-md-12 control-label">Question :-</label>
									<div class="col-md-12">
										<input type="text" name="question" id="question" placeholder="Enter Question " value="<?php if(isset($row['question'])){ echo $row['question']; } ?>" class="form-control" required  >								  
									</div>
								</div>
							</div>

						<?php } ?>	
							
					</div>
					
					<?php if(isset($_GET['id'])){ 
						$options=json_decode($row['options'],true);
					
						?>
					<div class="row">
							<div class="col-md-6 col-xs-12">
								<div class="form-group">
									<label class="col-md-12 control-label">Options A :-</label>
									<div class="col-md-12">
										<input type="text" name="options_a" id="options_a" placeholder="Enter Option A " value="<?php echo $options['A'];?>" class="form-control" required  >
									</div>
								</div>
							</div>
							<div class="col-md-6 col-xs-12">
								<div class="form-group">
									<label class="col-md-12 control-label">Options B :-</label>
									<div class="col-md-12">
										<input type="text" name="options_b" id="options_b" placeholder="Enter Option B " value="<?php echo $options['B'];?>" class="form-control" required  >
									</div>
								</div>
							</div>
							
					</div>
					<div class="row">
							<div class="col-md-6 col-xs-12">
								<div class="form-group">
									<label class="col-md-12 control-label">Options C :-</label>
									<div class="col-md-12">
										<input type="text" name="options_c" id="options_c" placeholder="Enter Option C " value="<?php echo $options['C'];?>" class="form-control" required  >
									</div>
								</div>
							</div>
							<div class="col-md-6 col-xs-12">
								<div class="form-group">
									<label class="col-md-12 control-label">Options D :-</label>
									<div class="col-md-12">
										<input type="text" name="options_d" id="options_d" placeholder="Enter Option D " value="<?php echo $options['D'];?>" class="form-control" required  >
									</div>
								</div>
							</div>
							
					</div>
					<?php } else { ?>
						<div class="row">
							<div class="col-md-6 col-xs-12">
								<div class="form-group">
									<label class="col-md-12 control-label">Options A :-</label>
									<div class="col-md-12">
										<input type="text" name="options_a" id="options_a" placeholder="Enter Option A " value="<?php if(isset($row['options'])){ echo $row['options']; } ?>" class="form-control" required  >
									</div>
								</div>
							</div>
							<div class="col-md-6 col-xs-12">
								<div class="form-group">
									<label class="col-md-12 control-label">Options B :-</label>
									<div class="col-md-12">
										<input type="text" name="options_b" id="options_b" placeholder="Enter Option B " value="<?php if(isset($row['options'])){ echo $row['options']; } ?>" class="form-control" required  >
									</div>
								</div>
							</div>
							
					</div>
					<div class="row">
							<div class="col-md-6 col-xs-12">
								<div class="form-group">
									<label class="col-md-12 control-label">Options C :-</label>
									<div class="col-md-12">
										<input type="text" name="options_c" id="options_c" placeholder="Enter Option C " value="<?php if(isset($row['options'])){ echo $row['options']; } ?>" class="form-control" required  >
									</div>
								</div>
							</div>
							<div class="col-md-6 col-xs-12">
								<div class="form-group">
									<label class="col-md-12 control-label">Options D :-</label>
									<div class="col-md-12">
										<input type="text" name="options_d" id="options_d" placeholder="Enter Option D " value="<?php if(isset($row['options'])){ echo $row['options']; } ?>" class="form-control" required  >
									</div>
								</div>
							</div>
							
					</div>
	

					<?php } ?>
					<div class="row">

						<?php if(isset($_GET['id'])){ ?>

							<?php  if($row['language_id'] != '1') { ?>
							<div class="col-md-12 col-xs-12">
								<div class="form-group">
									<label class="col-md-12 control-label">Answer :-</label>
									<div class="col-md-12">
										<input type="text" name="answer" id="answer" placeholder="Answer " value="<?php echo json_decode($row['answer']);  ?>" class="form-control" required  >
									</div>
								</div>
							</div>
							<?php } else { ?>
								<div class="col-md-12 col-xs-12">
								<div class="form-group">
									<label class="col-md-12 control-label">Answer :-</label>
									<div class="col-md-12">
										<input type="text" name="answer" id="answer" placeholder="Answer " value="<?php if(isset($row['answer'])){ echo $row['answer']; } ?>" class="form-control" required  >
									</div>
								</div>
							</div>

							<?php } ?>
						<?php } else { ?>
							<div class="col-md-12 col-xs-12">
								<div class="form-group">
									<label class="col-md-12 control-label">Answer :-</label>
									<div class="col-md-12">
										<input type="text" name="answer" id="answer" placeholder="Answer " value="<?php if(isset($row['answer'])){ echo $row['answer']; } ?>" class="form-control" required  >
									</div>
								</div>
							</div>

						<?php } ?>
						
					</div>

                  <div class="form-group">
                    <div class="col-md-9">
                      <button type="submit" name="submit" class="btn btn-primary">Save</button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
            
	      </div>
	    </div>
	  </div>
	</div>
<?php if(isset($_GET['id'])) { ?>
<script>
	$("#language_id option[value='<?php echo $row['language_id']; ?>']").attr('selected',"selected");	
	$("#language_id option[value='<?php echo $row['language_id']; ?>']").trigger('change');


</script>
<?php } ?>
       
<?php include("includes/footer.php");?>       
