<?php include("includes/header.php");

	require("includes/function.php");
	require("language/language.php");
	 
	
	$qry="SELECT * FROM tbl_settings where id='1'";
  $result=mysqli_query($mysqli,$qry);
  $settings_row=mysqli_fetch_assoc($result);

  if(isset($_POST['admob_submit']))
  { 
    
      $banner_ad=$_POST['banner_ad'];
      if($banner_ad=="on")
      {
        $banner_ad="true";
      }
      else
      {
        $banner_ad="false";
      }

        $data = array(
                'interstital_ad_id'  =>  $_POST['interstital_ad_id'],               
                'banner_ad'  => $banner_ad ,
                'banner_ad_id'  =>  $_POST['banner_ad_id'],
                'reward_ad_id'  =>  $_POST['reward_ad_id']
                 );

    
      $settings_edit=Update('tbl_settings', $data, "WHERE id = '1'");
  
      
        $_SESSION['msg']="11";
        $_SESSION['type']="success";
        header( "Location:settings.php");
        exit;
 
 
  }

  if(isset($_POST['notification_submit']))
  {

        $data = array(
                'onesignal_app_id' => $_POST['onesignal_app_id'],
                'onesignal_rest_key' => $_POST['onesignal_rest_key'],
                  );

    
      $settings_edit=Update('tbl_settings', $data, "WHERE id = '1'");
  
 
        $_SESSION['msg']="11";
        $_SESSION['type']="success";
        header( "Location:settings.php");
        exit;
 
  }

  if(isset($_POST['api_submit']))
  {
   
      $force_update=$_POST['force_update'];
      $playing_allowed=$_POST['playing_allowed'];

      if($force_update=="on")
      {
        $force_update="true";
      }
      else
      {
        $force_update="false";
      }

      if($playing_allowed=="on")
      {
        $playing_allowed="true";
      }
      else
      {
        $playing_allowed="false";
      }
    
        $data = array(                
              'refer_from'  =>  $_POST['refer_from'],                               
              'refer_to'  =>  $_POST['refer_to'],                               
              'update_title'=>$_POST['update_title'],
              'update_version'=>$_POST['update_version'],
              'update_url'=>$_POST['update_url'],
              'update_msg'=>addslashes($_POST['update_msg']),
              'force_update'=>$force_update,
              'playing_allowed'=>$_POST['playing_allowed'],
              'playing_msg'=>$_POST['playing_msg'],
              'referral_msg'=>$_POST['referral_msg'],
                  );

    
      $settings_edit=Update('tbl_settings', $data, "WHERE id = '1'");
 
        $_SESSION['msg']="11";
        $_SESSION['type']="success";
        header( "Location:settings.php");
        exit;
  
 
  }


?>

<style>
  .switch {
    position: relative;
    display: inline-block;
    width: 60px;
    height: 10px;
}

/* Hide default HTML checkbox */
.switch input {
  opacity: 0;
  width: 0;
  height: 0;
}

/* The slider */
.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #616161;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}
.round {
    color: #fff;
    width: 53px;
    height: 19px;
    display: inline-block;
    font-weight: 400;
    text-align: center;
    line-height: 52px;
}
.slider:before {
    position: absolute;
    content: "";
    height: 26px;
    width: 26px;
    left: 0px;
    bottom: -3px;
    background-color: #212529;
    -webkit-transition: .4s;
    transition: .4s;
}
</style>


<div class="page-wrapper">
  
  <div class="page-breadcrumb">
    <div class="row">
      <div class="col-5 align-self-center">
        <h4 class="page-title">API SETTINGS</h4>
        <div class="d-flex align-items-center">
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="home.php">Home</a></li>
              <li class="breadcrumb-item active" aria-current="page"> API SETTINGS</li>
            </ol>
          </nav>
        </div>
      </div>
      <div class="col-7 align-self-center">
        
      </div>
    </div>
  </div>
  
  <div class="container-fluid">
    
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-head">
           Settings
          </div>
          <div class="card-body">
           

<!-- Nav tabs -->
<ul class="nav nav-tabs" role="tablist">
    <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#admob_settings" role="tab"><span class="hidden-sm-up"></span> <span class="hidden-xs-down">Admob Settings</span></a> </li>
    <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#notification_settings" role="tab"><span class="hidden-sm-up"></span> <span class="hidden-xs-down">Notification Settings</span></a> </li>
    <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#api_settings" role="tab"><span class="hidden-sm-up"></span> <span class="hidden-xs-down">API  Settings</span></a> </li>
</ul>
<!-- Tab panes -->
<div class="tab-content tabcontent-border">
    <div class="tab-pane active" id="admob_settings" role="tabpanel">
        <div class="p-20">
            <form action="" name="admob_settings" method="post" class="form form-horizontal" enctype="multipart/form-data">
                <div class="section">
          <div class="section-body">            
            <div class="row">
          
              <div class="col-md-12">                
              <div class="col-md-12">
                <div class="card card-head">Android</div>               
                
                    <div class="form-group">
                      <label class="col-md-6 control-label">Ad Enable/Disable:-</label>
                      <div class="col-md-9">

                          <label class="switch">
                          <input type="checkbox" name="banner_ad" id="banner_ad"  <?php if($settings_row['banner_ad']=='true'){ echo "checked"; }?> >
                          <span class="slider round"></span>
                          </label>
                      </div>
                    </div>



                    <div class="form-group">
                      <label class="col-md-3 control-label mr_bottom20">Banner ID :-</label>
                      <div class="col-md-9">
                      <input type="text" name="banner_ad_id" id="banner_ad_id" value="<?php echo $settings_row['banner_ad_id'];?>" class="form-control">
                      </div>
                    </div>   

                 
              </div>
              <div class="col-md-12">
               
                    <div class="form-group">
                      <label class="col-md-3 control-label mr_bottom20">Interstital ID :-</label>
                      <div class="col-md-9">
                      <input type="text" name="interstital_ad_id" id="interstital_ad_id" value="<?php echo $settings_row['interstital_ad_id'];?>" class="form-control">
                      </div>
                    </div>
                                        
                  
              </div>
              <div class="col-md-12">
               
                    <div class="form-group">
                      <label class="col-md-3 control-label mr_bottom20">Reward Ad ID :-</label>
                      <div class="col-md-9">
                      <input type="text" name="reward_ad_id" id="reward_ad_id" value="<?php echo $settings_row['reward_ad_id'];?>" class="form-control">
                      </div>
                    </div>
                                        
                  
              </div>

              </div>
         
            </div>                        
            <div class="form-group">
              <div class="col-md-9">
              <button type="submit" name="admob_submit" class="btn btn-primary">Save</button>
              </div>
            </div>
            </div>
          </div>
                </form>
        </div>
    </div>
    <div class="tab-pane  p-20" id="notification_settings" role="tabpanel">
       <div class="p-20">
          <form action="" name="settings_api" method="post" class="form form-horizontal" enctype="multipart/form-data" id="api_form">
                <div class="section">
                <div class="section-body">
                  <div class="form-group">
                    <label class="col-md-3 control-label">One Signal App Key :-</label>
                    <div class="col-md-6">
                      <input type="text" name="onesignal_app_id" id="onesignal_app_id" value="<?php echo $settings_row['onesignal_app_id'];?>" class="form-control">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-3 control-label">One Signal Api Key :-</label>
                    <div class="col-md-6">
                      <input type="text" name="onesignal_rest_key" id="onesignal_rest_key" value="<?php echo $settings_row['onesignal_rest_key'];?>" class="form-control">
                    </div>
                  </div>              
                  <div class="form-group">
                  <div class="col-md-9 col-md-offset-3">
                    <button type="submit" name="notification_submit" class="btn btn-primary">Save</button>
                  </div>
                  </div>
                </div>
                </div>
              </form>

       </div>

    </div>
    <div class="tab-pane p-20" id="api_settings" role="tabpanel">
       
         <form action="" name="api_settings" method="post" class="form form-horizontal" enctype="multipart/form-data" id="api_form">
              <div class="section">
                <div class="section-body d-flex">
                  <div class="col-md-6 col-xs-12">  
                   

                <div class="banner_ads_block">
                  <div class="card-head" style="margin-bottom:10px">
                    <label class="control-label">API Settings :-</label>                                  
                  </div>
                   <div class="form-group">
                      <label class="col-md-12 control-label">Playing Allowed:-</label>
                      <div class="col-md-12">
                       <select name="playing_allowed" id="playing_allowed" class="form-control" >
                        <option>Select option</option>

                        <option value="true" <?php if(isset($settings_row['playing_allowed'])){ if($settings_row['playing_allowed']=='true'){ echo "selected"; } } ?> >true</option>
                        <option value="false" <?php if(isset($settings_row['playing_allowed'])){ if($settings_row['playing_allowed']=='false'){ echo "selected"; } } ?> >false</option>
                      </select>
                 
                          </div>
                    </div>
                    <div class="form-group">
                    <label class="col-md-12 control-label">Playing Msg:-</label>
                    <div class="col-md-12">
                       
                      <input type="text" name="playing_msg" id="playing_msg" value="<?php echo $settings_row['playing_msg'];?>" class="form-control"> 
                    </div>                    
                   </div>
                    <div class="form-group">
                    <label class="col-md-12 control-label">Referral Msg:-</label>
                    <div class="col-md-12">
                       
                      <input type="text" name="referral_msg" id="referral_msg" value="<?php echo $settings_row['referral_msg'];?>" class="form-control"> 
                    </div>                    
                   </div>

                  <div class="col-xs-12">
                   <div class="form-group">
                    <label class="col-md-12 control-label">Refer From Point:-</label>
                    <div class="col-md-12">
                       
                      <input type="number" name="refer_from" id="refer_from" value="<?php echo $settings_row['refer_from'];?>" class="form-control"> 
                    </div>                    
                   </div>
                   <div class="form-group">
                    <label class="col-md-12 control-label">Refer To Point:-</label>
                    <div class="col-md-12">
                       
                      <input type="number" name="refer_to" id="refer_to" value="<?php echo $settings_row['refer_to'];?>" class="form-control"> 
                    </div>                    
                   </div>
                  </div>
                </div>                
                  
                  </div>
                  <div class="col-md-6 col-xs-12">  
                   

                <div class="banner_ads_block">
                  <div class="card-head" style="margin-bottom:10px">
                    <label class="control-label">Update Settings :-</label>                                  
                  </div>

                  <div class="form-group">
                      <label class="col-md-6 control-label">Force Update:-</label>
                      <div class="col-md-9">

                     <label class="switch">
                          <input type="checkbox" name="force_update" id="force_update"  <?php if(isset($settings_row['force_update'])){ if($settings_row['force_update']=='true'){ echo "checked"; } } ?> >
                          <span class="slider round"></span>
                          </label>
                          </div>
                    </div>

                  <div class="col-xs-12">
                   <div class="form-group">
                    <label class="col-md-12 control-label">Update Title:-</label>
                    <div class="col-md-12">
                       
                      <input type="text" name="update_title" id="update_title" value="<?php echo $settings_row['update_title'];?>" class="form-control"> 
                    </div>
                    
                  </div>
                  <div class="form-group">
                    <label class="col-md-12 control-label">Update Version:-</label>
                    <div class="col-md-12">
                       
                      <input type="text" name="update_version" id="update_version" value="<?php echo $settings_row['update_version'];?>" class="form-control"> 
                    </div>
                    
                  </div>
                  <div class="form-group">
                    <label class="col-md-12 control-label">Update Url:-</label>
                    <div class="col-md-12">
                       
                      <input type="text" name="update_url" id="update_url" value="<?php echo $settings_row['update_url'];?>" class="form-control"> 
                    </div>
                    
                  </div>
                  
                  </div>
                </div>
                 </div>                 
                
                 
                </div>
                <div class="col-xs-12" style="margin:0px;padding:0px">
                  <div class="form-group">
                    <label class="col-md-12 control-label">Update Message:-</label>
                    <div class="col-md-12">
                       <textarea name="update_msg" id="privacy_policy" class="form-control"><?php echo stripslashes($settings_row['update_msg']);?></textarea>
                        <script>CKEDITOR.replace( 'update_msg' );</script>
                    
                    </div>
                    
                  </div>
                </div>
                 <div class="form-group">
                    <div class="col-md-9">
                      <button type="submit" name="api_submit" class="btn btn-primary">Save</button>
                    </div>
                  </div>
              
               </form>
       </div>

    </div>
</div>



  
        
<?php include("includes/footer.php");?>       
