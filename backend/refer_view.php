<?php include("includes/header.php");
  require("includes/function.php");
  require("language/language.php");

  $refer_code=$_GET['refer_code'];
  $wall_qry="SELECT r.id,r.refer_to,(select ua.name from users as ua where ua.refer_code=r.refer_to) as referer_name FROM referral_info r LEFT JOIN users as u On u.refer_code= r.refer_from where r.refer_from='{$refer_code}'  order by r.created_at DESC";

  $result=mysqli_query($mysqli,$wall_qry)  or die("Failed ".mysqli($mysqli)); 

?>

<style>
  .checkbox-container {
      display: initial;
  position: relative;
  padding-left: 24px;
  vertical-align: sub;
  margin-bottom: 12px;
  cursor: pointer;
  font-size: 22px;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
}

/* Hide the browser's default checkbox */
.checkbox-container input {
  position: absolute;

  opacity: 0;
  cursor: pointer;
  height: 0;
  width: 0;
}

/* Create a custom checkbox */
.checkmark {
  position: absolute;
    top: 0;
    left: 0;
    height: 20px;
    width: 20px;
    background: #fff;
    border: 2px solid rgba(0, 0, 0, 0.54);
    border-radius: 0.125em;
    cursor: pointer;
    transition: background .3s;
}

/* On mouse-over, add a grey background color */
.checkbox-container:hover input ~ .checkmark {
  background-color:#ccc;
}

/* When the checkbox is checked, add a blue background */
.checkbox-container input:checked ~ .checkmark {
  background: #212529;
    border: none;
}

/* Create the checkmark/indicator (hidden when not checked) */
.checkmark:after {
  content: "";
  position: absolute;
  display: none;
}

/* Show the checkmark when checked */
.checkbox-container input:checked ~ .checkmark:after {
  display: block;
}

/* Style the checkmark/indicator */
.checkbox-container .checkmark:after {

 left: 7px;
    top: 1px;
    width: 7px;
    height: 14px;    vertical-align: sub;
    border: solid white;
    border-width: 0 3px 3px 0;
    -webkit-transform: rotate(45deg);
    -ms-transform: rotate(45deg);
    transform: rotate(42deg);
}
</style>



<div class="page-wrapper">
  
  <div class="page-breadcrumb">
    <div class="row">
      <div class="col-5 align-self-center">
        <h4 class="page-title">Users Referer </h4>
        <div class="d-flex align-items-center">
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="home.php">Home</a></li>
              <li class="breadcrumb-item active" aria-current="page">Users Referer</li>
            </ol>
          </nav>
        </div>
      </div>
      <div class="col-7 align-self-center">
        <div class="d-flex no-block justify-content-end align-items-center">
         
        </div>
      </div>
    </div>
  </div>
  
  <div class="container-fluid">
    
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-head d-flex">
            <div class="col-5">
           Users Referer 
            </div>
             <div class="col-7" style="text-align:end">
         
          </div>
          </div>
          <div class="card-body">
            
            <table id="table-responsive" class="display nowrap" style="width:100%;margin-top:10px;" >
              <thead>
                <tr>
                  <th >S.No.</th>
                    <th>Name</th>                    
                  <th>Referer</th>                                 
                
                </tr>
              </thead>
              <tbody>
                <?php
                $i=1;
                while($row=mysqli_fetch_array($result))
                {
                  $cache=uniqid();
                ?>
                <tr>
              
                  <td><?php echo $i;?></td>
                 
                  <td><?php echo $row['referer_name']; ?></td>
                  <td><?php echo $row['refer_to']; ?></td>
                
              </tr>
              <?php
              $i++;
              }
              ?>
            </tbody>
            
          </table>
        </div>
      </div>
    </div>
  </div>

  <script>
    var itre=1;
    $(function(){
        $("#checkall").change(function(){
            if($(this).is(":checked") == true)
            {
                  $('input[type=checkbox]').each(function () {                     
                     $(this).prop( "checked", true );                    
                  });
            }
            else
            {
                  $('input[type=checkbox]').each(function () {                    
                    $('input[type=checkbox]').prop("checked", false); ;                    
                  });
            }

        });
        $("form#checkbox-form").submit(function(e){
               e.stopPropagation()
                var id=[];

                  $("table input[type=checkbox]").each(function () {   
                        
                       if($(this).is(":checked") == true){
                        var getid=$(this).attr("id"); 
                                     
                         id.push(getid);
                        }
                    });
                  if(id.length>0)
                  {
                    if(confirm("Are Yor Sure you want to delete Selected Images ?"))
                    {
                      console.log("Id array is "+id);
                      $("#multi_check").val(id);
                    }
                    else
                    {
                      return false;
                    }
                  }
                  else
                  {
                    alert("Select At least One Check box");
                    return false;
                  }
        });
       
    });

    
  </script>
  
  <?php include("includes/footer.php");?>