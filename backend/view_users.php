<?php include("includes/header.php");
  require("includes/function.php");
  require("language/language.php");
 
$wall_qry="SELECT * FROM users order by id desc";

$result=mysqli_query($mysqli,$wall_qry)  or die("Failed ".mysqli($mysqli)); 

?>

<style>
  .checkbox-container {
      display: initial;
  position: relative;
  padding-left: 24px;
  vertical-align: sub;
  margin-bottom: 12px;
  cursor: pointer;
  font-size: 22px;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
}

/* Hide the browser's default checkbox */
.checkbox-container input {
  position: absolute;

  opacity: 0;
  cursor: pointer;
  height: 0;
  width: 0;
}

/* Create a custom checkbox */
.checkmark {
  position: absolute;
    top: 0;
    left: 0;
    height: 20px;
    width: 20px;
    background: #fff;
    border: 2px solid rgba(0, 0, 0, 0.54);
    border-radius: 0.125em;
    cursor: pointer;
    transition: background .3s;
}

/* On mouse-over, add a grey background color */
.checkbox-container:hover input ~ .checkmark {
  background-color:#ccc;
}

/* When the checkbox is checked, add a blue background */
.checkbox-container input:checked ~ .checkmark {
  background: #212529;
    border: none;
}

/* Create the checkmark/indicator (hidden when not checked) */
.checkmark:after {
  content: "";
  position: absolute;
  display: none;
}

/* Show the checkmark when checked */
.checkbox-container input:checked ~ .checkmark:after {
  display: block;
}

/* Style the checkmark/indicator */
.checkbox-container .checkmark:after {

 left: 7px;
    top: 1px;
    width: 7px;
    height: 14px;    vertical-align: sub;
    border: solid white;
    border-width: 0 3px 3px 0;
    -webkit-transform: rotate(45deg);
    -ms-transform: rotate(45deg);
    transform: rotate(42deg);
}

  .switch {
    position: relative;
    display: inline-block;
    width: 60px;
    height: 10px;
}

/* Hide default HTML checkbox */
.switch input {
  opacity: 0;
  width: 0;
  height: 0;
}

/* The slider */
.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #616161;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}
.round {
    color: #fff;
    width: 53px;
    height: 19px;
    display: inline-block;
    font-weight: 400;
    text-align: center;
    line-height: 52px;
}
.slider:before {
    position: absolute;
    content: "";
    height: 26px;
    width: 26px;
    left: 0px;
    bottom: -3px;
    background-color: #212529;
    -webkit-transition: .4s;
    transition: .4s;
}
</style>



<div class="page-wrapper">
  
  <div class="page-breadcrumb">
    <div class="row">
      <div class="col-5 align-self-center">
        <h4 class="page-title">Users List</h4>
        <div class="d-flex align-items-center">
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="home.php">Home</a></li>
              <li class="breadcrumb-item active" aria-current="page">Users</li>
            </ol>
          </nav>
        </div>
      </div>
     
    </div>
  </div>
  
  <div class="container-fluid">
    
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-head d-flex">
            <div class="col-5">
           Users List 
            </div>
            
          </div>
          <div class="card-body">
            
            <table id="table-responsive" class="display nowrap" style="width:100%;margin-top:10px;" >
              <thead>
                <tr>
                 
                  <th width="10%">S.No.</th>
                  <th>Email </th> 
                  <th>Name</th>                 
                  <th>Refer Code</th>                 
                  <th>Refer Info</th>                 
                  <th>Blocked Status</th>                 
                 
                </tr>
              </thead>
              <tbody>
                <?php
                $i=1;
                while($row=mysqli_fetch_array($result))
                {
                  $cache=uniqid();
                ?>
                <tr>
                  
                  <td><?php echo $i;?></td>
                  <td><?php echo $row['email']; ?></td>
                  <td><?php echo $row['name']; ?></td>
                  <td><?php echo $row['refer_code']; ?></td>               
                  <td>
                    <a href="<?php echo "refer_view.php?refer_code={$row['refer_code']}";?>">View Referer</a>
                      
                    </td>               
                  <td id="blocked-id">
                    <label class="switch">
                      <input type="checkbox" data-id="<?php echo $row['id']; ?>"  name="blocked_status_<?php echo $i; ?>" id="blocked_status" <?php echo ($row['blocked_status']=="Y")?"checked":""; ?> >
                      <span class="slider round"></span>
                    </label>
                  </td>
                
              </tr>
              <?php
              $i++;
              }
              ?>
            </tbody>
            
          </table>
        </div>
      </div>
    </div>
  </div>

  <script>
    
    $('#blocked-id input[type="checkbox"]').click(function(){
     var blocked=$(this).is(":checked");
       if(blocked==true)
       {
        blocked="Y";
       }
       else
       {
        blocked="N";
       }
     var id=$(this).attr("data-id");
      $.post("ajax.php",{action:"block_status",blocked:blocked,id:id},function(result){
      
      });

    });
    
  </script>
  
  <?php include("includes/footer.php");?>