<?php
    include("includes/connection.php");
		include("language/language.php");
 ?>

<!DOCTYPE html>
<html dir="ltr">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo $file_path;?>images/<?php echo APP_LOGO;?>">
    <title><?php echo APP_NAME;?></title>
    <!-- Custom CSS -->
    <link href="<?php echo $file_path;?>assets/libs/toastr/build/toastr.min.css" rel="stylesheet">
    <link href="<?php echo $file_path;?>assets/css/style.min.css" rel="stylesheet">
        
    <link href="<?php echo $file_path; ?>assets/css/mystyle.css" rel="stylesheet">
    <script src="<?php echo $file_path;?>assets/libs/jquery/dist/jquery.min.js"></script>
   
</head>

<body>
    <div class="main-wrapper">
             
        <div class="auth-wrapper d-flex no-block justify-content-center align-items-center" style="    background: lightgrey;">
            <div class="auth-box">
                <div id="loginform">
                    <div class="logo">
                        <span class="db" style="padding-bottom:10px;">
                          <img src="<?php echo $file_path; ?>/images/<?php echo APP_LOGO;?>" style="width:70px;padding-bottom:10px" alt="logo" />
                        </span>
                        <h5 class="font-medium mb-3">Enter Purchase Code</h5>
                    </div>
                    <!-- Form -->
                    <div class="row">
                        <div class="col-12">
                            
                            <form class="form-horizontal mt-3" id="purchaseCodeForm" action="authLicence.php" method="post">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon1"><i class="ti-key"></i></span>
                                    </div>
                                    <input type="text" class="form-control form-control-lg" placeholder="Enter Purchase Code" id="purchase_code" name="Purchase Code" aria-describedby="basic-addon1" required="">
                                </div>
                               
                                
                                <div class="form-group text-center">
                                    <div class="col-xs-12 pb-3">
                                        <button id="submit-id" class="btn btn-block btn-lg btn-info" type="submit">Submit</button>
                                    </div>
                                </div>
                                <div id="loadder-box" style="text-align:center;"></div>
                                
                                <div class="form-group mb-0 mt-2">
                                    
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
        
    </div>
    <script src="<?php echo $file_path;?>assets/libs/toastr/build/toastr.min.js"></script>
    <script src="<?php echo $file_path;?>assets/extra-libs/toastr/toastr-init.js"></script>
    <script>var licensecode="";var domainUrl="<?php echo 'https://'.$_SERVER['SERVER_NAME']; ?>";var response={"type":"","msg":""};var count=1;</script>
    <script src="<?php echo $file_path; ?>assets/js/license.js"></script>
   
    <?php if(isset($_SESSION['msg'])){ ?>
    <script>
    toastr.<?php echo $_SESSION['type']; ?>('<?php echo $client_lang[$_SESSION['msg']]; ?>','', { "closeButton": true});
    </script>
    <?php 
      unset($_SESSION['msg']);
      unset($_SESSION['type']);
      }
    ?> 
    
</body>

</html>