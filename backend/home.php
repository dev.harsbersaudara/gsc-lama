<?php include("includes/header.php");

$qry_users="SELECT COUNT(*) as num FROM users";
$total_users= mysqli_fetch_array(mysqli_query($mysqli,$qry_users));
$total_users = $total_users['num'];

$qry_questions="SELECT COUNT(*) as num FROM questions";
$total_questions = mysqli_fetch_array(mysqli_query($mysqli,$qry_questions));
$total_questions = $total_questions['num'];

?>
 <div class="page-wrapper">
          
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title">Dashboard</h4>
                        <div class="d-flex align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="home.php">Home</a></li>
                                    
                                </ol>
                            </nav>
                        </div>
                    </div>
                    <div class="col-7 align-self-center">
                       
                    </div>
                </div>
            </div>
          
            <div class="container-fluid">
              
                <div class="row">
                    <!-- column -->
                    <div class="col-lg-4">
                        <div class="card bg-primary text-white  card-hover">
                            <div class="card-body">
                                <h4 class="card-title"> Users</h4>
                                <div class="d-flex align-items-center mt-4">
                                  <i class="icon fa fa-users fa-4x"></i>
                              
                                    <div class="ml-auto">
                                        <h3 class="font-medium white-text mb-0"><?php echo $total_users;?></h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- column -->
                    <div class="col-lg-4">
                        <div class="card bg-cyan text-white  card-hover">
                            <div class="card-body">
                                <h4 class="card-title"> Questions</h4>
                                <div class="d-flex align-items-center mt-4">
                                   <i class="icon fa fa-question-circle fa-4x"></i>
                              
                                    <div class="ml-auto">
                                        <h3 class="font-medium white-text mb-0"><?php echo $total_questions;?></h3>
                                    </div>
                                </div>
                            </div>
                          
                        </div>
                    </div>
                    
                </div>
               
            </div>
        
<?php include("includes/footer.php");?>       
