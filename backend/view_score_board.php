<?php include("includes/header.php");
  require("includes/function.php");
  require("language/language.php");

      $contest_id=$_GET['contest_id'];
      $wall_qry="SELECT sb.score_point,sb.refer_point,(sb.score_point+sb.refer_point) as total_point ,users.email,users.name FROM score_board  sb inner join users on users.id=sb.user_id where sb.contest_id='{$contest_id}' and (sb.score_point+sb.refer_point !=0 ) order by total_point desc,sb.updated_at ASC";
      $result=mysqli_query($mysqli,$wall_qry)  or die("Failed ".mysqli_error($mysqli));
  


?>

<style>
  .checkbox-container {
      display: initial;
  position: relative;
  padding-left: 24px;
  vertical-align: sub;
  margin-bottom: 12px;
  cursor: pointer;
  font-size: 22px;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
}

/* Hide the browser's default checkbox */
.checkbox-container input {
  position: absolute;

  opacity: 0;
  cursor: pointer;
  height: 0;
  width: 0;
}

/* Create a custom checkbox */
.checkmark {
  position: absolute;
    top: 0;
    left: 0;
    height: 20px;
    width: 20px;
    background: #fff;
    border: 2px solid rgba(0, 0, 0, 0.54);
    border-radius: 0.125em;
    cursor: pointer;
    transition: background .3s;
}

/* On mouse-over, add a grey background color */
.checkbox-container:hover input ~ .checkmark {
  background-color:#ccc;
}

/* When the checkbox is checked, add a blue background */
.checkbox-container input:checked ~ .checkmark {
  background: #212529;
    border: none;
}

/* Create the checkmark/indicator (hidden when not checked) */
.checkmark:after {
  content: "";
  position: absolute;
  display: none;
}

/* Show the checkmark when checked */
.checkbox-container input:checked ~ .checkmark:after {
  display: block;
}

/* Style the checkmark/indicator */
.checkbox-container .checkmark:after {

 left: 7px;
    top: 1px;
    width: 7px;
    height: 14px;    vertical-align: sub;
    border: solid white;
    border-width: 0 3px 3px 0;
    -webkit-transform: rotate(45deg);
    -ms-transform: rotate(45deg);
    transform: rotate(42deg);
}
</style>



<div class="page-wrapper">
  
  <div class="page-breadcrumb">
    <div class="row">
      <div class="col-5 align-self-center">
        <h4 class="page-title">Score Board List</h4>
        <div class="d-flex align-items-center">
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="home.php">Home</a></li>
              <li class="breadcrumb-item active" aria-current="page">Score Board List</li>
            </ol>
          </nav>
        </div>
      </div>
      <div class="col-7 align-self-center">
       
      </div>
    </div>
  </div>
  
  <div class="container-fluid">
    
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-head d-flex">
            <div class="col-5">
            Score Board List 
            </div>
             
          </div>
          <div class="card-body">
            
            <table id="table-responsive" class="display nowrap" style="width:100%;margin-top:10px;" >
              <thead>
                <tr>
                 
                  <th style="width:10%;">Rank</th>
                  <th>User Name </th> 
                           
                  <th style="width:40px;">Score Point</th>                 
                  <th style="width:40px;">Refer Point</th>                 
                  <th style="width:40px;">Total Point</th>                 
                 
                </tr>
              </thead>
              <tbody>
                <?php
                $i=1;
                while($row=mysqli_fetch_array($result))
                {
                  $cache=uniqid();
                ?>
                <tr>
                 
                  <td><?php echo $i;?></td>
                  <td><?php echo $row['name']; ?></td>
                
                  <td style="width:40px;"><?php echo $row['score_point']; ?></td>
                  <td style="width:40px;"><?php echo $row['refer_point']; ?></td>
                  <td style="width:40px;"><?php echo $row['total_point']; ?></td>
                
                  
              <?php
              $i++;
              }
              ?>
            </tbody>
            
          </table>
        </div>
      </div>
    </div>
  </div>

  <?php include("includes/footer.php");?>