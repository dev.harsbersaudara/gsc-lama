            <footer class="footer text-center">
            Copyright © <?php echo date('Y');?> Quizzle. All Rights Reserved.

            </footer>
            
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
   

    

     <script>var licensecode="<?php echo LICENSE;?>";var domainUrl="<?php echo 'http://'.$_SERVER['SERVER_NAME']; ?>";var response={type:"",msg:""}</script>
    <script src="<?php echo $file_path;?>assets/js/app.min.js"></script>   
    <script src="<?php echo $file_path;?>assets/js/app-style-switcher.js"></script> 
    <script src="<?php echo $file_path;?>assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js"></script>
    <script src="<?php echo $file_path;?>assets/extra-libs/sparkline/sparkline.js"></script>  
    <script src="<?php echo $file_path;?>assets/js/waves.js"></script>
    <script src="<?php echo $file_path;?>assets/js/sidebarmenu.js"></script>  
    <script src="<?php echo $file_path;?>assets/js/jquery.js"></script>
    <script src="<?php echo $file_path;?>assets/libs/toastr/build/toastr.min.js"></script>
    <script src="<?php echo $file_path;?>assets/extra-libs/toastr/toastr-init.js"></script>    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.4/croppie.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/rowreorder/1.2.5/js/dataTables.rowReorder.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo $file_path;?>assets/libs/magnific-popup/dist/jquery.magnific-popup.min.js"></script>
    <script src="<?php echo $file_path;?>assets/libs/magnific-popup/meg.init.js"></script>
    <script src="<?php echo $file_path;?>assets/libs/select2/dist/js/select2.full.min.js"></script>
    <script src="<?php echo $file_path;?>assets/libs/select2/dist/js/select2.min.js"></script>
    <script src="<?php echo $file_path;?>assets/dist/js/pages/forms/select2/select2.init.js"></script>
    <script src="<?php echo $file_path;?>assets/js/bootstrap-datepicker.min.js"></script>

    <script>
            $(document).ready(function() {
                $('#table-responsive').DataTable({
                    responsive: true
                });
            });       
    </script>
    <script>
    // Date Picker
    jQuery(".mydatepicker, #datepicker,input[type='date']").datepicker({
        orientation: 'bottom',
       
    });
   
    </script>

    

    <?php if(isset($_SESSION['msg'])){ ?>
    <script>
    toastr.<?php echo $_SESSION['type']; ?>('<?php echo $client_lang[$_SESSION['msg']]; ?>','', { "closeButton": true});
    </script>
    <?php 
      unset($_SESSION['msg']);
      unset($_SESSION['type']);
      }
    ?> 
    <?php if(isset($_SESSION['login_msg'])){ ?>
    <script>
    toastr.<?php echo $_SESSION['login_type']; ?>('Successfully Login...','', { "closeButton": true});
    </script>
    <?php 
      unset($_SESSION['login_msg']);
      unset($_SESSION['login_type']);
      }
    ?>
</body>
</html>