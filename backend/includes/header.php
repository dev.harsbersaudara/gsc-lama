<?php include("includes/connection.php");
      include("includes/session_check.php");   
?>
<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="">
    <meta name="description" content="">
    <meta http-equiv="expires" content="0">

    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo $file_path;?>images/<?php echo APP_LOGO;?>">
    <title><?php echo APP_NAME;?></title>
    
   
    <link href="<?php echo $file_path; ?>assets/css/style.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.4/croppie.css">

    <link href="<?php echo $file_path; ?>assets/libs/magnific-popup/dist/magnific-popup.css" rel="stylesheet">

   <link href="<?php echo $file_path;?>assets/libs/toastr/build/toastr.min.css" rel="stylesheet">
   <link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet">
   <link href="https://cdn.datatables.net/rowreorder/1.2.5/css/rowReorder.dataTables.min.css" rel="stylesheet">
   <link href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css" rel="stylesheet">
   <link rel="stylesheet" type="text/css" href="<?php echo $file_path; ?>assets/libs/select2/dist/css/select2.min.css">
 

    <link href="<?php echo $file_path; ?>assets/css/mystyle.css" rel="stylesheet">
    <link href="<?php echo $file_path; ?>assets/css/bootstrap-datepicker.min.css" rel="stylesheet">
    <script src="https://cdn.ckeditor.com/4.12.1/standard/ckeditor.js"></script>
    <script src="<?php echo $file_path;?>assets/libs/jquery/dist/jquery.min.js"></script>
        <!-- Bootstrap tether Core JavaScript -->
    <script src="<?php echo $file_path;?>assets/libs/popper.js/dist/umd/popper.min.js"></script>
    <script src="<?php echo $file_path;?>assets/libs/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.4/croppie.js"></script>
    <script src="https://cdn.ckeditor.com/ckeditor5/12.4.0/classic/ckeditor.js"></script>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->

<style>
    .btn-info {
    color: #fff;
    background-color: #01B493 !important;
    border-color: #01B493 !important;
}
</style>
</head>

<body>
   
    <div id="main-wrapper">
      
        <header class="topbar">
            <nav class="navbar top-navbar navbar-expand-md navbar-dark">
                <div class="navbar-header">
                    <!-- This is for the sidebar toggle which is visible on mobile only -->
                    <a class="nav-toggler waves-effect waves-light d-block d-md-none" href="javascript:void(0)"><i class="ti-menu ti-close"></i></a>
                    
                    <a class="navbar-brand" href="index.php">
                        <!-- Logo icon -->
                        <b class="logo-icon">
                             <img src="<?php echo $file_path;?>images/<?php echo APP_LOGO;?>" alt="homepage" class="dark-logo" style="width:60px;" />
                           
                        </b>
                       
                        <span class="logo-text">
                         
                        </span>
                    </a>
                   
                    <a class="topbartoggler d-block d-md-none waves-effect waves-light" href="javascript:void(0)" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><i class="ti-more"></i></a>
                </div>
                
                <div class="navbar-collapse collapse" id="navbarSupportedContent">
                   
                    <ul class="navbar-nav float-left mr-auto">
                        <li class="nav-item d-none d-md-block"><a class="nav-link sidebartoggler waves-effect waves-light" href="javascript:void(0)" data-sidebartype="mini-sidebar"><i class="mdi mdi-menu font-24"></i></a></li>
                    </ul>
                    
                    <ul class="navbar-nav float-right">
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark pro-pic" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <?php if(PROFILE_IMG){?>               
                                    <img class="profile-img" src="<?php echo $file_path;?>images/<?php echo PROFILE_IMG;?>?<?php echo uniqid();?>" style="width: 50px;height:50px;object-fit: cover; border-radius: 50%;">
                                    <?php }else{?>
                                    <img class="profile-img" src="<?php echo $file_path;?>assets/images/profile.png" style="width: 50px;
    border-radius: 50%;">
                                    <?php }?>
                            <div class="dropdown-menu dropdown-menu-right user-dd animated flipInY">
                                <span class="with-arrow"><span class="bg-primary"></span></span>
                                
                                <a class="dropdown-item" href="profile.php"><i class="ti-user mr-1 ml-1"></i>Profile</a>
                                
                                <a class="dropdown-item" href="logout.php"><i class="fa fa-power-off mr-1 ml-1"></i> Logout</a>
                                
                            </div>
                        </li>
                     
                    </ul>
                </div>
            </nav>
        </header>
      
        <aside class="left-sidebar">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        
                        <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark" href="home.php" ><i class="mdi mdi-view-dashboard"></i><span class="hide-menu">Dashboard </span></a>
                            
                        </li> 
                        <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="fa fa-language" aria-hidden="true"></i><span class="hide-menu">Languages </span></a>
                            <ul aria-expanded="false" class="collapse  first-level">
                                <li class="sidebar-item"><a href="language_view.php" class="sidebar-link"><i class="mdi mdi-adjust"></i><span class="hide-menu"> View Language </span></a></li>
                                <li class="sidebar-item"><a href="language_add.php?action=add" class="sidebar-link"><i class="mdi mdi-adjust"></i><span class="hide-menu"> Add Language </span></a></li>
                                
                            </ul>
                        </li>
                        
                        <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="fa fa-question-circle" aria-hidden="true"></i><span class="hide-menu">Question</span></a>
                            <ul aria-expanded="false" class="collapse  first-level">
                                <li class="sidebar-item"><a href="view_question.php" class="sidebar-link"><i class="mdi mdi-adjust"></i><span class="hide-menu"> View Question </span></a></li>
                                <li class="sidebar-item"><a href="add_question.php?action=add" class="sidebar-link"><i class="mdi mdi-adjust"></i><span class="hide-menu"> Add Question </span></a></li>
                                <li class="sidebar-item"><a href="bulk.php?action=add" class="sidebar-link"><i class="mdi mdi-adjust"></i><span class="hide-menu"> Add Bulk Excel </span></a></li>
                                
                            </ul>
                        </li>
                         
                        <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="fa fa-users" aria-hidden="true"></i><span class="hide-menu">Users Details</span></a>
                            <ul aria-expanded="false" class="collapse  first-level">
                                <li class="sidebar-item"><a href="view_users.php" class="sidebar-link"><i class="mdi mdi-adjust"></i><span class="hide-menu"> View Users List </span></a></li>
                          
                               
                            </ul>
                        </li>
                        <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="fa fa-trophy" aria-hidden="true"></i><span class="hide-menu">Contest Details</span></a>
                            <ul aria-expanded="false" class="collapse  first-level">
                                <li class="sidebar-item"><a href="view_contest.php" class="sidebar-link"><i class="mdi mdi-adjust"></i><span class="hide-menu"> View Contest List </span></a></li>
                                 <li class="sidebar-item"><a href="add_contest.php?action=add" class="sidebar-link"><i class="mdi mdi-adjust"></i><span class="hide-menu"> Add Contest</span></a></li>
                                
                               
                            </ul>
                        </li>
                       
                        <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="ti-alarm-clock"></i><span class="hide-menu">Notification </span></a>
                            <ul aria-expanded="false" class="collapse  first-level">
                                <li class="sidebar-item"><a href="send_notification.php" class="sidebar-link"><i class="mdi mdi-adjust"></i><span class="hide-menu"> Send Notification </span></a></li>
                                
                            </ul>
                        </li>
                         <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="fa fa-window-maximize"></i><span class="hide-menu">Contact Query </span></a>
                            <ul aria-expanded="false" class="collapse  first-level">
                                <li class="sidebar-item"><a href="contact.php" class="sidebar-link"><i class="mdi mdi-adjust"></i><span class="hide-menu"> User Contact Query </span></a></li>
                                
                            </ul>
                        </li>
                        <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="fas fa-cogs"></i><span class="hide-menu">Settings </span></a>
                            <ul aria-expanded="false" class="collapse  first-level">
                                <li class="sidebar-item"><a href="settings.php" class="sidebar-link"><i class="mdi mdi-adjust"></i><span class="hide-menu">Api Settings </span></a></li>
                                <li class="sidebar-item"><a href="api_urls.php" class="sidebar-link"><i class="mdi mdi-adjust"></i><span class="hide-menu">Api Urls </span></a></li>   
                                                       
                            </ul>
                        </li>
                        
                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </aside>

