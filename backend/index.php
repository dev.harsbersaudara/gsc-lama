<?php
    include("includes/connection.php");
		include("language/language.php");
        if(LICENSE =="")
    {
        header("Location:license.php");
        exit;
    }
    
	if(isset($_SESSION['admin_name']))
	{
		header("Location:home.php");
		exit;
	}
?>


<!DOCTYPE html>
<html dir="ltr">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo $file_path;?>images/<?php echo APP_LOGO;?>">
    <title><?php echo APP_NAME;?></title>
    <!-- Custom CSS -->
      <link href="<?php echo $file_path;?>assets/libs/toastr/build/toastr.min.css" rel="stylesheet">
    <link href="<?php echo $file_path;?>assets/css/style.min.css" rel="stylesheet">

    <link href="<?php echo $file_path; ?>assets/css/mystyle.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body>
    <div class="main-wrapper">
             
        <div class="auth-wrapper d-flex no-block justify-content-center align-items-center" style="    background: lightgrey;">
            <div class="auth-box">
                <div id="loginform">
                    <div class="logo">
                        <span class="db" style="padding-bottom:10px;">
                          <img src="<?php echo $file_path; ?>/images/<?php echo APP_LOGO;?>" style="width:70px;padding-bottom:10px" alt="logo" />
                        </span>
                        <h5 class="font-medium mb-3">Sign In to <?php echo APP_NAME;?></h5>
                    </div>
                    <!-- Form -->
                    <div class="row">
                        <div class="col-12">
                            <?php
                            $qry="SELECT * FROM tbl_admin where id='1'";
                            $result=mysqli_query($mysqli,$qry);
                            $get=mysqli_fetch_assoc($result);
                              ?>
                            <form class="form-horizontal mt-3" id="loginform" action="login_db.php" method="post">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon1"><i class="ti-user"></i></span>
                                    </div>
                                    <input type="text" class="form-control form-control-lg" placeholder="Username" value="<?php echo $get['username'];?>" aria-label="username" name="username" aria-describedby="basic-addon1" required="">
                                </div>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon2"><i class="ti-key"></i></span>
                                    </div>
                                    <input type="password" class="form-control form-control-lg" placeholder="Password" value="<?php echo $get['password'];?>" name="password" aria-label="Password" aria-describedby="basic-addon1" required="">
                                </div>
                                
                                <div class="form-group text-center">
                                    <div class="col-xs-12 pb-3">
                                        <button class="btn btn-block btn-lg btn-info" type="submit">Log In</button>
                                    </div>
                                </div>
                                
                                <div class="form-group mb-0 mt-2">
                                    
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
        
    </div>


   
    <script src="<?php echo $file_path;?>assets/libs/jquery/dist/jquery.min.js"></script>
   
    <script src="<?php echo $file_path;?>assets/libs/popper.js/dist/umd/popper.min.js"></script>
    <script src="<?php echo $file_path;?>assets/libs/bootstrap/dist/js/bootstrap.min.js"></script>

    <script src="<?php echo $file_path;?>assets/libs/toastr/build/toastr.min.js"></script>
    <script src="<?php echo $file_path;?>assets/extra-libs/toastr/toastr-init.js"></script>
    

    <?php if(isset($_SESSION['msg'])){ ?>
    <script>
    toastr.<?php echo $_SESSION['type']; ?>('<?php echo $client_lang[$_SESSION['msg']]; ?> ', '', { "closeButton": true });
    </script>

    <?php 
      unset($_SESSION['msg']);
      unset($_SESSION['type']);
      }
    ?>

    <script>
        $('[data-toggle="tooltip"]').tooltip();
        $(".preloader").fadeOut();
       
        $('#to-recover').on("click", function() {
            $("#loginform").slideUp();
            $("#recoverform").fadeIn();
        });
    </script>
</body>

</html>